﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using AvidGolfer.Data;
using AvidGolfer.Components;
using AvidGolfer.Entities;
using AuthorizeNet.Entities;
using umbraco.cms.businesslogic.member;
using umbraco.cms.businesslogic.media;
using umbraco.presentation.nodeFactory;

namespace AvidGolferProcessTransaction
{
    public class ProcessTransaction
    {

        SupplementalContentManager cm = new SupplementalContentManager();

        public ProcessTransaction()
        {

        }

        public void processOrders() 
        {
            List<Order> orders = new List<Order>();
            List<OrderItem> items = new List<OrderItem>();

            orders = cm.GetOrderForProcessing();

            foreach (Order order in orders)
            {
                processCurrentOrder(order);
            }


 
        }

        public void processCurrentOrder(Order order)
        {
            Member tMember = Member.GetMemberFromEmail(order.membershipemail);
            AuthorizeNetCustomer customerProfile = GetCustomer(order.membershipemail);
            string success = StoreManager.PurchaseOrder(order, tMember, customerProfile);

            UpdateOrder(order);

        }

        public AuthorizeNetCustomer GetCustomer(string email)
        {
            AuthorizeNetCustomer customerProfile = new AuthorizeNetCustomer();
            Member tMember = Member.GetMemberFromEmail(email);
            customerProfile = StoreManager.GetCustomerProfile(tMember);


            return customerProfile;

        }

        public void UpdateOrder(Order order)
        {
            order.modifydate = DateTime.Now;
            cm.OrderSave(order);
            int index = 0;
            foreach (OrderItem item in order.orderitems)
            {
                if(item.orderStatus.Equals("Open")) 
                {
                    if (index == 0)
                    {
                        item.orderStatus = "Active";
                    }
                    else
                    {
                        item.orderStatus = "Purchased";
                    }
                    item.modifydate = DateTime.Now;
                    cm.SaveIndividualOrderItem(item);
                    index++;
                }

            }
        }
    }
}
