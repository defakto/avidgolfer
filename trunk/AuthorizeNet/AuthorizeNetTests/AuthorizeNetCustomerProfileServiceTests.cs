﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AuthorizeNetCustomerService;
using AuthorizeNet.Entities;

namespace AuthorizeNetTests
{

    
    [TestClass]
    [Serializable]
    public class AuthorizeNetCustomerProfileServiceTests
    {
        private AuthorizeNetCustomer Customer { get; set; }
        private AuthorizeNetServiceClient ServiceClient { get; set; }
        private long RandomNumber { get; set; }

        //4222222222222

        private AuthorizeNetCustomer CreateLegitCustomer()
        {
            var billingInfo = new AuthorizeNetPersonInfo
            {
                Address = "4517 Miami Drive",
                City = "Plano",
                Country = "USA",
                FirstName = "Holly",
                LastName = "Laveau",
                PhoneNumber = "972-836-2677",
                State = "Texas",
                Zip = "75093"
            };

            var paymentInfo = new AuthorizeNetPaymentInfo
            {
                BillingPersonInfo = billingInfo,
                CardCode = "939",
                CreditCardNumber = "4222222222222",
                Expiry = "2014-03"
            };

            var customer = new AuthorizeNetCustomer
            {
                MerchantCustomerId = 123,
                Email = "nomadixone@gmail.com",
                Description = "This is a test record" + RandomNumber.ToString(),
                ShippingPersonInfo = billingInfo,
                PaymentInfo = paymentInfo
            };

            return customer;
        }


        private AuthorizeNetCustomer CreateCustomer()
        {
            var billingInfo = new AuthorizeNetPersonInfo
            {
                Address = "123 Main Street",
                City = "Dallas",
                Country = "USA",
                FaxNumber = "2141234567",
                FirstName = "John",
                LastName = "Smith",
                PhoneNumber = "2141234567",
                State = "Texas",
                Zip = "75123"
            };

            var paymentInfo = new AuthorizeNetPaymentInfo
            {
                BillingPersonInfo = billingInfo,
                CardCode = "111",
                CreditCardNumber = "4111111111111111",
                Expiry = "2020-12"
            };

            var customer = new AuthorizeNetCustomer
            {
                MerchantCustomerId = 123,
                Email = "test" + RandomNumber.ToString() + "@test.com",
                Description = "This is a test record" + RandomNumber.ToString(),
                ShippingPersonInfo = billingInfo,
                PaymentInfo = paymentInfo
            };

            return customer;
        }

        private void MakeCustomerOnServer(AuthorizeNetCustomer customer)
        {
            if (customer.AuthorizeNetCustomerId == 0)
            {
                try
                {
                    ServiceClient.CreateAuthorizeNetCustomer(Customer);
                }
                catch (Exception ex)
                {
                    Assert.Fail("Create of Customer object failed:" + ex.ToString());
                }
            }
        }

        private AuthorizeNetTransaction CreateTransaction(AuthorizeNetCustomer customer)
        {
            var transaction = new AuthorizeNetTransaction
            {
                TotalAmount = 100m,
                TaxAmount = 10m,
                ShippingAmount = 10m,
                OrderNumber = "Order " + RandomNumber,
                Customer = customer
            };

            var lineItems = new List<AuthorizeNetOrderLineItem>();

            var lineItem = new AuthorizeNetOrderLineItem
            {
                Description = "Jumbo Catheters are our premium product for those who have reason to be proud",
                ItemId = "SKU1234567890",
                Name = "Jumbo Catheter",
                Quantity = 1m,
                UnitPrice = 100m
            };

            lineItems.Add(lineItem);
            transaction.LineItems = lineItems;

            return transaction;
        }

        [TestInitialize]
        public void Init()
        {
            ServiceClient = new AuthorizeNetServiceClient();
            RandomNumber = DateTime.Now.Ticks;
            Customer = CreateCustomer();
        }

        [TestMethod]
        public void CreateReadUpdateCustomerTest()
        {
            MakeCustomerOnServer(Customer);
            var id = Customer.AuthorizeNetCustomerId;

            Assert.IsTrue(Customer.AuthorizeNetCustomerId > 0, "Didn't create our Customer successfully");
            RandomNumber = DateTime.Now.Ticks;

            try
            {
                Customer = ServiceClient.GetAuthorizeNetCustomer(id);
            }
            catch (Exception ex)
            {
                Assert.Fail("Get of Customer object failed:" + ex.ToString());
            }

            var email = "test" + RandomNumber.ToString() + "@test.com";
            var expiry = (RandomNumber % 8 + 2012).ToString() + "-0" + (RandomNumber % 9 + 1).ToString();
            var firstName = "Jebodiah" + RandomNumber.ToString(); ;

            Customer.Email = email;
            Customer.PaymentInfo.Expiry = expiry;
            Customer.ShippingPersonInfo.FirstName = firstName;

            try
            {
                ServiceClient.UpdateAuthorizeNetCustomer(Customer);
            }
            catch (Exception ex)
            {
                Assert.Fail("Update of Customer object failed:" + ex.ToString());
            }

            try
            {
                Customer = ServiceClient.GetAuthorizeNetCustomer(id);
            }
            catch (Exception ex)
            {
                Assert.Fail("Get of Customer object failed:" + ex.ToString());
            }

            Assert.AreEqual(email, Customer.Email, "Email didn't update");
            Assert.AreEqual("XXXX", Customer.PaymentInfo.Expiry, "Payment info didn't update");
            Assert.AreEqual(firstName, Customer.ShippingPersonInfo.FirstName, "Shipping info didn't update");
        }

        [TestMethod]
        public void ARBCreateReadUpdateTest()
        {
        }

        [TestMethod]
        public void SubmitTransactionTest()
        {
            MakeCustomerOnServer(Customer);
            var transaction = CreateTransaction(Customer);

            try
            {
                ServiceClient.CreateTransaction(transaction);
            }
            catch (Exception ex)
            {
                Assert.Fail("Create of transaction failed:" + ex.ToString());
            }
        }

        [TestMethod]
        public void SubmitPreAuthTransactionTest()
        {
            MakeCustomerOnServer(Customer);
            var transaction = CreateTransaction(Customer);

            try
            {
                ServiceClient.CreatePreAuthTransaction(transaction);
            }
            catch (Exception ex)
            {
                Assert.Fail("Create of transaction failed:" + ex.ToString());
            }

            try
            {
                ServiceClient.SubmitPreAuthTransaction(transaction);
            }
            catch (Exception ex)
            {
                Assert.Fail("Create of transaction failed:" + ex.ToString());
            }

        }


        [TestMethod]
        public void FailSubmitTransactionTest()
        {
            Customer.PaymentInfo.BillingPersonInfo.Address = "";
            Customer.PaymentInfo.BillingPersonInfo.Zip = "";
            Customer.PaymentInfo.CardCode = "";
            var exceptionThrown = false;

            try
            {
                ServiceClient.CreateAuthorizeNetCustomer(Customer);
            }
            catch (TransactionDeclinedException ex)
            {
                exceptionThrown = true;
            }

            //Assert.IsTrue(exceptionThrown, "Customer Create should have failed but didn't");

            Customer = CreateLegitCustomer();
            Customer.PaymentInfo.BillingPersonInfo.Zip = "75023";
            Customer.PaymentInfo.CardCode = "";

            MakeCustomerOnServer(Customer);
            var transaction = CreateTransaction(Customer);
            transaction.TotalAmount = 14m;

            exceptionThrown = false;

            try
            {
                ServiceClient.CreateTransaction(transaction);
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown, "Transaction should have failed but didn't");
        }
    }
}
