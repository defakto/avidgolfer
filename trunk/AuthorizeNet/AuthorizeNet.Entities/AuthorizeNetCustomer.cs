﻿using System;

namespace AuthorizeNet.Entities
{
    [Serializable]
    public class AuthorizeNetCustomer
    {
        public int MerchantCustomerId { get; set; }
        public long AuthorizeNetCustomerId { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }

        public AuthorizeNetPersonInfo ShippingPersonInfo { get; set; }
        public AuthorizeNetPaymentInfo PaymentInfo { get; set; }

        public AuthorizeNetCustomer()
        {
            PaymentInfo = new AuthorizeNetPaymentInfo();
            ShippingPersonInfo = new AuthorizeNetPersonInfo();
        }
    }
}
