﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuthorizeNet.Entities
{
    [Serializable]
    public class AuthorizeNetPaymentInfo
    {
        public long Id { get; set; }
        public bool IsCreditCard { get; set; }
        public string CreditCardNumber { get; set; }
        public string Expiry { get; set; }
        public string CardCode { get; set; }

        public string BankAccountNumber { get; set; }
        public string BankRoutingNumber { get; set; }
        public string BankAccountName { get; set; }
        public string BankName { get; set; }
        
        public BankingAccountType BankAccountType { get; set; }
        public AuthorizeNetPersonInfo BillingPersonInfo { get; set; }

        public AuthorizeNetPaymentInfo()
        {
            IsCreditCard = true;
            BillingPersonInfo = new AuthorizeNetPersonInfo();
        }

    }

    public enum BankingAccountType
    {
        Checking = 0,
        Savings = 1,
        BusinessChecking = 2
    }

}
