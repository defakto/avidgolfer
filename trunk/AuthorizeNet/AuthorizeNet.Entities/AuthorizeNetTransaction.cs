﻿using System;
using System.Collections.Generic;

namespace AuthorizeNet.Entities
{

    [Serializable]
    public class AuthorizeNetTransaction
    {
        public decimal TotalAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal ShippingAmount { get; set; }
        public string OrderNumber { get; set; }
        public string OrderDescription { get; set; }
        public string Id { get; set; }
        public IEnumerable<AuthorizeNetOrderLineItem> LineItems { get; set; }
        public AuthorizeNetCustomer Customer { get; set; }

        public AuthorizeNetTransaction()
        {
            LineItems = new List<AuthorizeNetOrderLineItem>();
            Customer = new AuthorizeNetCustomer();
        }
    }
}
