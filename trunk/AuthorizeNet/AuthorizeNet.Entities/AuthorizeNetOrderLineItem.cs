﻿using System;

namespace AuthorizeNet.Entities
{
    [Serializable]
    public class AuthorizeNetOrderLineItem
    {
        public string ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
