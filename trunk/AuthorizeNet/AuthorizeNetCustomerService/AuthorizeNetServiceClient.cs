﻿using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text;
using AuthorizeNet.Entities;
using AuthorizeNetCustomerService.AuthorizeNetServiceReference;
using System;

namespace AuthorizeNetCustomerService
{

    /// <summary>
    /// A class for wrapping the Authorize.Net service interactions
    /// </summary>
    public class AuthorizeNetServiceClient
    {
        /// <summary>
        /// Gets and sets the service reference for the authorize net webservice
        /// </summary>
        private ServiceSoapClient ServiceReference { get; set; }

        /// <summary>
        /// Gets and sets the authentication token for the Authorize.Net webservice
        /// </summary>
        private MerchantAuthenticationType AuthenticationToken { get; set; }

        /// <summary>
        /// Gets and sets the validation level of the Authorize.Net api calls
        /// </summary>
        private ValidationModeEnum ValidationLevel { get; set; }

        /// <summary>
        /// Default constructor for <see cref="AuthorizeNetServiceClient"/>
        /// </summary>
        public AuthorizeNetServiceClient()
        {
            ServiceReference = new ServiceSoapClient();
            var merchantName = ConfigurationManager.AppSettings["AuthorizeNetMerchantName"];
            var merchantKey = ConfigurationManager.AppSettings["AuthorizeNetMerchantKey"];
            var validationLevel = ConfigurationManager.AppSettings["AuthorizeNetValidationLevel"];

            switch (validationLevel)
            {
                case "live":
                    ValidationLevel = ValidationModeEnum.liveMode;
                    break;
                case "test":
                    ValidationLevel = ValidationModeEnum.testMode;
                    break;
                default:
                    ValidationLevel = ValidationModeEnum.none;
                    break;
            }

            AuthenticationToken = new MerchantAuthenticationType
            {
                name = merchantName,
                transactionKey = merchantKey
            };
        }

        /// <summary>
        /// Creates a new Authorize.Net customer profile object
        /// </summary>
        /// <param name="customer">The local data entity version that we wish to create</param>
        public void CreateAuthorizeNetCustomer(AuthorizeNetCustomer customer)
        {
            var response = ServiceReference.CreateCustomerProfile(AuthenticationToken, customer.Convert(), ValidationLevel);
            ValidateResponse(response);
            customer.AuthorizeNetCustomerId = response.customerProfileId;
            customer.PaymentInfo.Id = response.customerPaymentProfileIdList.FirstOrDefault();
            customer.ShippingPersonInfo.Id = response.customerShippingAddressIdList.FirstOrDefault();
        }

        /// <summary>
        /// Takes the given response object, checks it for errors then throws an exception
        /// </summary>
        /// <param name="response">The Authorize.Net Response object to check for errors</param>
        private void ValidateResponse(ANetApiResponseType response)
        {
            var transactionResponse = response as CreateCustomerProfileTransactionResponseType;

            if (response.resultCode == MessageTypeEnum.Error)
            {
                var sb = new StringBuilder();
                string code = null;
                string returnMessage = null;
                sb.AppendLine("Error occurred in submitting to Authorize.Net: ");

                for (int i = 0; i < response.messages.Length; i++)
                {
                    var message = response.messages[i];
                    if (code == null)
                    {
                        code = message.code;
                        returnMessage = message.text;
                    }

                    sb.AppendLine("Message: " + message.text);
                }

                if (transactionResponse != null)
                {
                    sb.AppendLine(transactionResponse.directResponse);
                }

                switch (code)
                {
                    case "E00014":
                        throw new RequiredFieldNullException(returnMessage);
                        break;
                    case "E00027":
                        throw new TransactionDeclinedException(returnMessage);
                        break;
                    case "E00039":
                        throw new DuplicateRecordException(returnMessage);
                        break;
                    case "E00040":
                        throw new RecordNotFoundException(returnMessage);
                        break;
                    default:
                        throw new Exception(sb.ToString());
                        break;
                }
            }
        }

        /// <summary>
        /// Updates the provided customer object with a call to Authorize.Net
        /// </summary>
        /// <param name="customer">The customer object to update</param>
        public void UpdateAuthorizeNetCustomer(AuthorizeNetCustomer customer)
        {
            var response = ServiceReference.UpdateCustomerProfile(AuthenticationToken, customer.ConvertForUpdate());
            ValidateResponse(response);
            var paymentResponse = ServiceReference.UpdateCustomerPaymentProfile(AuthenticationToken, customer.AuthorizeNetCustomerId, customer.PaymentInfo.ConvertToPaymentProfileEx(), ValidationLevel);
            ValidateResponse(paymentResponse);
            var shippingResponse = ServiceReference.UpdateCustomerShippingAddress(AuthenticationToken, customer.AuthorizeNetCustomerId, customer.ShippingPersonInfo.ConvertToShippingAddressEx());
            ValidateResponse(shippingResponse);
        }

        /// <summary>
        /// Gets the specified customer from the Authorize.Net service
        /// </summary>
        /// <param name="id">The Authorize.Net ID for this customer</param>
        /// <returns>Our customer entity</returns>
        public AuthorizeNetCustomer GetAuthorizeNetCustomer(long id)
        {
            var response = ServiceReference.GetCustomerProfile(AuthenticationToken, id);
            ValidateResponse(response);
            return response.profile.Convert();
        }

        /// <summary>
        /// Takes the provided transaction object and submits to the Authorize.Net payment gateway
        /// </summary>
        /// <param name="transactionDetails">The object to submit to the gateway</param>
        /// <returns>The response code object from the server</returns>
        private CreateCustomerProfileTransactionResponseType SubmitTransaction(object transactionDetails)
        {
            var transaction = new ProfileTransactionType
            {
                Item = transactionDetails
            };

            var response = ServiceReference.CreateCustomerProfileTransaction(AuthenticationToken, transaction, "");
            ValidateResponse(response);
            return response;
        }

        /// <summary>
        /// Parses the provided response string and gets the transaction id
        /// </summary>
        /// <param name="response">The response string to parse</param>
        /// <returns>The transaction code</returns>
        private string GetTransactionIdFromResponseString(string response)
        {
            var array = response.Split(new char[] { ',' });
            return array[6];
        }

        /// <summary>
        /// Creates a verify and bill now transaction
        /// </summary>
        /// <param name="transaction">The details for the transaction</param>
        public void CreateTransaction(AuthorizeNetTransaction transaction)
        {
            var authAndCapture = transaction.Convert();
            var response = SubmitTransaction(authAndCapture);
            transaction.Id = GetTransactionIdFromResponseString(response.directResponse);
        }

        /// <summary>
        /// Creates a verify only transaction
        /// </summary>
        /// <param name="transaction">The details for the transaction</param>
        public void CreatePreAuthTransaction(AuthorizeNetTransaction transaction)
        {
            var authOnly = transaction.ConvertToAuthOnlyTransaction();
            var response = SubmitTransaction(authOnly);
            transaction.Id = GetTransactionIdFromResponseString(response.directResponse);
        }

        /// <summary>
        /// Finalizes a previously authorized transaction, or actually performing the billing
        /// </summary>
        /// <param name="transaction">The details for the transaction</param>
        public void SubmitPreAuthTransaction(AuthorizeNetTransaction transaction)
        {
            var priorAuthCapture = transaction.ConvertToPriorAuthTransaction();
            var response = SubmitTransaction(priorAuthCapture);
            transaction.Id = GetTransactionIdFromResponseString(response.directResponse);
        }

        public void CreateAuthorizeNetRecurringBilling()
        {
        }

    }

    /// <summary>
    /// Occurs with transaction code E00014
    /// </summary>
    public class RequiredFieldNullException : Exception
    {
        public RequiredFieldNullException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Occurs with transaction code E00027
    /// </summary>
    public class TransactionDeclinedException : Exception
    {
        public TransactionDeclinedException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Occurs with transaction code E00039
    /// </summary>
    public class DuplicateRecordException : Exception
    {
        public DuplicateRecordException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Occurs with transaction code E00040
    /// </summary>
    public class RecordNotFoundException : Exception
    {
        public RecordNotFoundException(string message)
            : base(message)
        {
        }
    }


    /// <summary>
    /// Extension methods for the Authorize.Net objects
    /// </summary>
    public static class AuthorizeNetExtensions
    {
        /// <summary>
        /// Converts an <see cref="BankingAccountType"/> to a <see cref="BankAccountTypeEnum"/>
        /// </summary>
        /// <param name="bankingAccountType">The <see cref="BankingAccountType"/> to convert</param>
        /// <returns>The converted <see cref="BankAccountTypeEnum"/></returns>
        public static BankAccountTypeEnum Convert(this BankingAccountType bankingAccountType)
        {
            switch (bankingAccountType)
            {
                case BankingAccountType.Checking:
                    return BankAccountTypeEnum.checking;
                    break;
                case BankingAccountType.Savings:
                    return BankAccountTypeEnum.savings;
                    break;
                case BankingAccountType.BusinessChecking:
                    return BankAccountTypeEnum.businessChecking;
                    break;
                default:
                    return BankAccountTypeEnum.checking;
                    break;
            }
        }

        /// <summary>
        /// Converts an <see cref="CustomerProfileMaskedType"/> to a <see cref="AuthorizeNetCustomer"/>
        /// </summary>
        /// <param name="customerProfile">The <see cref="CustomerProfileMaskedType"/> to convert</param>
        /// <returns>The converted <see cref="AuthorizeNetCustomer"/></returns>
        public static AuthorizeNetCustomer Convert(this CustomerProfileMaskedType customerProfile)
        {
            var customer = new AuthorizeNetCustomer
            {
                AuthorizeNetCustomerId = customerProfile.customerProfileId,
                Description = customerProfile.description,
                Email = customerProfile.email,
                MerchantCustomerId = int.Parse(customerProfile.merchantCustomerId),
                PaymentInfo = customerProfile.paymentProfiles.FirstOrDefault().Convert(),
                ShippingPersonInfo = customerProfile.shipToList.FirstOrDefault().Convert()
            };

            return customer;
        }

        /// <summary>
        /// Converts an <see cref="CustomerPaymentProfileMaskedType"/> to a <see cref="AuthorizeNetPaymentInfo"/>
        /// </summary>
        /// <param name="paymentProfile">The <see cref="CustomerPaymentProfileMaskedType"/> to convert</param>
        /// <returns>The converted <see cref="AuthorizeNetPaymentInfo"/></returns>
        public static AuthorizeNetPaymentInfo Convert(this CustomerPaymentProfileMaskedType paymentProfile)
        {
            if (paymentProfile == null)
            {
                return new AuthorizeNetPaymentInfo();
            }

            var payment = new AuthorizeNetPaymentInfo
            {
                Id = paymentProfile.customerPaymentProfileId,
                BillingPersonInfo = paymentProfile.billTo.Convert()
            };

            if (paymentProfile.payment.Item is CreditCardMaskedType)
            {
                var item = paymentProfile.payment.Item as CreditCardMaskedType;
                payment.CreditCardNumber = item.cardNumber;
                payment.Expiry = item.expirationDate;
            }
            else if (paymentProfile.payment.Item is BankAccountMaskedType)
            {
                var item = paymentProfile.payment.Item as BankAccountMaskedType;
                payment.BankAccountName = item.nameOnAccount;
                payment.BankAccountNumber = item.accountNumber;
                payment.BankName = item.bankName;
                payment.IsCreditCard = false;
                payment.BankRoutingNumber = item.routingNumber;
                payment.BankAccountType = item.accountType.Convert();
            }

            return payment;
        }

        /// <summary>
        /// Converts an <see cref="BankAccountTypeEnum"/> to a <see cref="BankingAccountType"/>
        /// </summary>
        /// <param name="accountType">The <see cref="BankAccountTypeEnum"/> to convert</param>
        /// <returns>The converted <see cref="BankAccountType"/></returns>
        public static BankingAccountType Convert(this BankAccountTypeEnum accountType)
        {
            switch (accountType)
            {
                case BankAccountTypeEnum.checking:
                    return BankingAccountType.Checking;
                    break;
                case BankAccountTypeEnum.savings:
                    return BankingAccountType.Savings;
                    break;
                case BankAccountTypeEnum.businessChecking:
                    return BankingAccountType.BusinessChecking;
                    break;
                default:
                    return BankingAccountType.Checking;
                    break;
            }
        }


        /// <summary>
        /// Converts an <see cref="CustomerAddressType"/> to a <see cref="AuthorizeNetPersonInfo"/>
        /// </summary>
        /// <param name="addressEx">The <see cref="CustomerAddressType"/> to convert</param>
        /// <returns>The converted <see cref="AuthorizeNetPersonInfo"/></returns>
        public static AuthorizeNetPersonInfo Convert(this CustomerAddressType addressEx)
        {
            if (addressEx == null)
            {
                return new AuthorizeNetPersonInfo();
            }

            var address = new AuthorizeNetPersonInfo
            {
                Address = addressEx.address,
                City = addressEx.city,
                Country = addressEx.country,
                FaxNumber = addressEx.faxNumber,
                FirstName = addressEx.firstName,
                LastName = addressEx.lastName,
                PhoneNumber = addressEx.phoneNumber,
                State = addressEx.state,
                Zip = addressEx.zip
            };

            return address;
        }


        /// <summary>
        /// Converts an <see cref="CustomerAddressExType"/> to a <see cref="AuthorizeNetPersonInfo"/>
        /// </summary>
        /// <param name="addressEx">The <see cref="CustomerAddressExType"/> to convert</param>
        /// <returns>The converted <see cref="AuthorizeNetPersonInfo"/></returns>
        public static AuthorizeNetPersonInfo Convert(this CustomerAddressExType addressEx)
        {
            if (addressEx == null)
            {
                return new AuthorizeNetPersonInfo();
            }

            var address = new AuthorizeNetPersonInfo
            {
                Address = addressEx.address,
                City = addressEx.city,
                Country = addressEx.country,
                FaxNumber = addressEx.faxNumber,
                FirstName = addressEx.firstName,
                Id = addressEx.customerAddressId,
                LastName = addressEx.lastName,
                PhoneNumber = addressEx.phoneNumber,
                State = addressEx.state,
                Zip = addressEx.zip
            };

            return address;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetPersonInfo"/> to a <see cref="CustomerAddressExType"/>
        /// </summary>
        /// <param name="personInfo">The <see cref="AuthorizeNetPersonInfo"/> to convert</param>
        /// <returns>The converted <see cref="CustomerAddressExType"/></returns>
        public static CustomerAddressExType ConvertToShippingAddressEx(this AuthorizeNetPersonInfo personInfo)
        {
            var customerAddressExType = new CustomerAddressExType
            {
                customerAddressId = personInfo.Id,
                address = personInfo.Address,
                city = personInfo.City,
                country = personInfo.Country,
                faxNumber = personInfo.FaxNumber,
                firstName = personInfo.FirstName,
                lastName = personInfo.LastName,
                phoneNumber = personInfo.PhoneNumber,
                state = personInfo.State,
                zip = personInfo.Zip
            };

            return customerAddressExType;
        }


        /// <summary>
        /// Converts an <see cref="AuthorizeNetPersonInfo"/> to a <see cref="CustomerAddressType"/>
        /// </summary>
        /// <param name="personInfo">The <see cref="AuthorizeNetPersonInfo"/> to convert</param>
        /// <returns>The converted <see cref="CustomerAddressType"/></returns>
        public static CustomerAddressType Convert(this AuthorizeNetPersonInfo personInfo)
        {
            var customerAddressType = new CustomerAddressType
            {
                address = personInfo.Address,
                city = personInfo.City,
                country = personInfo.Country,
                faxNumber = personInfo.FaxNumber,
                firstName = personInfo.FirstName,
                lastName = personInfo.LastName,
                phoneNumber = personInfo.PhoneNumber,
                state = personInfo.State,
                zip = personInfo.Zip
            };

            return customerAddressType;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetCustomer"/> to a <see cref="CustomerProfileExType"/>
        /// </summary>
        /// <param name="customer">The <see cref="AuthorizeNetCustomer"/> to convert</param>
        /// <returns>The converted <see cref="CustomerProfileExType"/></returns>
        public static CustomerProfileExType ConvertForUpdate(this AuthorizeNetCustomer customer)
        {
            var customerProfileExType = new CustomerProfileExType
            {
                customerProfileId = customer.AuthorizeNetCustomerId,
                description = customer.Description,
                email = customer.Email,
                merchantCustomerId = customer.MerchantCustomerId.ToString()
            };

            return customerProfileExType;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetCustomer"/> to a <see cref="CustomerPaymentProfileExType"/>
        /// </summary>
        /// <param name="customer">The <see cref="AuthorizeNetCustomer"/> to convert</param>
        /// <returns>The converted <see cref="CustomerPaymentProfileExType"/></returns>
        public static CustomerPaymentProfileExType ConvertToPaymentProfileEx(this AuthorizeNetPaymentInfo paymentInfo)
        {
            var customerPaymentProfileExType = new CustomerPaymentProfileExType
            {
                billTo = paymentInfo.BillingPersonInfo.Convert(),
                payment = paymentInfo.ConvertToPayment(),
                customerPaymentProfileId = paymentInfo.Id
            };

            return customerPaymentProfileExType;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetPaymentInfo"/> to a <see cref="PaymentType"/>
        /// </summary>
        /// <param name="customer">The <see cref="AuthorizeNetPaymentInfo"/> to convert</param>
        /// <returns>The converted <see cref="PaymentType"/></returns>
        public static PaymentType ConvertToPayment(this AuthorizeNetPaymentInfo payment)
        {

            var paymentInfo = new PaymentType();

            if (payment.IsCreditCard)
            {
                var item = new CreditCardType
                {
                    cardCode = payment.CardCode,
                    cardNumber = payment.CreditCardNumber,
                    expirationDate = payment.Expiry
                };

                paymentInfo.Item = item;
            }
            else
            {
                var item = new BankAccountType
                {
                    accountNumber = payment.BankAccountNumber,
                    accountType = payment.BankAccountType.Convert(),
                    bankName = payment.BankName,
                    nameOnAccount = payment.BankAccountName,
                    routingNumber = payment.BankRoutingNumber
                };

                paymentInfo.Item = item;
            }

            return paymentInfo;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetCustomer"/> to a <see cref="CustomerPaymentProfileType"/>
        /// </summary>
        /// <param name="customer">The <see cref="AuthorizeNetCustomer"/> to convert</param>
        /// <returns>The converted <see cref="CustomerPaymentProfileType"/></returns>
        public static CustomerPaymentProfileType ConvertToPaymentProfile(this AuthorizeNetPaymentInfo paymentInfo)
        {
            var customerPaymentProfile = new CustomerPaymentProfileType
            {
                billTo = paymentInfo.BillingPersonInfo.Convert(),
                payment = paymentInfo.ConvertToPayment()
            };

            return customerPaymentProfile;
        }
        
        /// <summary>
        /// Converts an <see cref="AuthorizeNetCustomer"/> to a <see cref="CustomerProfileType"/>
        /// </summary>
        /// <param name="customer">The <see cref="AuthorizeNetCustomer"/> to convert</param>
        /// <returns>The converted <see cref="CustomerProfileType"/></returns>
        public static CustomerProfileType Convert(this AuthorizeNetCustomer customer)
        {
            var shippingList = new List<CustomerAddressType>();
            shippingList.Add(customer.ShippingPersonInfo.Convert());

            var customerPaymentProfiles = new List<CustomerPaymentProfileType>();
            customerPaymentProfiles.Add(customer.PaymentInfo.ConvertToPaymentProfile());

            var authorizeNetCustomerObject = new CustomerProfileType
            {
                email = customer.Email,
                description = customer.Description,
                merchantCustomerId = customer.MerchantCustomerId.ToString(),
                shipToList = shippingList.ToArray(),
                paymentProfiles = customerPaymentProfiles.ToArray()
            };

            return authorizeNetCustomerObject;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetTransaction"/> to a <see cref="ProfileTransAuthCaptureType"/>
        /// </summary>
        /// <param name="transaction">The <see cref="AuthorizeNetTransaction"/> to convert</param>
        /// <returns>The converted <see cref="ProfileTransAuthCaptureType"/></returns>
        public static ProfileTransAuthCaptureType Convert(this AuthorizeNetTransaction transaction)
        {
            var authTransaction = new ProfileTransAuthCaptureType
            {
                amount = transaction.TotalAmount,
                customerProfileId = transaction.Customer.AuthorizeNetCustomerId,
                customerPaymentProfileId = transaction.Customer.PaymentInfo.Id,
                customerShippingAddressId = transaction.Customer.ShippingPersonInfo.Id,
                customerShippingAddressIdSpecified = true,
                tax = transaction.TaxAmount.Convert(),
                order = transaction.OrderNumber.Convert(),
                shipping = transaction.ShippingAmount.Convert(),
                lineItems = transaction.LineItems.Convert().ToArray()
            };
            // add description to authTransaction 
            authTransaction.order.description = transaction.OrderDescription;
            return authTransaction;
        }
        
        /// <summary>
        /// Converts an <see cref="AuthorizeNetTransaction"/> to a <see cref="ProfileTransAuthOnlyType"/>
        /// </summary>
        /// <param name="transaction">The <see cref="AuthorizeNetTransaction"/> to convert</param>
        /// <returns>The converted <see cref="ProfileTransAuthOnlyType"/></returns>
        public static ProfileTransAuthOnlyType ConvertToAuthOnlyTransaction(this AuthorizeNetTransaction transaction)
        {
            var authTransaction = new ProfileTransAuthOnlyType
            {
                amount = transaction.TotalAmount,
                customerProfileId = transaction.Customer.AuthorizeNetCustomerId,
                customerPaymentProfileId = transaction.Customer.PaymentInfo.Id,
                customerShippingAddressId = transaction.Customer.ShippingPersonInfo.Id,
                customerShippingAddressIdSpecified = true,
                tax = transaction.TaxAmount.Convert(),
                order = transaction.OrderNumber.Convert(),
                shipping = transaction.ShippingAmount.Convert(),
                lineItems = transaction.LineItems.Convert().ToArray()
            };

            return authTransaction;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetTransaction"/> to a <see cref="ProfileTransAuthOnlyType"/>
        /// </summary>
        /// <param name="transaction">The <see cref="AuthorizeNetTransaction"/> to convert</param>
        /// <returns>The converted <see cref="ProfileTransAuthOnlyType"/></returns>
        public static ProfileTransPriorAuthCaptureType ConvertToPriorAuthTransaction(this AuthorizeNetTransaction transaction)
        {
            var authTransaction = new ProfileTransPriorAuthCaptureType
            {
                amount = transaction.TotalAmount,
                customerProfileId = transaction.Customer.AuthorizeNetCustomerId,
                customerPaymentProfileId = transaction.Customer.PaymentInfo.Id,
                customerShippingAddressId = transaction.Customer.ShippingPersonInfo.Id,
                customerShippingAddressIdSpecified = true,
                tax = transaction.TaxAmount.Convert(),
                shipping = transaction.ShippingAmount.Convert(),
                lineItems = transaction.LineItems.Convert().ToArray(),
                transId = transaction.Id
            };

            return authTransaction;
        }

        /// <summary>
        /// Converts an <see cref="decimal"/> to an <see cref="ExtendedAmountType"/>
        /// </summary>
        /// <param name="amount">The <see cref="decimal"/> to convert</param>
        /// <returns>The converted <see cref="ExtendedAmountType"/></returns>
        public static ExtendedAmountType Convert(this decimal amount)
        {
            var amountType = new ExtendedAmountType
            {
                amount = amount
            };

            return amountType;
        }

        /// <summary>
        /// Converts an <see cref="string"/> to an <see cref="OrderExType"/>
        /// </summary>
        /// <param name="orderNumber">The <see cref="string"/> to convert</param>
        /// <returns>The converted <see cref="OrderExType"/></returns>
        public static OrderExType Convert(this string orderNumber)
        {
            var order = new OrderExType
            {
                invoiceNumber = orderNumber
            };

            return order;
        }

        /// <summary>
        /// Converts an <see cref="AuthorizeNetOrderLineItem"/> to a <see cref="LineItemType"/>
        /// </summary>
        /// <param name="lineItem">The <see cref="AuthorizeNetOrderLineItem"/> to convert</param>
        /// <returns>The converted <see cref="LineItemType"/></returns>
        public static LineItemType Convert(this AuthorizeNetOrderLineItem lineItem)
        {
            var lineItemType = new LineItemType
            {
                description = lineItem.Description,
                itemId = lineItem.ItemId,
                name = lineItem.Name,
                quantity = lineItem.Quantity,
                unitPrice = lineItem.UnitPrice
            };

            return lineItemType;
        }

        /// <summary>
        /// Converts an <see cref="IEnumerable<AuthorizeNetOrderLineItem>"/> to a <see cref="IEnumerable<LineItemType>"/>
        /// </summary>
        /// <param name="lineItems">The <see cref="IEnumerable<AuthorizeNetOrderLineItem>"/> to convert</param>
        /// <returns>The converted <see cref="IEnumerable<LineItemType>"/></returns>
        public static IEnumerable<LineItemType> Convert(this IEnumerable<AuthorizeNetOrderLineItem> lineItems)
        {
            var lineItemTypes = new List<LineItemType>();
            foreach (var item in lineItems)
            {
                lineItemTypes.Add(item.Convert());
            }

            return lineItemTypes;
        }

    }
}
