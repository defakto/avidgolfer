/* Javascript file for the home page */
/* carousel functions for home page main image gallery */
$(function() {
	/* attach an unique class name to each thumbnail image */
	$('#thumbs .thumb a').each(function(i) {
		$(this).addClass('itm' + i);
		
		/* add onclick event to thumbnail to make the main carousel scroll to the right slide */
		$(this).click(function() {
			$('#imageScroll').trigger('slideTo', [i, 0, true]);
			return false;
		}); /* end of second nested function */
	}); /* end of first nested function */
	/* highlight the first item on first load */
	$('#thumbs a.itm0').addClass('selected');
	
	/* Carousel for first item on first load */
	$('#imageScroll').carouFredSel({
		direction: 'left',
		synchronise: '#thumbs, true, true, 0', 
		circular: true,
		infinite: false,
		items: 1,
		auto: true,
		scroll: {
			fx: 'directscroll',
			onBefore: function() {
				/* Everytime the main slideshow changes this ensures that the thumbnail and page are displayed correctly */
				/* Get current position */
				var pos = $(this).triggerHandler('currentPosition');
				
				/* reset and select the current thumbnail item */
				$('#thumbs a').removeClass('selected');
				$('#thumbs a.itm' + pos).addClass('selected');
				
				/* Move the thumbnail to the right page */
				/*var page = Math.floor( pos / 3);
				$('#thumbs').trigger('slideToPage', page); */
			} /* end of onBefore event */
		} /* end of scroll attribute */
	}); /* end of carousel initialize function */
	/* Carousel for the thumbnails */
	$('#thumbs').carouFredSel({
		direction: 'left',
		synchronise: '#imageScroll, true, true, 0', 
		circular: true,
		infinite: false,
		items: 4,
		auto: false,
		align: false,
		prev: '#prev',
		next: '#next',
		scroll: {
			items: 1
		}
	});
}); /* end of main function */
