﻿using System;
using System.Configuration;
using System.Configuration.Provider;

namespace Defakto.Components.Provider
{
    public class ProviderCollection<T> : ProviderCollection where T : Defakto.Components.Provider.ProviderBase
    {

        #region Public Properties

        public new T this[string name]
        {
            get { return (T)base[name]; }
        }

        public T this[int index]
        {
            get
            {
                int counter = 0;

                foreach (T provider in this)
                {
                    if (counter == index)
                        return provider;

                    counter++;
                }

                return null;
            }
        }

        #endregion Public Properties


        #region Public Methods

        public override void Add(System.Configuration.Provider.ProviderBase provider)
        {
            if (!(provider is T))
                throw new ArgumentException( String.Format("The provider is not of type {0}.", typeof(T).ToString()) );

            this.Add((T)provider);
        }

        public void Add(T provider)
        {
            if (!provider.IsInitialized)
                provider.Initialize();

            base.Add(provider);
        }

        #endregion Public Methods


    }
}
