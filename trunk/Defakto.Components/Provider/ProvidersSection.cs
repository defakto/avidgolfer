﻿using System;
using System.Configuration;

namespace Defakto.Components.Provider
{
    public class ProvidersSection : ConfigurationSection
    {

        #region Private Properties

        private const string _defaultProviderProperty = "defaultProvider";

        #endregion


        #region Public Properties

        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers
        {
            get { return (ProviderSettingsCollection)base["providers"]; }
        }

        [ConfigurationProperty(_defaultProviderProperty, IsRequired = false)]
        public string DefaultProvider
        {
            get { return (string)base[_defaultProviderProperty]; }
            set { base[_defaultProviderProperty] = value; }
        }


        #endregion


    }
}
