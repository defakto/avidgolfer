﻿using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Collections.Generic;
using System.Text;
using System.Web.Configuration;

namespace Defakto.Components.Provider
{
    public class ProviderRepository<T> where T : ProviderBase
    {

        #region Private Properties

        private T _provider = null;
        private ProviderCollection<T> _providers = null;
        private volatile object _syncRoot = new object();

        private string _sectionName = String.Empty;

        #endregion Private Properties


        #region Public Properties

        public T Provider
        {
            get
            {
                if (_provider == null && _providers.Count > 0)
                    _provider = _providers[0];

                return _provider;
            }
        }

        public ProviderCollection<T> Providers
        {
            get { return _providers; }
        }

        public string SectionName
        {
            get { return _sectionName; }
        }

        #endregion Public Properties

        #region Constructors

        public ProviderRepository() : this(true)
        {
        }

        public ProviderRepository(bool throwErrorIfUnableToLoadSection)
        {
            if (String.IsNullOrEmpty(_sectionName))
                _sectionName = typeof(T).ToString();

            LoadProviders(throwErrorIfUnableToLoadSection);
        }

        public ProviderRepository(string sectionName) : this(sectionName, true)
        {
        }

        public ProviderRepository(string sectionName, bool throwErrorIfUnableToLoadSection)
        {
            _sectionName = sectionName;

            LoadProviders(throwErrorIfUnableToLoadSection);
        }

        public ProviderRepository(T provider)
        {
            _providers = new ProviderCollection<T>();
            _providers.Add(provider);

            _provider = provider;
        }

        public ProviderRepository(ProviderCollection<T> providers)
        {
            _providers = providers;
        }

        #endregion Constructors



        protected virtual void LoadProviders(bool throwErrorIfUnableToLoadSection)
        {
            // Avoid claiming lock if providers are already loaded
            if (_providers == null)
            {
                lock (_syncRoot)
                {
                    // Do this again to make sure provider is still null
                    if (_providers == null)
                    {
                        _providers = new ProviderCollection<T>();

                        // Get a reference to the section
                        ProvidersSection section = null;
                        try
                        {
                            section = ConfigurationManager.GetSection(_sectionName) as ProvidersSection;
                        }
                        catch (Exception)
                        {
                            section = null;
                        }

                        if (section == null)
                        {
                            if (throwErrorIfUnableToLoadSection)
                                throw new ProviderException( String.Format("Unable to load configuration section: '{0}'.", _sectionName) );
                        }
                        else
                        {
                            ProvidersHelper.InstantiateProviders(section.Providers, _providers, typeof(T));

                            if (_providers.Count > 0)
                            {
                                // If there is a default provider specified, then grab it, 
                                // else grab the first provider in the collection
                                if (!String.IsNullOrEmpty(section.DefaultProvider))
                                    _provider = (T)_providers[section.DefaultProvider];
                                else
                                    _provider = _providers[0];
                            }

                            if (throwErrorIfUnableToLoadSection)
                            {
                                if (_provider == null)
                                {
                                    throw new ProviderException( String.Format("Unable to load default provider for section: '{0}'.",_sectionName) );
                                }
                            }
                        }
                    }
                } // lock
            }
        }

    }
}
