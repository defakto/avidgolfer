﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;

namespace Defakto.Components.Provider
{
    public class ProviderBase : System.Configuration.Provider.ProviderBase
    {

        #region Private Properties

        private NameValueCollection _configuration = new NameValueCollection();
        private bool _isInitialized = false;
        private string _name = String.Empty;

        #endregion Private Properties


        #region Public Properties

        public NameValueCollection Configuration
        {
            get { return _configuration; }
        }

        public new string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public bool IsInitialized
        {
            get { return _isInitialized; }
        }

        #endregion Public Properties


        #region Overrides

        public void Initialize()
        {
            Initialize(String.Empty);
        }

        public void Initialize(string name)
        {
            Initialize(name, new NameValueCollection());
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                config = new NameValueCollection();

            if (String.IsNullOrEmpty(name))
            {
                if (!String.IsNullOrEmpty(_name))
                    name = _name;
                else
                    name = this.GetType().Name;
            }
            _name = name;

            _configuration.Clear();
            foreach (string key in config.Keys)
            {
                _configuration.Add(key, config[key]);
            }

            base.Initialize(name, config);

            _isInitialized = true;
        }

        #endregion Overrides


    }
}
