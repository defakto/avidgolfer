﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;

//using System.Linq;
//using System.Text;

namespace Defakto.Components
{
    [Serializable]
    public partial class Utilities : IUtilities, IDisposable
    {
        const string MatchEmailPattern =
          @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				        [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
           + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				        [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
        // also:  ^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$


        public Utilities()
        {
        }

        //String.Format("{0:MM/dd/yyyy}"

        public string CalculateAge(DateTime dob)
        {
            //  DateTime date = new DateTime(1984, 10, 6);
            TimeSpan ts = DateTime.Now.Subtract(dob);
            return (ts.Days / 365).ToString();
        }

        public bool IsEmail(string email)
        {
            if (!string.IsNullOrEmpty(email)) 
                return Regex.IsMatch(email, MatchEmailPattern);
            else 
                return false;
        }


        public bool PasswordStrength(string password)
        {
            string pattern = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6}$";
            return (!Regex.IsMatch(password, pattern));
        }

        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public string PageURL(string page)
        {
            using (DPage p = new DPage())
            {
                page = page.ToLower();
                if (page.Equals("account"))
                    page = p.account;
                else if (page.Equals("admin"))
                    page = p.admin;
                else if (page.Equals("cancel"))
                    page = p.cancel;
                else if (page.Equals("confirmation"))
                    page = p.confirmation;
                else if (page.Equals("login"))
                    page = p.login;
                else if (page.Equals("password"))
                    page = p.password;
                else if (page.Equals("emailpassword"))
                    page = p.emailpassword;
                else if (page.Equals("profile"))
                    page = p.profile;
                else if (page.Equals("register"))
                    page = p.register;
                else if (page.Equals("subscribe"))
                    page = p.subscribe;
                else if (page.Equals("checkout"))
                    page = p.checkout;
                else if (page.Equals("purchase"))
                    page = p.purchase;
                else
                {
                }

                //if (page.Equals("account"))
                //    page = p.account;
                //else if (page.Equals("cancel"))  // subscribe
                //    page = p.cancel;
                //else
                //{
                //}
            }

            return page;
        }

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////
    }

    public class DPage : IDisposable
    {
        public string account
        {
            get
            {
                return "~/usercontrols/UserAccount.ascx";
            }
        }

        public string admin
        {
            get
            {
                return "~/usercontrols/AGManager.ascx";
            }
        }

        public string cancel
        {
            get
            {
                return "~/usercontrols/SubscribeCancel.ascx";
            }
        }

        public string checkout
        {
            get
            {
                return "~/usercontrols/SubscribeCheckout.ascx";
            }
        }

        public string confirmation
        {
            get
            {
                return "~/usercontrols/UserConfirmation.ascx";
            }
        }

        public string login //
        {
            get
            {
                return "~/usercontrols/UserLogIn.ascx";
            }
        }

        public string password
        {
            get
            {
                return "~/usercontrols/UserPassword.ascx";
            }
        }

        public string emailpassword
        {
            get
            {
                return "~/usercontrols/UserEmailPassword.ascx";
            }
        }

        public string profile
        {
            get
            {
                return "~/usercontrols/UserProfile.ascx";
            }
        }

        public string purchase
        {
            get
            {
                return "~/usercontrols/SubscribePurchase.ascx";
            }
        }

        public string poconfirm
        {
            get
            {
                return "~/usercontrols/AGSubscribePOConfirm.ascx";
            }
        }

        public string register
        {
            get
            {
                return "~/usercontrols/UserRegister.ascx";
            }
        }


        public string subscribe
        {
            get
            {
                return "~/usercontrols/Subscribe.ascx";
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////
    }

}
