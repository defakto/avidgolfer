﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Serialization;
using Defakto.Components;

namespace Defakto.Configuration
{
    public class DCConfiguration
    {
        public static readonly string CacheKey = "DCConfiguration";
        private static readonly object configLocker = new object();

        #region private members

        Hashtable providers = new Hashtable();

        private int cacheFactor = 5;
        private bool enableBackgroundCacheCallbacks = true;
        private XmlDocument XmlDoc = null;

        #endregion

		#region Constructor

		public DCConfiguration(XmlDocument doc)
		{
			XmlDoc = doc;
			//LoadValuesFromConfigurationXml();
		}

		#endregion


        #region GetXML

        /// <summary>
        /// Enables reading of the configuration file's XML without reloading the file
        /// </summary>
        /// <param name="nodePath"></param>
        /// <returns></returns>
        public XmlNode GetConfigSection(string nodePath)
        {
            return XmlDoc.SelectSingleNode(nodePath);
        }

        #endregion




    }
}
