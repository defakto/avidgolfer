﻿using System;
using System.Web.UI;

//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

namespace Defakto.Components
{
    public interface IUtilities : IDisposable
    {
        bool PasswordStrength(string password);

        int RandomNumber(int min, int max);

        string PageURL(string page);

    }

}
