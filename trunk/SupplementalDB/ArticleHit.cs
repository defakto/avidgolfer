﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AvidGolfer.Data;
using AvidGolfer.Components;
using AvidGolfer.Entities;
using System.Xml;
using System.Xml.XPath;

namespace SupplementalDB
{
    public class ArticleHit
    {
        public ArticleHit()
        {
           
        }
        
        public static XPathNodeIterator createMostViewedList(string region)
        {
          
            SupplementalContentManager cm = new SupplementalContentManager();
            List<MostViewed> articles = new List<MostViewed>();

            XmlDocument xmlDoc = new XmlDocument();

            articles = cm.GetMostViewedArticles(region);

            XmlElement root = xmlDoc.CreateElement("articles");

            xmlDoc.AppendChild(root);

            foreach (var value in articles)
            {
                XmlElement el1 = xmlDoc.CreateElement("article");
                root.AppendChild(el1);

                XmlElement el = xmlDoc.CreateElement("articleId");

                el.InnerText = value.NodeId;

                el1.AppendChild(el);

            }

            XPathNodeIterator xNodeIt = xmlDoc.CreateNavigator().Select(".");

            return xNodeIt;
        }

        // method for checking if current user has an active subscription for the current region
        public static Boolean CurrentSubscription(string userId, string region) 
        {
            SupplementalContentManager cm = new SupplementalContentManager();
            int Id = 0;
            try
            {
                Id = Convert.ToInt32(userId);
            }
            catch (Exception ex)
            {
            }

            // set region to empty for now allows for the option to make region specific later
            region = "";
            Boolean exists = cm.CurrentOrderExists(Id, region);

            return exists;
        }

    }
}
