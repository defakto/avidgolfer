﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AvidGolfer.Data;
using AvidGolfer.Components;
using AvidGolfer.Entities;
using System.Xml;
using System.Xml.XPath;

namespace SupplementalDB
{
    public class CartGirl
    {
        public CartGirl()
        {

        }

        public static XPathNodeIterator createCartGirlList()
        {
            SupplementalContentManager cm = new SupplementalContentManager();
            List<CartGirlVote> girls = new List<CartGirlVote>();

            XmlDocument xmlDoc = new XmlDocument();

            girls = cm.GetCartGirls();

            XmlElement root = xmlDoc.CreateElement("girls");

            xmlDoc.AppendChild(root);

            foreach (var value in girls)
            {
                XmlElement el1 = xmlDoc.CreateElement("girl");
                root.AppendChild(el1);

                XmlElement el2 = xmlDoc.CreateElement("nodeId");
                el2.InnerText = value.NodeId;
                el1.AppendChild(el2);

                XmlElement el3 = xmlDoc.CreateElement("voteCount");
                el3.InnerText = Convert.ToString(value.TotalVotes);
                el1.AppendChild(el3);
            }

            XPathNodeIterator xNodeIt = xmlDoc.CreateNavigator().Select(".");

            return xNodeIt;

        }
    }
}
