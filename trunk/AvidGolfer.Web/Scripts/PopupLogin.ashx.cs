﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using AvidGolfer.Components;

namespace AvidGolfer.Web.Scripts
{
    /// <summary>
    /// Summary description for PopupLogin
    /// </summary>
    public class PopupLogin : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            try
            {
                string username = context.Request["username"];
                string password = context.Request["password"];

                using (IMembershipManager member = new MembershipManager())
                {
                    if (member.LogInUser(username, password)) {

                        context.Response.Write("success");
                    }
                    else
                    {
                        context.Response.Write("fail");
                    }
                }


            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message:  {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

                context.Response.Write(sb.ToString());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}