﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Linq;
using System.Web;
using AvidGolfer.Components;

namespace AvidGolfer.Web.Scripts
{
    /// <summary>
    /// Summary description for IncrementHitCount
    /// </summary>
    public class IncrementHitCount : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            try
            {
                string nodeId = context.Request["nodeId"];
                SupplementalContentManager smb = new SupplementalContentManager();
                //smb.IncreaseArticleHitCount(nodeId);

                context.Response.Write("success");
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message:  {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

                context.Response.Write(sb.ToString());
            }
        
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}