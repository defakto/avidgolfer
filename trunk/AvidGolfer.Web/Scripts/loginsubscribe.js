﻿
/// <reference path="jquery-1.8.0.min.js" />

$(document).ready(function () {

    /* search city, state, county by zip code */
    $(function () {

        $("#tbZip, #tbZipS").bind('blur', function (e) {

            var method = $(this).attr('id').toString();
            method = method.substring(method.length, 1).toUpperCase();
            if (method != 'S')
                method = '';

            ZipSearch(method);
        });
    });


    /* search city, state, county by zip code */
    function ZipSearch(method) {

        var loc = window.location.href;

        var zip = document.getElementById("<%= tbZip" + method + ".ClientID %>").innerHTML;

        if (zip.length != 5)
            return;

        $.ajax({
            type: 'POST',
            url: loc + "/ZipLocationSelect",
            dataType: "json",
            data: "{'zip':'" + zip + "'}",
            contentType: "application/json; charset=utf-8"
        })
        .success(function (response) {

            var zips = response.hasOwnProperty("d") ? response.d.toString().split(":") : "";

            if (zips == "")
                return;

            if ($("<%= tbCity" + method + ".ClientID %>").length != 0) {
                if (zips[0] != "")
                    document.getElementById("<%= tbCity" + method + ".ClientID %>").innerText = zips[0];
            }

            if ($("<%= tbState" + method + ".ClientID %>").length != 0) {
                if (zips.length > 1)
                    document.getElementById("<%= tbState" + method + ".ClientID %>").innerText = zips[1];
            }

            if ($("<%= tbCounty" + method + ".ClientID %>").length != 0) {
                if (zips.length > 2)
                    document.getElementById("<%= tbCounty" + method + ".ClientID %>").innerText = zips[2];
            }

        })
        .error(function (response) {
            alert(response.d);
        });
    } /* end function ZipSearch(method) */


}); /* end document.ready */