<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
        ]]>
    </msxml:script>

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderDetermineIssueYear">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="renderDetermineIssueYear">
        <xsl:param name="home"/>

        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>
        <xsl:choose>
            <xsl:when test="count($home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue) = 0">
                <xsl:call-template name="renderWidgetVoteCartGirl">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year) - 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="renderWidgetVoteCartGirl">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>


    </xsl:template>
    <xsl:template name="renderWidgetVoteCartGirl">
        <xsl:param name="home"/>
        <xsl:param name="year"/>
        <!-- these two variables allow for the closing and opening of ul tags within a conditional statement-->
        <xsl:variable name="ulOpen">
            <xsl:text>&lt;ul&gt;</xsl:text>
        </xsl:variable>
        <xsl:variable name="ulClose">
            <xsl:text>&lt;/ul&gt;</xsl:text>
        </xsl:variable>
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>

        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="voteEnabled" select="$home/SettingsFolder/EnableCartGirlVote/* [regionType = $cRegion]" />
        <xsl:variable name="Class" select="umbraco.library:Replace($cRegion, ' ', '')" />
        <xsl:variable name="regionClass" select="umbraco.library:Replace($Class, '.', '')" />
        <xsl:variable name="items" select="$home/AGIssuesArea/YearFolder[@nodeName=$voteEnabled/voteYear]/Issue/CartGirlProfile [regionType = $cRegion or regionType = 'All']" />
        
            <xsl:variable name="girlUrl" select="$home/umbTextpage/CartGirlPage [regionType = $cRegion]/@id"/>
        
        <xsl:if test="$voteEnabled/enableVote = 1">
            <xsl:if test="count($items) &gt; 0">
                <div class="voteCGContainer vote {$regionClass}">
                    <div class="blue_line">
                        <xsl:if test="$home/@id = $currentPage/@id">
                            <xsl:attribute name="class">blue_line home</xsl:attribute>
                        </xsl:if>
                    </div>
                    <div class="vote_sidebar">
                        <div class="vote_header">
                            <img src="/images/vote_sidebar_header.png" width="273" height="14" align="center" />
                        </div>
                        <div class="main_vote">
                            <div id="vPrev" class="left_scroll">
                                <a href="#">
                                    <img src="/images/left_scroll.png" width="10" height="45" />
                                </a>
                            </div>
                            <div id="voteGirl">
                                <!--<xsl:if test="count($items) &gt; 0">-->
                                <xsl:for-each select="$items">
                                    <!--<xsl:choose>
                                <xsl:when test="./regionType = 'All' or ./regionType = $cRegion">-->
                                    <xsl:variable name="image" select="./CartGirlPhoto[1]" />
                                    <xsl:variable name="url" select="umbraco.library:GetMedia($image/photo,'false')/umbracoFile"/>
                                    <a href="{umbraco.library:NiceUrl($girlUrl)}">
                                        <img src="{$url}" width="292" height="251" />
                                    </a>
                                    <!--</xsl:when>
                            </xsl:choose>-->
                                </xsl:for-each>
                                <!--</xsl:if>-->
                            </div>

                            <div id="vNext" class="right_scroll">
                                <a href="#">
                                    <img src="/images/right_scroll.png" width="10" height="45" />
                                </a>
                            </div>
                        </div>
                        <div id="cartVote" class="vote_options">
                            <xsl:if test="count($items) &gt; 0">
                                <ul>
                                    <xsl:for-each select="$items">
                                        <xsl:choose>
                                            <xsl:when test="./regionType = 'All' or ./regionType = $cRegion">
                                                <xsl:variable name="image" select="./CartGirlPhoto[1]" />
                                                <xsl:variable name="url" select="umbraco.library:GetMedia($image/thumbnail,'false')/umbracoFile"/>
                                                <li class="vGirl">
                                                    <a href="#">
                                                        <img src="{$url}" width="44" height="44" />
                                                    </a>
                                                </li>
                                                <!--
                                          <xsl:if test="count($items) = 6">
                                              <xsl:value-of select="$ulClose" disable-output-escaping="yes"/>
                                              <xsl:value-of select="$ulOpen" disable-output-escaping="yes" />
                                         </xsl:if>
										 -->
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:for-each>
                                </ul>
                            </xsl:if>

                        </div>
                        <div class="clear"></div>
                        <div class="blue_line"></div>
                        <div class="vote_now">
                            <a href="{umbraco.library:NiceUrl($girlUrl)}">
                                <img src="/images/vote_now.png" />
                            </a>
                        </div>
                    </div>
                </div>
            </xsl:if>
        </xsl:if>
    </xsl:template>



</xsl:stylesheet>