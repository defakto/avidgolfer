<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            var numberOfPages = 0;
            var pageNumber = 0;
     
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
            
            function setPageCount(pageCount) {
                numberOfPages = pageCount;
                
                return numberOfPages;
            }
            
            function numberPages() {
            
                return ++pageNumber;
            }
            
            function countDownPage() {
                return --numberOfPages;
            }   
        ]]>
    </msxml:script>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<!--<xsl:template match="/">
		<xsl:call-template name="renderCoursePostList">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
		</xsl:call-template>
	</xsl:template>-->
    
    <xsl:template match="/">
        <xsl:variable name="qsIndex" select="umbraco.library:RequestQueryString('p')" />

        <xsl:choose>
            <xsl:when test="$qsIndex !=''">
                <xsl:call-template name="determinePageIndex">
                    <xsl:with-param name="pageIndex" select="$qsIndex"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="determinePageIndex">
                    <xsl:with-param name="pageIndex" select="1"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="determinePageIndex">
        <xsl:param name="pageIndex"/>

        <xsl:call-template name="renderCoursePostList">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
            <xsl:with-param name="pageIndex" select="$pageIndex"/>
        </xsl:call-template>
    </xsl:template>
    
	  <xsl:template name="renderCoursePostList">
		<xsl:param name="home"/>
        <xsl:param name="pageIndex" />
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>
        <xsl:variable name="recordsPerPage" select="5"/>
        
        <xsl:variable name="regions" select="$home/SettingsFolder/RegionType/* [title != 'All']" />
          <xsl:variable name="cost" select="$home/SettingsFolder/PriceRanges/*" />
        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
          <xsl:variable name="dRegion">
              <xsl:choose>
                  <xsl:when test="umbraco.library:RequestQueryString('region') != ''">
                      <xsl:value-of select="umbraco.library:RequestQueryString('region')"/>
                  </xsl:when> 
              </xsl:choose>
          </xsl:variable>
          <xsl:variable name="dPrice">
              <xsl:choose>
                  <xsl:when test="umbraco.library:RequestQueryString('price') != ''">
                      <xsl:value-of select="umbraco.library:RequestQueryString('price')"/>
                  </xsl:when>
              </xsl:choose>
          </xsl:variable>
         
          <xsl:variable name="courseList">
              <xsl:choose>
                  <xsl:when test="$dRegion != '' and $dPrice != ''">
                      <xsl:copy-of select="$currentPage/CourseEntry [(regionType = $dRegion or regionType = 'All') and priceRange = $dPrice]" />
                   
                  </xsl:when>
                  <xsl:when test="$dRegion != '' and $dPrice = ''">
                      <xsl:copy-of select="$currentPage/CourseEntry [regionType = $dRegion or regionType = 'All']"/>
                     
                  </xsl:when>
                  <xsl:when test="$dRegion = '' and $dPrice != ''">
                      <xsl:copy-of select="$currentPage/CourseEntry [priceRange = $dPrice]" />
                      
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:copy-of select="$currentPage/CourseEntry" />
                  </xsl:otherwise>
              </xsl:choose>
          </xsl:variable>
          <xsl:variable name="pageCount" select="ceiling(count(msxml:node-set($courseList)/CourseEntry) div $recordsPerPage)" />
          <xsl:variable name="fpageCount" select="js:setPageCount($pageCount)" />
          <div class="courseDropdown">
              <div class="dRegion">
              REGION<br />
              <select name="regionSelect" id="regionSelect" class="regionSelect" onchange="javascript:dropdownChange('{$pageIndex}');">
                  <option value="">Select a region</option>
                  <xsl:if test="count($regions) &gt; 0">
                      <xsl:for-each select="$regions">
                          <option value="{./title}">
                              <xsl:value-of select="./title"/>
                          </option>
                      </xsl:for-each>
                  </xsl:if>
              </select>
              </div>
              <div class="dPrice">
              PRICE RANGE<br />
              <select name="price" id="priceSelect" onchange="javascript:dropdownChange('{$pageIndex}');">
                  <option value="">Select a price range</option>
                  <xsl:if test="count($cost) &gt; 0">
                      <xsl:for-each select="$cost">
                          <option value="{./title}">
                              <xsl:value-of select="./title"/>
                          </option>
                      </xsl:for-each>
                  </xsl:if>
              </select>
              </div>
              <div class="dCourse">
              COURSE NAME<br />
              <select name="courses" id="courseSelect" onchange="window.document.location.href=this.options[this.selectedIndex].value;">
                  <option value="">Select a course name</option>
                  <xsl:if test="count(msxml:node-set($courseList)/CourseEntry) &gt; 0">
                      <xsl:for-each select="msxml:node-set($courseList)/CourseEntry">
                          <option value="{umbraco.library:NiceUrl(./@id)}">
                              <xsl:value-of select="./title"/>
                          </option>
                      </xsl:for-each>
                  </xsl:if>
              </select>
              </div>
          </div>
          <!--<textarea>
              <xsl:copy-of select="$cost"/>
          </textarea>-->
          <div class="clear"></div>
          			<xsl:for-each select="msxml:node-set($courseList)/CourseEntry [string(data [@alias='umbracoNaviHide']) != '1']">
                <xsl:if test="position() &gt; $recordsPerPage * number($pageIndex - 1) and position() &lt;= number($recordsPerPage * number($pageIndex - 1) + $recordsPerPage )">
                    <div class="content_teaser">
                        <xsl:choose>
                            <xsl:when test="./photo !=''">
                                <img src="{umbraco.library:GetMedia(./photo, 0)/umbracoFile}" width="260" height="200" align="left" />
                            </xsl:when>
                            <xsl:otherwise>
                                <img src="/images/blank.png" width="260" height="200" align="left" />
                            </xsl:otherwise>
                        </xsl:choose>
                        <h1>
                            <a href="{umbraco.library:NiceUrl(./@id)}">
                                <xsl:value-of select="./title" />
                            </a>
                        </h1>
                        <div class="coursePostListLeftPane">
                            <h2>
                                <xsl:value-of select="./titlePrefix" />
                            </h2>
                            <xsl:value-of select="./address1" /> <br />
                            <xsl:value-of select="./address2" /> <br />
                            Region: <xsl:value-of select="./regionDesc" /> <br />
                        </div>

                        <div class="coursePostListRightPane">
                            Phone: <xsl:value-of select="./phone" /> <br />
                            Price Range: <xsl:value-of select="./priceRange" /> <br />
                            Region: <xsl:value-of select="./regionDesc" /> <br />
                            <br />
                            Designer: <xsl:value-of select="./designer" /> <br />
                            Year Opened: <xsl:value-of select="./yearOpened" /> <br />
                            <br />
                            <xsl:value-of select="./introduction" />
                            <a href="{umbraco.library:NiceUrl(./@id)}">
                                <span class="read_more">Read More</span>
                            </a>


                        </div>

                        <div class="coursePostListReadMore">
                        </div>
                    </div>
                    <div class="clear"></div>
                </xsl:if>
			</xsl:for-each>
          <!--</xsl:if>-->
          <xsl:if test="$pageCount &gt; 1">
              <div class="article_pagination">
                  <div class="previous_left">
                      <xsl:choose>
                          <xsl:when test="number($pageIndex) &gt; 1">
                              <a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={number($pageIndex)-1}&#38;region={$dRegion}&#38;price={$dPrice}">
                                  <img src="/images/caret-left.png" /> Previous Page
                              </a>
                          </xsl:when>
                          <xsl:otherwise>
                              <span class="disabled_link" href="#">
                                  <img src="/images/caret-left-inactive.png" /> Previous Page
                              </span>
                          </xsl:otherwise>
                      </xsl:choose>
                  </div>
                  <div class="next_right">
                      <xsl:choose>
                          <xsl:when test="number($pageIndex) &lt; number($pageCount) ">
                              <a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={number($pageIndex)+1}&#38;region={$dRegion}&#38;price={$dPrice}">
                                  Next Page <img src="/images/caret-right.png" />
                              </a>
                          </xsl:when>
                          <xsl:otherwise>
                              <span class="disabled_link" href="#">
                                  Next Page <img src="/images/caret-right-inactive.png" />
                              </span>
                          </xsl:otherwise>
                      </xsl:choose>
                  </div>
                  <div class="pagination">
                      <xsl:for-each select="msxml:node-set($courseList)/CourseEntry">
                            <xsl:variable name="count" select="js:countDownPage()" />
                            
                          <xsl:choose>
                              <xsl:when test="$count &gt;= 0">
                                 <xsl:variable name="nPage" select="js:numberPages()" /> 
                                  <a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={$nPage}&#38;region={$dRegion}&#38;price={$dPrice}">
                              <xsl:value-of select="$nPage" />
                              <xsl:if test="$count != 0"> |</xsl:if>
                          </a>
                              </xsl:when>
                          </xsl:choose>
                         
                      </xsl:for-each> 
                  </div>
                  <div class="clear"></div>
              </div>
          </xsl:if>
	  </xsl:template>
  	  
</xsl:stylesheet>