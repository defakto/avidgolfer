﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderDetermineIssueYear">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="renderDetermineIssueYear">
        <xsl:param name="home"/>

        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>

        <xsl:choose>
            <xsl:when test="count($home/AGDigitalArchiveArea/YearFolder[@nodeName=$year]/DigitalArchiveItem) = 0">
                <xsl:call-template name="renderDigitalArchive">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year) - 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="renderDigitalArchive">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template> 

    <xsl:template name="renderDigitalArchive">
        <xsl:param name="home" />
        <xsl:param name="year" />
        <xsl:variable name="dYear">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestQueryString('year') != ''">
                    <xsl:value-of select="umbraco.library:RequestQueryString('year')" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$year"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="regionList" select="$home/SettingsFolder/RegionType/* [title != 'All']" />
        <xsl:variable name="yearList" select="$home/AGDigitalArchiveArea/YearFolder" />
            <div class="digitalYear">
                <strong> Select a Year:</strong><br />
                <select name="yearSelect" id="selectYear" onchange="javascript:yearDigitalDropdownChange()">
                    <xsl:if test="count($yearList) &gt; 0">
                        <xsl:for-each select="$yearList">
                            <option value="{./@nodeName}">
                                <xsl:value-of select="./@nodeName" />
                            </option>
                        </xsl:for-each>
                    </xsl:if>
                </select>
            </div>
        <div class="clear"></div>
        <p class="digitalInst">Click on a link below to view the digital archive version of any previous publication from your nearest city/region:</p>
       
        <xsl:for-each select="$regionList">
            <xsl:variable name="rTitle" select="title" />
            <xsl:variable name="digitalList" select="$home/AGDigitalArchiveArea/YearFolder[@nodeName=$dYear]/DigitalArchiveItem [region = $rTitle]" />
            <p class="digitalHeader">
                <strong>
                    <xsl:value-of select="$rTitle"/>
                </strong>
            </p>
            <ul class="digitalList">
                <xsl:for-each select="$digitalList">
                    <xsl:variable name="pos" select="number(position() div 2)" />
                    <xsl:choose>
                        <xsl:when test="number(position() div 2) mod 0">
                            <li class="dRight">
                                <xsl:variable name="url" >
                                    <xsl:choose>
                                        <xsl:when test="coverimage != ''">
                                            <xsl:value-of select="umbraco.library:GetMedia(coverimage,'false')/umbracoFile" />
                                        </xsl:when>
                                        <xsl:otherwise>/images/blank.png</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>

                                <xsl:variable name="dLink" select="link" />
                                    <span class="digitalImage">
                                        <img src="{$url}" width="77" height="105" rel="81,110" />
                                    </span>
                                    <span class="digitalLink">
                                            <strong>
                                                <xsl:choose>
                                                    <xsl:when test="umbraco.library:IsLoggedOn()">
                                                        <a href="{$dLink}" target="_blank">
                                                            <xsl:value-of select="title"/>
                                                        </a>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <a href="#" class="openlogin">
                                                            <xsl:value-of select="title" />
                                                        </a>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </strong>
                                    </span>
                            </li>
                        </xsl:when>
                        <xsl:otherwise>
                            <li class="dLeft">
                                <xsl:variable name="url" >
                                    <xsl:choose>
                                        <xsl:when test="coverimage != ''">
                                            <xsl:value-of select="umbraco.library:GetMedia(coverimage,'false')/umbracoFile" />
                                        </xsl:when>
                                        <xsl:otherwise>/images/blank.png</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>

                                <xsl:variable name="dLink" select="link" />
                                    <span class="digitalImage">
                                        <img src="{$url}" width="77" height="105" rel="81,110" />
                                    </span>
                                    <span class="digitalLink">
 
                                            <strong>
                                                <xsl:choose>
                                                    <xsl:when test="umbraco.library:IsLoggedOn()">
                                                        <a href="{$dLink}" target="_blank">
                                                            <xsl:value-of select="title"/>
                                                        </a>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <a href="#" class="openlogin">
                                                            <xsl:value-of select="title" />
                                                        </a>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </strong>
                                    </span>
                            </li>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </ul>
            <div class="clear"></div>
        </xsl:for-each>
        <div id="modalLogin">
            <a href="javascript:closeLoginClick()">
                X
            </a>
            <p>
                You must be logged in to view digital archive editions
            </p>
            <p>
                <label for="mLogin">Username</label><br />
                <input name="mLogin" id="mLogin" type="text" />
                <br />
                <label for="mPassword">Password</label>
                <br />
                <input type="password" name="mPassword" id="mPassword" />
                <br />
                <span class="logError">Invalid Login / Password Combination.</span>
                <br />
                <input type="button" id="mSubmit" value="Login" onclick="popupLogin()" />
            </p>
            <p>
                <a href="/subscribe.aspx">Get an Avidgolfer Subscription</a>
            </p>
        </div>
    </xsl:template>

</xsl:stylesheet>
