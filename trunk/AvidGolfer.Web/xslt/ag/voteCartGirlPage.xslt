﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
             var numberOfPages = 0;
            var pageNumber = 0;
            
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
            
            function setPageCount(pageCount) {
                numberOfPages = pageCount;
                
                return numberOfPages;
            }
            
            function numberPages() {
            
                return ++pageNumber;
            }
            
            function countDownPage() {
                return --numberOfPages;
            }
        ]]>
    </msxml:script>
    
    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderDetermineIssueYear">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template name="renderDetermineIssueYear">
        <xsl:param name="home"/>
        
        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>

        <xsl:choose>
            <xsl:when test="count($home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue) = 0">
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year) - 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="qsIndex" select="umbraco.library:RequestQueryString('p')" />
        <xsl:choose>
            <xsl:when test="$qsIndex !=''">
                <xsl:call-template name="determinePageIndex">
                    <xsl:with-param name="pageIndex" select="$qsIndex"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="determinePageIndex">
                    <xsl:with-param name="pageIndex" select="1"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template name="determinePageIndex">
        <xsl:param name="pageIndex"/>

        <xsl:call-template name="renderCartGirlList">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
            <xsl:with-param name="pageIndex" select="$pageIndex"/>
        </xsl:call-template>
    </xsl:template>
    

    <xsl:template name="renderCartGirlList">
        <xsl:param name="home"/>
        <xsl:param name="pageIndex" />
        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>
      
        <!--<xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>-->
        <xsl:variable name="cRegion" select="$currentPage/regionType" />
        <xsl:variable name="test" select="$home/TextPage/CartGirlPage/*" />
        <xsl:variable name="Class" select="umbraco.library:Replace($cRegion, ' ','')" />
        <xsl:variable name="regionClass" select="umbraco.library:Replace($Class, '.', '')" />
        <xsl:variable name="items" select="$home/AGIssuesArea/YearFolder/Issue/CartGirlProfile [regionType = $cRegion or regionType = 'All']" />
        <xsl:variable name="recordsPerPage" select="12" />
        <xsl:variable name="pageCount" select="ceiling(count($items) div $recordsPerPage)" />
        <xsl:variable name="fpageCount" select="js:setPageCount($pageCount)" />
        <xsl:variable name="voteEnable" select="$home/SettingsFolder/EnableCartGirlVote/* [regionType = $cRegion]" />
        <div class="cg_vote_section">
            <xsl:if test="count($items) &gt; 0">
                <xsl:for-each select="$items [string(data [@alias='umbracoNavHide']) != '1']">
                    <xsl:if test="position() &gt; $recordsPerPage * number($pageIndex - 1) and position() &lt;= number($recordsPerPage * number($pageIndex - 1) + $recordsPerPage )">
                        
							<xsl:variable name="url" >
								<xsl:choose>
									<xsl:when test="./votePageImage != ''">
										<xsl:value-of select="umbraco.library:GetMedia(votePageImage,'false')/umbracoFile" />
									</xsl:when>
									<xsl:otherwise>/images/blank.png</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
						
                            <xsl:variable name="id" select="@id" />
                            <xsl:variable name="title" select="title" />
                            <div class="vote_tile">
								<a href="{umbraco.library:NiceUrl(./@id)}">
									<img src="{$url}" height="210" width="210" />
								</a>
                                <h1>
                                    <xsl:value-of select="title"/>
                                </h1>
                                <h2>
                                    <xsl:value-of select="location"/>
                                </h2>
                                <a href="{umbraco.library:NiceUrl(./@id)}">
                                    <span class="view_profile">View Profile</span>
                                </a>
                                <xsl:if test="$voteEnable/enableVote = 1">
                                    <xsl:if test="year =$voteEnable/voteYear">
                                        <input type="button" value="VOTE" class="vote {$regionClass}" onclick="voteGirl('{$id}', '{$cRegion}')"  />
                                    </xsl:if>
                                </xsl:if>
                            </div>
                        </xsl:if>
                </xsl:for-each>
            </xsl:if>
        </div>
        <div class="clear"></div>
        <xsl:if test="$pageCount &gt; 1">
            <div class="article_pagination">
                <div class="previous_left">
                    <xsl:choose>
                        <xsl:when test="number($pageIndex) &gt; 1">
                            <a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={number($pageIndex)-1}">
                                <img src="/images/caret-left.png" /> Previous Page
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            <span class="disabled_link" href="#">
                                <img src="/images/caret-left-inactive.png" /> Previous Page
                            </span>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
                <div class="next_right">
                    <xsl:choose>
                        <xsl:when test="number($pageIndex) &lt; number($pageCount) ">
                            <a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={number($pageIndex)+1}">
                                Next Page <img src="/images/caret-right.png" />
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            <span class="disabled_link" href="#">
                                Next Page <img src="/images/caret-right-inactive.png" />
                            </span>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
                <div class="pagination">
                    <xsl:for-each select="$items">
                        <xsl:variable name="count" select="js:countDownPage()" />

                        <xsl:choose>
                            <xsl:when test="$count &gt;= 0">
                                <xsl:variable name="nPage" select="js:numberPages()" />
                                <a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={$nPage}">
                                    <xsl:value-of select="$nPage" />
                                    <xsl:if test="$count != 0"> |</xsl:if>
                                </a>
                            </xsl:when>
                        </xsl:choose>

                    </xsl:for-each>

                </div>
                <div class="clear"></div>
            </div>
        </xsl:if>
                               
        <div class="clear"></div>
    </xsl:template>
</xsl:stylesheet>

