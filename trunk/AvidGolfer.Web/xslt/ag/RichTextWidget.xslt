<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:tagsLib="urn:tagsLib" xmlns:BlogLibrary="urn:BlogLibrary"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets tagsLib BlogLibrary js">
    
    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
        ]]>
    </msxml:script>

<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
<xsl:param name="datasource" select="/macro/datasource" />

<xsl:variable name="root" select="$currentPage/ancestor-or-self::root"/>
<xsl:variable name="widgetSettings" select="$root/*/Widgets/*[@id=$datasource]" />

<xsl:template match="/">

  <xsl:call-template name="renderWidget">
    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
	
  </xsl:call-template>
</xsl:template>

<xsl:template name="renderWidget">

	<xsl:param name="home"/>
    <xsl:variable name="default">
        <xsl:text>Dallas Ft. Worth</xsl:text>
    </xsl:variable>
    <xsl:variable name="cRegion">
        <xsl:choose>
            <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$default"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:if test="$widgetSettings/regionType = $cRegion or $widgetSettings/regionType = 'All' ">
        <div class="richTextWidget {$widgetSettings/cssClass}">
			<xsl:if test="$widgetSettings/title !=''">
				<h2><xsl:value-of select="$widgetSettings/title" /></h2>
			</xsl:if>
			<div class="richTextWidgetCopy">
				<xsl:value-of select="$widgetSettings/copy" disable-output-escaping="yes" />
				&nbsp;
			</div>
		</div>
    </xsl:if>
		
			
</xsl:template>


</xsl:stylesheet>