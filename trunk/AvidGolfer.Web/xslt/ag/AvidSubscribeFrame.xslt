﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">


    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <a class="fancySFrame" href="https://subscriptions.myavidgolfer.com/" />
    </xsl:template>

</xsl:stylesheet>

