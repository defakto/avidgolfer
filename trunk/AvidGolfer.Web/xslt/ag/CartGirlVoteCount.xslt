﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:girlVoteCount="urn:girlVoteCount"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib girlVoteCount js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
          
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
        ]]>
    </msxml:script>

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderDetermineIssueYear">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template name="renderDetermineIssueYear">
        <xsl:param name="home"/>

        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>

        <xsl:choose>
            <xsl:when test="count($home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue) = 0">
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year) - 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template name="renderCartGirlList">
        <xsl:param name="home" />
        <xsl:param name="year" />
        <xsl:variable name="yearList" select="$home/AGIssuesArea/YearFolder" />
        <xsl:variable name="regionList" select="$home/SettingsFolder/RegionType/* [title != 'All']" />
        <xsl:variable name="vote" select="girlVoteCount:createCartGirlList()" />

        <xsl:variable name="dRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestQueryString('gregion') != ''">
                    <xsl:value-of select="umbraco.library:RequestQueryString('gregion')" />
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="dYear">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestQueryString('year') != ''">
                    <xsl:value-of select="umbraco.library:RequestQueryString('year')" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$year"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="cartGirls">
            <xsl:choose>
                <xsl:when test="$dRegion != '' and $dYear != ''">
                    <xsl:copy-of select="$home/AGIssuesArea/YearFolder/Issue/CartGirlProfile [(regionType = $dRegion or regionType = 'All') and year = $dYear]" />
                </xsl:when>
                <xsl:when test="$dRegion != '' and $dYear = ''">
                    <xsl:copy-of select="$home/AGIssuesArea/YearFolder/Issue/CartGirlProfile [regionType = $dRegion or regionType = 'All']" />
                </xsl:when>
                <xsl:when test="$dRegion = '' and $dYear != ''">
                    <xsl:copy-of select="$home/AGIssuesArea/YearFolder/Issue/CartGirlProfile [year = $dYear]" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="$home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue/CartGirlProfile"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <div class="cartGirlAdmin">
            <h3>
                Cart Girl Vote &nbsp;
                <xsl:value-of select="$dYear"/>&nbsp;
                <xsl:value-of select="$dRegion"/>
                <br />
                
            </h3>
            <div class="voteDropdown">
                <div class="dYear">
                    YEAR<br />
                    <select name="yearSelect" id="yearSelect" onchange="javascript:girlDropdownChange()">
                        <option value="">Select a year</option>
                        <xsl:if test="count($yearList) &gt; 0">
                            <xsl:for-each select="$yearList">
                                <option value="{./@nodeName}">
                                    <xsl:value-of select="./@nodeName" />
                                </option>
                            </xsl:for-each>
                        </xsl:if>
                    </select>
                </div>
                <div class="dRegion">
                    REGION<br />
                    <select name="regionSelect" id="girlRegion" onchange="javascript:girlDropdownChange()">
                        <option value="">Select a region</option>
                        <xsl:if test="count($regionList) &gt; 0">
                            <xsl:for-each select="$regionList">
                                <option value="{./title}">
                                    <xsl:value-of select="./title" />
                                </option>
                            </xsl:for-each>
                        </xsl:if>
                    </select>
                </div>
            </div>
            <xsl:if test="count(msxml:node-set($cartGirls)/CartGirlProfile) &gt; 0">
            <table class="cartGirlAdmin">
                <tr>
                    <th>
                       Node Id 
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Region
                    </th>
                    <th>
                        Total Votes
                    </th>
                </tr>
                    <xsl:for-each select="msxml:node-set($cartGirls)/CartGirlProfile">
                        <xsl:sort select="./regionType"/>
                        <xsl:variable name="id" select="./@id" />
                        <tr>
                            <td>
                                <xsl:value-of select="./@id"/>
                            </td>
                            <td>
                                <xsl:value-of select="title" />
                            </td>
                            <td>
                                <xsl:value-of select="./regionType"/>
                            </td>
                            <td>
                                <xsl:if test="count($vote) &gt; 0">
                                    <xsl:for-each select="$vote//girl">
                                        
                                        <xsl:if test="$id = nodeId">
                                            <xsl:value-of select="voteCount"/>
                                        </xsl:if>
                                    </xsl:for-each>
                                </xsl:if>
                            </td>
                        </tr>
                    </xsl:for-each>
            </table>
            </xsl:if>
        </div>
    </xsl:template>
   
</xsl:stylesheet>
