<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:avidHitCount="urn:avidHitCount"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib avidHitCount js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
            
        ]]>
    </msxml:script>

	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:call-template name="renderMostViewedArticles">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="renderMostViewedArticles">
		<xsl:param name="home"/>
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>

        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="items" select="avidHitCount:createMostViewedList($cRegion)" />
        
		<div class="most_viewed">
			<h1>Most Viewed Articles</h1>
			<ul class="top_five">
                <xsl:if test="count($items) &gt; 0">
                    <xsl:for-each select="$items//article">
                        <xsl:variable name="article" select="umbraco.library:GetXmlNodeById(articleId)" />
                        <!--<xsl:variable name="url" select="umbraco.library:GetMedia($article/thumbnail, false)/umbracoFile" />-->
                        <xsl:variable name="articleId" select="$article/id" />
                        <xsl:variable name="page" select='umbraco.library:NiceUrl(articleId)' />
                        <li>
                            <a href="{$page}">
								<xsl:choose>
									<xsl:when test="$article/thumbnail !=''">
										<img src="{umbraco.library:GetMedia($article/thumbnail, false)/umbracoFile}" width="108" height="83" />
									</xsl:when>
									<xsl:otherwise>
										<img src="/images/blank.png" width="108" height="83" />
									</xsl:otherwise>
								</xsl:choose>
								<br />
								<xsl:value-of select="$article/title"/>
							</a>
                        </li>
                    </xsl:for-each>
                </xsl:if>
			</ul>
		</div>
	</xsl:template>
</xsl:stylesheet>