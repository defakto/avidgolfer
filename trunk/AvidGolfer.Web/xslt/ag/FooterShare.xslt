<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">


	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:call-template name="renderSMShare">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="renderSMShare">
		<xsl:param name="home"/> 

		<div class="footer_share">
			<xsl:if test="$home/youTubeLink != ''"><a href="{$home/youTubeLink}" target="_blank"><img src="/images/footer_yt_share.png" width="32" height="32" /></a></xsl:if>
			<xsl:if test="$home/twitterLink != ''"><a href="{$home/twitterLink}" target="_blank"><img src="/images/footer_twitter_share.png" width="32" height="32" /></a></xsl:if>
			<xsl:if test="$home/facebookLink != ''"><a href="{$home/facebookLink}" target="_blank"><img src="/images/footer_fb_share.png" width="32" height="32" /></a></xsl:if>
		</div>
		
	</xsl:template>
		  

</xsl:stylesheet>