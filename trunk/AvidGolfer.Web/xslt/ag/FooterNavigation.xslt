<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">


	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<div id="fHome" class="footer_logo">
			<a href="/" ><img src="/images/clear.gif" width="84" height="50" /></a>
		</div>
		
		<div class="footer_links">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/subscribe">Subscribe</a></li>
				<li><a href="/cart-girls">Cart Girls</a></li>
				<li><a href="/blog">Blog</a></li>
				<li><a href="/courses">Courses</a></li>
				<li><a href="/instruction">Instruction</a></li>
				<!--<li><a href="#">Magazine</a></li>-->
			</ul>
			<ul>
				<li><a href="/about">About</a></li>
				<li><a href="/contact-us">Contact Us</a></li>
				<li><a href="/tech-support">Tech Support</a></li>
				<li><a href="/advertise">Advertise</a></li>
				<li><a href="/legal">Legal</a></li>
                <xsl:if test="umbraco.library:IsLoggedOn()">
                    <li>
                        <a href="/digital-archive">Digital Archives</a>
                    </li>
                </xsl:if>
			</ul>
		</div>	
		
	
	</xsl:template>

</xsl:stylesheet>