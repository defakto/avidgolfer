<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom:javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
        ]]>
    </msxml:script>

	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:call-template name="renderAdBanners">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="renderAdBanners">
		<xsl:param name="home"/>
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>

        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="adverts" select="$home/SettingsFolder/AdBanner/*" />
        <xsl:choose>
            <xsl:when test="./regionType = 'All' or ./regionType = $cRegion and ./adPosition = 'Left'">
                
            </xsl:when>
        </xsl:choose>
        <xsl:for-each select="$adverts">
                <xsl:if test="./regionType = 'All' or ./regionType = $cRegion and ./adPosition = 'Left'">
                    <div class="ad_left">
                    <xsl:variable name="url" select="./adLink" />
                    <a href="{$url}" target="_blank">
                        <img src="{umbraco.library:GetMedia(./adImage, 0)/umbracoFile}" width="160" height="600" />
                    </a>
                   </div> 
                </xsl:if>
            <xsl:if test="./regionType = 'All' or ./regionType = $cRegion and ./adPosition = 'Right'">
               <div class="ad_right">
                <xsl:variable name="url" select="./adLink" />
                <a href="{$url}" target="_blank">
                    <img src="{umbraco.library:GetMedia(./adImage, 0)/umbracoFile}" width="160" height="600" />
                </a>
              </div>      
            </xsl:if>
		
        </xsl:for-each>
		
		
	</xsl:template>
		  

</xsl:stylesheet>