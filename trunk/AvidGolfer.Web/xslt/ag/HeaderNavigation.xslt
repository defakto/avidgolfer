<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:avidHitCount="urn:avidHitCount"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library avidHitCount tagsLib js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
        ]]>
    </msxml:script>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>
    <xsl:template match="/">
        <xsl:call-template name="renderDetermineIssueYear">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="renderDetermineIssueYear">
        <xsl:param name="home"/>
        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>

        <xsl:choose>
            <xsl:when test="count($home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue) = 0">
                <xsl:call-template name="renderMainNavigation">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year) - 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="renderMainNavigation">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    
	<xsl:template name="renderMainNavigation">
        <xsl:param name="home"/>
        <xsl:param name="year"/>
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>

        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="cMonth" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'MMMM')" />
        <xsl:variable name="check" select="$currentPage/ancestor-or-self::umbHomepage/enableVotingOnSite" />
        <xsl:if test="umbraco.library:IsLoggedOn()">
            <div id="digitalLink">
                <a href="/digital-archive.aspx">View Digital Archives</a>
            </div>
        </xsl:if>
				<ul class="menu sf-menu">
					<li class="subscribe">
						<a href="/subscribe.aspx">Subscribe</a>
					</li>
						<xsl:variable name="cGirl" select="$home/AGIssuesArea/YearFolder[@nodeName=$year]/descendant::Issue/CartGirlProfile [regionType = $cRegion or regionType = 'All']" />
						<xsl:for-each select="$cGirl">
                            <xsl:choose>
                                <xsl:when test="position() = 1">
                                    <li class="cart_girls">
                                        <a href="{umbraco.library:NiceUrl(./@id)}">Cart Girls</a>
                                     </li>      
                                </xsl:when>
                            </xsl:choose>   
						</xsl:for-each>
					<li class="blog">
						<a href="/blog.aspx">Blog</a>
					</li>
					<li class="courses">
						<a href="/courses.aspx">Courses</a>
					</li>
					
						<xsl:variable name="iArticle"  select="$home/AGIssuesArea/YearFolder[@nodeName=$year]/descendant::Issue/IssueArticle [(regionType = $cRegion or regionType = 'All') and articleType = 'Instruction']" />
                        <xsl:for-each select="$iArticle">
                            <xsl:choose>
                                <xsl:when test="position() = 1">
                                    <li class="instructions">
                                         <a href="{umbraco.library:NiceUrl(./@id)}">Instruction</a>
                                    </li>    
                                </xsl:when>
                            </xsl:choose>
						</xsl:for-each>
                    <xsl:variable name="iDeLink"  select="$home/SettingsFolder/DigitalEditionLinks/descendant::DigitalEditionLink [(regionType = $cRegion)]" />
                    <xsl:for-each select="$iDeLink">
                        <xsl:choose>
                            <xsl:when test="$iDeLink != ''">
                                <xsl:if test="position() = 1">
                                    <li class="magazine">
                                        <a href="{./link}" target="_blank">Magazine</a>
                                    </li>
                                </xsl:if>
                            </xsl:when>
                            <xsl:otherwise>
                                <li class="magazine">
                                    <a href="/subscribe.aspx">Magazine</a>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                    <!--<xsl:choose>
                        <xsl:when test="umbraco.library:IsLoggedOn()">
                        <xsl:variable name="user" select="umbraco.library:GetCurrentMember()/@id" />
                       <xsl:variable name="currentSubscription" select="avidHitCount:CurrentSubscription($user, $cRegion)" />
                            <xsl:choose>
                                <xsl:when test="$currentSubscription = 'True'">
                                    <xsl:variable name="iDeLink"  select="$home/SettingsFolder/DigitalEditionLinks/descendant::DigitalEditionLink [(regionType = $cRegion)]" />
                                    <xsl:for-each select="$iDeLink">
                                    <xsl:choose>
                                        <xsl:when test="$iDeLink != ''">
                                            <xsl:if test="position() = 1">
                                                <li class="magazine">
                                                    <a href="{./link}" target="_blank">Magazine</a>
                                                </li>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <li class="magazine">
                                                <a href="/subscribe.aspx">Magazine</a>
                                            </li>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                  </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <li class="magazine">
                                        <a href="/subscribe.aspx">Magazine</a>
                                    </li>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <li class="magazine">
                                <a href="/subscribe.aspx">Magazine</a>
                            </li>
                        </xsl:otherwise>
                    </xsl:choose>-->
                    <!--<xsl:if test="umbraco.library:IsLoggedOn()">
                       <xsl:variable name="user" select="umbraco.library:GetCurrentMember()/@id" />
                       <xsl:variable name="currentSubscription" select="avidHitCount:CurrentSubscription($user, $cRegion)" />
                        <xsl:if test="$currentSubscription = 'True'">
					<xsl:variable name="iDeLink"  select="$home/SettingsFolder/DigitalEditionLinks/descendant::DigitalEditionLink [(regionType = $cRegion)]" />
					<xsl:for-each select="$iDeLink">
						<xsl:if test="position() = 1">
							<li class="magazine">
								<a href="{./link}" target="_blank">Magazine</a>						
							</li>
						</xsl:if>
					</xsl:for-each>
                        </xsl:if>
                    </xsl:if>-->
				</ul>
	</xsl:template>

</xsl:stylesheet>