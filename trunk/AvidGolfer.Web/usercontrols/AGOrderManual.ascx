﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AGOrderManual.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.AGOrderManual" %>

<script type="text/javascript">
    $(document).ready(function () {
        $("#<%= txtOrderDate.ClientID %>").datepicker();
        $("#<%= txtStart.ClientID %>").datepicker();
        $("#<%= txtEnd.ClientID %>").datepicker();

    });
</script>
<asp:Literal ID="ltMessage" runat="server" Visible="false" />
<asp:Panel ID="pnlNewSubscriber" runat="server" DefaultButton="btnNewUser" CssClass="newOrderContainer">
	<style>
		.left_content_middle{
			width:100% !important;
		}
		.article{
			width:100% !important;
		}
		.newOrderContainer, .newOrderContainer #newOrder{
			width:100%;
		}
	</style>
   <div id="newOrder">
        <asp:ValidationSummary ID="validateOrder" runat="server" HeaderText="The following errors occured:" 
              ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" ValidationGroup="newOrder"  ForeColor="Red" />
        <asp:HiddenField ID="hdnId" runat="server" Value="0" />
        <h1>Create a New Order</h1>
     <ul class="neworder listleft">
           <li>
                First Name:<br />
                <asp:TextBox ID="txtFirstName" runat="server" />
                 <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="txtFirstName"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a first name."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
            </li>
             <li>
                Last Name:<br />
                <asp:Textbox ID="txtLastName" runat="server" />
                 <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtLastName"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a last name."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
            </li>
            <li>
                Username:<br />
                <asp:TextBox ID="txtUsername" runat="server" />
                 <asp:RequiredFieldValidator ID="reqUsername" runat="server" ControlToValidate="txtUsername"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include an user name."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
            </li>
            <li id="liPassword" runat="server">
                Password:<br />
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" />
                <asp:RequiredFieldValidator ID="reqPassword" runat="server" 
                        ControlToValidate="txtPassword" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="Password is required." ToolTip="Password is required." 
                        ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassword" runat="server" 
                        ControlToValidate="txtPassword" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="Invalid Password, use the guidlines provided below." 
                        ValidationExpression="(?=.*\d).{7,16}" ValidationGroup="newOrder" />
                    <br />
                    <br />
                    <sup class="subsuptext"><span>Minimum of &nbsp;
                    <%= Membership.MinRequiredPasswordLength %> &nbsp; characters in length, containing:
                    </span>
                    <br />
                    <span>at least one number </span>

            </li>
            <li id="liConfirm" runat="server">
                Confirm Password:<br />
                <asp:TextBox ID="txtConfirm" runat="server" TextMode="Password" />
                 <asp:RequiredFieldValidator ID="rfvPasswordConfirm" runat="server" 
                        ControlToValidate="txtConfirm" CssClass="failurenotification" 
                        Display="Dynamic" ErrorMessage="Confirm Password is required." 
                        ToolTip="Confirm Password is required." ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvPassword" runat="server" 
                        ControlToCompare="txtPassword" ControlToValidate="txtConfirm" 
                        CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="The Password and Confirmation Password must match." 
                        ValidationGroup="newOrder">*</asp:CompareValidator>
            </li>
             <li>
                Address:<br />
                <asp:Textbox ID="txtAddress" runat="server" Columns="50" />
                 <asp:RequiredFieldValidator ID="reqAddress" runat="server" ControlToValidate="txtAddress"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include an address."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
            </li>
            <li>
                City:<br />
                <asp:TextBox ID="txtCity" runat="server" />
                 <asp:RequiredFieldValidator ID="reqCity" runat="server" ControlToValidate="txtCity"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a city."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
            </li>
            <li>
                State:<br />
                <asp:DropDownList ID="ddlState" runat="server" />
            </li>
            <li>
                Zip Code:<br />
                <asp:TextBox ID="txtZip" runat="server" />
                <asp:RequiredFieldValidator ID="reqZip" runat="server" ControlToValidate="txtZip"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a zip code"
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="reqZipFormat" runat="server" CssClass="failurenotification" 
                    Display="Dynamic" ErrorMessage="Please include a zip code" ValidationGroup="newOrder"
                    ValidationExpression="[0-9]{5}" ControlToValidate="txtZip"></asp:RegularExpressionValidator>
            </li>
            <li>
                Email:<br />
                <asp:TextBox ID="txtEmail" runat="server" Columns="40" />
                <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a email address."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="reqEmailFormat" runat="server" CssClass="failurenotification"
                    Display="Dynamic" ControlToValidate="txtEmail" ValidationGroup="newOrder" 
                    ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$">*
               </asp:RegularExpressionValidator>
            </li>
             <li>
                Phone:<br />
                <asp:TextBox ID="txtPhone" runat="server" />
                <asp:RequiredFieldValidator ID="reqPhone" runat="server" ControlToValidate="txtPhone"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a phone number."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regPhone" runat="server" Display="Dynamic" ErrorMessage="Please use numbers only."
                    CssClass="failurenotification" ValidationExpression="^[0-9]{10}$" ControlToValidate="txtPhone"
                    ValidationGroup="newOrder">*</asp:RegularExpressionValidator>
                 <br />
                 <div class="subsuptext">Please use numbers only thank you.</div>
            </li>
            <li>
                <asp:Button ID="btnNewUser" runat="server" Text="Submit" 
                    onclick="btnNewUser_Click" />
            </li>
     </ul>
     <ul class="neworder listleft">
         <li>
            Subscription Source:<br />
            <asp:DropDownList ID="ddlOrderType" runat="server" />
            <asp:RequiredFieldValidator ID="reqOrderType" runat="server" ControlToValidate="ddlOrderType"
                CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please select an order type." 
                ValidationGroup="newOrder" InitialValue="0">*</asp:RequiredFieldValidator>
          </li>
          <li>
            Order Date:<br />
            <asp:TextBox ID="txtOrderDate" runat="server" />
            <asp:RequiredFieldValidator ID="reqOrderDate" runat="server" ControlToValidate="txtOrderDate"   
                CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include an order date."
                ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
          </li>
          <li>
            Subscription Package:<br />
            <asp:DropDownList ID="ddlPackage" runat="server" />
                <asp:RequiredFieldValidator ID="reqPackage" runat="server" ControlToValidate="ddlPackage"   
                CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include the package."
                ValidationGroup="newOrder" InitialValue="0">*</asp:RequiredFieldValidator>
          </li>
           <li>
                Number of Copies:<br />
                <asp:TextBox ID="txtCopies" runat="server" Columns="2"  />
                 <asp:RequiredFieldValidator ID="reqCopies" runat="server" ControlToValidate="txtCopies"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include the number of subscriptions."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regCopies" runat="server" CssClass="failurenotification"
                    Display="Dynamic" ControlToValidate="txtCopies" ValidationGroup="newOrder" 
                    ValidationExpression="^[0-9]+$">*
               </asp:RegularExpressionValidator>
            </li>
            <li>
                Start Date:<br />
                <asp:TextBox ID="txtStart" runat="server" />
                 <asp:RequiredFieldValidator ID="reqsDate" runat="server" ControlToValidate="txtStart"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a start date."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
            </li>
            <li>
                End Date:<br />
                <asp:TextBox ID="txtEnd" runat="server" />
                 <asp:RequiredFieldValidator ID="reqeDate" runat="server" ControlToValidate="txtEnd"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include an end date."
                    ValidationGroup="newOrder">*</asp:RequiredFieldValidator>
            </li>
            <li>
                Order Status:<br />
                <asp:DropDownList ID="ddlStatus" runat="server" />
                 <asp:RequiredFieldValidator ID="reqStatus" runat="server" ControlToValidate="ddlStatus"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please select an order status."
                    ValidationGroup="newOrder" InitialValue="0">*</asp:RequiredFieldValidator>
            </li>
     </ul>
	 <div class="clear"></div>
   </div>
</asp:Panel>

