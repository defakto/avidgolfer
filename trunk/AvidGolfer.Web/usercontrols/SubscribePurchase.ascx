﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscribePurchase.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.SubscribePurchase" %>
<script type="text/javascript">
    function disableButtons() {
      //  $("#<%= buttonPurchase.ClientID %>").attr("disabled", true);
        
        $("#<%= buttonPurchase.ClientID %>").css("display", "none");
        $("#process").css({ "display": "inline", "height": "50px" });
        $("#btnFake").attr("disabled", true);
        $("#<%= buttonBack.ClientID %>").attr("disabled", true);
        $("#<%= buttonCancel.ClientID %>").attr("disabled", true);
        return true;
    }
</script>
<asp:Panel ID="panelPurchase" runat="server">
<div id="divPage" class="purchasePage">
    
	<h1>
		Subscription Summary
	</h1>
    <div class="purchasedivdefault">
        <div class="purchasedivpkgleft">
       
           <%-- <asp:Image ID="imageProduct" runat="server" CssClass="purchaseimage" />--%>
        </div>
        <div class="purchasedivpkgright">
            <table>
            <asp:Repeater ID="rptOrder" runat="server">
                <ItemTemplate>
                      <tr>
                        <td class="purchasetdleft">
                            Product:
                        </td>
                        <td class="purchasetdwidth">
                        </td>
                        <td class="purchasetdright">
                            <asp:Literal ID="literalProduct" runat="server" Text='<%# Eval("product") %>' ></asp:Literal>
                        </td>
                    </tr>
                     <tr>
                        <td class="purchasetdleft">
                            Description:
                        </td>
                        <td>
                        </td>
                        <td class="purchasetdright">
                            <asp:Literal ID="literalDescription" runat="server" Text='<%#Eval("description") %>' ></asp:Literal>
                        </td>
                    </tr>
                    <!--
                     <tr>
                        <td class="purchasetdleft">
                            End Date:
                        </td>
                        <td>
                        </td>
                        <td class="purchasetdright">
                            <asp:Literal ID="literalEndDate" runat="server">@enddate</asp:Literal>
                        </td>
                    </tr>
                    -->
                     <tr>
                        <td class="purchasetdleft">
                            <asp:Literal ID="literalPriceCaption" runat="server">Price:</asp:Literal>
                        </td>
                        <td>
                        </td>
                        <td class="purchasetdright">
                           $<asp:Literal ID="literalPrice" runat="server" Text='<%# Eval("price") %>'>@price</asp:Literal>
                        </td>
                    </tr>
                    
                    <!--
                     <tr>
                        <td class="purchasetdleft">
                            <asp:Literal ID="literalTaxCaption" runat="server">Tax:</asp:Literal>
                        </td>
                        <td>
                        </td>
                        <td class="purchasetdul">
                            <asp:Literal ID="literalTax" runat="server">0.00</asp:Literal>
                        </td>
                    </tr>
                    -->
                     <tr>
                        <td class="purchasetdleft">
                            Subtotal:
                        </td>
                        <td>
                        </td>
                        <td class="purchasetdright">
                            $<asp:Literal ID="literalTotal" runat="server" Text='<%# Eval("subtotal") %>'></asp:Literal>
                        </td>
                         <tr>
                        <td class="purchasetdleft">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                        <td class="purchasetdul">
                            &nbsp;
                        </td>
                    </tr>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td class="purchasetdleft">
                    Total:
                </td>
                <td>
                </td>
                <td class="purchasetdright">
                    $<asp:Literal ID="ltlTotal" runat="server" /> 
                </td>
            </tr>
            </table>
        </div>
    </div>

    <div class="purchasedivdefault">
        <ul class="nobulletlist">
            <li class="padbottom">
                Credit Card No:
                &nbsp;&nbsp;
                <asp:Literal ID="literalCCN" runat="server"></asp:Literal> <!-- xxxxXXXXxxxx1234    -->
            </li>
            <li class="padbottom">
                Expiration:
                &nbsp;&nbsp;
                <asp:Literal ID="literalExpire" runat="server"></asp:Literal> <!-- MM YYYY    -->
            </li>
            <li>
                CCV:
                &nbsp;&nbsp;
                <asp:Literal ID="literalCCV" runat="server"></asp:Literal> <!-- XXX    -->
            </li>
        </ul>
    </div>

    <div class="purchasedivdefault">
        <table>
            
            <tr>
                <td class="subscribehead">
                    Billing Information
                </td>
                <td class="purchasetdwidthsmall">
                </td>
                <td class="subscribehead">
                    Shipping Information
                </td>
            </tr>
            <tr>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalName" runat="server" >@name</asp:Literal> <!-- First Last Name  -->
                </td>
                <td>
                </td>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalNameS" runat="server" >@name</asp:Literal> <!-- First Last Name  -->
                </td>
            </tr>
            <tr>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalAddress" runat="server" >@address</asp:Literal> <!-- Address  -->
                </td>
                <td>
                </td>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalAddressS" runat="server" >@address</asp:Literal> <!-- Address  -->
                </td>
            </tr>
            <tr>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalLocation" runat="server" >City, State  Zip-zip4</asp:Literal> <!-- City, State  Zip-zip4  -->
                </td>
                <td>
                </td>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalLocationS" runat="server" >@City, State  Zip-zip4</asp:Literal> <!-- City, State  -->
                </td>
            </tr>
            <tr>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalContact" runat="server" >Contact:</asp:Literal> <!-- email  -->
                </td>
                <td>
                </td>
                <td>
                    <asp:Literal ID="literalContactS" runat="server" >Contact:</asp:Literal> <!-- email  -->
                </td>
            </tr>
            <tr>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalEMail" runat="server" >yourname@someplace.com</asp:Literal> <!-- email  -->
                </td>
                <td>
                </td>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalEMailS" runat="server" >yourname@someplace.com</asp:Literal> <!-- email  -->
                </td>
            </tr>
            <tr>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalPhone" runat="server" >xxx.xxx.xxxx</asp:Literal> <!-- phone  -->
                </td>
                <td>
                </td>
                <td class="purchasetdleft">
                    <asp:Literal ID="literalPhoneS" runat="server" >xxx.xxx.xxxx</asp:Literal> <!-- phone  -->
                </td>
            </tr>
        </table>
    </div>

        <div class="divmessage">
            <asp:UpdatePanel ID="upMessage" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelMessage" runat="server" >
                    <span id="labelmessages" runat="server" class="labelmessage"></span>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="buttonPurchase" />
            </Triggers>
            </asp:UpdatePanel>
        </div>

    <div ID="tdSubscribecommand" class="purchasecommand">
        <span >
            <asp:Button ID="buttonBack" runat="server" CssClass="buttondefault" 
                Text="Back" onclick="buttonBack_Click"  />
            <span id="process" style="display: none;"><input id="btnFake" type="button" value="Processing" 
                    class="buttondefault"  /></span>
            <asp:Button ID="buttonPurchase" runat="server" CssClass="buttondefault" 
                Text="Purchase" onclick="buttonPurchase_Click"  />
            <asp:Button ID="buttonCancel" runat="server" CssClass="buttondefaultleft" 
                Text="Cancel" onclick="buttonCancel_Click"  />
        </span>
    </div>

</div>
</asp:Panel>
