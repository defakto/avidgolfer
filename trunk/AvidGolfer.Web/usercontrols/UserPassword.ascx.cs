﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AvidGolfer.Components;

namespace AvidGolfer.Web.usercontrols
{
    public partial class UserPassword : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        protected void buttonPassword_Click(object sender, EventArgs e)
        {
            PasswordChange();
        }

        private void PasswordChange()
        {
            string message = string.Empty;
            lblErr.Visible = false;

            using (IMembershipManager member = new MembershipManager())
            {
                if (member.PasswordChange(tbPasswordOld.Text, tbPassword.Text))
                    Response.Redirect("~/Login.aspx?page=confirmation&panel=password");
                else
                {
                    lblErr.Text = "Password change failed.";
                    lblErr.Visible = true;
                }
            }
        }

    }
}