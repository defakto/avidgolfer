﻿using System;
using System.Web.UI;

using AvidGolfer.Components;
using AvidGolfer.Entities;
using Defakto.Components;

namespace AvidGolfer.Web.usercontrols
{
    public partial class Subscription : System.Web.UI.UserControl
    {
        private string lastpage
        {
            get
            {
                return "" + Session["LastASCX"] as string;
            }
            set
            {
                Session["LastASCX"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ControlLoad();
        }

        void ControlLoad()
        {
            string page = "subscribe";

            if (!string.IsNullOrEmpty(Request.QueryString["page"]))
                page = Request.QueryString["page"].Trim().ToLower();
            else
                if (!string.IsNullOrEmpty(lastpage))
                    page = lastpage;

            using (IUtilities p = new Utilities())
                page = p.PageURL(page);

            placeholderASCX.Controls.Clear();
            UserControl usercontrol = (UserControl)LoadControl(page);

            placeholderASCX.Controls.Add(usercontrol);

            lastpage = page;
        }
    }
}