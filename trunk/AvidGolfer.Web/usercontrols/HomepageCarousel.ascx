﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomepageCarousel.ascx.cs" Inherits="HomepageCarousel.HomepageCarousel" %>
        <div id="images" class="main_image_scroll">
			<div id="imageScroll" class="main_image">
				<img src="/images/main_image_3.png" width="1600" height="400" />
				<img src="/images/caliberb.jpg" width="1600" height="400" />
				<img src="/images/catb.jpg" width="1600" height="400" />
				<img src="/images/slipb.jpg" width="1600" height="400" />
				<img src="/images/bearb.jpg" width="1600" height="400" />
			</div> 
			<div class="scroll">
				<div id="prev" class="left_scroll thumb">
					<img src="/images/left_scroll.png" width="13" height="60" />
				</div>
				<div >
					<ul id="thumbs" class="scroll_options">
						<li class="thumb"><a href="#"><span class="scroll_number">1</span><img src="/images/passbook_scroll.png" width="80" height="80" align="center" />Get the Passbook</a></li>
						<li class="thumb"><a href="#"><span class="scroll_number">2</span><img src="/images/girl_scroll.png" width="80" height="80" align="left" />Cart Girl of the<br /> Month: Stephanie<br /> Rashea Britt</a></li>
						<li class="thumb"><a href="#"><span class="scroll_number">4</span><img src="/images/banana_scroll.png" width="80" height="80" align="left" />Lose the<br /> Bannana Ball</a></li>
						<li class="thumb"><a href="#"><span class="scroll_number">5</span><img src="/images/goodfellas_scroll.png" width="80" height="80" align="left" />Goodfellas: <br /> The Celebrity<br /> Issue</a></li>
						<li class="thumb"><a href="#"><span class="scroll_number">6</span><img src="/images/bear_scroll.gif" width="80" height="80" align="left" />Bears: <br /> The Bear<br /> Issue</a></li>
					</ul>
				</div>
				<div id="next" class="right_scroll thumb">
					<img src="/images/right_scroll.png" width="13" height="60" />
				</div>
			</div>
		</div>
