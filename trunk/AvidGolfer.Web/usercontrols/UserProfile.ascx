﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserProfile" %>

<asp:Panel ID="panelProfile" runat="server">
<div ><%--id="divPage"--%>

    <div> <%--class="padleft"--%>

        <h4>Update / Create User Profile</h4>

        <p class="paratext">
            Use the form below to Update/create your user profile.
        </p>


        <asp:Panel ID="panelProfileform" runat="server">

        <div class="paratextbold" >
            Account Information
            <asp:ValidationSummary ID="vsProfile" runat="server" CssClass="failurenotification" ForeColor=""                                        ValidationGroup="vgProfile"/>
        </div>

        <table class="profileInfo">
        <tr>
            <td>
               First Name
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbFirstName" runat="server" CssClass="textentry" TabIndex="2" /><br />
                  <asp:RequiredFieldValidator ID="rfvFirstName" runat="server"
                    ControlToValidate="tbFirstName"
                    CssClass="failurenotification"
                    Display="Dynamic" 
                    ErrorMessage="First Name Required"
                    Text="*"
                    ValidationGroup="vgProfile" />
            </td>
        </tr>
        <tr>
            <td>
                Last Name
                
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbLastName" runat="server" CssClass="textentrylarge" TabIndex="3" /><br />
                <asp:RequiredFieldValidator ID="rfvLastName" runat="server"
                    ControlToValidate="tbLastName"
                    CssClass="failurenotification"
                    Display="Dynamic" 
                    ErrorMessage="Last Name Required"
                    ValidationGroup="vgProfile">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Address
            </td>
        </tr> 
        <tr>
            <td>
                 <asp:TextBox ID="tbAddress" runat="server" CssClass="textentrylarge" TabIndex="6" /><br />
                 <asp:RequiredFieldValidator ID="rfvAddress" runat="server"
                    ControlToValidate="tbAddress"
                    CssClass="failurenotification"
                    Display="Dynamic" 
                    ErrorMessage="Address Required"
                    ValidationGroup="vgProfile">*</asp:RequiredFieldValidator>
            </td>
        </tr> 
        <tr>
            <td>
                Zip 
            </td>
            <td>
                Zip +4
            </td>
        </tr> 
        <tr>
            <td>
                <asp:TextBox ID="tbZip" runat="server" CssClass="textentrysmall" MaxLength="5" TabIndex="7" /><br /> 
                <asp:RequiredFieldValidator ID="rfvbZip" runat="server"
                        ControlToValidate="tbZip"
                        CssClass="failurenotification"
                        Display="Dynamic" 
                        ErrorMessage="Zip Required, 5 numeric"
                        ValidationGroup="vgProfile">*</asp:RequiredFieldValidator>

                <asp:RegularExpressionValidator ID="reZip" runat="server" 
                    ControlToValidate="tbZip"
                    CssClass="failurenotification"
                    Display="Dynamic" 
                    ErrorMessage="Invalid Zip, require 5 numeric"
                    ValidationExpression="[0-9]{5}" />
            </td>
            <td>
                    <asp:TextBox ID="tbZip4" runat="server" CssClass="textentrysmall" MaxLength="4"  TabIndex="8" /><br />
                    <asp:RegularExpressionValidator ID="reZip4" runat="server" 
                        ControlToValidate="tbZip4"
                        CssClass="failurenotification"
                        Display="Dynamic" 
                        ErrorMessage="Invalid Zip, require 4 numeric characters."
                        ValidationExpression="[0-9]{4}">*</asp:RegularExpressionValidator>

                </td>
        </tr>  
        <tr>
            <td>
                City
            </td>
        </tr>    
        <tr>
            <td>
                <asp:TextBox ID="tbCity" runat="server" CssClass="textentry" TabIndex="9" /><br />
                <asp:RequiredFieldValidator ID="rfvCity" runat="server"
                    ControlToValidate="tbCity"
                    CssClass="failurenotification"
                    Display="Dynamic" 
                    ErrorMessage="City Required"
                    ValidationGroup="vgProfile">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                State
            </td>
        </tr>   
        <tr>
            <td>
                <asp:DropDownList ID="ddlState" runat="server" TabIndex="10" /><br />
                <asp:RequiredFieldValidator ID="rfvState" runat="server" 
                    ControlToValidate="ddlState" 
                    CssClass="failurenotification" 
                    Display="Dynamic" 
                    ErrorMessage="State Required" 
                    ValidationGroup="vgProfile">*</asp:RequiredFieldValidator>
            </td>
        </tr>  
        <tr>
            <td>
                Phone
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbPhone" runat="server" 
                    CssClass="textentrymedium" 
                    MaxLength="14" 
                    TabIndex="11" />
                <asp:RequiredFieldValidator ID="reqPhone" runat="server" ControlToValidate="tbPhone"   
                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a phone number."
                    ValidationGroup="vgProfile">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regPhone" runat="server" Display="Dynamic" ErrorMessage="Please use numbers only."
                    CssClass="failurenotification" ValidationExpression="^[0-9]{10}$" ControlToValidate="tbPhone"
                    ValidationGroup="vgProfile">*</asp:RegularExpressionValidator>
                <br />
				<div class="subsuptext">Please use numbers only thank you</div>
            </td>
        </tr>      
        </table>
        </asp:Panel>

        <asp:Panel ID="panelProfilecommand" runat="server" CssClass="profilecommand" >
            <table>
                <tr>
                    <td>
                        <asp:Button ID="buttonProfile" runat="server" CssClass="buttondefault" 
                            TabIndex="12" Text="Save" ValidationGroup="vgProfile" onclick="buttonProfile_Click" 
                             />
                    </td>
                    <td class="padleft">
                        <asp:Button ID="buttonCancel" runat="server" CssClass="buttondefault" 
                            TabIndex="1" Text="Cancel" onclick="buttonCancel_Click"  />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>
    <!--
            <li class="profilepadbottom">Gender<br />
            <span>
                <asp:RadioButton ID="rbMale" runat="server" Checked="true" Text="Male" GroupName="gnGender" TabIndex="4" />
                &nbsp;&nbsp;
                <asp:RadioButton ID="rbFemale" runat="server" Text="Female" GroupName="gnGender" TabIndex="5" />
            </span>
            </li>        
            -->
</div>
</asp:Panel>