﻿using System;
using System.Collections.Generic;
using System.Linq;

using AvidGolfer.Components;
using AvidGolfer.Data;
using AvidGolfer.Entities;
using umbraco.cms.businesslogic.member;
using umbraco.cms.businesslogic.media;
using umbraco.presentation.nodeFactory;
using System.Web.UI.WebControls;

namespace AvidGolfer.Web.usercontrols
{
    public partial class Subscribe : System.Web.UI.UserControl
    {
        static string productid = "";
        static string quantity = "1";
        static string pageforward = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            // ToDo: what is the query string variable to be passed
            if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                productid = Request.QueryString["productid"].Trim();
            if (!Page.IsPostBack)
            {
                //Session["Quantity"] = 1;
                drQuantity.Items.Clear();
            }
            ProductLoad(productid);
        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(PageRedirectURL());
        }

        protected void buttonContinue_Click(object sender, EventArgs e)
        {
            Checkout();
        }

        protected void hlLogIn_Click(object sender, EventArgs e)
        {
            Login(pageforward);
        }

        void Checkout()
        {
            // user logged in?
            if (!Member.IsLoggedOn())
                Login(pageforward);

            pageforward = string.Format("~/Order.aspx?page=checkout&productid={0}&quantity={1}", productid, quantity);

            Response.Redirect(pageforward);
        }

        void Login(string page)
        {
            pageforward = string.Format("~/Order.aspx?page=checkout&productid={0}&quantity={1}", productid, quantity);
            Session["LoginForward"] = pageforward;
            //var login = string.Format("~/Login.aspx?page=login&productid={0}&quantity={1}", productid, quantity);
            var login = string.Format("~/Login.aspx?page=login&productid={0}", productid);
            Response.Redirect(login);
        }

        void ProductLoad(string _productid = "")
        {
            //// ToDo: test productid remove value on deploy
            //if (_productid.Equals(""))
            //    _productid = "20515";

            //// ToDo:  remove on deploy
            //pageforward = string.Format("~/Order.aspx?page=checkout&productid={0}", _productid);
            //pageforward = string.Format("~/Order.aspx?page=checkout&productid={0}&quantity={1}", _productid, quantity);

            int productid = 0;
            if (!Int32.TryParse(_productid, out productid))
                productid = 0;

            /*
            List<Product> products = new List<Product>();
            using (SupplementalContentManager db = new SupplementalContentManager())
                    products = db.ProductSelect(productid: productid);

            if (products.Count < 1)
                return;
            Product product = products.Where(p => p.productid.Equals(productid)).FirstOrDefault();
            */

            Node product = new Node(productid);
            if(product == null)
                Response.Redirect("/");
            
            

            //pageforward = string.Format("~/Order.aspx?page=checkout&productid={0}", _productid);

            if (product.GetProperty("subscriptionImage") != null)
            {
                try
                {
                    Media file = new Media(Convert.ToInt32(product.GetProperty("subscriptionImage").Value));
                    if (file != null)
                    {
                        string url = file.getProperty("umbracoFile").Value.ToString();
                        imageProduct.ImageUrl = url;
                        //imageProduct.ImageUrl = product.GetProperty("subscriptionImage").Value;   // ../images/pkg-dallas.png
                        imageProduct.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                imageProduct.Visible = false;
            }

            

            literalProduct.Text = (product.GetProperty("subscriptionName") != null)? product.GetProperty("subscriptionName").Value : String.Empty;
            literalDescription.Text = (product.GetProperty("briefDescription") != null)? product.GetProperty("briefDescription").Value : String.Empty;
            literalEndDate.Text = (product.GetProperty("endDate") != null)? product.GetProperty("endDate").Value : String.Empty;
            literalPrice.Text = (product.GetProperty("price") != null)? product.GetProperty("price").Value : String.Empty;

            int maxAllowed = Convert.ToInt32(product.GetProperty("maxQuantity").Value);

            for (int i = 1; i <= maxAllowed; i++)
            {
                ListItem item = new ListItem(i.ToString());
                drQuantity.Items.Add(item);
            }

            //Session["ProductID"] = _productid;
            //Session["Quantity"] = 1;
            quantity = "1";
            Session["Sku"] = product.GetProperty("subscriptionCode").Value;
            Session["ProductName"] = product.Name;
            string region = product.GetProperty("regionType").Value;
            region = region.Replace("<XPathCheckBoxList><nodeName>", "");
            region = region.Replace("</nodeName></XPathCheckBoxList>", "");
            Session["Region"] = region;
        }

        string PageRedirectURL(string action = "")
        {
            string url = "~/"; // default to home page

            try
            {
                if ((!Session[action].Equals(null)) && (((string)Session[action]).Length > 0))
                {
                    url = Session[action] as string;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;

            }

            return url;
        }

        protected void drQuantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["Quantity"] = drQuantity.SelectedValue;
            quantity = drQuantity.SelectedValue;
        }

    }
}