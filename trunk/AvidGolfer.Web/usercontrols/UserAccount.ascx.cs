﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using AvidGolfer.Components;
using AvidGolfer.Entities;
using umbraco.cms.businesslogic.member;

namespace AvidGolfer.Web.usercontrols
{
    public partial class UserAccount : System.Web.UI.UserControl
    {
        List<SelectList> status;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SubscriptonHistory();
            }
            
        }

        protected void lbPassword_Click(object sender, EventArgs e)
        {
            AccountRedirect(page: "password");

        }

        protected void lbProfile_Click(object sender, EventArgs e)
        {
            AccountRedirect(page: "profile");
        }

       protected void lbSubscribe_Click(object sender, EventArgs e)
        {
            AccountRedirect(control: "Login", page: "subscribe");
        }

        void AccountRedirect(string control = "Login", string page = "login")
        {
            Response.Redirect(string.Format("~/{0}.aspx?page={1}", control, page));
        }

        string PageRedirectURL(string action = "")
        {
            string url = "~/"; // default to home page
            string forward = string.Empty;

            try
            {
                forward = (string)Session[action] + "";
                if (!forward.Equals(""))
                {
                    url = forward;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            return url;
        }

        private void SubscriptonHistory()
        {
            Member member = Member.GetCurrentMember();
            try
            {
                if (member.Equals(null) || (member.Id < 1))
                    return;
            }
            catch (Exception ex)
            {
                return;
            }

            SupplementalContentManager scm = new SupplementalContentManager();

            List<OrderItemHistory> oih = scm.GetOrderHistoryByOptions(memberid: member.Id);

            if (oih.Count != 0)
            {
                gvOrderHistory.DataSource = oih;
                gvOrderHistory.DataBind();
            }
           

            // GetOrderHistoryByOptions

            // using (SupplementalContentManager scm = new SupplementalContentManager())
            //    divOrderHistory.Controls.Add(scm.OrderHistory(memberid: member.Id));

            //  ToDo: uncomment to set table styleon
            //try
            //{
            //    Member member = Member.GetCurrentMember();
            //    Table table = FindControl("divOrderHistory").FindControl("table" + member.Id.ToString()) as Table;
            //    if (!table.Equals(null))
            //        table.CssClass = "accountTable";
            //}
            //catch (Exception ex)
            //{
            //}

        }

        // ToDo: remove on deploy
        protected void lbAdmin_Click(object sender, EventArgs e)
        {
            AccountRedirect(control: "Login", page: "admin");
        }

    }
}