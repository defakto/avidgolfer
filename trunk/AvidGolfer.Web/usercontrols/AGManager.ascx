﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AGManager.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.AGManager" %>

<asp:Panel ID="panelAGManager" runat="server">

<div id="divPage" class="accountPage">

	<div class="subscribehead">
        <span>
		    Administrative Manager &nbsp;&nbsp;&nbsp;

            <asp:UpdatePanel ID="upMessage" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Literal ID="literalMessage" runat="server">Message</asp:Literal>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="buttonSearch" />
            </Triggers>
            </asp:UpdatePanel>
        </span>
	</div>

    <div class="admindivdefault">
        Search
        <br />
        <table>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlSearch" runat="server"
                        onselectedindexchanged="ddlSearch_SelectedIndexChanged">
		                <asp:ListItem Value="userid" Text="UserID"></asp:ListItem>
		                <asp:ListItem Value="email" Text="EMail Address"></asp:ListItem>
		                <asp:ListItem Value="orderid" Text="Order Number"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="purchasetdwidthmicro">
                </td>
                <td>
                    <asp:UpdatePanel ID="upSearchFor" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="tbSearch" runat="server" CssClass="textentrylarge">Search for</asp:TextBox>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlSearch" EventName="SelectedIndexChanged" />
                    </Triggers>
                    </asp:UpdatePanel>
                </td>
                 <td class="purchasetdwidthmicro">
                </td>
                <td>
                    <asp:Button ID="buttonSearch" runat="server" Text="Search" 
                        onclick="buttonSearch_Click" />
                </td>
            </tr>
        </table>

    </div>

    <div>
        <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Panel ID="panelSearch" runat="server">

                <div id="divOrderHistory" class="accountTablediv" runat="server">
                </div>

            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="buttonSearch" />
        </Triggers>
        </asp:UpdatePanel>

        <div class="checkoutcommand">
            <asp:Button ID="buttonSave" runat="server"   
                CssClass="buttondefault" 
                Text="Save" onclick="buttonSave_Click"
                Visible="false" />
        </div>
    </div>

</div>
</asp:Panel>
