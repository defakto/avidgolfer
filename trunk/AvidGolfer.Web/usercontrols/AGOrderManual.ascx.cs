﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvidGolfer.Components;
using AvidGolfer.Entities;
using AvidGolfer.Data;
using System.Text;
using umbraco.NodeFactory;
using System.Net;
using AuthorizeNet.Entities;
using umbraco.cms.businesslogic.member;
using System.Web.Security;

namespace AvidGolfer.Web.usercontrols
{
    public partial class AGOrderManual : System.Web.UI.UserControl
    {

        protected int orderStatusId = Convert.ToInt32(ConfigurationManager.AppSettings["orderStatusId"]);
        protected int orderTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["orderTypeId"]);
    
        List<SelectList> usstates;

        static string productid = "";
        static AuthorizeNetCustomer customer;

        protected void Page_Load(object sender, EventArgs e)
        {
            umbraco.BusinessLogic.User admin = umbraco.BusinessLogic.User.GetCurrent();

            if (admin == null)
            {
                Response.Redirect("/");
            }

            if (!Page.IsPostBack)
            {
                LoadOrderStatusDropdown();
                LoadOrderTypeDropdown();
                LoadSubscriptionType();
                USStates();

                string strUserId = "";
                if (Context.Request.QueryString["userId"] != null)
                    strUserId = Context.Request.QueryString["userId"];

                if (strUserId != "")
                    PrePopForm(strUserId);

                


            }

        }

        protected void PrePopForm(string strUserId)
        {
            int userId = Convert.ToInt32(strUserId);
            Member m = new Member(userId);

            if (m != null)
            {
                hdnId.Value = m.Id.ToString();
                txtFirstName.Text = m.getProperty("firstname").Value.ToString();
                txtLastName.Text = m.getProperty("lastname").Value.ToString();
                txtUsername.Text = m.LoginName;
                liConfirm.Visible = false;
                liPassword.Visible = false;
                txtAddress.Text = m.getProperty("address1").Value.ToString();
                txtCity.Text = m.getProperty("city").Value.ToString();
                ddlState.SelectedValue = m.getProperty("state").Value.ToString();
                txtZip.Text = m.getProperty("zip").Value.ToString();
                txtEmail.Text = m.Email;
                txtPhone.Text = m.getProperty("phone").Value.ToString();

            }

        }

        protected void LoadOrderTypeDropdown()
        {

            Node orderType = new Node(orderTypeId);

            Nodes childNodes = orderType.Children;

            foreach (Node node in childNodes)
            {
                ddlOrderType.Items.Add(new ListItem(node.GetProperty("title").Value));
            }

            ddlOrderType.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void LoadOrderStatusDropdown()
        {

            Node orderStatus = new Node(orderStatusId);

            Nodes childNodes = orderStatus.Children;

            foreach (Node node in childNodes)
            {
                ddlStatus.Items.Add(new ListItem(node.GetProperty("title").Value));
            }

            ddlStatus.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void LoadSubscriptionType()
        {
            int subTypeId = 1149;

            Node subType = new Node(subTypeId);

            Nodes childNodes = subType.Children;

            foreach (Node node in childNodes)
            {
                ddlPackage.Items.Add(new ListItem(node.GetProperty("subscriptionName").Value, node.Id.ToString()));
            }

            ddlPackage.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void USStates()
        {
            usstates = SessionList("USStates");
            ddlState = DDLBind(ddlState, usstates);
            ddlState.Width = new Unit("180px");

            List<SelectList> shipstates = usstates;

            ddlState = DDLBind(ddlState, shipstates);
            ddlState.Width = ddlState.Width; 
        }

       protected DropDownList DDLBind(DropDownList ddl, List<SelectList> list)
        {
            ddl.DataSource = list;
            ddl.DataTextField = "display";
            ddl.DataValueField = "value";
            ddl.DataBind();

            ddl.SelectedIndex = -1;

            return ddl;
        }

       protected List<SelectList> SessionList(string session)
       {
           bool isLoad = false;

           List<SelectList> list = Session[session] as List<SelectList>;
           try
           {
               if (!list.Equals(null))
                   isLoad = true;
           }
           catch (Exception ex)
           {
               string error = ex.Message;
               error = string.Empty;
           }

           if (!isLoad)
           {
               list = new List<SelectList>();
               using (SupplementalEntities se = new SupplementalEntities())
                   if (session.ToLower().IndexOf("month") > -1)
                       list = se.ListExpireMonth();
                   else if (session.ToLower().IndexOf("year") > -1)
                       list = se.ListExpireYear();
                   else if (session.ToLower().IndexOf("usstates") > -1)
                       list = se.USStates();
                   else
                   {
                   }

               Session[session] = list;
           }

           return list;
       }

       protected User CreateUser(string firstName, string lastName, string email, string address, string zip, string city,
          string state, string phone, string userName, string password)
       {
           SupplementalContentManager scm = new SupplementalContentManager();
           string message = string.Empty;
           //string password = System.Web.Security.Membership.GeneratePassword(7, 1);
           //string userName = firstName + "_" + lastName;
           User user = new User();

           using (IMembershipManager member = new MembershipManager())
           {
               message = member.RegisterUnique(userName, email);

               if (message.Equals(""))
               {
                   user.username = userName;
                   user.password = password;
                   user.email = email;
                   user.firstname = firstName;
                   user.lastname = lastName;
                   user.address1 = address;
                   user.city = city;
                   user.state = state;
                   user.Zip = zip;
                   user.phone = phone;
                   user.isMember = true;

                   member.RegisterUser(user);
                   user.userid = member.UserId(userName);
                   scm.CreateUser(user);
                   ProfileModify(user);
               }
               else if (message == "UserName")
               {
                   ltMessage.Text = "The Username is already taken.";
               }
               else
               {
                   ltMessage.Text = "The email is already taken.";
               }
           }

           return user;
       }

       public bool ProfileModify(User user)
       {
           bool isSuccess = false;

           try
           {

               //Member memInfo = GetMemberFromCache(currentMem.Id);
               Member member = Member.GetMemberFromEmail(user.email);

               user.userid = member.Id.ToString();
               user.username = member.LoginName;

               //if(user.email != "")
               //    member.getProperty("email").Value = user.email;

               member.getProperty("firstname").Value = user.firstname;
               member.getProperty("lastname").Value = user.lastname;

               // member.getProperty("gender").Value = user.gender;
               member.getProperty("phone").Value = user.phone;
               member.getProperty("address1").Value = user.address1;
               member.getProperty("address2").Value = user.address2;
               member.getProperty("city").Value = user.city;
               member.getProperty("state").Value = user.state;
               member.getProperty("zip").Value = user.Zip;
               member.getProperty("zip4").Value = user.zip4;

               //string ismember = "0";
               //if (user.isMember)
               string ismember = "1";
               member.getProperty("isMember").Value = ismember;

               member.Save();

               Member.AddMemberToCache(member);
             

               isSuccess = true;
           }
           catch (Exception ex)
           {
               string error = ex.Message;
           }

           return isSuccess;
       }

       protected void OrderCreate(Order order, int quantity, int packageId, DateTime endDate)
       {
           Node package = new Node(packageId);

           decimal price = Convert.ToDecimal(package.GetProperty("price").Value);
           decimal tax = Convert.ToDecimal(package.GetProperty("tax").Value);
           string productName = package.GetProperty("subscriptionName").Value;
           string region = package.GetProperty("regionType").Value;
           region = region.Replace("<XPathCheckBoxList><nodeName>", "");
           region = region.Replace("</nodeName></XPathCheckBoxList>", "");
           string sku = package.GetProperty("subscriptionCode").Value; 

           if (tax != 0)
           {
               order.shippingamount = ((price + (price * tax)) * quantity);
           }
           else
           {
               order.shippingamount = price * quantity;
           }

           for (int i = 1; i <= quantity; i++)
           {
               OrderItem item = new OrderItem();

               item.productid = packageId;
               item.productname = productName;
               item.itemid = 1;
               item.quantity = 1;
               item.price = price;
               item.tax = tax;
               item.total = ((item.price + (item.price * item.tax)) * item.quantity);
               item.createdate = order.createdate;
               item.expiredate = endDate;
               item.itemstatus = (int)OrderStatus.Open;
               item.subscriptionStatus = SubscriptionStatus.Active.ToString();

               if (i == 1)
               {
                   item.orderStatus = "Active";
                   item.startdate = order.createdate;
               }
               else
               {
                   item.orderStatus = "Purchased";
               }

               item.sku = sku;
               item.regionType = region;

               order.orderitems.Add(item);
           }

           using (SupplementalContentManager scm = new SupplementalContentManager())
               scm.OrderSave(order);

       }

       protected void btnNewUser_Click(object sender, EventArgs e)
       {
           Page.Validate("newOrder");
           if (Page.IsValid)
           {
               try
               {
                   SupplementalContentManager scm = new SupplementalContentManager();
                   User user = new User();
                   int hidden = Convert.ToInt32(hdnId.Value);
                   validateOrder.ShowSummary = false;
                   string orderType = ddlOrderType.SelectedValue;
                   //string package = ddlPackage.Text;
                   int packageId = Convert.ToInt32(ddlPackage.SelectedValue);
                   DateTime orderDate = Convert.ToDateTime(txtOrderDate.Text);
                   //string subscriptionPackage = ddlPackage.SelectedValue;
                   string firstName = txtFirstName.Text;
                   string lastName = txtLastName.Text;
                   string userName = txtUsername.Text;
                   string password = txtPassword.Text;
                   string address = txtAddress.Text;
                   string city = txtCity.Text;
                   string state = ddlState.SelectedValue;
                   string zipCode = txtZip.Text;
                   string email = txtEmail.Text;
                   string phone = txtPhone.Text;
                   int numberCopies = Convert.ToInt32(txtCopies.Text);
                   DateTime startDate = Convert.ToDateTime(txtStart.Text);
                   DateTime endDate = Convert.ToDateTime(txtEnd.Text);
                   string status = ddlStatus.SelectedValue;

                   if (hidden == 0)
                   {
                       user = CreateUser(firstName, lastName, email, address, zipCode, city, state, phone, userName, password);
                   }
                   else
                   {
                       Member m = new Member(hidden);
                       user.userid = m.Id.ToString();
                       user.email = email;
                       user.firstname = firstName;
                       user.lastname = lastName;
                       user.address1 = address;
                       user.city = city;
                       user.state = state;
                       user.Zip = zipCode;
                       user.phone = phone;
                       user.isMember = Convert.ToBoolean(m.getProperty("isMember").Value);
                       ProfileModify(user);
                       scm.CreateUser(user);

               
                   }

                   if (user.userid != "")
                   {
                       Order order = new Order();
                       order.userid = Convert.ToInt32(user.userid);
                       order.status = (int)OrderStatus.Pending;
                       order.subscription = (int)SubscriptionStatus.Active;
                       order.createdate = orderDate;
                       order.billemail = email;
                       order.billphone = phone;
                       order.shipemail = email;
                       order.shipphone = phone;
                       order.billstreet = address;
                       order.billzip = zipCode;
                       order.shipstreet = address;
                       order.shipzip = zipCode;
                       // order.shippingamount = Convert.ToInt32("");
                       order.ordertype = orderType;

                       OrderCreate(order, numberCopies, packageId, endDate);
                       ltMessage.Text = "Order Successfully Created.";
                       ClearForm();
                   }
 
               }
               catch (Exception ex)
               {
                   ltMessage.Text = "Error createing new order." + ex.Message;
                   ltMessage.Visible = true;
                   //ClearForm();
               }

               //ClearForm();
               
               ltMessage.Visible = true;
           }
           else
           {
               validateOrder.ShowSummary = true;
           }
       }

       protected void ClearForm()
       {
           txtCity.Text = "";
           txtCopies.Text = "";
           txtEmail.Text = "";
           txtEnd.Text = "";
           txtFirstName.Text = "";
           txtLastName.Text = "";
           txtOrderDate.Text = "";
           txtPhone.Text = "";
           txtStart.Text = "";
           txtZip.Text = "";
           txtUsername.Text = "";
           txtAddress.Text = "";
           ddlOrderType.ClearSelection();
           ddlPackage.ClearSelection();
           ddlState.ClearSelection();
           ddlState.ClearSelection();
       }
    }
}