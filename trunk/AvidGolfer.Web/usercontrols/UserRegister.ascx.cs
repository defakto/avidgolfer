﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvidGolfer.Components;
using AvidGolfer.Entities;
using Defakto.Components;
using System.Web.Security;

namespace AvidGolfer.Web.usercontrols
{
    public partial class UserRegister : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(PageRedirectURL("LoginBack"));
        }

        protected void buttonRegister_Click(object sender, EventArgs e)
        {
            UserCreate();
        }

        void UserCreate()
        {
            string message = string.Empty;
            lblErr.Visible = false;
            SupplementalContentManager scm = new SupplementalContentManager();

            try
            {
                using (IMembershipManager member = new MembershipManager())
                {
                    string email = tbEmail.Text;
                    string[] splitEmail = email.Split('@');
                    Random rnd = new Random();
                    string Id = rnd.Next(100, 100000).ToString();
                   
                    string userName = splitEmail[0] + "_" + Id;

                    message = member.RegisterUnique(userName, tbEmail.Text);
                    if (message.Equals(""))
                    {
                        using (User user = new User())
                        {
                            
                            
                            user.username = userName;

                            user.password = tbPassword.Text;
                            user.email = tbEmail.Text;
                            
                            if (!member.RegisterUser(user))
                            {
                                message = "Unable to register account. Please try again later or contact the Website Administrator.";
                            }
                            else
                            {
                                user.userid = member.UserId(userName);
                                scm.CreateUser(user);
                            }
                               
                        }
                    }
                    else
                        message = string.Format(@"This {0} is already registered. You must use a unique username and email.", message.ToLower());
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            if (message.Length > 0)
            {
                lblErr.Text = message;
                lblErr.Visible = true;
            }
            else
                Response.Redirect("~/Login.aspx?page=confirmation&panel=register");
        }

        // I am not sure why this method exists or why a session variable is needed.
        string PageRedirectURL(string action = "")
        {
            string url = "~/"; // default to home page
            string forward = string.Empty;

            try
            {
                forward = (string)Session[action] + "";
                if (!forward.Equals(""))
                {
                    url = forward;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            return url;
        }
    }
}