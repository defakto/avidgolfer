﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using umbraco;
using umbraco.interfaces;
using umbraco.NodeFactory;
using System.IO;
using System.Web.Security;

using AvidGolfer.Entities;
using umbraco.cms.businesslogic.member;

using AvidGolfer.Data;
using AvidGolfer.Components;

namespace AvidGolfer.Web.usercontrols
{
  
    public partial class CreateAccounts : System.Web.UI.UserControl
    {
        protected string _debug = String.Empty;
        protected umbraco.BusinessLogic.User admin = umbraco.BusinessLogic.User.GetCurrent();
        protected List<User> fUsers = new List<User>();

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                // add this line if necessary
                // || !Roles.IsUserInRole("admin")
                if (admin == null)
                {
                    Response.Redirect("/");
                }
            }

            base.OnLoad(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;

            string fileExtension = Path.GetExtension(fuAccounts.FileName);

            string accountId = string.Empty;
            string email = string.Empty;
            string firstName = string.Empty;
            string lastName = string.Empty;
            string address = string.Empty;
            string address2 = string.Empty;
            string zip = string.Empty;
            string zip4 = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string phone = string.Empty;
            string country = string.Empty;
             

            if (fileExtension == ".txt")
            {
                var reader = new StreamReader(fuAccounts.FileContent);

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    string parseString = values[0];
                    string[] sArray = parseString.Split('\t');

                    accountId = sArray[0];
                    firstName = sArray[1];
                    lastName = sArray[2];
                    address = sArray[3];
                    address2 = sArray[4];
                    city = sArray[5];
                    state = sArray[6];
                    zip = sArray[7];
                    country = sArray[8];
                    email = sArray[9];
                    phone = sArray[10];

                    if (string.IsNullOrEmpty(email))
                    {
                        email = accountId + "@emailnotsupplied.com";
                    }

                        CreateUser(accountId, firstName, lastName, email, address, zip, city, state, phone);

                         email = string.Empty;
                         firstName = string.Empty;
                         lastName = string.Empty;
                         address = string.Empty;
                         zip = string.Empty;
                         zip4 = string.Empty;
                         city = string.Empty;
                         state = string.Empty;
                         phone = string.Empty;
                         country = string.Empty;  
                }
                WriteMembersToFile();
                lblMessage.Text = "File uploaded successfully";
                lblMessage.Visible = true;
            }
            else
            {
                lblMessage.Text = "Please upload a text file. (.txt)";
                lblMessage.Visible = true;
            }
        }

        protected void CreateUser(string accountId, string firstName, string lastName, string email, string address, string zip, string city,
            string state, string phone)
        {
            SupplementalContentManager scm = new SupplementalContentManager();
            string message = string.Empty;
            string password = Membership.GeneratePassword(7, 1);
            string userName = lastName + "_" + accountId;
            userName = userName.Replace(",", "");
            using (IMembershipManager member = new MembershipManager())
            {
                message = member.RegisterUnique(userName, email);

                if (message.Equals(""))
                {
                    using (User user = new User())
                    {
                        user.username = userName;
                        user.password = password;
                        user.email = email;
                        user.firstname = firstName;
                        user.lastname = lastName;
                        user.address1 = address;
                        user.city = city;
                        user.state = state;
                        user.Zip = zip;
                        user.phone = phone;
                        user.isMember = true;

                        member.RegisterUser(user);
                        user.userid = member.UserId(userName);
                        scm.CreateUser(user);
                        fUsers.Add(user);
                        ProfileModify(user);
                        
                    }
                }
            }
        }

        public bool ProfileModify(User user)
        {
            bool isSuccess = false;

            //try
            //{

                //Member memInfo = GetMemberFromCache(currentMem.Id);
                Member member = Member.GetMemberFromEmail(user.email);

                user.userid = member.Id.ToString();
                user.username = member.LoginName;

                //if(user.email != "")
                //    member.getProperty("email").Value = user.email;

                member.getProperty("firstname").Value = user.firstname;
                member.getProperty("lastname").Value = user.lastname;

                // member.getProperty("gender").Value = user.gender;
                member.getProperty("phone").Value = user.phone;
                member.getProperty("address1").Value = user.address1;
                member.getProperty("address2").Value = user.address2;
                member.getProperty("city").Value = user.city;
                member.getProperty("state").Value = user.state;
                member.getProperty("zip").Value = user.Zip;
                member.getProperty("zip4").Value = user.zip4;

                //string ismember = "0";
                //if (user.isMember)
                string  ismember = "1";
                member.getProperty("isMember").Value = ismember;

                member.Save();

                Member.AddMemberToCache(member);
                //FormsAuthentication.SetAuthCookie(user.username, false);

                isSuccess = true;
            //}
            //catch (Exception ex)
            //{
            //    string error = ex.Message;
            //}

            return isSuccess;
        }
        protected void WriteMembersToFile()
        {
            string appPath = Request.PhysicalApplicationPath;
            string filePath = appPath + "Members.txt";
            

               using(StreamWriter w = new StreamWriter(File.Open(filePath, FileMode.Append)))
               {
                    foreach (User user in fUsers)
                    {
                        string nextLine = String.Format("{0}\t{1}\t{2}\t{3}\t{4}", user.username,
                            user.firstname, user.lastname, user.password, user.email);

                        w.WriteLine(nextLine);
                    
                    }

                    w.Flush();
                    w.Close();
               }
        }
    }
}