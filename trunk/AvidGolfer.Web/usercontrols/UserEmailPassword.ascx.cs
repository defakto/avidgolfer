﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvidGolfer.Components;

namespace AvidGolfer.Web.usercontrols
{
    public partial class UserEmailPassword : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        protected void btnEmailPassword_Click(object sender, EventArgs e)
        {
            PasswordReset();
        }

        private void PasswordReset()
        {
            string message = string.Empty;
            lblErr.Visible = false;

            using (IMembershipManager member = new MembershipManager())
            {

                if (member.PasswordEMail(tbEmail.Text))
                    Response.Redirect("~/Login.aspx?page=confirmation&panel=recover");
                else
                {
                    lblErr.Text = "Email not found.";
                    lblErr.Visible = true;
                }
            }
        }
    }
}