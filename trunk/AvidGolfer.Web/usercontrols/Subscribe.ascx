﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Subscribe.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.Subscribe" %>

<asp:Panel ID="panelSubscribe" runat="server">
<div id="divPage" class="subscribePage">

    <h1>
        Subscription Checkout
    </h1>

    <div class="subscribedivheight">

        <div class="subscribedivleft">
            <asp:Image ID="imageProduct" runat="server" CssClass="subscribeimage" />
        </div>

        <div class="subscribedivright">
            <ul class="nobulletlist">
                <li class="padbottom">
                    <asp:Literal ID="literalProduct" runat="server" >@product</asp:Literal>&nbsp;&nbsp;
                    Quantity: <asp:DropDownList ID="drQuantity" runat="server" 
                        onselectedindexchanged="drQuantity_SelectedIndexChanged"></asp:DropDownList>
                </li>
                <li class="padbottom">
                    <asp:Literal ID="literalDescription" runat="server" >@description</asp:Literal>
                </li>
                <!--
                <li class="padbottom">
                    <span>Effective through: &nbsp;
                        <asp:Literal ID="literalEndDate" runat="server">@enddate</asp:Literal>
                    </span>
                </li>
                -->
                <li class="padbottom">
                    <span>Price: &nbsp;
                        $<asp:Literal ID="literalPrice" runat="server">@price</asp:Literal>
                    </span>
                </li>
                <!--
                <li class="padbottom">
                    <asp:Panel ID="panelLogIn" runat="server">
                    <div class="subscribelogin">
                        <asp:LinkButton ID="hlLogIn" runat="server" onclick="hlLogIn_Click" >
                            LogIn for express checkout!
                        </asp:LinkButton>
                    </div>
                    </asp:Panel>
                </li>
                -->
            </ul>
        </div>
    </div>
	<div class="clear"></div>
    <div ID="tdSubscribecommand" class="subscribecommand">
        <span >
            <asp:Button ID="buttonContinue" runat="server" CssClass="buttondefault" 
                Text="Checkout" TabIndex="2" onclick="buttonContinue_Click" />
            <asp:Button ID="buttonCancel" runat="server" CssClass="buttondefaultleft" 
                Text="Cancel" TabIndex="1" onclick="buttonCancel_Click"  />
        </span>
    </div>

</div>
</asp:Panel>
