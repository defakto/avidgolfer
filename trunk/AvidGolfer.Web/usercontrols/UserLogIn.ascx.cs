﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.cms.businesslogic.member;

using AvidGolfer.Components;
using Defakto.Components;

using AvidGolfer.Components;
using AvidGolfer.Data;
using AvidGolfer.Entities;
using System.Web.Security;

namespace AvidGolfer.Web.usercontrols
{
    public partial class UserLogIn : System.Web.UI.UserControl
    {
        protected string msgInvalidLogin = (string)(ConfigurationManager.AppSettings["msgInvalidLogin"]);
        protected string msgAccountArchived = (string)(ConfigurationManager.AppSettings["msgAccountArchived"]);

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("/");
        }

        protected void buttonLogIn_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void Login()
        {
            lblErr.Visible = false;

            using (IMembershipManager member = new MembershipManager())
            {
                string username = Membership.GetUserNameByEmail(tbEmailAddress.Text);
                string email = "";

                if (username != null)
                {
                    if (member.LogInUser(username, tbPassword.Text, email))
                    {
                        Member m = Member.GetMemberFromLoginName(username);

                        if (m.getProperty("accountArchived").Value.ToString().Equals("1"))
                        {
                            member.Logout();
                            lblErr.Text = msgAccountArchived;
                            lblErr.Visible = true;
                        }
                        else
                        {
                            Response.Redirect(PageRedirectURL("LoginForward"));
                            //Response.Redirect(PageRedirectURL(Session["LoginForward"].ToString()));
                            //Response.Redirect("~/Login.aspx?page=account");
                        }
                    }
                    else
                    {
                        lblErr.Text = msgInvalidLogin;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.Text = "No member exists for the email address provided.";
                    lblErr.Visible = true;
                }
             
               
            }

        }

        string PageRedirectURL(string action = "")
        {
            string url = "~/Login.aspx?page=account"; // default to user account manager
            string forward = string.Empty;

            try
            {
                forward = (string)Session[action] + "";
                if (!forward.Equals(""))
                {
                    url = forward;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            return url;
        }


    }
}