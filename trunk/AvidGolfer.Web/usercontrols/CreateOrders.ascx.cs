﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using umbraco;
using umbraco.interfaces;
using umbraco.NodeFactory;
using System.IO;
using System.Web.Security;

using AvidGolfer.Entities;
using umbraco.cms.businesslogic.member;

using AvidGolfer.Data;
using AvidGolfer.Components;

namespace AvidGolfer.Web.usercontrols
{
    public partial class CreateOrders : System.Web.UI.UserControl
    {
        protected string _debug = String.Empty;
        protected umbraco.BusinessLogic.User admin = umbraco.BusinessLogic.User.GetCurrent();
        protected List<User> fUsers = new List<User>();

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                // add this line if necessary
                // || !Roles.IsUserInRole("admin")
                if (admin == null)
                {
                    Response.Redirect("/");
                }
            }

            base.OnLoad(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            string fileExtension = Path.GetExtension(fuAccounts.FileName);

            SupplementalContentManager scm = new SupplementalContentManager();
            IMembershipManager member = new MembershipManager();
            string delivery = string.Empty;
            string package = string.Empty;
            string expireYear = string.Empty;
            string expireMonth = string.Empty;
            string accountNumber = string.Empty;
            string firstName = string.Empty;
            string lastName = string.Empty;
            string email = string.Empty;
            string userid = string.Empty;
            int copies = 0;
            string orderStatus = string.Empty;
            string orderType = string.Empty; // maps to Marketing column in spreadsheet
            DateTime createDate;
            DateTime updated;

            if (fileExtension == ".txt")
            {
                var reader = new StreamReader(fuAccounts.FileContent);

                while (!reader.EndOfStream)
                {
                    Order order = new Order();

                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    string parseString = values[0];
                    string[] sArray = parseString.Split('\t');

                    delivery = sArray[0];
                    package = sArray[1];
                    expireYear = sArray[2];
                    expireMonth = sArray[3];
                    accountNumber = sArray[4];
                    firstName = sArray[5];
                    lastName = sArray[6];
                    email = sArray[7];
                    copies = Convert.ToInt32(sArray[8]);
                    orderStatus = sArray[9];
                    orderType = sArray[10];
                    createDate = Convert.ToDateTime(sArray[11]);
                    updated = Convert.ToDateTime(sArray[12]);

                    string expire = expireMonth + "/" + "01" + "/" + expireYear;
                    DateTime expireDate = Convert.ToDateTime(expire);

                    string userName = lastName + "_" + accountNumber;
                    order.orderid = Convert.ToInt64(accountNumber);
                    order.userid = Convert.ToInt32(member.UserId(userName));
                    order.ordertype = orderType;
                    order.createdate = createDate;
                    order.modifydate = updated;
                    

                    scm.SavePreviousOrder(order);

                    for (int i = 0; i < copies; i++)
                    {
                        CreateOrderItems(order, expireDate, package, orderStatus);
                    }
                    
                }
            }
            else
            {
                lblMessage.Text = "Please upload a text file. (.txt)";
                lblMessage.Visible = true;
            }
        }

        protected void CreateOrderItems(Order order, DateTime expire, string sku, string orderStatus)
        {
            SupplementalContentManager scm = new SupplementalContentManager();
            OrderItem item = new OrderItem();
            
            item.orderid = order.orderid;
            item.startdate = order.createdate;
            item.modifydate = order.modifydate;
            item.createdate = order.createdate;
            item.expiredate = expire;
            item.sku = sku;
            item.orderStatus = orderStatus;
            item.subscriptionStatus = orderStatus;

            scm.SaveIndividualOrderItem(item);

        }
    }
}