﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using AvidGolfer.Components;
using AvidGolfer.Data;
using AvidGolfer.Entities;

namespace AvidGolfer.Web.usercontrols
{
    public partial class UserProfile : System.Web.UI.UserControl
    {
        List<SelectList> usstates;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ProfileInitialize();
            }
            
        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            //Response.Redirect(PageRedirectURL("ProfileForward"));
            Response.Redirect("~/");
        }

        protected void buttonProfile_Click(object sender, EventArgs e)
        {
            ProfileModify();
        }

        DropDownList DDLBind(DropDownList ddl, List<SelectList> list)
        {
            ddl.DataSource = list;
            ddl.DataTextField = "display";
            ddl.DataValueField = "value";
            ddl.DataBind();

            ddl.SelectedIndex = -1;

            return ddl;
        }

        string PageRedirectURL(string action = "")
        {
            string url = "~/"; // default to home page
            string forward = string.Empty;

            try
            {
                forward = (string)Session[action] + "";
                if (!forward.Equals(""))
                {
                    url = forward;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            return url;
        }

        void ProfileInitialize()
        {
            // ToDo:  need to move webmethods to masterpage(sitemaster) check with MikeH.
            tbZip.Attributes.Add("onblur", "javascript:ZipSearch('')");

            USStates();

            ProfileLoad();
        }

        void ProfileLoad()
        {

            User user;

            using (IMembershipManager member = new MembershipManager())
                user = member.ProfileSelect();

            if (user.Equals(null))
                return;

            int value = 0;
            if (!Int32.TryParse(user.userid, out value))
                return;

            tbLastName.Text = user.lastname;
            tbFirstName.Text = user.firstname;

            if (user.gender.Equals("F"))
                rbFemale.Checked = true;
            else
                rbMale.Checked = true;

            tbAddress.Text = user.address1;

            tbZip.Text = user.Zip;

            tbZip4.Text = user.zip4;

            tbCity.Text = user.city;

            try
            {
                ddlState.SelectedValue = user.state;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            tbPhone.Text = user.phone;
        }

        void ProfileModify()
        {
            SupplementalContentManager scm = new SupplementalContentManager();

            User user = new User();

            user.lastname = tbLastName.Text;
            user.firstname = tbFirstName.Text;

            if (rbMale.Checked)
                user.gender = "M";
            else
                user.gender = "F";

            user.address1 = tbAddress.Text;
            user.Zip = tbZip.Text;
            user.zip4 = tbZip4.Text;

            user.city = tbCity.Text;
            user.state = ddlState.SelectedValue;

            user.phone = tbPhone.Text;

            string message = string.Empty;

            using (IMembershipManager member = new MembershipManager())
            {
                if (member.ProfileModify(user))
                {
                    scm.CreateUser(user);
                    //Session["ProfileForward"] = PageRedirectURL("LoginForward");
                    Response.Redirect("~/Login.aspx?page=confirmation&panel=profile");
                }
                else
                    message = @"Profile create\change failed.\n\nRetry.";
            }

            ProfileMessage(message);
        }

        void ProfileMessage(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "profilealert", string.Format("alert('{0}');", message), true);
        }

        List<SelectList> SessionList(string session)
        {
            bool isLoad = false;

            List<SelectList> list = Session[session] as List<SelectList>;
            try
            {
                if (!list.Equals(null))
                    isLoad = true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            if (!isLoad)
            {
                list = new List<SelectList>();
                using (SupplementalEntities se = new SupplementalEntities())
                    if (session.ToLower().IndexOf("month") > -1)
                        list = se.ListExpireMonth();
                    else if (session.ToLower().IndexOf("year") > -1)
                        list = se.ListExpireYear();
                    else if (session.ToLower().IndexOf("usstates") > -1)
                        list = se.USStates();
                    else
                    {
                    }

                Session[session] = list;
            }

            return list;
        }

        // ToDo:  need to move webmethods to masterpage(sitemaster) check with MikeH.
        [System.Web.Services.WebMethod]
        public static string ZipLocationSelect(string zip)
        {
            using (SupplementalContentManager scm = new SupplementalContentManager())
                return scm.ZipLocationSelect(zip);

        }

        void USStates()
        {
            usstates = SessionList("USStates");
            ddlState = DDLBind(ddlState, usstates);
            ddlState.Width = new Unit("180px");

        }

    }
}