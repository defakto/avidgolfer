﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

using AvidGolfer.Components;
using AvidGolfer.Entities;
using AuthorizeNet.Entities;
using umbraco.cms.businesslogic.member;
using umbraco.cms.businesslogic.media;
using umbraco.presentation.nodeFactory;

namespace AvidGolfer.Web.usercontrols
{
    public partial class SubscribePurchase : System.Web.UI.UserControl
    {
        static AuthorizeNetCustomer aganc;
        static Product product;
        static string productid = "";
        static string quant = "";
        List<OrderDetails> orderDetails = new List<OrderDetails>();
        struct OrderDetails
        {
            string _product;
            string _description;
            decimal _price;
            decimal _subtotal;

            public string product 
            {
                get { return _product; }
                set { _product = value; }
            }

            public string description
            {
                get { return _description; }
                set { _description = value; }
            }

            public decimal price
            {
                get { return _price; }
                set { _price = value; }
            }

            public decimal subtotal
            {
                get { return _subtotal; }
                set { _subtotal = value; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                productid = Request.QueryString["productid"].Trim();

            if (!string.IsNullOrEmpty(Request.QueryString["quantity"]))
                quant = Request.QueryString["quantity"].Trim();

            PurchaseInitialize();
        }

        protected void buttonBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/Order.aspx?page=checkout&productid={0}&quantity={1}", productid, quant));
        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(PageRedirectURL());
        }

        protected void buttonPurchase_Click(object sender, EventArgs e)
        {
            try
            {
                PurchaseAuthorize();  // Need error handling here
                //ShowMessage(("Error: test"));
            }
            catch (Exception ex)
            {
                ShowMessage(("Error: " + ex.Message.ToString().Replace("Error occurred in submitting to Authorize.Net:", "")));
                return;
            }

            
        }

        void CustomerInfoLoad(AuthorizeNetCustomer aganc)
        {

            literalName.Text = aganc.PaymentInfo.BillingPersonInfo.FirstName + " " +aganc.PaymentInfo.BillingPersonInfo.LastName;

            literalAddress.Text = aganc.PaymentInfo.BillingPersonInfo.Address;
            literalLocation.Text = aganc.PaymentInfo.BillingPersonInfo.City + " " +
                aganc.PaymentInfo.BillingPersonInfo.State + "  " +
                aganc.PaymentInfo.BillingPersonInfo.Zip;

            if (!string.IsNullOrEmpty(aganc.PaymentInfo.BillingPersonInfo.EMail))
                literalEMail.Text = aganc.PaymentInfo.BillingPersonInfo.EMail;
            else
                literalEMail.Text = aganc.Email;

            literalPhone.Text = aganc.PaymentInfo.BillingPersonInfo.PhoneNumber;

            literalNameS.Text = aganc.ShippingPersonInfo.FirstName + " " + aganc.ShippingPersonInfo.LastName;

            literalAddressS.Text = aganc.ShippingPersonInfo.Address;
            literalLocationS.Text = aganc.ShippingPersonInfo.City + " " +
                aganc.ShippingPersonInfo.State + "  " +
                aganc.ShippingPersonInfo.Zip;

            if (!string.IsNullOrEmpty(aganc.ShippingPersonInfo.EMail))
                literalEMailS.Text = aganc.ShippingPersonInfo.EMail;
            else
                literalEMailS.Text = aganc.Email;

            literalPhoneS.Text = aganc.ShippingPersonInfo.PhoneNumber;

            literalCCN.Text = aganc.PaymentInfo.CreditCardNumber;
            literalExpire.Text = aganc.PaymentInfo.Expiry;
            literalCCV.Text = aganc.PaymentInfo.CardCode;

            ProductLoad();
        }

        void PurchaseInitialize()
        {
            //bool isLoad = false;

            //aganc = Session["ANCustomer"] as AuthorizeNetCustomer;
            //try
            //{
            //    if (!aganc.Equals(null))
            //        isLoad = true;
            //}
            //catch (Exception ex)
            //{
            //}

            //if (!isLoad)
            //    {
            //        try
            //        {
                        aganc = StoreManager.GetCustomerProfile(Member.GetCurrentMember());
                        //Session["ANCustomer"] = aganc;
                //    }
                //    catch (Exception ex)
                //    {
                //        Response.Redirect(PageRedirectURL());

                //    }
                //}

            CustomerInfoLoad(aganc);
        }

        string PageRedirectURL(string action = "")
        {
            string url = "~/"; // default to home page

            try
            {
                if ((!Session[action].Equals(null)) && (((string)Session[action]).Length > 0))
                {
                    url = Session[action] as string;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            return url;
        }

        Order OrderCreate(AuthorizeNetCustomer aganc)
        {
            //Product product = Session["AGProduct"] as Product;

            Order order = new Order();
          
            Member member = Member.GetCurrentMember();

            order.userid = member.Id;
            order.status = (int)OrderStatus.Pending;
            order.subscription = (int)SubscriptionStatus.Active;

            order.createdate = DateTime.Now;
            order.billemail = literalEMail.Text;
            order.billphone = literalPhone.Text;
            
            order.shipemail = literalEMailS.Text;
            order.shipphone = literalPhoneS.Text;
            
            order.billstreet = aganc.PaymentInfo.BillingPersonInfo.Address;
            order.billzip = aganc.PaymentInfo.BillingPersonInfo.Zip;

            order.shipstreet = aganc.ShippingPersonInfo.Address;
            order.shipzip = aganc.ShippingPersonInfo.Zip;
            order.shippingamount = Convert.ToDecimal(ltlTotal.Text);
            order.ordertype = "Online";
            int index = 0;
             foreach (var value in orderDetails)
            {
                OrderItem item = new OrderItem();

                //item.productid = Convert.ToInt32(Session["ProductID"]);
                item.productid = Convert.ToInt32(productid);
                item.productname = product.name;
                //item.productname = Session["ProductName"].ToString();
                item.itemid = 1;
                item.quantity = 1;
                item.price = product.price;
                item.tax = product.tax;
                item.total = ((item.price + (item.price * item.tax)) * item.quantity);
                item.createdate = DateTime.Now;
                item.expiredate = product.expiredate;
                item.itemstatus = (int)OrderStatus.Open;
                item.subscriptionStatus = SubscriptionStatus.Pending.ToString();
                 
                if (index == 0)
                {
                    item.orderStatus = "Active";
                    item.startdate = DateTime.Now;
                }
                else
                {
                    item.orderStatus = "Purchased";
                }
                
                //item.sku = Session["Sku"].ToString();
                item.sku = product.sku;
                item.regionType = Session["Region"].ToString();

                order.orderitems.Add(item);
                index++;
            }

            using (SupplementalContentManager scm = new SupplementalContentManager())
            scm.OrderSave(order);

           

            return order;
        }

        Product PopulateProductById(int productId)
        {
            Product p = new Product();

            Node product = new Node(productId);
            if (product == null)
                Response.Redirect("/");

            var imageUrl = String.Empty;

            if (product.GetProperty("subscriptionImage") != null)
            {
                try
                {
                    Media file = new Media(Convert.ToInt32(product.GetProperty("subscriptionImage").Value));
                    if (file != null)
                        imageUrl = file.getProperty("umbracoFile").Value.ToString();
                    p.imageurl = imageUrl;
                    //imageProduct.ImageUrl = imageUrl;
                }
                catch (Exception ex)
                {
                }
            }

            p.name = (product.GetProperty("subscriptionName") != null) ? product.GetProperty("subscriptionName").Value : String.Empty;
            p.description = (product.GetProperty("briefDescription") != null) ? product.GetProperty("briefDescription").Value : String.Empty;

            if (product.GetProperty("endDate") != null)
                p.expiredate = Convert.ToDateTime(product.GetProperty("endDate").Value);

            p.price = (product.GetProperty("price") != null) ? Convert.ToDecimal(product.GetProperty("price").Value) : 0;
            p.tax = (product.GetProperty("tax") != null) ? Convert.ToDecimal(product.GetProperty("tax").Value) : 0;
            p.productid = product.Id;
            p.sku = product.Id.ToString();

            return p;


        }

        void ProductLoad()
        {
            // Assumption: quantities are defaulted to 1ea, every package has to have seperate order
            bool isLoad = false;
            decimal total = 0;
            decimal subtotal = 0;
            //Product product = Session["AGProduct"] as Product;
            product = PopulateProductById(Convert.ToInt32(productid));
            //product = Session["AGProduct"] as Product;
            try
            {
                if (!product.Equals(null))
                    isLoad = true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            //if (!isLoad)
            //{
            //    int productid = 0;
            //    if (!Int32.TryParse((string)Session["ProductID"], out productid))
            //        productid = 0;

            //    //using (SupplementalContentManager scm = new SupplementalContentManager())
            //    //    products = scm.ProductSelect(productid: productid);

            //    product = PopulateProductById(productid);

            //    //Session["AGProduct"] = product;
            //}
            //int quantity = Convert.ToInt32(Session["Quantity"].ToString());
            int quantity = Convert.ToInt32(quant);
            
            for(int i = 1; i <= quantity; i++) 
            {
                OrderDetails od = new OrderDetails();
                od.product = product.name;
                od.description = product.description;
                od.price = product.price;
                od.subtotal = subtotal + product.price;
                subtotal = od.subtotal;
                orderDetails.Add(od);
            }

            total = subtotal;
            ltlTotal.Text = total.ToString();
            rptOrder.DataSource = orderDetails;
            rptOrder.DataBind();
            
        }

        void PurchaseAuthorize()
        {

            Order order = OrderCreate(aganc);
            Member member = Member.GetCurrentMember();

            try
            {
                StoreManager.PurchaseOrder(order, member, aganc);

                order.status = (int)OrderStatus.Purchased;

                using (SupplementalContentManager scm = new SupplementalContentManager())
                    scm.OrderSave(order);

                member.getProperty("isMember").Value = "1";
                member.Save();

                PurchaseConfirmation(order, member, aganc);

                Response.Redirect("~/Order.aspx?page=confirmation&panel=subscription");
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }
        }

        void PurchaseConfirmation(Order order, Member member, AuthorizeNetCustomer aganc)
        {
            using (IEMailManager manager = new EMailManager())
            {
                string htmlTemplate = manager.ReadEmailTemplate("subscribeconfirm.html");

                if (htmlTemplate.Equals(""))  // did not get template
                    return;

                string subject = "Avid Golfer Subscription Confirmation!";

                string fromEmail = "customerservice@myavidgolfer.com";

                string value = member.getProperty("firstname").Value + " " + member.getProperty("lastname").Value;
                if (value.Trim().Length < 1)
                    value = member.LoginName;

                htmlTemplate = htmlTemplate.Replace("@username", value.Trim());

                htmlTemplate = htmlTemplate.Replace("@creditcard", aganc.PaymentInfo.CreditCardNumber);

                htmlTemplate = htmlTemplate.Replace("@expiration", aganc.PaymentInfo.Expiry);
                htmlTemplate = htmlTemplate.Replace("@transaction", order.tansactionid.ToString());
                htmlTemplate = htmlTemplate.Replace("@dtstamp", order.createdate.ToString());

                Product product = Session["AGProduct"] as Product;

                htmlTemplate = htmlTemplate.Replace("@product", product.name);
                htmlTemplate = htmlTemplate.Replace("@description", product.description);
                htmlTemplate = htmlTemplate.Replace("@enddate", product.expiredate.ToString("MMMM d, yyyy"));
                htmlTemplate = htmlTemplate.Replace("@price", product.price.ToString("C2"));
                htmlTemplate = htmlTemplate.Replace("@qty", order.orderitems.Count().ToString());
                htmlTemplate = htmlTemplate.Replace("@tax", product.tax.ToString("F2") + "%");
                htmlTemplate = htmlTemplate.Replace("@total", order.shippingamount.ToString("C2"));

                htmlTemplate = htmlTemplate.Replace("@subscription", order.orderid.ToString());

                value = aganc.ShippingPersonInfo.FirstName + " " + aganc.ShippingPersonInfo.LastName;
                htmlTemplate = htmlTemplate.Replace("@nameS", value.Trim());

                htmlTemplate = htmlTemplate.Replace("@addressS", aganc.ShippingPersonInfo.Address);
                htmlTemplate = htmlTemplate.Replace("@cityS", aganc.ShippingPersonInfo.City);
                htmlTemplate = htmlTemplate.Replace("@stateS", aganc.ShippingPersonInfo.State);
                htmlTemplate = htmlTemplate.Replace("@zipS", aganc.ShippingPersonInfo.Zip);
                htmlTemplate = htmlTemplate.Replace("@phoneS", aganc.ShippingPersonInfo.PhoneNumber);
                htmlTemplate = htmlTemplate.Replace("@emailS", aganc.ShippingPersonInfo.EMail);

                value = aganc.PaymentInfo.BillingPersonInfo.FirstName + " " + aganc.PaymentInfo.BillingPersonInfo.LastName;
                htmlTemplate = htmlTemplate.Replace("@name", value.Trim());

                htmlTemplate = htmlTemplate.Replace("@address", aganc.PaymentInfo.BillingPersonInfo.Address);
                htmlTemplate = htmlTemplate.Replace("@city", aganc.PaymentInfo.BillingPersonInfo.City);
                htmlTemplate = htmlTemplate.Replace("@state", aganc.PaymentInfo.BillingPersonInfo.State);
                htmlTemplate = htmlTemplate.Replace("@zip", aganc.PaymentInfo.BillingPersonInfo.Zip);
                htmlTemplate = htmlTemplate.Replace("@phone", aganc.PaymentInfo.BillingPersonInfo.PhoneNumber);
                htmlTemplate = htmlTemplate.Replace("@email", aganc.PaymentInfo.BillingPersonInfo.EMail);

                manager.SendEmail(fromEmail, member.Email, subject, htmlTemplate, null);

            }
        }

        void ShowMessage(string messages)
        {
            //labelMessage.Visible = (messages != "" || css != "");
            panelMessage.Visible = true;
            buttonBack.Enabled = true;
            buttonCancel.Enabled = true;
            labelmessages.InnerText = messages;
        }
    }
}