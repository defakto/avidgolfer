﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionReport.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.SubscriptionReport" %>
<h1>Subscription Report</h1>

Search Order Items by region type, subscription code, order status or subscription status
<br />

<div id="orderSearch">
    Search By:<br />
    <table>
        <tr>
            <td>
                <asp:Label ID="lblregion" runat="server" AssociatedControlID="ddlRegion" Text="Region:" />
            </td>
            <td>
                <asp:Label ID="lblSubCode" runat="server" AssociatedControlID="ddlSubCode" Text="Subscription Code:" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlRegion" runat="server"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlSubCode" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="lblOrderStatus" runat="server" AssociatedControlID="ddlOrderStatus" Text="Order Status:" /> 
            </td>
            <td>
                <asp:Label ID="lblSubStatus" runat="server" AssociatedControlID="ddlSubStatus" Text="Subscription Status:" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlOrderStatus" runat="server"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlSubStatus" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            <asp:Button ID="btnSearch" runat="server" Text="Search" 
                 onclick="btnSearch_Click" />
            </td>
        </tr>
    </table> 
</div>
<div id="orderReport">
    <asp:Button ID="btnDownload" runat="server" Text="Download Report" 
        onclick="btnDownload_Click" /><br />
    <table>
        <asp:Repeater ID="rpReport" runat="server" onitemcommand="rpReport_ItemCommand">
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID="lblUserID" runat="server" Text="User ID:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltUserID" runat="server" Text='<%# Eval("userid") %>' /><br />
                    
                        <asp:Label ID="lblFirstName" runat="server" Text="First Name:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltFirstName" runat="server" Text='<%# Eval("firstname") %>' /><br />
                    
                        <asp:Label ID="lblLastName" runat="server" Text="Last Name:" Font-Bold="true"  />&nbsp;
                        <asp:Literal ID="ltLastName" runat="server" Text='<%# Eval("lastname") %>' /><br />
                    </td>
                    <td>
                        <asp:Label ID="lblOrderType" runat="server" Text="Order Type:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltOrderType" runat="server" Text='<%# Eval("ordertype") %>' /><br />
                        <asp:Label ID="lblSku" runat="server" Text="Subscription Code:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltSku" runat="server" Text='<%# Eval("sku") %>' /><br />
                        <asp:Label ID="lblOrderStatus" runat="server" Text="Order Status:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltOrderStatus" runat="server" Text='<%# Eval("orderstatus") %>' /><br />
                        <asp:Label ID="lblSubStatus" runat="server" Text="Subscription Status:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltSubscriptionStatus" runat="server" Text='<%# Eval("subscriptionstatus") %>' /><br />
                        <asp:Label ID="lblRegion" runat="server" Text="Region:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltRegion" runat="server" Text='<%# Eval("regiontype") %>' /><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3>Billing Address</h3>
                        <asp:Literal ID="ltBillStreet" runat="server" Text='<%# Eval("billstreet") %>' /><br />
                        <asp:Literal ID="ltBillCity" runat="server" Text='<%# Eval("billcity") %>' />&nbsp;
                        <asp:Literal ID="ltBillState" runat="server" Text='<%# Eval("billstate") %>' />,&nbsp;
                        <asp:Literal ID="ltBillZip" runat="server" Text='<%# Eval("billzip") %>' /><br />
                        <asp:Literal ID="ltBillPhone" runat="server" Text='<%# Eval("billphone") %>' /><br />
                        <asp:Literal ID="ltBillEmail" runat="server" Text='<%# Eval("billemail") %>' />
                    </td>
                    <td>
                        <h3>Shipping Address</h3>
                        <asp:Literal ID="ltShipStreet" runat="server" Text='<%# Eval("shipstreet") %>' /><br />
                        <asp:Literal ID="ltShipCity" runat="server" Text='<%# Eval("shipcity") %>' />&nbsp;
                        <asp:Literal ID="ltShipState" runat="server" Text='<%# Eval("shipstate") %>' />,&nbsp;
                        <asp:Literal ID="ltShipZip" runat="server" Text='<%# Eval("shipzip") %>' /><br />
                        <asp:Literal ID="ltShipPhone" runat="server" Text='<%# Eval("shipphone") %>' /><br />
                        <asp:Literal ID="ltShipEmail" runat="server" Text='<%# Eval("shipemail") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTransactionId" runat="server" Text="Transaction ID:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltTransactionId" runat="server" Text='<%# Eval("transactionid") %>' /><br />
                        <asp:Label ID="lblOrderId" runat="server" Text="Order ID:" Font-Bold="true" />&nbsp;
                        <asp:LinkButton ID="lbOrderId" runat="server" Text='<%# Eval("orderid") %>' CommandName="OrderId"
                            CommandArgument='<%# Eval("orderid") %>' Font-Underline="true"></asp:LinkButton><br />
                        <asp:Label ID="lblProductId" runat="server" Text="Product ID:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltProductId" runat="server" Text='<%# Eval("productid") %>' /><br />
                        <asp:Label ID="lblProductName" runat="server" Text="Product Name:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltProductName" runat="server" Text='<%# Eval("productname") %>' /><br />
                        <asp:Label ID="lblItemId" runat="server" Text="Item ID:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltItemId" runat="server" Text='<%# Eval("itemid") %>' /><br />
                        <asp:Label ID="lblPrice" runat="server" Text="Price:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltPrice" runat="server" Text='<%# Eval("Price","{0:N2}" ) %>' /><br />
                        <asp:Label ID="lblTax" runat="server" Text="Tax:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltTax" runat='server' Text='<%# Eval("tax", "{0:N2}") %>' /><br />
                        <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true" />&nbsp;
                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("total", "{0:N2}") %>' /><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3>Dates</h3>
                        <asp:Label ID="lblCreateDate" runat="server" Text="Date Created:" /><br />
                        <asp:Literal ID="ltCreateDate" runat="server" Text='<%# Eval("createdate", "{0:M-dd-yyyy}") %>' /><br />
                        <asp:Label ID="lblModifyDate" runat="server" Text="Date Modified:" /><br />
                        <asp:Literal ID="ltModifyDate" runat="server" Text='<%# Eval("modifydate", "{0:M-dd-yyyy}") %>' /><br />
                        <asp:Label ID="lblReviewDate" runat="server" Text="Review Date:" /><br />
                        <asp:Literal ID="ltReviewDate" runat="server" Text='<%# Eval("reviewdate", "{0:M-dd-yyyy}") %>' /><br />
                        <asp:Label ID="lblShipDate" runat="server" Text="Ship Date:" /><br />
                        <asp:Literal ID="ltShipDate" runat="server" Text='<%# Eval("shipdate", "{0:M-dd-yyyy}") %>' /><br />
                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date:" /><br />
                        <asp:Literal ID="ltStartDate" runat="server" Text='<%# Eval("startdate", "{0:M-dd-yyyy}") %>' /><br />
                        <asp:Label ID="lblExpireDate" runat="server" Text="Expire Date:"  /><br />
                        <asp:Literal ID="ltExpireDate" runat="server" Text='<%# Eval("expiredate", "{0:M-dd-yyyy}") %>' />
                      
                    </td>
                </tr>
                <tr>
                     <td colspan="2">
                        <hr />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    

</div>
 