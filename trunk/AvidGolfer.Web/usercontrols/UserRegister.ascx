﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserRegister.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserRegister" %>

<asp:Panel ID="panelRegister" runat="server">
<div id="divPage" class="defaultPage">

    <div class="padleft">

        <h1>Create a New Account</h1>

        <h2>
            Use the form below to create a new account or <a href="/Login.aspx?page=login" class="paratextblue">login here</a> if you already have an account.
        </h2>
        <h2>
            If your email is already registered with us and you need your username and password, <a href="/Login.aspx?page=emailpassword" class="paratextblue">click here</a>.
        </h2>

        <h3>Account Information</h3>

        <div>
            <asp:Label ID="lblErr" CssClass="error" runat="server" Visible="false"></asp:Label>
            <asp:ValidationSummary ID="vsRegister" runat="server" CssClass="failurenotification" ForeColor="" ValidationGroup="vgRegister"/>
            <asp:ValidationSummary ID="vsCancel" runat="server" CssClass="failurenotification" ForeColor="" ValidationGroup="vsCancel"/>
        </div>

        <asp:Panel ID="panelRegisterform" runat="server">
            <ul class="nobulletlist">
               <%-- <li class="padbottom"><span>User Name</span><br />
                    <asp:TextBox ID="tbUserName" runat="server" CssClass="textentry" TabIndex="1" />
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" 
                        ControlToValidate="tbUserName" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="User Name is required." ToolTip="User Name is required." 
                        ValidationGroup="vgRegister">*</asp:RequiredFieldValidator>
                </li>--%>
                <li class="padbottom"><span>Email</span><br />
                    <asp:TextBox ID="tbEmail" runat="server" CssClass="textentry" TabIndex="2" />
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" 
                        ControlToValidate="tbEmail" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="Invalid EMail, i.e.:  yourname@someplace.com" 
                        ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$" />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" 
                        ControlToValidate="tbEmail" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="E-mail is required." ToolTip="E-mail is required." 
                        ValidationGroup="vgRegister">*</asp:RequiredFieldValidator>
                    <br />
                    <sup class="subsuptext">youremail@emailprovider.com</sup>
                </li>
                <li class="padbottom"><span>Password</span><br />
                    <asp:TextBox ID="tbPassword" runat="server" CssClass="textentry" TabIndex="3" 
                        TextMode="Password" />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" 
                        ControlToValidate="tbPassword" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="Password is required." ToolTip="Password is required." 
                        ValidationGroup="vgRegister">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassword" runat="server" 
                        ControlToValidate="tbPassword" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="Invalid Password, use the guidlines provided below." 
                        ValidationExpression="(?=.*\d).{7,16}" />
						<!-- (?=.*[A-Za-z])(?=.*[0-9])(?=.*[*!.@])[A-Za-z0-9*!.]{7,16} -->
                    <br />
                    <sup class="subsuptext"><span>Minimum of &nbsp;
                    <%= Membership.MinRequiredPasswordLength %> &nbsp; characters in length, containing:
                    </span>
                    <br />
                    <span>at least one number </span>
                </li>
                <li class="padbottom"><span>Password Confirm</span><br />
                    <asp:TextBox ID="tbPasswordConfirm" runat="server" CssClass="textentry" 
                        TabIndex="4" TextMode="Password" />
                    <asp:RequiredFieldValidator ID="rfvPasswordConfirm" runat="server" 
                        ControlToValidate="tbPasswordConfirm" CssClass="failurenotification" 
                        Display="Dynamic" ErrorMessage="Confirm Password is required." 
                        ToolTip="Confirm Password is required." ValidationGroup="vgRegister">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvPassword" runat="server" 
                        ControlToCompare="tbPassword" ControlToValidate="tbPasswordConfirm" 
                        CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="The Password and Confirmation Password must match." 
                        ValidationGroup="vgRegister">*</asp:CompareValidator>
                </li>
            </ul>
        </asp:Panel>
        <asp:Panel ID="panelRegistercommand" runat="server" CssClass="padbottom" >
            <table>
                <tr>
                    <td>
                        <asp:Button ID="buttonRegister" runat="server" CssClass="buttondefault" 
                            TabIndex="5" Text="Create User" ValidationGroup="vgRegister" 
                            onclick="buttonRegister_Click" />
                    </td>
                    <td class="padleft">
                        <asp:Button ID="buttonCancel" runat="server" CssClass="buttondefault" ValidationGroup="vsCancel" 
                            TabIndex="6" Text="Cancel" onclick="buttonCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

</div>
</asp:Panel>
