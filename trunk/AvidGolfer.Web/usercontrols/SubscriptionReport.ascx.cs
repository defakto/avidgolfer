﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.NodeFactory;
using AvidGolfer.Data;
using AvidGolfer.Entities;
using AvidGolfer.Components;
using System.Data;
using System.IO;
using System.Text;

namespace AvidGolfer.Web.usercontrols
{
    public partial class SubscriptionReport : System.Web.UI.UserControl
    {

        protected int orderStatusId = Convert.ToInt32(ConfigurationManager.AppSettings["orderStatusId"]);
        protected int subStatusId = Convert.ToInt32(ConfigurationManager.AppSettings["subStatusId"]);
        protected int orderTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["orderTypeId"]);
        protected int regionTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["regionTypeId"]);
        protected int subCodeId = Convert.ToInt32(ConfigurationManager.AppSettings["subCodeId"]);
       
        protected void Page_Load(object sender, EventArgs e)
        {
            umbraco.BusinessLogic.User admin = umbraco.BusinessLogic.User.GetCurrent();

            if (admin == null)
            {
                Response.Redirect("/");
            }

            if (!Page.IsPostBack)
            {
                List<OrderItemHistory> oh = new List<OrderItemHistory>();
                rpReport.DataSource = oh;
                rpReport.DataBind();
                LoadRegionDropdown();
                LoadSubCodeDropdown();
                LoadSubStatusDropdown();
                LoadOrderStatusDropdown();
            }
               
           
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SupplementalContentManager scm = new SupplementalContentManager();
                List<OrderItemHistory> oh = new List<OrderItemHistory>();
                DataTable csvTable = new DataTable();
                string regionType = (ddlRegion.SelectedIndex == 0) ? String.Empty : ddlRegion.SelectedValue;
                string sku = (ddlSubCode.SelectedIndex == 0) ? String.Empty : ddlSubCode.SelectedValue;
                string orderstatus = (ddlOrderStatus.SelectedIndex == 0) ? String.Empty : ddlOrderStatus.SelectedValue;
                string subscriptionStatus = (ddlSubStatus.SelectedIndex == 0) ? String.Empty : ddlSubStatus.SelectedValue;

                oh = scm.OrderHistoryReport(regionType, sku, orderstatus, subscriptionStatus);
                csvTable = scm.OrderHistoryCsv(regionType, sku, orderstatus, subscriptionStatus);
                Session["dt"] = csvTable;

                rpReport.DataSource = oh;
                rpReport.DataBind();
            }
            catch
            {
            }
            
           
 
        }

        protected void LoadRegionDropdown()
        {
            Node regionType = new Node(regionTypeId);
           
            Nodes childNodes = regionType.Children;

            foreach (Node node in childNodes)
            {
                string nodeType = node.GetProperty("title").Value;
                if (nodeType != "All")
                {
                    ddlRegion.Items.Add(new ListItem(node.GetProperty("title").Value));
                }
                        
            }

            ddlRegion.Items.Insert(0, new ListItem("Select", "0"));

        }

        protected void LoadSubCodeDropdown()
        {
            Node subCode = new Node(subCodeId);

            Nodes childNodes = subCode.Children;

            foreach (Node node in childNodes)
            {
               ddlSubCode.Items.Add(new ListItem(node.GetProperty("title").Value));
               
            }

            ddlSubCode.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void LoadOrderStatusDropdown()
        {

            Node orderStatus = new Node(orderStatusId);

            Nodes childNodes = orderStatus.Children;

            foreach (Node node in childNodes)
            {
                ddlOrderStatus.Items.Add(new ListItem(node.GetProperty("title").Value));
            }

            ddlOrderStatus.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void LoadSubStatusDropdown()
        {

            Node subStatus = new Node(subStatusId);
            Nodes childNodes = subStatus.Children;

            foreach (Node node in childNodes)
            {
                ddlSubStatus.Items.Add(new ListItem(node.GetProperty("title").Value));
            }

            ddlSubStatus.Items.Insert(0, new ListItem("Select", "0"));
        }

       
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (rpReport.Items.Count != 0)
            {
                try
                {
               
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachement; filename={0}", "SubscriptionReport.csv"));
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/text";

                    DataTable dTable = Session["dt"] as DataTable;
                    int columnCount = dTable.Columns.Count;

                    StringBuilder sb = new StringBuilder();

                    if (columnCount != 0)
                    {
                        for (int i = 0; i < columnCount; i++)
                        {
                            sb.Append(dTable.Columns[i].ColumnName);
                            sb.Append(i == columnCount -1 ? "\n" : ",");
                        }

                        foreach (DataRow dr in dTable.Rows)
                        {
                            for (int i = 0; i < columnCount; i++)
                            {
                                        string item = dr[i].ToString();
                                        item = item.Replace("<br />", "");
                                        item = item.Replace("\r\n", "");
                                        item = item.Replace(",", "");
                                        item = item.Replace("\"", "");
                                        sb.Append(item);
                                sb.Append(i == columnCount -1 ? "\n" : ",");   

                            }


                        }
                        HttpContext.Current.Response.Output.Write(sb.ToString());
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                        //HttpContext.Current.ApplicationInstance.CompleteRequest();
                    } 
                }
                catch(Exception ex)
                {
                
                }

            }
           
        }

        protected void rpReport_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "OrderId")
            {
                string orderId = e.CommandArgument.ToString();
                string url = string.Format("~/order/manage.aspx?orderId={0}", orderId);
                Response.Redirect(url);

            }
        }
    }
}