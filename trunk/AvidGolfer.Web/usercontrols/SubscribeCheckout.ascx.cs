﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using AvidGolfer.Components;
using AvidGolfer.Entities;
using AuthorizeNet.Entities;
using umbraco.cms.businesslogic.member;

//using System.Web;


namespace AvidGolfer.Web.usercontrols
{
    public partial class SubscribeCheckout : System.Web.UI.UserControl
    {
        List<SelectList> creditcardtype;
        List<SelectList> expiremonth;
        List<SelectList> expireyear;
        List<SelectList> usstates;

        static string productid = "";
        static string quantity = "";
        static AuthorizeNetCustomer customer;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Member.IsLoggedOn())
                Response.Redirect("/");

            CheckoutInitialize();
        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(PageRedirectURL());
        }

        protected void buttonSummary_Click(object sender, EventArgs e)
        {
            PurchaseOrder();
        }

        protected void hlLogIn_Click(object sender, EventArgs e)
        {
            Login("login");
        }

        protected void hlRegister_Click(object sender, EventArgs e)
        {
            Login("register");
        }

        protected void hlSameBill_Click(object sender, EventArgs e)
        {
            PurchaseLoadShip();
            upShip.Update();
        }

        protected void lbCreditCard_Click(object sender, EventArgs e)
        {
            CCUpdate();
        }

        void ANCSave()
        {
            //customer = Session["ANCustomer"] as AuthorizeNetCustomer;

            //AuthorizeNetCustomer customer = new AuthorizeNetCustomer();
            Member member = Member.GetCurrentMember();
            customer = StoreManager.GetCustomerProfile(member);


            customer.PaymentInfo.BillingPersonInfo.FirstName = tbFirstName.Text;
            customer.PaymentInfo.BillingPersonInfo.LastName = tbLastName.Text;
            customer.PaymentInfo.BillingPersonInfo.Address = tbAddress.Text;
            customer.PaymentInfo.BillingPersonInfo.City = tbCity.Text;
            customer.PaymentInfo.BillingPersonInfo.State = ddlState.SelectedValue;
            customer.PaymentInfo.BillingPersonInfo.Zip = tbZip.Text;
            customer.PaymentInfo.BillingPersonInfo.PhoneNumber = tbPhone.Text;

            customer.PaymentInfo.BillingPersonInfo.EMail = tbEmail.Text;
            customer.Email = tbEmail.Text;

            customer.ShippingPersonInfo.FirstName = tbFirstNameS.Text;
            customer.ShippingPersonInfo.LastName = tbLastNameS.Text;
            customer.ShippingPersonInfo.Address = tbAddressS.Text;
            customer.ShippingPersonInfo.City = tbCityS.Text;
            customer.ShippingPersonInfo.State = ddlStateS.SelectedValue;
            customer.ShippingPersonInfo.Zip = tbZipS.Text;
            customer.ShippingPersonInfo.PhoneNumber = tbPhoneS.Text;

            customer.ShippingPersonInfo.EMail = tbEmailS.Text;


            if (string.IsNullOrEmpty(customer.Email))
                customer.Email = tbEmailS.Text;

            if ((panelCreditCard.Visible) || (tbCreditCard.Text.Length > 0))
            {
                customer.PaymentInfo.CreditCardNumber = tbCreditCard.Text;
                //string expiry = String.Format("{0}-{1}", ddlYear.SelectedValue, ddlMonth.SelectedValue);
                customer.PaymentInfo.Expiry = ddlYear.Text.Trim() + "-" + ddlMonth.Text.Substring(0, 2).Trim();
                customer.PaymentInfo.CardCode = tbCCV.Text;
            }

            
            try
            {
                /* prepopulate the completely unnecessary user table that has no real purpose other than to cause 
                 * headaches.
                 */
                User user = new User();
                user.userid = member.Id.ToString();
                user.firstname = customer.PaymentInfo.BillingPersonInfo.FirstName;
                user.lastname = customer.PaymentInfo.BillingPersonInfo.LastName;
                user.email = member.Email;
                user.phone = customer.PaymentInfo.BillingPersonInfo.PhoneNumber;
                // user.gender = member.getProperty("gender").Value.ToString();

                using (SupplementalContentManager scm = new SupplementalContentManager())
                    scm.CreateUser(user);

            }
            catch (Exception ex)
            {

            }

            //Session["ANCustomer"] = customer;

            StoreManager.SaveCustomerProfile(customer, Member.GetCurrentMember());
        }

        void CCType()
        {
            creditcardtype = new List<SelectList>();
            using (SupplementalEntities se = new SupplementalEntities())
                creditcardtype = se.ListCreditCardType();

            ddlCCT = DDLBind(ddlCCT, creditcardtype);
            ddlCCT.Width = new Unit("150px");
        }

        void CCUpdate()
        {
            panelCreditCardNo.Visible = false;
            panelCreditCard.Visible = true;
        }

        void CheckoutInitialize()
        {
            ControlInitialize();

            if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
                productid = Request.QueryString["productid"].Trim();

            if (!string.IsNullOrEmpty(Request.QueryString["quantity"]))
                quantity = Request.QueryString["quantity"].Trim();

            // ToDo: test packageid remove value on deploy
            if (productid.Equals(""))
                productid = "0";

            PurchaseInitialize();
        }

        void ControlInitialize()
        {
            //ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            //sm.RegisterAsyncPostBackControl(buttonSummary);
            //sm.RegisterAsyncPostBackControl(hlSameBill);
            //sm.RegisterAsyncPostBackControl(lbCreditCard);

            CCType();
            ExpireYear();
            ExpireMonth();
            USStates();

            // ToDo:  need to move webmethods to masterpage(sitemaster) check with MikeH.
            //tbZip.Attributes.Add("onblur", "javascript:ZipSearch('')");
            //tbZipS.Attributes.Add("onblur", "javascript:ZipSearch('S')");
            
                if (panelCreditCardNo.Visible)
                {
                    panelCreditCard.Visible = false;
                    panelCreditCardNo.Visible = true;
                }
                else
                {
                    panelCreditCardNo.Visible = false;
                    panelMessage.Visible = true;
                }  
        }

        void CustomerInfoLoad(AuthorizeNetCustomer customer)
        {
            if (string.IsNullOrEmpty(customer.PaymentInfo.CreditCardNumber))
                CCUpdate();
            else
                literalCreditCard.Text = customer.PaymentInfo.CreditCardNumber + "";

            tbFirstName.Text = customer.PaymentInfo.BillingPersonInfo.FirstName + "";
            tbLastName.Text = customer.PaymentInfo.BillingPersonInfo.LastName + "";
            tbAddress.Text = customer.PaymentInfo.BillingPersonInfo.Address + "";
            tbZip.Text = customer.PaymentInfo.BillingPersonInfo.Zip + "";
            tbZip4.Text = "";
            tbCity.Text = customer.PaymentInfo.BillingPersonInfo.City + "";

            try
            {
                ddlState.SelectedValue = customer.PaymentInfo.BillingPersonInfo.State + "";
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            tbPhone.Text = customer.PaymentInfo.BillingPersonInfo.PhoneNumber + "";

            tbEmail.Text = customer.PaymentInfo.BillingPersonInfo.EMail + "";
            if (string.IsNullOrEmpty(tbEmail.Text))
                tbEmail.Text = customer.Email;

            CustomerInfoLoadShip(customer.ShippingPersonInfo, tbEmail.Text);
        }

        void CustomerInfoLoad(Member member)
        {
            if (string.IsNullOrEmpty(customer.PaymentInfo.CreditCardNumber))
                CCUpdate();
            else
                literalCreditCard.Text = customer.PaymentInfo.CreditCardNumber + "";

            tbFirstName.Text = member.getProperty("firstname").Value + "";
            tbLastName.Text = member.getProperty("lastname").Value + "";
            tbAddress.Text = member.getProperty("address1").Value + "";
            tbZip.Text = member.getProperty("zip").Value + "";
            tbZip4.Text = member.getProperty("zip4").Value + "";
            tbCity.Text = member.getProperty("city").Value + "";

            try
            {
                ddlState.SelectedValue = member.getProperty("state").Value + "";
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            tbPhone.Text = member.getProperty("phone").Value + "";

            tbEmail.Text = member.Email + "";
        }

        void CustomerInfoLoadShip(AuthorizeNetPersonInfo customer, string email = "")
        {
            tbFirstNameS.Text = customer.FirstName + "";
            tbLastNameS.Text = customer.LastName + "";
            tbAddressS.Text = customer.Address + "";
            tbZipS.Text = customer.Zip + "";
            tbZip4S.Text = "";
            tbCityS.Text = customer.City + "";

            try
            {
                ddlStateS.SelectedValue = customer.State + "";
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            tbPhoneS.Text = customer.PhoneNumber + "";

            tbEmailS.Text = customer.EMail + "";
            if (string.IsNullOrEmpty(tbEmailS.Text))
                tbEmailS.Text = email;
        }

        DropDownList DDLBind(DropDownList ddl, List<SelectList> list)
        {
            ddl.DataSource = list;
            ddl.DataTextField = "display";
            ddl.DataValueField = "value";
            ddl.DataBind();

            ddl.SelectedIndex = -1;

            return ddl;
        }

        void ExpireMonth()
        {
            expiremonth = SessionList("ExpireMonth");
            ddlMonth = DDLBind(ddlMonth, expiremonth);
            ddlMonth.Width = new Unit("140px");
        }

        void ExpireYear()
        {
            expireyear = SessionList("ExpireYear");
            ddlYear = DDLBind(ddlYear, expireyear);
            ddlYear.Width = new Unit("70px");
        }

        void Login(string page)
        {
            Session["LoginForward"] = string.Format("~/Order.aspx?page=checkout&productid={0}", productid);
            Response.Redirect(string.Format("~/Login.aspx?page={0}", page));
        }

        string PageRedirectURL(string action = "")
        {
            string url = "~/"; // default to home page

            try
            {
                if ((!Session[action].Equals(null)) && (((string)Session[action]).Length > 0))
                {
                    url = Session[action] as string;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;

            }
            return url;
        }

        void PurchaseInitialize()
        {
            //string page = string.Format("~/Order.aspx?page=purchase&packageid={0}", productid);

            //Session["ProductID"] = productid;

            Member member = Member.GetCurrentMember();
            if (member.Equals(null) || (member.Id < 1))
                Login("login");

            bool isLoad = false;

            
                customer = StoreManager.GetCustomerProfile(member);
                
                //Session["ANCustomer"] = customer;
                CustomerInfoLoad(customer);
        }

        void PurchaseLoadShip()
        {
            tbFirstNameS.Text = tbFirstName.Text;
            tbLastNameS.Text = tbLastName.Text;
            tbAddressS.Text = tbAddress.Text;
            tbZipS.Text = tbZip.Text;
            tbZip4S.Text = tbZip4.Text;
            tbCityS.Text = tbCity.Text;

            try
            {
                ddlStateS.SelectedValue = ddlState.SelectedValue;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            tbEmailS.Text = tbEmail.Text;
            tbPhoneS.Text = tbPhone.Text;
        }

        void PurchaseOrder()
        {
            try
            {
                ANCSave();  // Need error handling here
            }
            catch (Exception ex)
            {
                ShowMessage(("Error: " + ex.Message.ToString().Replace("Error occurred in submitting to Authorize.Net:", "")));
                return;
            }
            string url = string.Format("~/Order.aspx?page=purchase&productid={0}&quantity={1}", productid, quantity);
            Response.Redirect(url);

            //Response.Redirect("~/Order.aspx?page=purchase");
        }

        List<SelectList> SessionList(string session)
        {
            bool isLoad = false;

            List<SelectList> list = Session[session] as List<SelectList>;
            try
            {
                if (!list.Equals(null))
                    isLoad = true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            if (!isLoad)
            {
                list = new List<SelectList>();
                using (SupplementalEntities se = new SupplementalEntities())
                    if (session.ToLower().IndexOf("month") > -1)
                        list = se.ListExpireMonth();
                    else if (session.ToLower().IndexOf("year") > -1)
                        list = se.ListExpireYear();
                    else if (session.ToLower().IndexOf("usstates") > -1)
                        list = se.USStates();
                    else
                    {
                    }

                Session[session] = list;
            }

            return list;
        }

        void ShowMessage(string messages)
        {
            //labelMessage.Visible = (messages != "" || css != "");
            panelMessage.Visible = true;
            panelCreditCard.Visible = true;
            panelCreditCardNo.Visible = false;
            labelmessages.InnerText = messages;
        }

        void USStates()
        {
            usstates = SessionList("USStates");
            ddlState = DDLBind(ddlState, usstates);
            ddlState.Width = new Unit("180px");

            List<SelectList> shipstates = usstates;

            ddlStateS = DDLBind(ddlStateS, shipstates);
            ddlStateS.Width = ddlState.Width;
        }

        // ToDo:  need to move webmethods to masterpage(sitemaster) check with MikeH.
        [System.Web.Services.WebMethod]
        public static string ZipLocationSelect(string zip)
        {
            using (SupplementalContentManager scm = new SupplementalContentManager())
                return scm.ZipLocationSelect(zip);
            
        }

        void TestLoad()
        {
            tbCreditCard.Text = "4222222222222";
            tbCCV.Text = "939";
            ddlCCT.SelectedIndex = 1;
            ddlMonth.SelectedIndex = 6;
            //Expiry = "2014-03"

            tbFirstName.Text = "Holly";
            tbLastName.Text = "Laveau";
            tbAddress.Text = "4517 Miami Drive";
            tbZip.Text = "75093";
            tbZip4.Text = "";
            tbCity.Text = "Plano";

            ddlState.SelectedValue = "TX";

            tbEmail.Text = customer.PaymentInfo.BillingPersonInfo.EMail;
            tbPhone.Text = "972-836-2677";

            tbFirstNameS.Text = "Holly";
            tbLastNameS.Text = "Laveau";
            tbAddressS.Text = "4517 Miami Drive";
            tbZipS.Text = "75093";
            tbZip4S.Text = "";

            tbCityS.Text = "Plano";
            ddlStateS.SelectedValue = "TX";

            tbEmailS.Text = customer.ShippingPersonInfo.EMail;
            tbPhoneS.Text = "972-836-2677";
        }

        protected void buttonTest_Click(object sender, EventArgs e)
        {
            TestLoad();
        }

        protected void btnCreditCancel_Click(object sender, EventArgs e)
        {
            panelCreditCardNo.Visible = true;
            panelCreditCard.Visible = false;
        }
    }
}