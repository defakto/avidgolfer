﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvidGolfer.Entities;
using AvidGolfer.Components;

namespace AvidGolfer.Web.usercontrols
{
    public partial class DailyDealForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRedeemOrder_Click(object sender, EventArgs e)
        {
            litErrMsg.Text = "";
            litErrMsg.Visible = false;

            if (txtEmail.Text.Trim() == "")
                litErrMsg.Text = "** Email Required";

            if (txtEmail.Text.ToLower().Trim() != txtConfirmEmail.Text.ToLower().Trim())
                litErrMsg.Text = "** The email you entered does not match.";

            if (litErrMsg.Text != "")
            {
                litErrMsg.Visible = true;
                return;
            }


            DailyDealOrder ddo = new DailyDealOrder();
            SupplementalContentManager scm = new SupplementalContentManager();

            ddo.FirstName = txtFirstName.Text;
            ddo.LastName = txtLastName.Text;
            ddo.Address1 = txtAddress1.Text;
            ddo.Address2 = txtAddress2.Text;
            ddo.City = txtCity.Text;
            ddo.State = selState.SelectedValue;
            ddo.ZipCode = txtZipCode.Text;
            ddo.Email = txtEmail.Text;
            ddo.CouponCode = txtCouponCode.Text;

            scm.SaveDailyDealOrder(ddo);
            
            DailyDealFormPage.Visible = false;
            DailyDealConfirmation.Visible = true;


        }
    }
}