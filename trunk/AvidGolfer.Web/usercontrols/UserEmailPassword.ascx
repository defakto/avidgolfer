﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserEmailPassword.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserEmailPassword" %>

<asp:Panel ID="pnlEmail" runat="server">
<div id="divPage" class="loginPage">
    <div class="padleft">
        <h1>Email Password</h1>
        <h2>
            Please enter your email address and click the send button.
        </h2>

        <div>
            <asp:Label ID="lblErr" CssClass="error" runat="server" Visible="false"></asp:Label>
            <asp:ValidationSummary ID="vsEmail" runat="server" CssClass="failurenotification" ForeColor="" ValidationGroup="vsEmail"/>
        </div>

        <asp:Panel ID="panelLogInform" runat="server">
            <ul class="nobulletlist">
                <li class="padbottom"><span>Email</span><br />
                    <asp:TextBox ID="tbEmail" runat="server" CssClass="textentry" TabIndex="2" />
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" 
                        ControlToValidate="tbEmail" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="Invalid EMail, i.e.:  yourname@someplace.com" 
                        ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$" />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" 
                        ControlToValidate="tbEmail" CssClass="failurenotification" Display="Dynamic" 
                        ErrorMessage="E-mail is required." ToolTip="E-mail is required." 
                        ValidationGroup="vsEmail">*</asp:RequiredFieldValidator>
                    <br />
                    <sup class="subsuptext">youremail@emailprovider.com</sup>
                </li>
            </ul>
        </asp:Panel>

        <asp:Panel ID="panelLogIncommand" runat="server" CssClass="padbottomtop" >
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnEmailPassword" runat="server" CssClass="buttondefault" 
                            TabIndex="5" Text="Send" ValidationGroup="vsEmail" onclick="btnEmailPassword_Click" 
                             />
                    </td>
                    <td class="padleft">
                        <asp:Button ID="buttonCancel" runat="server" CssClass="buttondefault" 
                            TabIndex="6" Text="Cancel" onclick="buttonCancel_Click"  />
                    </td>
                </tr>
            </table>
        </asp:Panel>



    </div>
</div>
</asp:Panel>
