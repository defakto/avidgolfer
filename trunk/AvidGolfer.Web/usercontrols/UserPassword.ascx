﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserPassword.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserPassword" %>

<asp:Panel ID="panelPassword" runat="server">
<div id="divPage" class="defaultPage">

    <div class="padleft">

        <h3>Change Password</h3>

        <p class="paratext">
            Use the form below to change password.
        </p>

        <div>
            <asp:Label ID="lblErr" CssClass="error" runat="server" Visible="false"></asp:Label>
            <asp:ValidationSummary ID="vsPassword" runat="server" CssClass="failurenotification" ForeColor="" ValidationGroup="vgPassword"/>
        </div>

        <asp:Panel ID="panelPasswordform" runat="server">
            <ul class="nobulletlist">
                <li class="padbottom">
                    Old Password<br />
                    <asp:TextBox ID="tbPasswordOld" runat="server" CssClass="textentry" TabIndex="2"  TextMode="Password"  />
                    <asp:RequiredFieldValidator ID="rfvPasswordNew" runat="server" 
                        ControlToValidate="tbPasswordOld" 
                        CssClass="failurenotification" 
                        Display="Dynamic" 
                        ErrorMessage="Old Password is required." 
                        ToolTip="Old Password is required." 
                        ValidationGroup="vgPassword">*</asp:RequiredFieldValidator>
                </li>
                <li class="padbottom">
                    Password<br />
                    <asp:TextBox ID="tbPassword" runat="server" CssClass="textentry" TabIndex="3" 
                        TextMode="Password" />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" 
                        ControlToValidate="tbPassword" 
                        CssClass="failurenotification" 
                        Display="Dynamic" 
                        ErrorMessage="New Password is required."
                        ToolTip="New Password is required." 
                        ValidationGroup="vgPassword">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassword" runat="server" 
                        ControlToValidate="tbPassword" 
                        CssClass="failurenotification" 
                        Display="Dynamic" 
                        ErrorMessage="Invalid Password, use the guidlines provided below." 
                        ValidationExpression="(?=.*\d).{7,16}" />
                    <br />
                    <sup class="subsuptext"><span>Minimum of &nbsp;
                    <%= Membership.MinRequiredPasswordLength %> &nbsp; characters in length, containing:
                    </span>
                    <br />
                    <span>at least one number </span>
                </li>
                <li class="padbottom">
                    Password Confirm<br />
                    <asp:TextBox ID="tbPasswordConfirm" runat="server" CssClass="textentry" 
                        TabIndex="4" TextMode="Password" />
                    <asp:RequiredFieldValidator ID="rfvPasswordConfirm" runat="server" 
                        ControlToValidate="tbPasswordConfirm" 
                        CssClass="failurenotification" 
                        Display="Dynamic" 
                        ErrorMessage="Confirm Password is required." 
                        ToolTip="Confirm Password is required." 
                        ValidationGroup="vgPassword">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvPassword" runat="server" 
                        ControlToCompare="tbPassword" 
                        ControlToValidate="tbPasswordConfirm" 
                        CssClass="failurenotification" 
                        Display="Dynamic" 
                        ErrorMessage="The Password and Confirmation Password must match." 
                        ValidationGroup="vgPassword">*</asp:CompareValidator>
                </li>
            </ul>
        </asp:Panel>
        <div class="clear"></div>
        <asp:Panel ID="panelPasswordcommand" runat="server" CssClass="padbottom" >
            <table>
                <tr>
                    <td>
                        <asp:Button ID="buttonPassword" runat="server" CssClass="buttondefault" 
                            TabIndex="5" Text="Save" 
                            ValidationGroup="vgPassword" 
                            onclick="buttonPassword_Click" 
                             />
                    </td>
                    <td class="padleft">
                        <asp:Button ID="buttonCancel" runat="server" 
                            CssClass="buttondefault" CausesValidation="false"
                            TabIndex="1" 
                            Text="Cancel" 
                            onclick="buttonCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

</div>
</asp:Panel>
