﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserConfirmation.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserConfirmation" %>

<asp:Panel ID="panelConfirmation" runat="server">
<div id="divPage" class="confirmationPage">

    <div class="padbottomleft">

        <h1>Thank You!</h1>

       <!-- Password changed Confirmation -->
        <asp:Panel ID="panelPassword" runat="server">
            <h2>Your Password has been successfully changed.</h2>
        </asp:Panel>

       <!-- Password recovery Confirmation -->
        <asp:Panel ID="panelPWRecover" runat="server">
            <h2>Your password have been sent to your email address!</h2>
        </asp:Panel>

       <!-- Profile create/modify Confirmation -->
        <asp:Panel ID="panelProfile" runat="server">
            <h2>Profile Saved.</h2>
        </asp:Panel>


         <!-- Registration Confirmation -->
        <asp:Panel ID="panelRegister" runat="server">
            <h2>User account has been successfully created.</h2>
        </asp:Panel>

        <!-- Subscription Confirmation -->
        <asp:Panel ID="panelSubscription" runat="server">
            <h2>Your Avid Golfer Magazine subscription order successfully completed.</h2>
            <h2>A confirmation email has been sent to your user account email address!</h2>
        </asp:Panel>

    </div>

    <br />
    <div class="padbottomtop">
        <asp:Button ID="buttonContinue" runat="server" 
            CssClass="buttondefault" 
            Text="Continue" 
            onclick="buttonContinue_Click" />
    </div>

</div>
</asp:Panel>
