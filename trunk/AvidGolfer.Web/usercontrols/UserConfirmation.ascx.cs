﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AvidGolfer.Web.usercontrols
{
    public partial class UserConfirmation : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ConfirmationInitialize();
            }
            
        }

        protected void buttonContinue_Click(object sender, EventArgs e)
        {
            ConfirmationRedirect();
        }

        protected void lbProfile_Click(object sender, EventArgs e)
        {
            string url = PageRedirectURL("LoginForward");
            Session["ProfileForward"] = url;
            Response.Redirect(url);
        }

        void ConfirmationInitialize()
        {
            panelPassword.Visible = false;
            panelProfile.Visible = false;
            panelPWRecover.Visible = false;
            panelRegister.Visible = false;
            panelSubscription.Visible = false;

            string panel = string.Empty;

            if (!string.IsNullOrEmpty(Request.QueryString["panel"]))
                panel = Request.QueryString["panel"].Trim().ToLower();

            if (panel.Equals("password"))
            {
                panelPassword.Visible = true;
            }
            else if (panel.Equals("profile"))
            {
                panelProfile.Visible = true;
            }
            else if (panel.Equals("recover"))
            {
                panelPWRecover.Visible = true;
            }
            else if (panel.Equals("register"))
            {
                panelRegister.Visible = true;
            }
            else if (panel.Equals("subscription"))
            {
                panelSubscription.Visible = true;
            }
            else
            {
            }

        }

        void ConfirmationRedirect()
        {
            string url = string.Empty;

            if (panelProfile.Visible)
            {
                url = "ProfileForward";
            }
            else if (panelRegister.Visible)
            {
                url = "LoginForward";
            }
            else if (panelSubscription.Visible)
            {
            }
            else
            {
                // default to home
                //else if (panelPassword.Visible)
                //else if (panelPWRecover.Visible)
                //else if (panelSubscription.Visible)

            }

            url = PageRedirectURL(url);
            Response.Redirect(url);

        }

        string PageRedirectURL(string action = "")
        {
            string url = "~/"; // default to home page
            string forward = string.Empty;

            try
            {
                forward = (string)Session[action] + "";
                if (!forward.Equals(""))
                {
                    url = forward;
                    Session.Remove(action);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                error = string.Empty;
            }

            return url;
        }
    }
}