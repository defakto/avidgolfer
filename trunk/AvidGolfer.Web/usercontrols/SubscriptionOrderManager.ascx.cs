﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvidGolfer.Components;
using AvidGolfer.Entities;
using AvidGolfer.Data;
using System.Text;
using umbraco.NodeFactory;
using System.Net;
using System.Data;

namespace AvidGolfer.Web.usercontrols
{
    public partial class SubscriptionOrderManager : System.Web.UI.UserControl
    {

        protected int orderTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["orderTypeId"]);
        protected int orderStatusId = Convert.ToInt32(ConfigurationManager.AppSettings["orderStatusId"]);
        protected int subStatusId = Convert.ToInt32(ConfigurationManager.AppSettings["subStatusId"]);
        protected int regionTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["regionTypeId"]);
        protected string persistPopup = String.Empty;

        //protected void OnInt(EventArgs e)
        //{
        //    base.OnInit(e);
            
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            umbraco.BusinessLogic.User admin = umbraco.BusinessLogic.User.GetCurrent();

            if (admin == null)
            {
                Response.Redirect("/");
            }

            if (!Page.IsPostBack)
            {
                string orderId = Request.QueryString["orderId"];
                persistPopup = String.Empty;
                pnlSearchResults.Visible = false;
                LoadSubscriptionStatusDropdown();
                LoadOrderStatusDropdown();
                LoadRegionTypeDropdown();

                if (orderId != null)
                {
                    txtOrderId.Text = orderId;
                    RetrieveItems();
                }
            }

            
        }

        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            rpUsers.DataSource = null;
            rpUsers.DataSourceID = null;
            rpUsers.DataBind();
            CurrentPage = 0;
            RetrieveItems();
        }

        protected void rpOrderItems_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                persistPopup = "edititem";
                long orderItemId = Convert.ToInt64(e.CommandArgument);
                populatePopup(orderItemId);
            }
           
        }

        protected void populatePopup(long itemId)
        {

            SupplementalContentManager scm = new SupplementalContentManager();
            OrderItem or = new OrderItem();
            or = scm.GetOrderItemById(itemId);
            lititemId.Text = or.orderitemid.ToString();
            hdnItemId.Value = or.orderitemid.ToString();
            ltProductId.Text = or.productid.ToString();
            ltOrderId.Text = or.orderid.ToString();
            ltQuantity.Text = or.quantity.ToString();
            txtPrice.Text = or.price.ToString();
            ltTax.Text = string.Format("{0:P2}", or.tax);
            string startDate = (or.startdate != null ? or.startdate.Value.ToString("M/dd/yyyy") : "");
            string expireDate = (or.expiredate != null ? or.expiredate.Value.ToString("M/dd/yyyy") : "");
            string shipDate = (or.shipdate != null ? or.shipdate.Value.ToString("M/dd/yyyy") : "");
            string createDate = (or.createdate != null ? or.createdate.Value.ToString("M/dd/yy h:mm tt") : "");
            string reviewDate = (or.reviewdate != null ? or.reviewdate.Value.ToString("M/dd/yy") : "");
            string notes = or.notes;
            notes = notes.Replace("<br />", "\r\n");
            txtProductName.Text = or.productname;
            string regionType = or.regionType;
            txtSku.Text = or.sku;
            string orderStatus = or.orderStatus;
            string subscriptionStatus = or.subscriptionStatus;
            ltTotal.Text = string.Format("{0:C}", or.total);
            ltModifyDate.Text = (or.modifydate != null ? or.modifydate.Value.ToString("M/dd/yy h:mm tt") : "");
            txtExpireDate.Text = expireDate;
            ltCreateDate.Text = createDate;
            txtStartDate.Text = startDate;
            txtShipDate.Text = shipDate;
            txtReviewDate.Text = reviewDate;

            Session["OldValue"] = orderStatus;

            if (!String.IsNullOrEmpty(notes))
                txtNotes.Text = Server.HtmlDecode(notes);

            if (!String.IsNullOrEmpty(orderStatus))
                ddlOrderStatus.SelectedValue = orderStatus;

            if (!String.IsNullOrEmpty(subscriptionStatus))
                ddlSubscriptionStatus.SelectedValue = subscriptionStatus;

            if (!String.IsNullOrEmpty(regionType))
                ddlRegionType.SelectedValue = regionType;
    
        }

        protected void LoadSubscriptionStatusDropdown()
        {
            

            Node subStatus = new Node(subStatusId);

            Nodes childNodes = subStatus.Children;

            foreach (Node node in childNodes)
            {
                ddlSubscriptionStatus.Items.Add(new ListItem(node.GetProperty("title").Value));
            }

            ddlSubscriptionStatus.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void LoadOrderStatusDropdown()
        {
            Node orderStatus = new Node(orderStatusId);
            Nodes childNodes = orderStatus.Children;

            foreach (Node node in childNodes)
                ddlOrderStatus.Items.Add(new ListItem(node.GetProperty("title").Value));

            ddlOrderStatus.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void LoadRegionTypeDropdown()
        {
            Node regionType = new Node(regionTypeId);
            Nodes childNodes = regionType.Children;

            foreach (Node node in childNodes)
                ddlRegionType.Items.Add(new ListItem(node.GetProperty("title").Value));

            ddlRegionType.Items.Insert(0, new ListItem("Select", "0"));
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            lititemId.Text = "";
            ltOrderId.Text = "";
            ltProductId.Text = "";
            ltQuantity.Text = "";
            txtPrice.Text = "";
            ltTax.Text = "";
            ltTotal.Text = "";
            txtNotes.Text = "";
            txtStartDate.Text = "";
            txtExpireDate.Text = "";
            txtShipDate.Text = "";
            ltCreateDate.Text = "";
            ltModifyDate.Text = "";
            txtReviewDate.Text = "";
            txtProductName.Text = "";
            ddlRegionType.SelectedIndex = 0;
            txtSku.Text = "";
            ddlOrderStatus.SelectedIndex = 0;
            ddlSubscriptionStatus.SelectedIndex = 0;
            persistPopup = String.Empty;
            reloadRepeater();
        }

        protected void btnRecord_Click(object sender, EventArgs e)
        {
            SupplementalContentManager scm = new SupplementalContentManager();
            long itemId = Convert.ToInt64(hdnItemId.Value);
            OrderItem or = new OrderItem();
            or = scm.GetOrderItemById(itemId);
           
            or.modifydate = DateTime.Now;
            string notes = txtNotes.Text;
            notes = notes.Replace("\r\n", "<br />");
            or.notes = notes;

            decimal totalprice = 0;
            if (txtPrice.Text != "")
            {
                Decimal.TryParse(txtPrice.Text, out totalprice);
                or.price = totalprice;
            }

            if (txtProductName.Text != "")
                or.productname = txtProductName.Text;

            or.sku = (txtSku.Text != "") ? txtSku.Text : or.sku;

            string newValue = ddlOrderStatus.SelectedValue;

            or.shipdate   = (txtShipDate.Text != ""  )? Convert.ToDateTime(txtShipDate.Text)   : or.shipdate;
            or.startdate  = (txtStartDate.Text != "" )? Convert.ToDateTime(txtStartDate.Text)  : or.startdate;
            or.expiredate = (txtExpireDate.Text != "")? Convert.ToDateTime(txtExpireDate.Text) : or.expiredate;
            or.reviewdate = (txtReviewDate.Text != "")? Convert.ToDateTime(txtReviewDate.Text) : or.reviewdate;

            if(ddlOrderStatus.SelectedValue != "0")
                or.orderStatus = ddlOrderStatus.SelectedValue;

            if(ddlSubscriptionStatus.SelectedValue != "0")
                 or.subscriptionStatus = ddlSubscriptionStatus.SelectedValue;

            if (ddlRegionType.SelectedValue != "0")
                or.regionType = ddlRegionType.SelectedValue;

            scm.SaveIndividualOrderItem(or);

            string oldValue = Session["OldValue"].ToString();

            if (oldValue.Equals("Active") && (newValue.Equals("Cancelled") || newValue.Equals("Expired")))
            {
                long orderId = or.orderid;
                string region = or.regionType;
                scm.UpdateOrderItemActive(orderId, region);

            }

            reloadRepeater();
        }

        protected void reloadRepeater()
        {

            pnlSearchNoResults.Visible = false;
            pnlSearchResults.Visible = false;

            var rList = (List<Order>)Session["orderList"];
            SupplementalContentManager scm = new SupplementalContentManager();
           
            pnlSearchNoResults.Visible = (rList.Count == 0);
            pnlSearchResults.Visible = (rList.Count != 0);

            foreach (Order order in rList)
            {
                OrderType = order.ordertype;
                Notes = order.notes;
            }

            rpOrder.DataSource = rList;
            rpOrder.DataBind();

            foreach (RepeaterItem rpItem in rpOrder.Items)
            {
                long orderId = System.Convert.ToInt64(((HiddenField)rpItem.FindControl("hdnOrderId")).Value);

                List<OrderItem> oi = new List<OrderItem>();

                oi = scm.GetOrderItemsByOrderId(orderId);
                ((Repeater)(rpItem.FindControl("rpOrderItems"))).DataSource = oi;
                ((Repeater)(rpItem.FindControl("rpOrderItems"))).DataBind();

            }
        }

        protected void rpOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                SupplementalEntities se = new SupplementalEntities();
                List<SelectList> states = se.USStates();

                DropDownList orderType = ((DropDownList)e.Item.FindControl("dlOrderType"));
                DropDownList billStates = ((DropDownList)e.Item.FindControl("ddlBillState"));
                DropDownList shipStates = ((DropDownList)e.Item.FindControl("ddlShipState"));
                TextBox ltNotes = (TextBox)e.Item.FindControl("ltNotes");

                Node orderStatus = new Node(orderTypeId);

                Nodes childNodes = orderStatus.Children;

                foreach (Node node in childNodes)
                {
                    orderType.Items.Add(new ListItem(node.GetProperty("title").Value));
                }

                if (!string.IsNullOrEmpty(OrderType))
                    orderType.SelectedValue = OrderType;

                if (!string.IsNullOrEmpty(Notes))
                {
                    string note = Notes;
                    note = note.Replace("<br />", "\r\n");
                    ltNotes.Text = note;
                }   
            }
        }

        protected void rpOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("Update"))
            {
                SupplementalContentManager scm = new SupplementalContentManager();

                long orderId = Convert.ToInt64(e.CommandArgument);
                Order or = new Order();

                TextBox txtBillStreet = (TextBox)e.Item.FindControl("txtBillStreet");
                TextBox txtBillZip = (TextBox)e.Item.FindControl("txtBillZip");
                TextBox txtBillPhone = (TextBox)e.Item.FindControl("txtBillPhone");
                TextBox txtBillEmail = (TextBox)e.Item.FindControl("txtBillEmail");
                TextBox txtShipStreet = (TextBox)e.Item.FindControl("txtShippingStreet");
                TextBox txtShipZip = (TextBox)e.Item.FindControl("txtShippingZip");
                TextBox txtShipPhone = (TextBox)e.Item.FindControl("txtShippingPhone");
                TextBox txtShipEmail = (TextBox)e.Item.FindControl("txtShippingEmail");
                TextBox txtAmount = (TextBox)e.Item.FindControl("txtShippingAmount");
                TextBox ltNotes = (TextBox)e.Item.FindControl("ltNotes");
                DropDownList dlOrder = (DropDownList)e.Item.FindControl("dlOrderType");

                or.orderid = orderId;
                
                or.notes = ltNotes.Text;
                or.ordertype = dlOrder.SelectedValue;
                or.billstreet = txtBillStreet.Text; 
                or.billzip = txtBillZip.Text;
                or.billphone = txtBillPhone.Text;
                or.billemail = txtBillEmail.Text;
                or.shipstreet = txtShipStreet.Text;
                or.shipzip = txtShipZip.Text;
                or.shipphone = txtShipPhone.Text;
                or.shipemail = txtShipEmail.Text;
                or.shippingamount = Convert.ToDecimal(txtAmount.Text);

                scm.OrderSave(or);

                updateRepeater(orderId);

            }                 
        }

        protected void updateRepeater(long Id)
        {
            List<Order> or = new List<Order>();

            pnlSearchNoResults.Visible = false;
            pnlSearchResults.Visible = false;

            SupplementalContentManager scm = new SupplementalContentManager();

            try
            {
                or = scm.GetOrders(orderId:Id);
            }
            catch (Exception ex)
            {
            }

            foreach (Order order in or)
            {
                OrderType = order.ordertype;
                Notes = order.notes;
            }
            Session["orderList"] = or;
            pnlSearchNoResults.Visible = (or.Count == 0);
            pnlSearchResults.Visible = (or.Count != 0);

            rpOrder.DataSource = or;
            rpOrder.DataBind();

            foreach (RepeaterItem rpItem in rpOrder.Items)
            {
                long orderId = System.Convert.ToInt64(((HiddenField)rpItem.FindControl("hdnOrderId")).Value);

                List<OrderItem> oi = new List<OrderItem>();

                oi = scm.GetOrderItemsByOrderId(orderId);
                ((Repeater)(rpItem.FindControl("rpOrderItems"))).DataSource = oi;
                ((Repeater)(rpItem.FindControl("rpOrderItems"))).DataBind();

            }
        }

        private string orderType;
        private string OrderType
        {
            get { return orderType; }
            set { orderType = value; }
        }

        private string notes;
        private string Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        protected void rpUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("Order"))
            {
                long orderId = Convert.ToInt64(e.CommandArgument.ToString());
                DisplayDetails(orderId);
            }
        }

        private void DisplayDetails(long orderId)
        {
            SupplementalContentManager scm = new SupplementalContentManager();
            List<Order> or = new List<Order>();
            
            try
            {
                or = scm.GetOrders(orderId: orderId);
            }
            catch (Exception ex)
            {
            }

            foreach (Order order in or)
            {
                OrderType = order.ordertype;
                Notes = order.notes;
            }
            Session["orderList"] = or;
            pnlSearchNoResults.Visible = (or.Count == 0);
            pnlSearchResults.Visible = (or.Count != 0);

            rpOrder.DataSource = or;
            rpOrder.DataBind();

            foreach (RepeaterItem rpItem in rpOrder.Items)
            {
                long oId = System.Convert.ToInt64(((HiddenField)rpItem.FindControl("hdnOrderId")).Value);

                List<OrderItem> oi = new List<OrderItem>();

                oi = scm.GetOrderItemsByOrderId(oId);
                ((Repeater)(rpItem.FindControl("rpOrderItems"))).DataSource = oi;
                ((Repeater)(rpItem.FindControl("rpOrderItems"))).DataBind();

            }
        }

        protected void RetrieveItems()
        {
            List<Order> or = new List<Order>();
            DataSet data = new DataSet();
            string orderString = txtOrderId.Text.Trim();
            long orderNum = 0;
            string lastName = txtLastSearch.Text.Trim();
            string zip = txtZipSearch.Text.Trim();
            string email = txtEmailSearch.Text.Trim();
            string phone = txtPhoneSearch.Text.Trim();

            if (!string.IsNullOrEmpty(orderString))
            {
                if (long.TryParse(orderString, out orderNum))
                {
                    orderNum = Convert.ToInt64(orderString);
                }
            }


            pnlSearchNoResults.Visible = false;
            pnlSearchResults.Visible = false;

            SupplementalContentManager scm = new SupplementalContentManager();

            try
            {
                data = scm.GetOrderDataset(orderNum, lastName, zip, email, phone);
            }
            catch (Exception ex)
            {
            }
            // paged data source
            PagedDataSource pg = new PagedDataSource();
            pg.DataSource = data.Tables[0].DefaultView;
            pg.AllowPaging = true;
            pg.PageSize = 2;
            pg.CurrentPageIndex = CurrentPage;
            lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of &nbsp;" + pg.PageCount.ToString();
            cmdPrev.Enabled = !pg.IsFirstPage;
            cmdNext.Enabled = !pg.IsLastPage;
            rpUsers.DataSource = pg;
            rpUsers.DataBind();
            pnlSearchNoResults.Visible = (data.Tables[0].Rows.Count == 0);
            pnlOrders.Visible = (data.Tables[0].Rows.Count != 0);
        }


        public int CurrentPage
        {
            get
            {
                // look for current page in view state
                object o = this.ViewState["_CurrentPage"];

                if (o == null)
                    return 0; // default page index of zero
                else
                    return (int)o;

            }
            set
            {
                this.ViewState["_CurrentPage"] = value;
            }
        }

        protected void cmdPrev_Click(object sender, EventArgs e)
        {
            // Set view state variable to the previous page
            CurrentPage -= 1;

            // reload control
            RetrieveItems();
        }

        protected void cmdNext_Click(object sender, EventArgs e)
        {
            // Set view state variable to the next page
            CurrentPage += 1;

            // Reload Control
            RetrieveItems();
        }
    }
}