﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvidGolfer.Entities
{
    public class OrderItemHistory
    {
        private OrderStatus _orderStatus = OrderStatus.Open;


        public int itemstatus
        {
            get
            {
                return (int)_orderStatus;
            }
            set
            {
                _orderStatus = (OrderStatus)value;
            }
        }

        public long orderitemid { get; set; }
        public long orderid { get; set; }
        public int itemid { get; set; }
        public int productid { get; set; }
        public int quantity { get; set; }

        public string productname { get; set; }

        public decimal price { get; set; }
        public decimal tax { get; set; }
        public decimal total { get; set; }

        public DateTime? startdate { get; set; }
        public DateTime? expiredate { get; set; }
        public DateTime? shipdate { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
        public DateTime? reviewdate { get; set; }
        public string ordernotes { get; set; }
        public string ordertype { get; set; }
        public string sku { get; set; }
        public string orderstatus { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string itemnotes { get; set; }
        public int userid
        {
            get;
            set;
        }

        public long transactionid
        {
            get;
            set;
        }

        public int billaddressid { get; set; }

        public string billstreet
        {
            get;
            set;
        }

        public string billzip
        {
            get;
            set;
        }

        public string billstate { get; set; }
        public string billcity { get; set; }

        public string billphone
        {
            get;
            set;
        }

        public string billemail
        {
            get;
            set;
        }

        public int shipaddressid { get; set; }

        public string shipstreet
        {
            get;
            set;
        }

        public string shipzip
        {
            get;
            set;
        }

        public string shipstate { get; set; }

        public string shipcity { get; set; }

        public string shipphone { get; set; }

        public string shipemail
        {
            get;
            set;
        }

        public string regiontype { get; set; }
        public string subscriptionstatus { get; set; }

    }
}
