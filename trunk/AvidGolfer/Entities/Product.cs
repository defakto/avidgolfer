﻿using System;
using System.Collections.Generic;

namespace AvidGolfer.Entities
{
    [Serializable]
    public class Product
    {
        public int defaktoid { get; set; }
        public int categoryid { get; set; }
        public int productid { get; set; }
        public string sku { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime availabledate { get; set; }
        public DateTime expiredate { get; set; }
        public decimal price { get; set; }
        public decimal tax { get; set; }
        public string imageurl { get; set; }
        public DateTime createdate { get; set; }
        public DateTime modifydate { get; set; }
        public DateTime reviewdate { get; set; }
        public string notes { get; set; }

        List<OrderItem> items;
    }
}
