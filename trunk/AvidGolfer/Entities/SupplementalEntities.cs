﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

using AvidGolfer.Components;

//using System.Linq;
//using System.Text;

namespace AvidGolfer.Entities
{
    [Serializable]
    public partial class SupplementalEntities : IDisposable
    {
        public List<SelectList> ListCreditCardType()
        {
            List<SelectList> card = new List<SelectList>() {
                new SelectList( name:"", display:"", value:""),
                new SelectList( name:"Visa", display:"Visa", value:"Visa"),
                new SelectList( name:"Master Card", display:"MC", value:"MC"),
                new SelectList( name:"Discover", display:"Discover", value:"Discover"),
                new SelectList( name:"American Express", display:"AmEx", value:"AmEx"),
                new SelectList( name:"Other", display:"Other", value:"Other"),
            };

            return card;
        }

        public List<SelectList> ListExpireMonth()
        {
            List<SelectList> expire = new List<SelectList>() {
                new SelectList( name:"01 - January", display:"01 - January", value:"01"),
                new SelectList( name:"02 - February", display:"02 - February", value:"02"),
                new SelectList( name:"03 - March", display:"03 - March", value:"03"),
                new SelectList( name:"04 - April", display:"04 - April", value:"04"),
                new SelectList( name:"05 - May", display:"05 - May", value:"05"),
                new SelectList( name:"06 - June", display:"06 - June", value:"06"),
                new SelectList( name:"07 - July", display:"07 - July", value:"07"),
                new SelectList( name:"08 - August", display:"08 - August", value:"08"),
                new SelectList( name:"09 - September", display:"09 - September", value:"09"),
                new SelectList( name:"10 - October", display:"10 - October", value:"10"),
                new SelectList( name:"11 - November", display:"11 - November", value:"11"),
                new SelectList( name:"12 - December", display:"12 - December", value:"12"),
            };

            return expire;
        }

        public List<SelectList> ListExpireYear()
        {
            List<SelectList> expireyear = new List<SelectList>();

            int year = DateTime.Now.Year;
            for (int i = year; i < (year + 10); ++i)
                expireyear.Add(new SelectList(name: i.ToString(), display: i.ToString(), value: i.ToString()));

            return expireyear;
        }

        public List<SelectList> ListSearch()
        {
            List<SelectList> search = new List<SelectList>() {
                new SelectList( name:"User", display:"UserID", value:"userid"),
                new SelectList( name:"EMail", display:"EMail Address", value:"email"),
                new SelectList( name:"Order", display:"Order Number", value:"orderid"),
            };

            return search;
        }

        
        public List<SelectList> ListStatus()
        {
            List<SelectList> status = new List<SelectList>();
            using (SupplementalContentManager scm = new SupplementalContentManager())
                status = scm.USStates();

            return status;
        }

        public List<SelectList> USStates()
        {
            List<SelectList> states = new List<SelectList>();
            using (SupplementalContentManager scm = new SupplementalContentManager())
                states = scm.USStates();
            return states;
        }
         

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////
    }

    [Serializable]
    public class SelectList
    {
        public SelectList()
        {
        }

        public SelectList(string name = "", string display = "", object value = null)
        {
            _name = name;
            _display = display;
            _value = value;

        }

        string _name = "";
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        string _display = "";
        public string display
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        object _value = "";
        public object value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

    }

}
