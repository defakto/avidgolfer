﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Defakto.Components.Provider;

namespace AvidGolfer.Entities
{
    public class MostViewed
    {
        #region private members

        private string _nodeId = "";
        private int _totalHits = 0;
        private string _region = "";

        #endregion

        #region public properties

        public string NodeId
        {
            get { return _nodeId; }
            set { _nodeId = value; }
        }

        public int TotalHits
        {
            get { return _totalHits; }
            set { _totalHits = value; }
        }

        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        #endregion

        #region constructor

        public MostViewed()
        {
        }

        #endregion

        public void IncrementHitCounter()
        {
            _totalHits += 1;
        }
    }
}
