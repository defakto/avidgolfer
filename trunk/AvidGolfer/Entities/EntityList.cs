﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvidGolfer.Entities
{
    public class EntityList
    {
        public string name
        {
            get;
            set;
        }

        public string display
        {
            get;
            set;
        }

        public string value
        {
            get;
            set;
        }
            
    }
}
