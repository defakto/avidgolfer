﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using AvidGolfer.Data;


//using System.Linq;
//using System.Text;

namespace AvidGolfer.Entities
{
    public enum OrderStatus
    {
        Open = 10,
        Pending = 20,
        Purchased = 30,
        Suspended = 40,
        Closed = 50,
        Active = 60, 
        Expired = 70,
        PassbookMailed = 80,
        Cancelled = 99
    }

    public enum SubscriptionStatus 
    {
        Pending = 10,
        Active = 20,
        Cancelled = 30,
        Expired = 40
    }

    [Serializable]
    public class Order
    {
        private OrderStatus _orderStatus = OrderStatus.Open;
        private SubscriptionStatus _subscriptionStatus = SubscriptionStatus.Active;
         
        #region Properties

        [Display(Name = "Status")]
        public int status
        {
            get
            {
                return (int)_orderStatus;
            }
            set
            {
                _orderStatus = (OrderStatus)value;
            }
        }

        [Display(Name = "Subscription")]
        public int subscription
        {
            get
            {
                return (int)_subscriptionStatus;
            }
            set
            {
                _subscriptionStatus = (SubscriptionStatus)value;
            }
        }

        [Display(Name = "Order")]
        public long orderid
        {
            get;
            set;
        }

        [Required]
        [Display(Name = "User")]
        public int userid
        {
            get;
            set;
        }

        [Display(Name = "Tansaction")]
        public long tansactionid
        {
            get;
            set;
        }

        public int billaddressid { get; set; }
        [Display(Name = "Bill Street")]
        public string billstreet
        {
            get;
            set;
        }
        [Display(Name = "Bill Zip")]
        public string billzip
        {
            get;
            set;
        }
        [Display(Name = "Bill Phone")]
        public string billphone
        {
            get;
            set;
        }
        [Display(Name = "Bill EMal")]
        public string billemail
        {
            get;
            set;
        }

        public string billcity
        {
            get;
            set;
        }

        public string billstate
        {
            get;
            set;
        }

        public int shipaddressid { get; set; }
        [Display(Name = "Ship Street")]
        public string shipstreet
        {
            get;
            set;
        }
        [Display(Name = "Ship Zip")]
        public string shipzip
        {
            get;
            set;
        }
        [Display(Name = "Ship Phone")]
        public string shipphone { get; set; }
        [Display(Name = "Ship EMal")]
        public string shipemail
        {
            get;
            set;
        }

        public string shipcity
        {
            get;
            set;
        }

        public string shipstate
        {
            get;
            set;
        }

        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
        public DateTime? reviewdate { get; set; }

        [Display(Name = "Note(s)")]
        public string notes
        {
            get;
            set;
        }

        public decimal shippingamount { get; set; }

        public string ordertype { get; set; }

        public string firstname { get; set; }
        public string lastname { get; set; }
        public string membershipemail { get; set; }

        public List<OrderItem> orderitems = new List<OrderItem>();


        #endregion
    }

}
