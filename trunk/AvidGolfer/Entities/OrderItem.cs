﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvidGolfer.Entities
{
    [Serializable]
    public class OrderItem
    {

        private OrderStatus _orderStatus = OrderStatus.Open;

        #region Properties

        public int itemstatus
        {
            get
            {
                return (int)_orderStatus;
            }
            set
            {
                _orderStatus = (OrderStatus)value;
            }
        }

        public long orderitemid { get; set; }
        public long orderid { get; set; }
        public int itemid { get; set; }
        public int productid { get; set; }
        public int quantity { get; set; }

        public string productname { get; set; }

        public decimal price { get; set; }
        public decimal tax { get; set; }
        public decimal total { get; set; }

        public DateTime? startdate { get; set; }
        public DateTime? expiredate { get; set; }
        public DateTime? shipdate { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
        public DateTime? reviewdate { get; set; }
        public int status { get; set; }
        public string notes { get; set; }
        public string sku { get; set; }
        public string orderStatus { get; set; }
        public string subscriptionStatus { get; set; }
        public string regionType { get; set; }

        #endregion
    }

}
