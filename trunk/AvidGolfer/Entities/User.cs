﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvidGolfer.Entities
{
    [Serializable]
    public class User : IDisposable
    {

        #region private members

        private bool _isMember = false;

        private string _username = "";
        private string _userid = "";

        private string _passWord = "";
        private string _passrecoverquestion = Guid.NewGuid().ToString();
        private string _passrecoveranswer = Guid.NewGuid().ToString();
        private string _email = "";
        private string _address1 = "";
        private string _address2 = "";
        private string _phone = "";
        private string _city = "";
        private string _state = "";
        private string _zip = "";
        private string _county = "";
        private bool _accountArchived = false;
        private string _accountNotes = "";


        //HES 2013.02.17
        private string _lastname = "";
        private string _firstname = "";
        private string _zip4 = "";
        private string _gender = "";
        private string _ancid = "";

        #endregion

        #region public properties

        public string userid
        {
            get
            {
                return _userid;
            }
            set
            {
                _userid = value;
            }
        }

        public string username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

        public string password
        {
            get { return _passWord; }
            set { _passWord = value; }
        }

        public string PassWordRecoveryQuestion
        {
            get { return _passrecoverquestion; }
            set { _passrecoverquestion = value; }
        }

        public string PassWordRecoveryAnswer
        {
            get { return _passrecoveranswer; }
            set { _passrecoveranswer = value; }
        }

        public string email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        public string address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        public string phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public string city
        {
            get { return _city; }
            set { _city = value; }
        }

        public string state
        {
            get { return _state; }
            set { _state = value; }
        }

        public string Zip
        {
            get{ return _zip; }
            set { _zip = value; }
        }

        public string county
        {
            get { return _county; }
            set { _county = value; }
        }

        public string lastname
        {
            get
            {
                return _lastname;
            }
            set
            {
                _lastname = value;
            }
        }

        public string firstname
        {
            get
            {
                return _firstname;
            }
            set
            {
                _firstname = value;
            }
        }

        public string zip4
        {
            get
            {
                return _zip4;
            }
            set
            {
                _zip4 = value;
            }
        }

        public string gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }

        public string ancid
        {
            get
            {
                return _ancid;
            }
            set
            {
                _ancid = value;
            }
        }

        public bool isMember
        {
            get
            {
                return _isMember;
            }
            set
            {
                _isMember = value;
            }
        }
        public bool accountArchived
        {
            get
            {
                return _accountArchived;
            }
            set
            {
                _accountArchived = value;
            }
        }
        public string accountNotes
        {
            get
            {
                return _accountNotes;
            }
            set
            {
                _accountNotes = value;
            }
        }

        
        #endregion

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////


    }
}

