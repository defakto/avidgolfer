﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvidGolfer.Entities
{
    public class DailyDealOrder
    {
        #region private members

        private Guid _orderId = Guid.NewGuid();
        private string _firstName = String.Empty;
        private string _lastName = String.Empty;
        private string _address1 = String.Empty;
        private string _address2 = String.Empty;
        private string _city = String.Empty;
        private string _state = String.Empty;
        private string _zipCode = String.Empty;
        private string _email = String.Empty;
        private string _couponCode = String.Empty;
        private DateTime _dateCreated = DateTime.Now;

        #endregion


        #region public properties

        public Guid OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string CouponCode
        {
            get { return _couponCode; }
            set { _couponCode = value; }
        }

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }


        #endregion


    }
}
