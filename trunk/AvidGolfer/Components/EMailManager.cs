﻿using System;
using System.Net.Mail;
using System.Text;
using System.Web;

using Defakto.Components;
using umbraco.cms.businesslogic.member;

namespace AvidGolfer.Components
{
    [Serializable]
    public partial class EMailManager : IEMailManager, IDisposable
    {

        string ContextDomainSelect()
        {
            return "http://" + HttpContext.Current.Request.Url.Host + "/";
        }

        public string ReadEmailTemplate(string templateName)
        {
            string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/email/" + templateName);
            System.IO.StreamReader emailfile = new System.IO.StreamReader(fileNameWithPath);
            string templateHtml = emailfile.ReadToEnd();
            emailfile.Close();

            return templateHtml;
        }

        #region SendEmail methods

        public void SendEmail(string fromEmail, string toEmail, string subject, string body, System.Web.HttpFileCollection fileAttachments, string fromName = "", string toName = "")
        {
            MailMessage mail = new MailMessage();
            try
            {
                if (String.IsNullOrEmpty(fromName)) fromName = fromEmail;
                if (String.IsNullOrEmpty(toName)) toName = toEmail;

                mail.From = new MailAddress(fromEmail, fromName);
                mail.Subject = subject;
                mail.Body = body.ToString();
                mail.To.Add(new MailAddress(toEmail, toName));

                if (fileAttachments != null)
                {
                    foreach (String fileKey in fileAttachments)
                    {
                        mail.Attachments.Add(new Attachment(fileAttachments[fileKey].InputStream, fileAttachments[fileKey].FileName.ToString()));
                    }
                }


                mail.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("localhost");
                client.Send(mail);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("3 - Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);
            }
        }

        public bool EMailPassWord(string toEmail, string username, string password)
        {
            string htmlTemplate = ReadEmailTemplate("passwordrecovery.html");

            if (htmlTemplate.Equals(""))  // did not get template
                return false;

            string subject = "Your MyAvidGolfer Account";

            string fromEmail = "customerservice@myavidgolfer.com";

            //string.Format(htmlTemplate, username, password);
            htmlTemplate = htmlTemplate.Replace("{0}", username);
            htmlTemplate = htmlTemplate.Replace("{1}", password);

            SendEmail(fromEmail, toEmail, subject, htmlTemplate, null);

            return true;
        }

        #endregion

        #region SendEmail methods

        public static void SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body)
        {
            SendEmail(fromEmail, fromName, toEmail, toName, subject, body, null);
        }

        public static void SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, HttpFileCollection fileAttachments)
        {
            MailMessage mail = new MailMessage();
            try
            {

                if (String.IsNullOrEmpty(fromName)) fromName = fromEmail;
                if (String.IsNullOrEmpty(toName)) toName = toEmail;

                mail.From = new MailAddress(fromEmail, fromName);
                mail.Subject = subject;
                mail.Body = body.ToString();
                mail.To.Add(new MailAddress(toEmail, toName));

                if (fileAttachments != null)
                {
                    foreach (String fileKey in fileAttachments)
                    {
                        mail.Attachments.Add(new Attachment(fileAttachments[fileKey].InputStream, fileAttachments[fileKey].FileName.ToString()));
                    }
                }


                mail.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("localhost");
                client.Send(mail);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("3 - Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);
            }

        }

        //public static void SendRegistrationEmail(string toEmail, string toName, string fullName, string userName, string password)
        //{
        //    Sitecore.Data.Items.Item emailTemplate = Sitecore.Context.Database.GetItem("/sitecore/content/home/settings/emailtemplates/newregistration");
        //    string htmlTemplate = String.Empty;
        //    string subject = String.Empty;

        //    subject = emailTemplate.Fields["subject"].ToString();
        //    htmlTemplate = emailTemplate.Fields["template"].ToString();

        //    Sitecore.Data.Items.Item home = Sitecore.Context.Database.GetItem("/sitecore/content/home");
        //    string fromEmail = home.Fields["email"].ToString();

        //    //replace tokens
        //    subject = subject.Replace("[FullName]", fullName);

        //    htmlTemplate = htmlTemplate.Replace("[FullName]", fullName);
        //    htmlTemplate = htmlTemplate.Replace("[UserName]", userName);
        //    htmlTemplate = htmlTemplate.Replace("[PassWord]", password);

        //    SendEmail(fromEmail, fromEmail, toEmail, toEmail, subject, htmlTemplate);

        //}

        //public static void SendRecoveredCredentialsEmail(string toEmail, string toName, string fullName, string userName, string password)
        //{
        //    //Sitecore.Data.Items.Item emailTemplate = Sitecore.Context.Database.GetItem("/sitecore/content/home/settings/emailtemplates/recoveredcredentials");
        //    string htmlTemplate = String.Empty;
        //    string subject = String.Empty;

        //    subject = emailTemplate.Fields["subject"].ToString();
        //    htmlTemplate = emailTemplate.Fields["template"].ToString();

        //    //Sitecore.Data.Items.Item home = Sitecore.Context.Database.GetItem("/sitecore/content/home");
        //    string fromEmail = home.Fields["email"].ToString();

        //    //replace tokens
        //    subject = subject.Replace("[FullName]", fullName);

        //    htmlTemplate = htmlTemplate.Replace("[FullName]", fullName);
        //    htmlTemplate = htmlTemplate.Replace("[UserName]", userName);
        //    htmlTemplate = htmlTemplate.Replace("[PassWord]", password);

        //    SendEmail(fromEmail, fromEmail, toEmail, toEmail, subject, htmlTemplate);

        //}

        //public static void NewOrderNotificationEmail(string toEmail, string toName, Sitecore.Security.Accounts.User user, Order order)
        //{
        //    Sitecore.Data.Items.Item emailTemplate = Sitecore.Context.Database.GetItem("/sitecore/content/home/settings/emailtemplates/newordernotification");
        //    string htmlTemplate = String.Empty;
        //    string subject = String.Empty;

        //    subject = emailTemplate.Fields["subject"].ToString() + " Order No:" + order.OrderId.ToString();
        //    htmlTemplate = emailTemplate.Fields["template"].ToString();

        //    Sitecore.Data.Items.Item home = Sitecore.Context.Database.GetItem("/sitecore/content/home");
        //    string fromEmail = home.Fields["email"].ToString();

        //    //Create Order Details
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendFormat("Order #: {0}<br />\n", order.OrderId.ToString());
        //    sb.AppendFormat("Authorize.net Transaction #: {0}<br />\n", order.TransactionId);
        //    sb.AppendFormat("Customer Name: {0}<br />\n", user.Profile.FullName);
        //    sb.AppendFormat("Customer Email: {0}<br />\n", user.Profile.Email);
        //    sb.AppendFormat("Customer Username: {0}<br /><br />\n\n", user.Profile.UserName);
        //    sb.AppendFormat("Treat : {0}<br />\n", order.OrderItems[0].ProductName);
        //    sb.AppendFormat("Voucher Code: {0}<br />\n", order.OrderItems[0].VoucherCode);
        //    sb.AppendFormat("Qty : {0}<br />\n", order.OrderItems[0].Quantity.ToString());
        //    sb.AppendFormat("Price : {0}<br />\n", String.Format("{0:C}", order.OrderItems[0].Price));

        //    decimal taxAmount = order.OrderItems[0].Price * order.OrderItems[0].TaxRate;
        //    sb.AppendFormat("Tax Amount : {0}<br />\n", String.Format("{0:C}", taxAmount));
        //    sb.AppendFormat("Shipping Amount : {0}<br />\n", String.Format("{0:C}", order.OrderItems[0].ShippingAmount));
        //    sb.AppendFormat("Donation Amount : {0}<br />\n", String.Format("{0:C}", order.OrderItems[0].DonationAmount));

        //    decimal totalAmount = (order.OrderItems[0].Price * order.OrderItems[0].TaxRate)
        //        + order.OrderItems[0].Price
        //        + order.OrderItems[0].ShippingAmount
        //        + order.OrderItems[0].DonationAmount;

        //    sb.AppendFormat("Total : {0}<br /><br />\n\n", String.Format("{0:C}", totalAmount));
        //    sb.Append("<br /><br />\n\n");
        //    sb.AppendFormat("Campus : {0}<br />\n", order.OrderItems[0].CampusName);
        //    sb.AppendFormat("Charity : {0}<br />\n", order.OrderItems[0].CharityName);
        //    sb.Append("<br /><br />\n\nShipping Information <br />\n");
        //    sb.AppendFormat("Ship Method : {0}<br /><br />\n\n", order.OrderItems[0].ShippingMethod);
        //    sb.AppendFormat("Name of Recipient : {0}<br />\n", order.OrderItems[0].ShipName);
        //    sb.AppendFormat("Phone Number : {0}<br />\n", order.OrderItems[0].ShipNumber);
        //    sb.AppendFormat("Contact Number : {0}<br />\n", order.OrderItems[0].ShipNumber);
        //    sb.AppendFormat("Contact Email : {0}<br />\n", order.OrderItems[0].ShipEmail);
        //    sb.AppendFormat("Address 1 : {0}<br />\n", order.OrderItems[0].ShipAddress1);
        //    sb.AppendFormat("Address 2 : {0}<br />\n", order.OrderItems[0].ShipAddress2);
        //    sb.AppendFormat("City : {0}<br />\n", order.OrderItems[0].ShipCity);
        //    sb.AppendFormat("State : {0}<br />\n", order.OrderItems[0].ShipState);
        //    sb.AppendFormat("Zip : {0}<br /><br />\n\n", order.OrderItems[0].ShipZip);

        //    string orderDetails = sb.ToString();

        //    //replace tokens
        //    htmlTemplate = htmlTemplate.Replace("[OrderDetails]", orderDetails);

        //    SendEmail(fromEmail, fromEmail, toEmail, toEmail, subject, htmlTemplate);

        //}

        #endregion

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////
    }

}
