﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorizeNet.Entities;
using AvidGolfer.Entities;
using AuthorizeNetCustomerService;
using umbraco.cms.businesslogic.member;

namespace AvidGolfer.Components
{
    [Serializable]
    public class StoreManager : IDisposable
    {
        public static AuthorizeNetCustomer GetCustomerProfile(Member customer)
        {
            AuthorizeNetCustomer customerProfile = new AuthorizeNetCustomer();
            long authorizeNetCustomerId = 0;

            Int64.TryParse(customer.getProperty("authorizeNetCustomerId").Value.ToString(), out authorizeNetCustomerId);

            if (!authorizeNetCustomerId.Equals(0))
            {
                try
                {
                    AuthorizeNetServiceClient serviceClient = new AuthorizeNetServiceClient();
                    customerProfile = serviceClient.GetAuthorizeNetCustomer(authorizeNetCustomerId);

                    return customerProfile;
                }
                catch(Exception ex)
                {
                    string message = ex.Message;
                    message = string.Empty;
                }

            }

            customer.getProperty("authorizeNetCustomerId").Value = authorizeNetCustomerId.ToString();
            customer.Save();

            customerProfile.MerchantCustomerId = customer.Id;
            customerProfile.Description = customer.Id.ToString();
            customerProfile.Email = customer.Email;

            return customerProfile;
        }

        public static string PurchaseOrder(Order order, Member customer, AuthorizeNetCustomer customerProfile)
        {
            AuthorizeNetServiceClient serviceClient = new AuthorizeNetServiceClient();
            AuthorizeNetTransaction trans = new AuthorizeNetTransaction();

            trans.Customer = customerProfile;
            trans.OrderNumber = order.orderid.ToString();

            decimal taxamount = 0;
            decimal totalamount = 0;

            var lineList = new List<AuthorizeNetOrderLineItem>();
            foreach (OrderItem item in order.orderitems)
            {
                // altering code to account for an order status of active.
                AuthorizeNetOrderLineItem line = new AuthorizeNetOrderLineItem();
                line.ItemId = item.productid.ToString();
                line.Name = item.productname;
                line.Quantity = (decimal)item.quantity;
                line.UnitPrice = item.price;
                lineList.Add(line);

                item.total = ((item.price + (item.price * item.tax)) * item.quantity);

                if (item.tax > 0)
                    taxamount = taxamount + ((item.price * item.tax) * item.quantity);

                totalamount = totalamount + item.total;

                // All Order Items will contain the same Product, so include the name and price in the transaction description
                // so that the Authorize.Net Description field will show the product in the transaction description.
                // This is an AvidGolfer request for reporting purposes.

                trans.OrderDescription = item.sku + " - QTY: " + order.orderitems.Count().ToString();

            }
          
                trans.LineItems = lineList;
                trans.TaxAmount = taxamount;

                order.shippingamount = totalamount;

                trans.TotalAmount = order.shippingamount;
                trans.ShippingAmount = trans.TotalAmount;

                //(order.OrderItems[0].Price * order.OrderItems[0].TaxRate) + order.OrderItems[0].Price + order.OrderItems                      [0].ShippingAmount;

                // Authorize and Capture
                serviceClient.CreateTransaction(trans);

                long value = 0;
                if (Int64.TryParse(trans.Id, out value))
                    order.tansactionid = value;
           

            //UmbracoEmailManager.SendApprovedOrderNotificationEmail(order, customer, customerProfile);

            return "";
        }

        public static void SaveCustomerProfile(AuthorizeNetCustomer customerProfile, Member customer)
        {
            AuthorizeNetServiceClient serviceClient = new AuthorizeNetServiceClient();

            if (customerProfile.AuthorizeNetCustomerId == 0)
            {
                serviceClient.CreateAuthorizeNetCustomer(customerProfile);

                if (customerProfile.AuthorizeNetCustomerId == 0)
                    throw new Exception("create customer profile failed");

                customer.getProperty("authorizeNetCustomerId").Value = customerProfile.AuthorizeNetCustomerId.ToString();

                customer.Save();
            }
            else
            {
                serviceClient.UpdateAuthorizeNetCustomer(customerProfile);
            }

        }

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////

    }
}
