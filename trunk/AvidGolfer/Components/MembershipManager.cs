﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;

using AvidGolfer.Entities;
using umbraco.cms.businesslogic.member;

using AvidGolfer.Data;

namespace AvidGolfer.Components
{
    [Serializable]
    public partial class MembershipManager : IMembershipManager, IDisposable
    {
        public bool LogInUser(string userName)
        {
            if (UserExists(userName))
            {
                Member member = Member.GetMemberFromLoginName(userName);

                Member.AddMemberToCache(member);

                FormsAuthentication.SetAuthCookie(userName, false);

                return true;
            }

            return false;
        }

        // HES 2013.02.20 overide to change user password
        public bool LogInUser(string username, string password = "", string email = "")
        {
            bool isAuthenticate = false;
            Member member;

            if (UserExists(username))
            {
                if (!password.Equals(""))
                {
                    isAuthenticate = Membership.ValidateUser(username, password);
                    if (isAuthenticate)
                    {
                        member = Member.GetMemberFromLoginNameAndPassword(username, password);
                        Member.AddMemberToCache(member);
                    }
                }
                else if (!email.Equals(""))
                {
                    member = Member.GetMemberFromEmail(email);
                    if ((!member.Equals(null)) && (member.Id > 0))
                    {
                        Member.AddMemberToCache(member);
                        isAuthenticate = true;
                    }
                }
            }

            if (isAuthenticate)
                FormsAuthentication.SetAuthCookie(username, false);

            return isAuthenticate;
        }

        public void Logout()
        {
            try
            {
                FormsAuthentication.SignOut();
            }
            catch { }

            Member m = Member.GetCurrentMember();

            try
            {
                Member.RemoveMemberFromCache(m);
            }
            catch { }

            try
            {
                Member.ClearMemberFromClient(m);
            }
            catch { }

            finally
            {

            }

        }

        public bool PasswordChange(string oldPassWord, string newPassWord)
        {
            Member member = Member.GetCurrentMember();
            

            if (member.Equals(null) || (member.Id < 1))
                return false;

            string loginName = member.LoginName;
            MembershipUser umember = Membership.GetUser(loginName);
            umember.ChangePassword(oldPassWord, newPassWord); 
            //member.ChangePassword(newPassWord);
            member.Save();
            string password = member.Password;
            member = Member.GetMemberFromLoginNameAndPassword(loginName, newPassWord);

            Member.AddMemberToCache(member);
            
            FormsAuthentication.SetAuthCookie(member.LoginName, false);

            return true;
        }

        public bool PasswordEMail(string email)
        {
            Member member = Member.GetMemberFromEmail(email);
            try
            {
                if (member.Equals(null) || (member.Id < 1))
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

            //string pw = member.Password;
            string user = member.LoginName;

            //string pw = System.Web.Security.Membership.GeneratePassword(8, 0);

            var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var pw = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            HMACSHA1 hash = new HMACSHA1();
            hash.Key = Encoding.Unicode.GetBytes(pw);
            string encodedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(pw)));

            member.ChangePassword(encodedPassword);
            member.Save();


            using (IEMailManager manager = new EMailManager())
                return manager.EMailPassWord(email, user, pw);
        }

        public bool ProfileModify(User user)
        {
            bool isSuccess = false;

            try
            {

                //Member memInfo = GetMemberFromCache(currentMem.Id);
                Member member = Member.GetCurrentMember();

                user.userid = member.Id.ToString();
                user.username = member.LoginName;

                //if(user.email != "")
                //    member.getProperty("email").Value = user.email;

                member.getProperty("firstname").Value = user.firstname;
                member.getProperty("lastname").Value = user.lastname;

                // member.getProperty("gender").Value = user.gender;
                member.getProperty("phone").Value = user.phone;
                member.getProperty("address1").Value = user.address1;
                member.getProperty("address2").Value = user.address2;
                member.getProperty("city").Value = user.city;
                member.getProperty("state").Value = user.state;
                member.getProperty("zip").Value = user.Zip;
                member.getProperty("zip4").Value = user.zip4;

                string ismember = "0";
                if (user.isMember)
                    ismember = "1";
                member.getProperty("isMember").Value = ismember;

                member.Save();

                Member.AddMemberToCache(member);
                FormsAuthentication.SetAuthCookie(user.username, false);

                isSuccess = true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

            return isSuccess;
        }

        public User ProfileSelect()
        {
            User user = new User();

            try
            {
                //bool isLogIn = LogInUser("dev1One", "dev1One");  // ToDo: Test Only

                Member member = Member.GetCurrentMember();

                if ((!member.Equals(null)) && (member.Id > 0))
                {
                    user.userid = member.Id.ToString();
                    user.username = member.LoginName;

                    user.email = member.Email;

                    user.firstname = member.getProperty("firstname").Value.ToString();
                    user.lastname = member.getProperty("lastname").Value.ToString();
                    user.gender = member.getProperty("gender").Value.ToString();
                    user.phone = member.getProperty("phone").Value.ToString();
                    user.address1 = member.getProperty("address1").Value.ToString();
                    user.address2 = member.getProperty("address2").Value.ToString();
                    user.city = member.getProperty("city").Value.ToString();
                    user.state = member.getProperty("state").Value.ToString();
                    user.Zip = member.getProperty("zip").Value.ToString();

                    user.zip4 = member.getProperty("zip4").Value.ToString();

                    user.ancid = member.getProperty("authorizeNetCustomerId").Value.ToString();

                    if (member.getProperty("isMember").Value.ToString().Equals("1"))
                        user.isMember = true;

                    if (member.getProperty("accountArchived").Value.ToString().Equals("1"))
                        user.accountArchived = true;


                    // Member.AddMemberToCache(member);
                    // FormsAuthentication.SetAuthCookie(user.username, false);
                }

            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

            return user;
        }

        public bool RegisterUser(User user)
        {
            bool isSuccess = false;

            try
            {
                MembershipCreateStatus status = new MembershipCreateStatus();

                MembershipUser userMember = Membership.CreateUser(user.username,
                user.password,
                user.email,
                user.PassWordRecoveryQuestion,
                user.PassWordRecoveryAnswer,
                true,
                out status);

                if (status.Equals(MembershipCreateStatus.Success))
                {
                    // Add Roles using the static string array DefaultRoles
                    // Roles.AddUserToRoles(user.UserName, MemberRoles.DefaultRoles);

                    // Add Umbraco Properties
                    Member member = Member.GetMemberFromEmail(userMember.Email);

                    if (member != null)
                    {
                        member.Save();

                        Member.AddMemberToCache(member);

                        FormsAuthentication.SetAuthCookie(userMember.UserName, false);

                        isSuccess = true;
                    }

                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }

            return isSuccess;
        }

        public string RegisterUnique(string username, string email)
        {
            Member member = Member.GetMemberFromEmail(email);
            if (member != null)
                return "EMail";

            member = Member.GetMemberFromLoginName(username);
            if (member != null)
                return "UserName";

            return string.Empty;
        }

        public bool UserExists(string userName)
        {
            Member member = Member.GetMemberFromLoginName(userName);
            return (member != null);
        }

        public string UserId(string userName)
        {
            Member member = Member.GetMemberFromLoginName(userName);

            return member.Id.ToString();
        }


        #region helpers

        string ErrorMessageID(MembershipCreateStatus status)
        {
            string fail = string.Empty;

            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    fail = "Username already exists. Please enter a different user name.";
                    break;
                case MembershipCreateStatus.DuplicateEmail:
                    fail = "A username for that e-mail address already exists. Please enter a different e-mail address.";
                    break;
                case MembershipCreateStatus.InvalidPassword:
                    fail = "The password provided is invalid. Please enter a valid password value.";
                    break;
                case MembershipCreateStatus.InvalidEmail:
                    fail = "The e-mail address provided is invalid. Please check the value and try again.";
                    break;
                case MembershipCreateStatus.InvalidAnswer:
                    fail = "The password retrieval answer provided is invalid. Please check the value and try again.";
                    break;
                case MembershipCreateStatus.InvalidQuestion:
                    fail = "The password retrieval question provided is invalid. Please check the value and try again.";
                    break;
                case MembershipCreateStatus.InvalidUserName:
                    fail = "The user name provided is invalid. Please check the value and try again.";
                    break;
                case MembershipCreateStatus.ProviderError:
                    fail = "The authentication provider message =ed an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
                    break;
                case MembershipCreateStatus.UserRejected:
                    fail = "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
                    break;
                default:
                    fail = "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
                    break;
            }

            return fail;
        }

        #endregion

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////
    }
}
