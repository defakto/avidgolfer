﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

using AvidGolfer.Data;
using AvidGolfer.Entities;
using Defakto.Components.Provider;
using umbraco;
using umbraco.cms.businesslogic.media;
using umbraco.cms.businesslogic.member;
using umbraco.cms.businesslogic.web;
using umbraco.NodeFactory;

namespace AvidGolfer.Components
{
    public class SupplementalContentManager : IDisposable
    {
        ProviderRepository<SupplementalDataProvider> _supplementalData = new ProviderRepository<SupplementalDataProvider>("SupplimentalDataProvider");

        #region CartGirls / Vote

        public string GetTestById(int byId)
        {
            return _supplementalData.Provider.GetTest(byId);
        }

        public CartGirlVote GetCartGirlsByNodeId(string nodeId)
        {
            return _supplementalData.Provider.GetCartGirlsByNodeId(nodeId);
        }

        public void CastCartGirlVote(string nodeId)
        {
            CartGirlVote cgv = GetCartGirlsByNodeId(nodeId);

            if (cgv.NodeId == "")
                cgv.NodeId = nodeId;

            cgv.IncrementVoteCounter();
            _supplementalData.Provider.CreateUpdateCartGirlVote(cgv);
        }

        public void CastCartGirlVote(CartGirlVote cgv, bool incrementVote = true)
        {

            if (cgv.NodeId != "")
            {

                if (incrementVote)
                    cgv.IncrementVoteCounter();

                _supplementalData.Provider.CreateUpdateCartGirlVote(cgv);
            }

        }

        public List<CartGirlVote> GetCartGirls()
        {
            return _supplementalData.Provider.GetCartGirls();
        }

        public MostViewed GetArticleByNodeId(string nodeId)
        {
            return _supplementalData.Provider.GetArticleByNodeId(nodeId);
        }

        public List<MostViewed> GetMostViewedArticles(string region)
        {
            return _supplementalData.Provider.GetMostViewedArticles(region);
        }

        public void IncreaseArticleHitCount(string nodeId, string region)
        {
            MostViewed amv = GetArticleByNodeId(nodeId);

            if (amv.NodeId == "")
            {
                amv.NodeId = nodeId;
            }
            amv.Region = region;

            amv.IncrementHitCounter();
            _supplementalData.Provider.CreateUpdateMostViewedArticle(amv);
        }

        public void IncreaseArticleHitCounter(MostViewed amv, bool incrementVote = true)
        {
            if (amv.NodeId != "")
            {
                if (incrementVote)
                {
                    amv.IncrementHitCounter();
                }

                _supplementalData.Provider.CreateUpdateMostViewedArticle(amv);
            }
        }

        public void SaveDailyDealOrder(DailyDealOrder ddOrder)
        {
            _supplementalData.Provider.CreateUpdateDailyDealOrder(ddOrder);
        }

        public List<DailyDealOrder> GetAllDailyDealOrders()
        {
            return _supplementalData.Provider.GetAllDailyDeals();
        }

        public List<DailyDealOrder> GetAllDailyDealsByDateRange(DateTime startDate, DateTime endDate)
        {
            return _supplementalData.Provider.GetAllDailyDealsByDateRange(startDate, endDate);
        }

        public List<OrderItemHistory> OrderHistoryReport(string regiontype = "", string sku = "", string orderstatus = "",
            string subscriptionstatus = "")
        {
            return _supplementalData.Provider.OrderHistoryReport(regiontype, sku, orderstatus, subscriptionstatus);
        }

        public DataTable OrderHistoryCsv(string regiontype = "", string sku = "", string orderstatus = "", string subscriptionstatus = "")
        {
            return _supplementalData.Provider.OrderHistoryCsv(regiontype, sku, orderstatus, subscriptionstatus);
        }

        public List<Order> GetOrderList(long orderId = 0, string email = "", int userId = 0, long transactionId = 0)
        {
            return _supplementalData.Provider.GetOrderList(orderId, email, userId, transactionId);
        }

        public List<Order> GetOrders(long orderId = 0, string lastName = "", string zip = "", string email = "", string phone = "", int userId = 0, long transactionId = 0)
        {
            return _supplementalData.Provider.GetOrders(orderId, lastName, zip, email, phone, userId, transactionId);
        }

        public List<OrderItem> GetOrderItemsByOrderId(long orderId = 0, long orderitemid = 0, int productid = 0)
        {
            return _supplementalData.Provider.GetOrderItemsByOrderId(orderId, orderitemid, productid);
        }

        public OrderItem GetOrderItemById(long orderitemId)
        {
            return _supplementalData.Provider.GetOrderItemById(orderitemId);
        }

        public DataSet GetOrderDataset(long orderId = 0, string lastName = "", string zip = "", string email = "", string phone = "")
        {
            return _supplementalData.Provider.GetOrderDataset(orderId, lastName, zip, email, phone);
        }

        public  void SaveIndividualOrderItem(OrderItem item)
        {
            _supplementalData.Provider.SaveIndividualOrderItem(item);
        }

        public void CreateUser(User user)
        {
            _supplementalData.Provider.CreateUser(user);
        }

        public Boolean CurrentOrderExists(int userId, string region)
        {
            return _supplementalData.Provider.CurrentOrderExists(userId, region);
        }

        public void UpdateOrderItemActive(long orderId, string region)
        {
             _supplementalData.Provider.UpdateOrderItemActive(orderId, region);
        }

        public List<Order> GetOrderForProcessing()
        {
            return _supplementalData.Provider.GetOrderForProcessing();
        }

        public List<OrderItem> GetOrderItemsForProcessing(long orderId)
        {
            return _supplementalData.Provider.GetOrderItemsForProcessing(orderId);
        }
        #endregion // CartGirls / Vote

        public void SavePreviousOrder(Order order)
        {
            _supplementalData.Provider.SavePreviousOrder(order);
        }

        // =========================================
        #region Admin Ops

        public List<SelectList> USStates()
        {
            DataTable table = _supplementalData.Provider.DBSPSelect();

            List<SelectList> states = table.AsEnumerable().Select(row =>
            new SelectList
                {
                    name = row.Field<int>("ID").ToString(),
                    display = row.Field<string>("Name"),
                    value = row.Field<string>("State")
                }).OrderBy(x => x.value).ToList();

            return states;
        }

        #endregion // Admin Ops
        // =========================================


        // =========================================
        #region Order LOB

        DropDownList DDLStatus(string id, string valued = "", bool isAdmin = false)
        {
            List<SelectList> status = new List<SelectList>();
            using (SupplementalEntities se = new SupplementalEntities())
                status = se.ListStatus();

            DropDownList ddl = new DropDownList();
            ddl.ID = id;
            ddl.DataSource = status;
            ddl.DataTextField = "display";
            ddl.DataValueField = "value";
            ddl.DataBind();

            int value = 0;
            if (Int32.TryParse(valued, out value))
                ddl.SelectedIndex = value;

            ddl.Width = new Unit("100px");

            ddl.Enabled = isAdmin;

            return ddl;
        }

        SqlCommand OrderCommand(Order order)
        {
            SqlCommand command = new SqlCommand();

            command.CommandType = CommandType.StoredProcedure;

            command.CommandText = "dbo.spOrderUpSert";

            if (!order.orderid.Equals(0))
                command.Parameters.Add("@orderid", SqlDbType.BigInt).Value = order.orderid;

            if (!order.userid.Equals(0))
                command.Parameters.Add("@userid", SqlDbType.Int).Value = order.userid;

            if (!order.tansactionid.Equals(0))
                command.Parameters.Add("@transactionid", SqlDbType.BigInt).Value = order.tansactionid;

            if (!string.IsNullOrEmpty(order.billphone))
                command.Parameters.Add("@billphone", SqlDbType.NVarChar).Value = order.billphone;

            if (!string.IsNullOrEmpty(order.billemail))
                command.Parameters.Add("@billemail", SqlDbType.NVarChar).Value = order.billemail;

            if (!string.IsNullOrEmpty(order.billstreet))
                command.Parameters.Add("@billstreet", SqlDbType.NVarChar).Value = order.billstreet;

            if (!string.IsNullOrEmpty(order.billzip))
                command.Parameters.Add("@billzip", SqlDbType.NVarChar).Value = order.billzip;

            if (!string.IsNullOrEmpty(order.shipphone))
                command.Parameters.Add("@shipphone", SqlDbType.NVarChar).Value = order.shipphone;

            if (!string.IsNullOrEmpty(order.shipemail))
                command.Parameters.Add("@shipemail", SqlDbType.NVarChar).Value = order.shipemail;

            if (!string.IsNullOrEmpty(order.shipstreet))
                command.Parameters.Add("@shipstreet", SqlDbType.NVarChar).Value = order.shipstreet;

            if (!string.IsNullOrEmpty(order.shipzip))
                command.Parameters.Add("@shipzip", SqlDbType.NVarChar).Value = order.shipzip;

            if (!string.IsNullOrEmpty(order.notes))
                command.Parameters.Add("@notes", SqlDbType.NVarChar).Value = order.notes;

            if (!order.shippingamount.Equals(0))
                command.Parameters.Add("@shippingamount", SqlDbType.Decimal).Value = order.shippingamount;

            if (!string.IsNullOrEmpty(order.ordertype))
                command.Parameters.Add("@ordertype", SqlDbType.VarChar).Value = order.ordertype;

            return command;
        }

        public List<OrderItemHistory> GetOrderHistoryByOptions(int memberid = 0, long orderid = 0, string email = "")
        {
            return _supplementalData.Provider.GetOrderItemHistoryByOptions(memberid, orderid, email);
        }

        public Table OrderHistory(int memberid = 0, long orderid = 0, bool isAdmin = false)
        {
            Table htmlTable = new Table();

            htmlTable.ID = "tableOrderHistory" + memberid.ToString();
            htmlTable.GridLines = GridLines.Both;
            htmlTable.HorizontalAlign = HorizontalAlign.Left;
            htmlTable.CellPadding = 2;
            htmlTable.CellSpacing = 2;

            DataTable dataTable = _supplementalData.Provider.OrderHistory(userid: memberid, orderid: orderid);

            if (dataTable.Rows.Count < 1)
                return htmlTable;

            TableCell tcell;
            TableRow trow;

            // headers
            trow = new TableRow();
            htmlTable.Rows.Add(trow);
            foreach (System.Data.DataColumn column in dataTable.Columns)
            {
                tcell = new TableCell();
                tcell.Text = column.Caption;
                trow.Cells.Add(tcell);
            }

            foreach (DataRow row in dataTable.Rows)
            {
                trow = new TableRow();
                htmlTable.Rows.Add(trow);

                foreach (DataColumn column in dataTable.Columns)
                {
                    tcell = new TableCell();

                    if (column.Caption.ToLower().Equals("status"))
                        tcell.Controls.Add(
                            DDLStatus(
                                (row["OrderID"] + "").ToString() + "_" + (row["ItemID"] + "").ToString(),
                                (row[column.ColumnName] + "").ToString(),
                                isAdmin
                               )
                            );
                    else
                        tcell.Text = row[column.ColumnName] + "";

                    trow.Cells.Add(tcell);
                }

            }

            return htmlTable;
        }

        SqlCommand OrderItemCommand(OrderItem orderitem)
        {
            SqlCommand command = new SqlCommand();

            command.CommandType = CommandType.StoredProcedure;

            command.CommandText = "dbo.spOrderItemUpSert";

            return OrderItemParameters(command, orderitem);
        }

        public SqlCommand OrderItemParameters(SqlCommand command, OrderItem orderitem)
        {
            command.Parameters.Clear();

            if (!orderitem.orderitemid.Equals(0))
                command.Parameters.Add("@orderitemid", SqlDbType.BigInt).Value = orderitem.orderitemid;

            if (!orderitem.orderid.Equals(0))
                command.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderitem.orderid;

            if (!orderitem.itemid.Equals(0))
                command.Parameters.Add("@itemid", SqlDbType.Int).Value = orderitem.itemid;

            if (!orderitem.productid.Equals(0))
                command.Parameters.Add("@productid", SqlDbType.Int).Value = orderitem.productid;

            if (!orderitem.quantity.Equals(0))
                command.Parameters.Add("@quantity", SqlDbType.Int).Value = orderitem.quantity;

            if (!orderitem.price.Equals(0))
                command.Parameters.Add("@price", SqlDbType.Decimal).Value = orderitem.price;

            if (!orderitem.tax.Equals(0))
                command.Parameters.Add("@tax", SqlDbType.Decimal).Value = orderitem.tax;

            if (!orderitem.itemstatus.Equals(0))
                command.Parameters.Add("@status", SqlDbType.Int).Value = orderitem.itemstatus;

            if (orderitem.startdate > DateTime.MinValue)
                command.Parameters.Add("@startdate", SqlDbType.DateTime).Value = orderitem.startdate;

            if (orderitem.expiredate > DateTime.MinValue)
                command.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = orderitem.expiredate;

            if (orderitem.shipdate > DateTime.MinValue)
                command.Parameters.Add("@shipdate", SqlDbType.DateTime).Value = orderitem.shipdate;

            if (!string.IsNullOrEmpty(orderitem.notes))
                command.Parameters.Add("@notes", SqlDbType.NVarChar).Value = orderitem.notes;

            if (!string.IsNullOrEmpty(orderitem.productname))
                command.Parameters.Add("@ProductName", SqlDbType.VarChar).Value = orderitem.productname;

            if (!string.IsNullOrEmpty(orderitem.regionType))
                command.Parameters.Add("@RegionType", SqlDbType.VarChar).Value = orderitem.regionType;

            if (!string.IsNullOrEmpty(orderitem.sku))
                command.Parameters.Add("@Sku", SqlDbType.VarChar).Value = orderitem.sku;

            if (!string.IsNullOrEmpty(orderitem.orderStatus))
                command.Parameters.Add("@OrderStatus", SqlDbType.VarChar).Value = orderitem.orderStatus;

            if (!string.IsNullOrEmpty(orderitem.subscriptionStatus))
                command.Parameters.Add("@SubscriptionStatus", SqlDbType.VarChar).Value = orderitem.subscriptionStatus;

            return command;
        }

        public void OrderSave(Order order, string billstreet = "", string billzip = "", string shipstreet = "", string shipzip = "")
        {
            SqlCommand command = OrderCommand(order);
            order.orderid = _supplementalData.Provider.OrderSave(command);

            if (order.orderitems.Count > 0)
            {
                foreach (OrderItem orderitem in order.orderitems)
                {
                    if (orderitem.orderid < 1)
                        orderitem.orderid = order.orderid;

                    orderitem.itemstatus = order.status;
                }

                command = OrderItemCommand(order.orderitems[0]);

                if (order.orderitems.Count < 2)
                    order.orderitems[0].orderitemid = _supplementalData.Provider.OrderSave(command);
                else // (order.orderitem.Count > 1)
                    _supplementalData.Provider.OrderItemSave(command, order.orderitems);
            }
        }

        public List<Product> ProductSelect(int productid = 0, string pname = "", string pdescription = "")
        {
            return _supplementalData.Provider.ProductSelect(productid, pname, pdescription);
        }

        private void ProductSave(Product product)
        {
            // ToDo:  save products to supplementalDB
            throw new NotImplementedException();
        }

        public Product ProductSelectPopulate(IDataRecord record)
        {
            Product product;

            product = new Product();

            if (!record.IsDBNull(record.GetOrdinal("DefaktoID")))
                product.defaktoid = (int)record["DefaktoID"];

            if (!record.IsDBNull(record.GetOrdinal("CategoryID")))
                product.categoryid = (int)record["CategoryID"];

            if (!record.IsDBNull(record.GetOrdinal("ProductID")))
                product.productid = (int)record["ProductID"];

            product.sku = record["SKU"] + "";
            product.name = record["Name"] + "";
            product.description = record["Description"] + "";

            if (!record.IsDBNull(record.GetOrdinal("AvailableDate")))
                product.availabledate = (DateTime)record["AvailableDate"];

            if (!record.IsDBNull(record.GetOrdinal("ExpireDate")))
                product.expiredate = (DateTime)record["ExpireDate"];

            if (!record.IsDBNull(record.GetOrdinal("Price")))
                product.price = (decimal)record["Price"];

            if (!record.IsDBNull(record.GetOrdinal("Tax")))
                product.tax = (decimal)record["Tax"];

            product.imageurl = record["ImageURL"] + "";
            product.notes = record["Notes"] + "";

            if (!record.IsDBNull(record.GetOrdinal("CreateDate")))
                product.createdate = (DateTime)record["CreateDate"];

            if (!record.IsDBNull(record.GetOrdinal("ModifyDate")))
                product.modifydate = (DateTime)record["ModifyDate"];

            if (!record.IsDBNull(record.GetOrdinal("ReviewDate")))
                product.reviewdate = (DateTime)record["ReviewDate"];

            // ToDo: save product
            //ProductSave(product);

            return product;
        }

        #endregion // Order LOB
        // =========================================

        public string ZipLocationSelect(string zip)
        {
            DataTable table = _supplementalData.Provider.ZipSelect(zip);

            if (table.Rows.Count < 1)
                return string.Empty;

            DataRow row = table.Rows[0];

            return (row["City"] + "").Trim() + ":" +
                    (row["State"] + "").Trim() + ":" +
                        (row["County"] + "").Trim();

        }

        // =========================================
        #region NodeFactory

        Node NodeCurrent()
        {
            Node node = Node.GetCurrent();

            // ToDo: is needed???
            //Node currentNode = Node.GetCurrent(); 
            //string myValue = currentNode.GetProperty("bodyText").Value; 
            //string nodename = currentNode.Name; 
            //DateTime nodeCreationDate = currentNode.CreateDate; 
            //string url = currentNode.NiceUrl;

            return node;
        }

        public Product NodeProduct(int nodeid)
        {
            Product product = new Product();

            Node node = new Node(nodeid);

            product.name = node.GetProperty("Name").Value + "";
            product.description = node.GetProperty("Description").Value + "";

            int value = 0;

            if (Int32.TryParse(node.GetProperty("CategoryID").Value, out value))
                product.categoryid = value;

            if (Int32.TryParse(node.GetProperty("ProductID").Value, out value))
                product.productid = value;

            product.sku = node.GetProperty("SKU").Value + "";

            product.notes = node.GetProperty("Notes").Value + "";

            product.imageurl = node.GetProperty("ImageURL").Value + "";

            Decimal price = new Decimal();

            if (Decimal.TryParse(node.GetProperty("Price").Value, out price))
                if (price > 0)
                     product.price = price;

            if (Decimal.TryParse(node.GetProperty("Tax").Value, out price))
                     product.tax = price;

            DateTime date = new DateTime();

            if (DateTime.TryParse(node.GetProperty("AvailableDate").Value, out date))
                if (date > DateTime.MinValue)
                     product.availabledate = date;
          
            if (node.CreateDate > DateTime.MinValue)
                product.createdate = node.CreateDate;

            if (DateTime.TryParse(node.GetProperty("ExpireDate").Value, out date))
                if (date > DateTime.MinValue)
                     product.expiredate = node.CreateDate.AddMonths(12);

            if (node.UpdateDate > DateTime.MinValue)
                    product.modifydate = node.UpdateDate;

            if (DateTime.TryParse(node.GetProperty("ReviewDate").Value, out date))
                if (date > DateTime.MinValue)
                     product.reviewdate = date;

            return product;
        }

        #endregion  //  NodeFactory
        // =========================================


        // =========================================
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        // =========================================

    }
}
