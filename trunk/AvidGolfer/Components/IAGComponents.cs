﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvidGolfer.Components
{
    public interface IAGComponents : IDisposable
    {
    }


    public interface IEMailManager : IDisposable
    {
        void SendEmail(string fromEmail, string toEmail, string subject, string body, System.Web.HttpFileCollection fileAttachments, string fromName = "", string toName = "");
    }



}
