﻿using System;

using AuthorizeNet.Entities;
using AvidGolfer.Entities;
using umbraco.cms.businesslogic.member;

namespace AvidGolfer.Components
{
    // Interface(s) for the Avid Golfer Components

    public interface IComponents : IDisposable
    {

    }

    public interface IEMailManager : IDisposable
    {
        bool EMailPassWord(string toEmail, string username, string password);
        string ReadEmailTemplate(string templateName);

        void SendEmail(
            string fromEmail, 
            string toEmail, 
            string subject, 
            string body, 
            System.Web.HttpFileCollection fileAttachments, 
            string fromName = "", 
            string toName = ""
            );

    }

    public interface IMembershipManager : IDisposable
    {
        bool LogInUser(string userName);
        bool LogInUser(string username, string password = "", string email = "");
        bool ProfileModify(User user);
        bool PasswordChange(string oldPassWord, string newPassWord);
        bool PasswordEMail(string email);
        bool RegisterUser(User user);
        bool UserExists(string userName);

        string RegisterUnique(string username, string email);

        User ProfileSelect();

        void Logout();

        string UserId(string p);
    }

}
