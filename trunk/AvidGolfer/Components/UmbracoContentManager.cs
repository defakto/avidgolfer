﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco;
using umbraco.NodeFactory;
using umbraco.cms.businesslogic.media;
using umbraco.cms.businesslogic.web;


namespace AvidGolfer.Components
{
    [Serializable]
    public class UmbracoContentManager
    {

        public List<string> GetWidgetNodeIdsFromString(string values)
        {
            List<string> nodeIds = new List<string>();

            try
            {
                foreach (string nodeId in values.Split(','))
                {
                    nodeIds.Add(nodeId);
                }

                return nodeIds;
            }
            catch
            {
                return new List<string>();
            }
        }


        public List<string> GetWidgetNodeIdsFromField(string fieldName)
        {
            Document doc = new Document(Node.GetCurrent().Id);
            List<string> nodeIds = new List<string>();

            try
            {
                foreach (string nodeId in doc.getProperty(fieldName).Value.ToString().Split(','))
                {
                    nodeIds.Add(nodeId);
                }

                return nodeIds;
            }
            catch
            {
                return new List<string>();
            }
        }

        public string GetDocumentTypeFromNodeId(int nodeId)
        {

            try
            {
                Document doc = new Document(nodeId);

                return doc.ContentType.Alias;
            }
            catch
            {
                return String.Empty;
            }
        }

        public static string GetSiteProperty(string propertyName)
        {
            Node homeNode = Node.GetNodeByXpath("//*[@id=" + Node.GetCurrent().Id + "]/ancestor-or-self::umbHomepage");

            if (homeNode != null && homeNode.GetProperty(propertyName) != null)
                return homeNode.GetProperty(propertyName).ToString();

            return String.Empty;
        }



    }
}
