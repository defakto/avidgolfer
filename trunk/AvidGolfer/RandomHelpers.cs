﻿using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace AvidGolfer
{
    public class RandomHelpers
    {

        public static void ExportToCSV(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "applicatoin/text";

            //gv.AllowPaging = false;
            //gv.DataBind();

            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < gv.Columns.Count; k++)
            {
                sb.Append(gv.Columns[k].HeaderText + ',');
            }
            sb.Append("\r\n");
            for (int i = 0; i < gv.Rows.Count; i++)
            {
                for (int k = 0; k < gv.Columns.Count; k++)
                {
                    sb.Append(gv.Rows[i].Cells[k].Text.Replace("&nbsp;", "") + ',');
                }
                
                sb.Append("\r\n");
            }

            HttpContext.Current.Response.Output.Write(sb.ToString());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }
    }
}
