﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using AvidGolfer.Components;
using AvidGolfer.Entities;

namespace AvidGolfer.Data
{
    public class SupplementalMsSqlDataProvider : SupplementalDataProvider, IDisposable
    {
        public override CartGirlVote GetCartGirlsByNodeId(string nodeId)
        {
            CartGirlVote cgv = new CartGirlVote();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetCartGirlsByNodeId", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@NodeId", SqlDbType.VarChar).Value = nodeId;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        cgv = PopulateCartGirlVoteFromIDataReader(dr);

                    }
                }
                sqlConn.Close();

            }

            return cgv;
        }

        public override void CreateUpdateCartGirlVote(CartGirlVote cgv)
        {
            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CreateUpdateCartGirlVote", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@nodeId", SqlDbType.Int).Value = cgv.NodeId;
                sqlCmd.Parameters.Add("@totalVotes", SqlDbType.Int).Value = (int)cgv.TotalVotes;

                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();

            }

        }

        public override List<CartGirlVote> GetCartGirls()
        {
            List<CartGirlVote> cgv = new List<CartGirlVote>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetCartGirls", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        cgv.Add(PopulateCartGirlVoteFromIDataReader(dr));
                    }
                }
                sqlConn.Close();
            }

            return cgv;
        }

        public override MostViewed GetArticleByNodeId(string nodeId)
        {
            MostViewed amv = new MostViewed();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetArticleByNodeId", sqlConn);
                sqlCmd.Parameters.Add("@nodeId", SqlDbType.Int).Value = nodeId;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        amv = PopulateMostViewedFromIDataReader(dr);
                    }
                }
                sqlConn.Close();
            }

            return amv;
        }

        public override List<MostViewed> GetMostViewedArticles(string region)
        {
            List<MostViewed> amv = new List<MostViewed>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetMostViewedArticles", sqlConn);
                sqlCmd.Parameters.Add("@Region", SqlDbType.VarChar).Value = region;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {

                    while (dr.Read())
                    {
                        amv.Add(PopulateMostViewedFromIDataReader(dr));
                    }
                }

                sqlConn.Close();
            }

            return amv;
        }

        public override void CreateUpdateMostViewedArticle(MostViewed cgView)
        {
            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CreateUpdateMostViewedArticle", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@NodeId", SqlDbType.Int).Value = cgView.NodeId;
                sqlCmd.Parameters.Add("@TotalHits", SqlDbType.Int).Value = cgView.TotalHits;
                sqlCmd.Parameters.Add("@Region", SqlDbType.VarChar).Value = cgView.Region;

                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        public override void CreateUpdateDailyDealOrder(DailyDealOrder ddOrder)
        {
            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CreateUpdateDailyDealOrder", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = ddOrder.OrderId;
                sqlCmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = ddOrder.FirstName;
                sqlCmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ddOrder.LastName;
                sqlCmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ddOrder.Email;
                sqlCmd.Parameters.Add("@Address1", SqlDbType.VarChar).Value = ddOrder.Address1;
                sqlCmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = ddOrder.Address2;
                sqlCmd.Parameters.Add("@City", SqlDbType.VarChar).Value = ddOrder.City;
                sqlCmd.Parameters.Add("@State", SqlDbType.VarChar).Value = ddOrder.State;
                sqlCmd.Parameters.Add("@ZipCode", SqlDbType.VarChar).Value = ddOrder.ZipCode;
                sqlCmd.Parameters.Add("@CouponCode", SqlDbType.VarChar).Value = ddOrder.CouponCode;

                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        public override List<DailyDealOrder> GetAllDailyDeals()
        {
            List<DailyDealOrder> ddOrder = new List<DailyDealOrder>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetAllDailyDeals", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {

                    while (dr.Read())
                    {
                        ddOrder.Add(PopulateDailyDealOrderFromIDataReader(dr));
                    }
                }

                sqlConn.Close();
            }

            return ddOrder;
        }

        public override List<DailyDealOrder> GetAllDailyDealsByDateRange(DateTime startDate, DateTime endDate)
        {
            List<DailyDealOrder> ddOrder = new List<DailyDealOrder>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetAllDailyDealsByDateRange", sqlConn);
                sqlCmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = startDate;
                sqlCmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = endDate;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {

                    while (dr.Read())
                    {
                        ddOrder.Add(PopulateDailyDealOrderFromIDataReader(dr));
                    }
                }

                sqlConn.Close();
            }

            return ddOrder;
        }

        public override string GetTest(int byId)
        {
            string testString = String.Empty;

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetTest", sqlConn);

                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@testid", SqlDbType.Int).Value = byId;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        testString = (string)dr["Test"];
                    }
                }
                sqlConn.Close();

            }

            return testString;
        }

        #region product / order methods

        public override DataTable OrderHistoryCsv(string regiontype = "", string sku = "", string orderstatus = "", string subscriptionstatus = "")
        {
            DataTable table = new DataTable();

            using(SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                using(SqlCommand sqlCmd = new SqlCommand("dbo.spOrderHistoryReport", sqlConn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    if (regiontype != "")
                        sqlCmd.Parameters.Add("@regiontype", SqlDbType.VarChar).Value = regiontype;

                    if (sku != "")
                        sqlCmd.Parameters.Add("@sku", SqlDbType.VarChar).Value = sku;

                    if (orderstatus != "")
                        sqlCmd.Parameters.Add("@orderstatus", SqlDbType.VarChar).Value = orderstatus;

                    if (subscriptionstatus != "")
                        sqlCmd.Parameters.Add("@subscriptionstatus", SqlDbType.VarChar).Value = subscriptionstatus;

                    if (sqlConn.State.ToString().ToLower().Equals("closed"))
                        sqlConn.Open();

                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                        adapter.Fill(table);
                }
            }

            return table;
        }

        public override DataSet GetOrderDataset(long orderId, string lastName, string zip, string email, string phone)
        {
            DataSet ds = new DataSet();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.spOrderList", sqlConn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (orderId > 0)
                        cmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderId;

                    if (!string.IsNullOrEmpty(lastName))
                    {
                        cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = lastName;
                    }

                    if (!string.IsNullOrEmpty(zip))
                    {
                        cmd.Parameters.Add("@Zip", SqlDbType.NVarChar).Value = zip;
                    }

                    if (!string.IsNullOrEmpty(email))
                    {
                        cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = email;
                    }

                    if (!string.IsNullOrEmpty(phone))
                    {
                        cmd.Parameters.Add("@phone", SqlDbType.NVarChar).Value = phone;
                    }

                    sqlConn.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        adapter.Fill(ds);
                    
                }
            }

            return ds;
        }

        public override DataTable OrderHistory(int userid = 0, long orderid = 0)
        {
            DataTable table = new DataTable();

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("dbo.spOrderHistory", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (userid > 0)
                        command.Parameters.Add("@userid", SqlDbType.Int).Value = userid;

                    if (orderid > 0)
                        command.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderid;

                    if (connection.State.ToString().ToLower().Equals("closed"))
                        connection.Open();

                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        adapter.Fill(table);
                }
            }

            return table;
        }

        public override long OrderSave(SqlCommand command)
        {
            long id = 0;

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                command.Connection = connection;

                if (connection.State.ToString().ToLower().Equals("closed"))
                    connection.Open();

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {

                    if (reader.HasRows)
                        while (reader.Read())
                            id = reader.GetInt64(0);

                    reader.Close();
                }
            }
            return id;
        }

        public override void SaveIndividualOrderItem(OrderItem item)
        {
            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("spOrderItemUpSert", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@orderitemid", SqlDbType.BigInt).Value = item.orderitemid;
                sqlCmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = item.orderid;
                sqlCmd.Parameters.Add("@itemid", SqlDbType.Int).Value = item.itemid;
                sqlCmd.Parameters.Add("@productid", SqlDbType.Int).Value = item.productid;
                sqlCmd.Parameters.Add("@quantity", SqlDbType.Int).Value = item.quantity;
                sqlCmd.Parameters.Add("@price", SqlDbType.Decimal).Value = item.price;
                sqlCmd.Parameters.Add("@tax", SqlDbType.Decimal).Value = item.tax;
                sqlCmd.Parameters.Add("@status", SqlDbType.Int).Value = item.status;
                sqlCmd.Parameters.Add("@startdate", SqlDbType.DateTime).Value = item.startdate;
                sqlCmd.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = item.expiredate;
                sqlCmd.Parameters.Add("@reviewdate", SqlDbType.DateTime).Value = item.reviewdate;
                sqlCmd.Parameters.Add("@shipdate", SqlDbType.DateTime).Value = item.shipdate;
                sqlCmd.Parameters.Add("@notes", SqlDbType.VarChar).Value = item.notes;
                sqlCmd.Parameters.Add("@ProductName", SqlDbType.VarChar).Value = item.productname;
                sqlCmd.Parameters.Add("@RegionType", SqlDbType.VarChar).Value = item.regionType;
                sqlCmd.Parameters.Add("@Sku", SqlDbType.VarChar).Value = item.sku;
                sqlCmd.Parameters.Add("@OrderStatus", SqlDbType.VarChar).Value = item.orderStatus;
                sqlCmd.Parameters.Add("@SubscriptionStatus", SqlDbType.VarChar).Value = item.subscriptionStatus;

                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();

            }
           
        }

        public override void SavePreviousOrder(Order order)
        {
            using(SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("spOrderPrevious", sqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = order.orderid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = order.userid;
                cmd.Parameters.Add("@ordertype", SqlDbType.VarChar).Value = order.ordertype;
                cmd.Parameters.Add("@createdate", SqlDbType.DateTime).Value = order.createdate;
                cmd.Parameters.Add("@modifydate", SqlDbType.DateTime).Value = order.modifydate;

                sqlConn.Open();
                cmd.ExecuteNonQuery();
                sqlConn.Close();

            }
        }

        public override void OrderItemSave(SqlCommand command, List<OrderItem> orderitems)
        {
            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                command.Connection = connection;

                SupplementalContentManager scm = new SupplementalContentManager();

                //SqlDataReader reader;
                
                foreach (OrderItem orderitem in orderitems)
                {
                    command = scm.OrderItemParameters(command, orderitem);

                    if (connection.State.ToString().ToLower().Equals("closed"))
                        connection.Open();
                    // original spot of reader
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.Default))
                    {
                         if (reader.HasRows)
                            while (reader.Read())
                                orderitem.orderitemid = reader.GetInt64(0);
                    }
                   
                }

                if (connection.State.ToString().ToLower().Equals("open"))
                    connection.Close();
            }
        }

        public override List<Product> ProductSelect(int productid = 0, string pname = "", string pdescription = "")
        {
            List<Product> products = new List<Product>();

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("dbo.spProductSelect", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (productid > 0)
                        command.Parameters.Add("@productid", SqlDbType.Int).Value = productid;

                    if (pname.Length > 0)
                        command.Parameters.Add("@name", SqlDbType.NVarChar).Value = pname;

                    if (pdescription.Length > 0)
                        command.Parameters.Add("@description", SqlDbType.NVarChar).Value = pdescription;

                    if (connection.State.ToString().ToLower().Equals("closed"))
                        connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {

                        if (reader.HasRows)
                        {
                            SupplementalContentManager scm = new SupplementalContentManager();

                            while (reader.Read())
                                products.Add(scm.ProductSelectPopulate(reader));
                        }
                        reader.Close();
                    }
                }
                return products;
            }
        }

        public override List<OrderItemHistory> GetOrderItemHistoryByOptions(int memberid = 0, long orderid = 0, string name = "", string email = "")
        {
            List<OrderItemHistory> oi = new List<OrderItemHistory>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.spOrderHistory", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                if (memberid > 0)
                    sqlCmd.Parameters.Add("@userid", SqlDbType.VarChar).Value = memberid;

                if (orderid > 0)
                    sqlCmd.Parameters.Add("@orderid", SqlDbType.VarChar).Value = orderid;

                if (email != "")
                    sqlCmd.Parameters.Add("@email", SqlDbType.VarChar).Value = email;

                if (name != "")
                    sqlCmd.Parameters.Add("@name", SqlDbType.VarChar).Value = name;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        oi.Add(PopulateOrderItemHistoryFromIDataReader(dr));
                    }
                }
                sqlConn.Close();

            }


            return oi;

        }
        public override List<Order> GetOrderList(long orderId = 0, string email = "", int userId = 0, long transactionId = 0)
        {
            List<Order> or = new List<Order>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.spOrderSelect", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                if (orderId > 0)
                    sqlCmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderId;

                if (email != "")
                    sqlCmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = email;

                if (userId > 0)
                    sqlCmd.Parameters.Add("@userid", SqlDbType.Int).Value = userId;

                if (transactionId > 0)
                    sqlCmd.Parameters.Add("@transactionid", SqlDbType.BigInt).Value = transactionId;

                sqlConn.Open();

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        or.Add(PopulateOrderFromIDataReader(dr));
                    }
                    
                }
                sqlConn.Close();
            }

            return or;
        }

        public override List<Order> GetOrders(long orderId, string lastName, string zip, string email, string phone, int userId, long transactionId)
        {
            List<Order> or = new List<Order>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("dbo.spOrderList", sqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                if (orderId > 0)
                    cmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderId;

                if (!string.IsNullOrEmpty(lastName))
                {
                    cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = lastName;
                }

                if (!string.IsNullOrEmpty(zip))
                {
                    cmd.Parameters.Add("@Zip", SqlDbType.NVarChar).Value = zip;
                }

                if (!string.IsNullOrEmpty(email))
                {
                    cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = email;
                }

                if (!string.IsNullOrEmpty(phone))
                {
                    cmd.Parameters.Add("@phone", SqlDbType.NVarChar).Value = phone;
                }
                if (userId > 0)
                {
                    cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userId;
                }

                if (transactionId > 0)
                {
                    cmd.Parameters.Add("@transactionid", SqlDbType.BigInt).Value = transactionId;
                }

                sqlConn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        or.Add(PopulateOrderFromIDataReader(dr));
                    }
                }

                sqlConn.Close();
 
            }

            return or;
        }

        public override List<OrderItem> GetOrderItemsByOrderId(long orderId = 0, long orderitemid = 0, int productid = 0)
        {
            List<OrderItem> or = new List<OrderItem>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.spOrderItemSelect", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                if (orderId > 0)
                    sqlCmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderId;

                if (orderitemid > 0)
                    sqlCmd.Parameters.Add("@orderitemid", SqlDbType.BigInt).Value = orderitemid;

                if (productid > 0)
                    sqlCmd.Parameters.Add("@productid", SqlDbType.BigInt);


                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        or.Add(PopulateOrderItemFromIDataReader(dr));
                    }
                    
                }

                sqlConn.Close();
            }

            return or;
        }

        public override OrderItem GetOrderItemById(long orderitemId)
        {
            OrderItem oi = new OrderItem();

            using(SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetOrderItemById", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@orderitemid", SqlDbType.BigInt).Value = orderitemId;

                sqlConn.Open();

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        oi = PopulateOrderItemFromIDataReader(dr);
                    }
                }

                sqlConn.Close();
            }

            return oi;
        }

        public override Order GetOrderById(long orderId = 0, int userId = 0, long transactionId = 0, string email = "")
        {
            Order or = new Order();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.spOrderSelect", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                if(orderId > 0)
                    sqlCmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderId;

                if (userId > 0)
                    sqlCmd.Parameters.Add("@userid", SqlDbType.Int).Value = userId;

                if (transactionId > 0)
                    sqlCmd.Parameters.Add("@tranactionid", SqlDbType.BigInt).Value = transactionId;

                if (email != "")
                    sqlCmd.Parameters.Add("@email", SqlDbType.VarChar).Value = email;


                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        or = PopulateOrderFromIDataReader(dr);
                    }
                }
                sqlConn.Close();

                return or;

            }
        }

        public override List<OrderItemHistory> OrderHistoryReport(string regiontype = "", string sku = "", string orderstatus = "",
            string subscriptionstatus = "")
        {
            List<OrderItemHistory> oih = new List<OrderItemHistory>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.spOrderHistoryReport", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(regiontype))
                    sqlCmd.Parameters.Add("@regiontype", SqlDbType.VarChar).Value = regiontype;

                if (!string.IsNullOrEmpty(sku))
          
                    sqlCmd.Parameters.Add("@sku", SqlDbType.VarChar).Value = sku;

                if (!string.IsNullOrEmpty(orderstatus))
                    sqlCmd.Parameters.Add("@orderstatus", SqlDbType.VarChar).Value = orderstatus;

                if (!string.IsNullOrEmpty(subscriptionstatus))
                    sqlCmd.Parameters.Add("@subscriptionstatus", SqlDbType.VarChar).Value = subscriptionstatus;

                sqlConn.Open();

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        oih.Add(PopulateOrderItemHistoryFromIDataReader(dr));
                    }
                }
                sqlConn.Close();

            }

            return oih;

        }

        public override void CreateUser(User user)
        {
            using(SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.spUserUpsert", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@userid", SqlDbType.VarChar).Value = user.userid;
                sqlCmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = user.lastname;
                sqlCmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = user.firstname;
                sqlCmd.Parameters.Add("@gender", SqlDbType.VarChar).Value = user.gender;
                sqlCmd.Parameters.Add("@email", SqlDbType.VarChar).Value = user.email;
                sqlCmd.Parameters.Add("@ismember", SqlDbType.VarChar).Value = user.isMember;

                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
                
            }
        }

        public override bool CurrentOrderExists(int userId, string region)
        {
            Boolean exists;

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.OrderCurrent", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@userId", SqlDbType.Int).Value = userId;
                sqlCmd.Parameters.Add("@regiontype", SqlDbType.VarChar).Value = region;
                
              

                sqlConn.Open();

                exists = Convert.ToBoolean(sqlCmd.ExecuteScalar().ToString());
                       
                sqlConn.Close();

            }

            return exists;
        }

        public override void UpdateOrderItemActive(long orderId, string region)
        {
            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.UpdateOrderItemActive", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@orderid", SqlDbType.VarChar).Value = orderId;
                sqlCmd.Parameters.Add("@regionType", SqlDbType.VarChar).Value = region;

               
                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();

            }
        }

        public override List<Order> GetOrderForProcessing()
        {
            List<Order> or = new List<Order>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.GetOrderHistoryForProcessing", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;


                sqlConn.Open();

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        or.Add(PopulateOrderFromIDataReader(dr));
                    }

                }
                sqlConn.Close();
            }

            return or;
        }

        public override List<OrderItem> GetOrderItemsForProcessing(long orderId)
        {
            List<OrderItem> or = new List<OrderItem>();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.spOrderItemSelect", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                if (orderId > 0)
                    sqlCmd.Parameters.Add("@orderid", SqlDbType.BigInt).Value = orderId;

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        or.Add(PopulateOrderItemFromIDataReader(dr));
                    }

                }

                sqlConn.Close();
            }

            return or;
        }


        #endregion  //  product / order methods

        #region default operation methods

        public override DataTable DBSPSelect()
        {
            DataTable table = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                SqlCommand sqlCmd = new SqlCommand("Select * From dbo.viewUSStates", sqlConn);
                sqlCmd.CommandType = CommandType.Text;

                if (sqlConn.State.ToString().ToLower().Equals("closed"))
                    sqlConn.Open();

                using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    adapter.Fill(table);
            }

            return table;
        }

        public override DataTable ZipSelect(string zip)
        {
            DataTable table = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand sqlCmd = new SqlCommand("dbo.spZipSelect", sqlConn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@zip", SqlDbType.NVarChar).Value = zip;

                    if (sqlConn.State.ToString().ToLower().Equals("closed"))
                        sqlConn.Open();

                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                        adapter.Fill(table);
                }
            }

            return table;
        }

        #endregion  //  default operation methods

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////

    }

}

