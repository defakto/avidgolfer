﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using AvidGolfer.Entities;
using AvidGolfer.Components;

namespace AvidGolfer.Data
{
    public abstract class SupplementalDataProvider : Defakto.Components.Provider.ProviderBase
    {

        public abstract string GetTest(int byId);

        public abstract CartGirlVote GetCartGirlsByNodeId(string nodeId);
        public abstract void CreateUpdateCartGirlVote(CartGirlVote cgVote);
        public abstract List<CartGirlVote> GetCartGirls();
        public abstract MostViewed GetArticleByNodeId(string nodeId);
        public abstract List<MostViewed> GetMostViewedArticles(string region);
        public abstract void CreateUpdateMostViewedArticle(MostViewed cgView);
        public abstract void CreateUpdateDailyDealOrder(DailyDealOrder ddOrder);
        public abstract List<DailyDealOrder> GetAllDailyDeals();
        public abstract List<DailyDealOrder> GetAllDailyDealsByDateRange(DateTime startDate, DateTime endDate);

        public abstract DataTable DBSPSelect();
        public abstract long OrderSave(SqlCommand command);

        // public abstract DataTable DBSPSelect(string query);
        public abstract DataTable ZipSelect(string zip);

        public abstract DataTable OrderHistory(int userid = 0, long orderid = 0);
        public abstract DataTable OrderHistoryCsv(string regiontype = "", string sku = "", string orderstatus = "",
            string subscriptionstatus = "");
        public abstract List<OrderItemHistory> OrderHistoryReport(string regiontype, string sku, string orderstatus, 
            string subscriptionstatus);
        public abstract void OrderItemSave(SqlCommand command, List<OrderItem> orderitems);

        public abstract List<Product> ProductSelect(int productid = 0, string pname = "", string pdescription = "");

        public abstract List<OrderItemHistory> GetOrderItemHistoryByOptions(int memberid = 0, long orderid = 0, string name = "", string email = "");
        public abstract Order GetOrderById(long orderId, int userId, long transactionId, string email);
        public abstract void SaveIndividualOrderItem(OrderItem item);
        public abstract OrderItem GetOrderItemById(long orderitemId);
        public abstract List<Order> GetOrderList(long orderId, string email, int userId, long transactionId);
        public abstract List<Order> GetOrders(long orderId, string lastName, string zip, string email, string phone, int userId, long transactionId);
        public abstract List<OrderItem> GetOrderItemsByOrderId(long orderId, long orderitemid, int productid);
        public abstract void CreateUser(User user);
        public abstract Boolean CurrentOrderExists(int userId, string region);
        public abstract void UpdateOrderItemActive(long orderId, string region);
        public abstract List<Order> GetOrderForProcessing();
        public abstract List<OrderItem> GetOrderItemsForProcessing(long orderId);
        public abstract DataSet GetOrderDataset(long orderId, string lastName, string zip, string email, string phone);
        public abstract void SavePreviousOrder(Order order);

        #region Helper Methods

        public static CartGirlVote PopulateCartGirlVoteFromIDataReader(IDataReader dr)
        {
            CartGirlVote cgVote = new CartGirlVote();

            cgVote.NodeId = (string)dr["nodeId"];
            cgVote.TotalVotes = (int)dr["totalVotes"];

            return cgVote;
        }

        public static DailyDealOrder PopulateDailyDealOrderFromIDataReader(IDataReader dr)
        {
            DailyDealOrder dOrder = new DailyDealOrder();

            dOrder.OrderId = (Guid)dr["orderId"];
            dOrder.FirstName = (string)dr["firstName"];
            dOrder.LastName = (string)dr["lastName"];
            dOrder.Email = (string)dr["email"];
            dOrder.Address1 = (string)dr["address1"];
            dOrder.Address2 = (string)dr["address2"];
            dOrder.City = (string)dr["city"];
            dOrder.State = (string)dr["state"];
            dOrder.ZipCode = (string)dr["zipCode"];
            dOrder.CouponCode = (string)dr["couponCode"];
            dOrder.DateCreated = (DateTime)dr["dateCreated"];

            return dOrder;
        }

        public static MostViewed PopulateMostViewedFromIDataReader(IDataReader dr)
        {
            MostViewed cgView = new MostViewed();
            cgView.NodeId = (string)dr["nodeId"];
            cgView.TotalHits = (int)dr["totalHits"];
            cgView.Region = (dr.IsDBNull(dr.GetOrdinal("region"))) ? String.Empty : (string)dr["region"];

            return cgView;
        }

        public static OrderItem PopulateOrderItemFromIDataReader(IDataReader dr)
        {
            OrderItem oi = new OrderItem();
            oi.orderitemid = (dr.IsDBNull(dr.GetOrdinal("OrderItemID"))) ? 0 : (long)dr["OrderItemID"];
            oi.orderid = (dr.IsDBNull(dr.GetOrdinal("OrderID"))) ? 0 : (long)dr["OrderID"];
            oi.itemid = (dr.IsDBNull(dr.GetOrdinal("ItemID"))) ? 0 : (int)dr["ItemID"];
            oi.productid = (dr.IsDBNull(dr.GetOrdinal("ProductID"))) ? 0 : (int)dr["ProductID"];
            oi.quantity = (dr.IsDBNull(dr.GetOrdinal("Quantity"))) ? 0 : (int)dr["Quantity"];
            oi.price = (dr.IsDBNull(dr.GetOrdinal("Price"))) ? 0 : (decimal)dr["Price"];
            oi.tax = (dr.IsDBNull(dr.GetOrdinal("Tax"))) ? 0 : (decimal)dr["Tax"];
            oi.status = (dr.IsDBNull(dr.GetOrdinal("Status"))) ? 0 : (int)dr["Status"];
            oi.startdate = (dr.IsDBNull(dr.GetOrdinal("StartDate"))) ? null : (DateTime?)dr["StartDate"];
            oi.expiredate = (dr.IsDBNull(dr.GetOrdinal("ExpireDate"))) ? null : (DateTime?)dr["ExpireDate"];
            oi.shipdate = (dr.IsDBNull(dr.GetOrdinal("ShipDate"))) ? null : (DateTime?)dr["ShipDate"];
            oi.createdate = (dr.IsDBNull(dr.GetOrdinal("CreateDate"))) ? null : (DateTime?)dr["CreateDate"];
            oi.modifydate = (dr.IsDBNull(dr.GetOrdinal("ModifyDate"))) ? null : (DateTime?)dr["ModifyDate"];
            oi.reviewdate = (dr.IsDBNull(dr.GetOrdinal("ReviewDate"))) ? null : (DateTime?)dr["ReviewDate"];
            oi.notes = (dr.IsDBNull(dr.GetOrdinal("Notes"))) ? String.Empty : (string)dr["Notes"];
            oi.productname = (dr.IsDBNull(dr.GetOrdinal("ProductName"))) ? String.Empty : (string)dr["ProductName"];
            oi.regionType = (dr.IsDBNull(dr.GetOrdinal("RegionType"))) ? String.Empty : (string)dr["RegionType"];
            oi.sku = (dr.IsDBNull(dr.GetOrdinal("Sku"))) ? String.Empty : (string)dr["Sku"];
            oi.orderStatus = (dr.IsDBNull(dr.GetOrdinal("OrderStatus"))) ? String.Empty : (string)dr["OrderStatus"];
            oi.subscriptionStatus = (dr.IsDBNull(dr.GetOrdinal("SubscriptionStatus"))) ? String.Empty : (string)dr["SubscriptionStatus"];
            oi.total = (dr.IsDBNull(dr.GetOrdinal("Total"))) ? 0 : (decimal)dr["Total"];
            
            return oi;

        }

        public static Order PopulateOrderFromIDataReader(IDataReader dr)
        {
            SupplementalContentManager scm = new SupplementalContentManager();
            Order or = new Order();

          
            
            or.orderid = (dr.IsDBNull(dr.GetOrdinal("OrderID"))) ? 0 : (long)dr["OrderID"];
            or.userid = (dr.IsDBNull(dr.GetOrdinal("UserID"))) ? 0 : (int)dr["UserID"];
            or.tansactionid = (dr.IsDBNull(dr.GetOrdinal("TransactionID"))) ? 0 : (long)dr["TransactionID"];
            or.billaddressid = (dr.IsDBNull(dr.GetOrdinal("BillAddressID"))) ? 0 : (int)dr["BillAddressID"];
            or.billphone = (dr.IsDBNull(dr.GetOrdinal("BillPhone"))) ? String.Empty : (string)dr["BillPhone"];
            or.billemail = (dr.IsDBNull(dr.GetOrdinal("BillEmail"))) ? String.Empty : (string)dr["BillEmail"];
            or.shipaddressid = (dr.IsDBNull(dr.GetOrdinal("ShipAddressID"))) ? 0 : (int)dr["ShipAddressID"];
            or.shipphone = (dr.IsDBNull(dr.GetOrdinal("ShipPhone"))) ? String.Empty : (string)dr["ShipPhone"];
            or.shipemail = (dr.IsDBNull(dr.GetOrdinal("ShipEMail"))) ? String.Empty : (string)dr["ShipEMail"];
            or.createdate = (dr.IsDBNull(dr.GetOrdinal("CreateDate"))) ? null : (DateTime?)dr["CreateDate"];
            or.modifydate = (dr.IsDBNull(dr.GetOrdinal("ModifyDate"))) ? null : (DateTime?)dr["ModifyDate"];
            or.reviewdate = (dr.IsDBNull(dr.GetOrdinal("ReviewDate"))) ? null : (DateTime?)dr["ReviewDate"];
            or.notes = (dr.IsDBNull(dr.GetOrdinal("Notes"))) ? String.Empty : (string)dr["Notes"];
            or.billstreet = (dr.IsDBNull(dr.GetOrdinal("BillStreet"))) ? String.Empty : (string)dr["BillStreet"];
            or.billzip = (dr.IsDBNull(dr.GetOrdinal("BillZip"))) ? String.Empty : (string)dr["BillZip"];
            or.billcity = (dr.IsDBNull(dr.GetOrdinal("BillCity"))) ? String.Empty : (string)dr["BillCity"];
            or.billstate = (dr.IsDBNull(dr.GetOrdinal("BillState"))) ? String.Empty : (string)dr["BillState"];
            or.shipstreet = (dr.IsDBNull(dr.GetOrdinal("ShipStreet"))) ? String.Empty : (string)dr["ShipStreet"];
            or.shipcity = (dr.IsDBNull(dr.GetOrdinal("ShipCity"))) ? String.Empty : (string)dr["ShipCity"];
            or.shipstate = (dr.IsDBNull(dr.GetOrdinal("ShipState"))) ? String.Empty : (string)dr["ShipState"];
            or.shipzip = (dr.IsDBNull(dr.GetOrdinal("ShipZip"))) ? String.Empty : (string)dr["ShipZip"]; 
            or.shippingamount = (dr.IsDBNull(dr.GetOrdinal("ShippingAmount"))) ? 0 : (decimal)dr["ShippingAmount"];
            or.ordertype = (dr.IsDBNull(dr.GetOrdinal("OrderType"))) ? String.Empty : (string)dr["OrderType"];
            or.firstname = (dr.IsDBNull(dr.GetOrdinal("FirstName"))) ? String.Empty : (string)dr["FirstName"];
            or.lastname = (dr.IsDBNull(dr.GetOrdinal("LastName"))) ? String.Empty : (string)dr["LastName"];
            or.membershipemail = (dr.IsDBNull(dr.GetOrdinal("MembershipEmail"))) ? String.Empty : (string)dr["MembershipEmail"];

            or.orderitems = scm.GetOrderItemsByOrderId(or.orderid);

            return or;
        }

        public static OrderItemHistory PopulateOrderItemHistoryFromIDataReader(IDataReader dr)
        {
            OrderItemHistory oh = new OrderItemHistory();

/*
	o.OrderID, o.UserID, o.TransactionId, o.BillAddressID, o.BillPhone, o.BillEMail, 
	o.ShipAddressID, o.ShipPhone, o.ShipEMail,  
	oi.OrderItemID, oi.ItemID, oi.ProductID, oi.Quantity, oi.Price, oi.Tax, oi.Status, 
	oi.StartDate, oi.ExpireDate, oi.ShipDate, oi.CreateDate, oi.ModifyDate, oi.ReviewDate, oi.Notes
*/

            oh.billaddressid = (dr.IsDBNull(dr.GetOrdinal("BillAddressID"))) ? 0 : (int)dr["BillAddressID"];
            oh.billemail = (dr.IsDBNull(dr.GetOrdinal("BillEmail"))) ? String.Empty : (string)dr["BillEmail"];
            oh.billphone = (dr.IsDBNull(dr.GetOrdinal("BillPhone"))) ? String.Empty : (string)dr["BillPhone"];
            oh.billstreet = (dr.IsDBNull(dr.GetOrdinal("BillAddress"))) ? String.Empty : (string)dr["BillAddress"];
            oh.billzip = (dr.IsDBNull(dr.GetOrdinal("BillZip"))) ? String.Empty : (string)dr["BillZip"];
            oh.createdate = (dr.IsDBNull(dr.GetOrdinal("CreateDate"))) ? null : (DateTime?)dr["CreateDate"];
            oh.expiredate = (dr.IsDBNull(dr.GetOrdinal("ExpireDate"))) ? null : (DateTime?)dr["ExpireDate"];
            oh.itemid = (dr.IsDBNull(dr.GetOrdinal("ItemId"))) ? 0 : (int)dr["ItemId"];
            oh.itemstatus = (dr.IsDBNull(dr.GetOrdinal("Status"))) ? 0 : (int)dr["Status"];
            oh.modifydate = (dr.IsDBNull(dr.GetOrdinal("ModifyDate"))) ? null : (DateTime?)dr["ModifyDate"];
            oh.ordernotes = (dr.IsDBNull(dr.GetOrdinal("OrderNotes"))) ? String.Empty : (string)dr["OrderNotes"];
            oh.orderid = (dr.IsDBNull(dr.GetOrdinal("OrderId"))) ? 0 : (long)dr["OrderId"];
            oh.orderitemid = (dr.IsDBNull(dr.GetOrdinal("OrderItemID"))) ? 0 : (long)dr["OrderItemID"];
            oh.itemnotes = (dr.IsDBNull(dr.GetOrdinal("ItemNotes"))) ? String.Empty : (string)dr["ItemNotes"];
            oh.price = (dr.IsDBNull(dr.GetOrdinal("Price"))) ? 0 : (decimal)dr["Price"];
            oh.productid = (dr.IsDBNull(dr.GetOrdinal("ProductID"))) ? 0 : (int)dr["ProductID"];
            oh.productname = (dr.IsDBNull(dr.GetOrdinal("Product"))) ? String.Empty : (string)dr["Product"];
            oh.quantity = (dr.IsDBNull(dr.GetOrdinal("Quantity"))) ? 0 : (int)dr["Quantity"];
            oh.reviewdate = (dr.IsDBNull(dr.GetOrdinal("ReviewDate"))) ? null : (DateTime?)dr["ReviewDate"];
            oh.shipaddressid = (dr.IsDBNull(dr.GetOrdinal("ShipAddressID"))) ? 0 : (int)dr["ShipAddressID"];
            oh.shipdate = (dr.IsDBNull(dr.GetOrdinal("ShipDate"))) ? null : (DateTime?)dr["ShipDate"];
            //oh.shipdate = (DateTime)dr["ShipDate"];
            oh.shipemail = (dr.IsDBNull(dr.GetOrdinal("ShipEmail"))) ? String.Empty : (string)dr["ShipEmail"];
            oh.shipphone = (dr.IsDBNull(dr.GetOrdinal("ShipPhone"))) ? String.Empty : (string)dr["ShipPhone"];
            oh.shipstreet = (dr.IsDBNull(dr.GetOrdinal("ShipAddress"))) ? String.Empty : (string)dr["ShipAddress"];
            oh.shipzip = (dr.IsDBNull(dr.GetOrdinal("ShipZip"))) ? String.Empty : (string)dr["ShipZip"];
            oh.transactionid = (dr.IsDBNull(dr.GetOrdinal("TransactionId"))) ? 0 : (long)dr["TransactionId"];
            oh.tax = (dr.IsDBNull(dr.GetOrdinal("Tax"))) ? 0 : (decimal)dr["Tax"];
            oh.total = (dr.IsDBNull(dr.GetOrdinal("Price"))) ? 0 : (decimal)dr["Price"];
            oh.userid = (dr.IsDBNull(dr.GetOrdinal("UserID"))) ? 0 : (int)dr["UserID"];
            oh.productname = (dr.IsDBNull(dr.GetOrdinal("Product"))) ? String.Empty : (string)dr["Product"];
            oh.regiontype = (dr.IsDBNull(dr.GetOrdinal("RegionType"))) ? String.Empty : (string)dr["RegionType"];
            oh.sku = (dr.IsDBNull(dr.GetOrdinal("Sku"))) ? String.Empty : (string)dr["Sku"];
            oh.ordertype = (dr.IsDBNull(dr.GetOrdinal("OrderType"))) ? String.Empty : (string)dr["OrderType"];
            oh.subscriptionstatus = (dr.IsDBNull(dr.GetOrdinal("SubscriptionStatus"))) ? String.Empty : 
                (string)dr["SubscriptionStatus"];
            oh.orderstatus = (dr.IsDBNull(dr.GetOrdinal("OrderStatus"))) ? String.Empty : (string)dr["OrderStatus"];
            oh.firstname = (dr.IsDBNull(dr.GetOrdinal("FirstName"))) ? String.Empty : (string)dr["FirstName"];
            oh.lastname = (dr.IsDBNull(dr.GetOrdinal("LastName"))) ? String.Empty : (string)dr["LastName"];
            oh.billstate = (dr.IsDBNull(dr.GetOrdinal("BillState"))) ? String.Empty : (string)dr["BillState"];
            oh.shipstate = (dr.IsDBNull(dr.GetOrdinal("ShipState"))) ? String.Empty : (string)dr["ShipState"];
            oh.billcity = (dr.IsDBNull(dr.GetOrdinal("BillCity"))) ? String.Empty : (string)dr["BillCity"];
            oh.shipcity = (dr.IsDBNull(dr.GetOrdinal("ShipCity"))) ? String.Empty : (string)dr["ShipCity"];


            return oh;
        }

        public string GetConnectionString()
        {
            string connectionStringName = this.Configuration["connectionStringName"];

            if (String.IsNullOrEmpty(connectionStringName))
                throw new ProviderException("Empty or missing connectionStringName");

            if (WebConfigurationManager.ConnectionStrings[connectionStringName] == null)
                throw new ProviderException("Missing connection string");

            return WebConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }

        #endregion

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////

    }
}
