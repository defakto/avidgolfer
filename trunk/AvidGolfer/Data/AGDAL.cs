﻿using System;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

using AvidGolfer.Entities;

//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

namespace AvidGolfer.Data
{
    public partial class AGDAL : IAGDAL, IDisposable
    {
        SqlConnection dbConnect;  //  Global sql connection object/ string

        string message = string.Empty;

        public AGDAL()
        {
            dbConnect = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["supplementalSqlServer"].ConnectionString);
        }

        /////////////////////////////////////////////////////////////////////////////////////
        #region return methods

        DataTable DataTableSchema(string tablename)
        {
            string qry = "Set FMTONLY On; Select * From [@tablename]; Set FMTONLY off";
            qry = qry.Replace("@tablename", tablename);

            DataTable table = new DataTable();
            table = DBSelect(qry, tablename);

            return table;
        }

        int DBExecuteNonQuery(string query)
        {
            int rowsaffected = 0;

            using (SqlCommand command = new SqlCommand(query, dbConnect))
            {
                try
                {
                    if (dbConnect.State.ToString().ToLower().Equals("closed"))
                        dbConnect.Open();

                    rowsaffected = command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    throw new ArgumentException(ex.Message);
                }
                finally
                {
                    if (dbConnect.State.ToString().ToLower().Equals("open"))
                        dbConnect.Close();
                }
            }
            return rowsaffected;
        }

        // sql data reader from query / storedprocedure
        SqlDataReader DBSearch(SqlDataReader reader, string query, bool isSP = false)
        {
            try
            {
                using (SqlCommand command = new SqlCommand(query, dbConnect))
                {
                    if (isSP)
                        command.CommandType = CommandType.StoredProcedure;

                    if (dbConnect.State.ToString().ToLower().Equals("closed"))
                        dbConnect.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                }

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

            return reader;
        }

        // sql data reader from command
        SqlDataReader DBSearch(SqlCommand command)
        {
            command.Connection = dbConnect;

            if (dbConnect.State.ToString().ToLower().Equals("closed"))
                dbConnect.Open();

            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            return reader;

        }

        DataTable DBSelect(string select, string tableName = "")
        {
            DataTable table = new DataTable();

            using (SqlCommand command = new SqlCommand(select, dbConnect))
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    try
                    {

                        if (dbConnect.State.ToString().ToLower().Equals("closed"))
                            dbConnect.Open();

                        adapter.Fill(table);
                    }
                    catch (SqlException ex)
                    {
                        throw new ArgumentException(ex.Message);
                    }
                    finally
                    {
                        if (dbConnect.State.ToString().ToLower().Equals("open"))
                            dbConnect.Close();
                    }
                }
            }

            if (tableName.Length > 0)
                table.TableName = tableName;

            return table;

        }

        #endregion  //  return methods
        /////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////
        #region Operations

        public User AGUserSelect(int userid = 0, string username = "", string email = "")
        {
            User aguser = new User();

            using (SqlCommand command = new SqlCommand("dbo.spAGUserSelect", dbConnect))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@aguserid", SqlDbType.Int).Value = userid;
                command.Parameters.Add("@agusername", SqlDbType.NVarChar).Value = username;
                command.Parameters.Add("@email", SqlDbType.NVarChar).Value = email;

                if (dbConnect.State.ToString().ToLower().Equals("closed"))
                    dbConnect.Open();

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    int value = 0;

                    if (reader.HasRows)
                        while (reader.Read())
                        {
                            if (Int32.TryParse(reader["AGUserID"].ToString(), out value))
                                aguser.AGUserID = value;

                            aguser.UserName = reader["AGUserName"] + "";
                            aguser.LastName = reader["LastName"] + "";
                            aguser.FirstName = reader["FirstName"] + "";
                            aguser.Gender = reader["Gender"] + "";
                            aguser.Email = reader["EMail"] + "";
                            aguser.Phone = reader["Phone"] + "";
                            aguser.Address1 = reader["Address"] + "";
                            aguser.MailZone = reader["MailZone"] + "";
                            aguser.City = reader["City"] + "";
                            aguser.State = reader["State"] + "";
                            aguser.Zip = reader["Zip"] + "";
                            aguser.Zip4 = reader["Zip4"] + "";

                            break;
                        }

                    reader.Close();
                }
            }

            return aguser;
        }

        public bool AGUserUpdate(ref User aguser)
        {
            bool isSuccess = false;

            using (SqlCommand command = new SqlCommand("dbo.spAGUserUpsert", dbConnect))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@aguserid", SqlDbType.Int).Value = aguser.AGUserID;
                command.Parameters.Add("@agusername", SqlDbType.NVarChar).Value = aguser.UserName;
                command.Parameters.Add("@lastname", SqlDbType.NVarChar).Value = aguser.LastName;
                command.Parameters.Add("@firstname", SqlDbType.NVarChar).Value = aguser.FirstName;
                command.Parameters.Add("@gender", SqlDbType.NVarChar).Value = aguser.Gender;
                command.Parameters.Add("@email", SqlDbType.NVarChar).Value = aguser.Email;
                command.Parameters.Add("@phone", SqlDbType.NVarChar).Value = aguser.Phone;
                command.Parameters.Add("@street", SqlDbType.NVarChar).Value = aguser.Address1;
                command.Parameters.Add("@zip", SqlDbType.NVarChar).Value = aguser.Zip;
                command.Parameters.Add("@zip4", SqlDbType.NVarChar).Value = aguser.Zip4;

                if (aguser.AddressType < 10)
                    aguser.AddressType = 10;
                command.Parameters.Add("@addresstype", SqlDbType.Int).Value = aguser.AddressType;

                command.Parameters.Add("@mailzone", SqlDbType.NVarChar).Value = aguser.MailZone;

                if (dbConnect.State.ToString().ToLower().Equals("closed"))
                    dbConnect.Open();

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    int value = 0;

                    if (reader.HasRows)
                    while (reader.Read())
                    {
                        if (Int32.TryParse(reader["AGUserID"].ToString(), out value))
                            aguser.AGUserID = value;

                        if (value > 0)
                            isSuccess = true;

                        break;
                    }
                    reader.Close();
                }
            }

            return isSuccess;
        }

        public void ZipLocationSelect(ref string city, ref string state, ref string county, string zip)
        {
            using (SqlCommand command = new SqlCommand("dbo.spZipSelect", dbConnect))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@zip", SqlDbType.NVarChar).Value = zip;

                if (dbConnect.State.ToString().ToLower().Equals("closed"))
                    dbConnect.Open();

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (reader.HasRows)
                        while (reader.Read())
                        {
                            city = reader["City"] + "";
                            state = reader["State"] + "";
                            county = reader["County"] + "";
                            break;
                        }
                    reader.Close();
                }
            }
        }

        #endregion  //  Operations
        /////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////
        #region Dispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion  //  Dispose
        /////////////////////////////////////////////////////////////////////////////////////
    }

}
