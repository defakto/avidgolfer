<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">


	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:call-template name="renderWidgetTwitterFeed">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="renderWidgetTwitterFeed">
		<xsl:param name="home"/> 
			
		<div id="twitter" class="twitter_feed">	
			<div id="twitter_t">Ideal Golfer</div>
			<div id="twitter_m">
				<div id="twitter_container">
					<ul id="twitter_update_list"></ul>
				</div>
			</div>
			<div id="twitter_b">
				<img src="images/small_twitter_share.png" />
				<a href="http://twitter.com/iDealGolfer" id="twitter-link" target="_blank">Join the conversation</a>
			</div>
		</div>
		
		<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
		<script type="text/javascript" src="https://api.twitter.com/1/statuses/user_timeline/iDealGolfer.json?callback=twitterCallback2&amp;count=3"></script>
		
		
	</xsl:template>
	
		  

</xsl:stylesheet>