﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">


    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <div id="modalPassbook"  onmouseover="clearPassbookTimer()" onmouseout="restartPassbookTimer()">
             
            <h1>GET THE PASSBOOK</h1>
            <a href="javascript:closePassbookButtonClick()">
                X
            </a>
            <p>
                <a class="getPassbook" href="/subscribe.aspx">Over 40 Free rounds of golf and more for only $49.95.</a> 
            </p>
                
        </div>
    </xsl:template>

</xsl:stylesheet>
