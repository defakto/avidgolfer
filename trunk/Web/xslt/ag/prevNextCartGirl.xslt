﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderDetermineIssueYear">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="renderDetermineIssueYear">
        <xsl:param name="home"/>

        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>

        <xsl:choose>
            <xsl:when test="count($home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue) = 0">
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year) - 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    <xsl:template name="renderCartGirlList">
        <xsl:param name="home"/>
        <xsl:param name="year"/>
        <xsl:variable name="pGirl" select="$currentPage/parent::Issue/following-sibling::Issue[1]/CartGirlProfile" />
        <xsl:variable name="hRegion" select="$currentPage/self::CartGirlProfile" />
        <xsl:variable name="articleType" select="$currentPage/articleType" />
        <xsl:variable name="tags" select="umbraco.library:Split($currentPage/tags, ',')" />
        <xsl:variable name="girlUrl" select="$home/umbTextpage/CartGirlPage [regionType = $hRegion/regionType]/@id" />
        <div class="past_cart_girls">
            <div class="past_left">
                <xsl:if test="count($pGirl) &gt; 0">
                    <xsl:for-each select="$pGirl">
                        <xsl:choose>
                            <xsl:when test=" ./regionType = $hRegion/regionType">
                                
								
								<xsl:variable name="url" >
									<xsl:choose>
										<xsl:when test="./votePageImage != ''">
											<xsl:value-of select="umbraco.library:GetMedia(votePageImage,'false')/umbracoFile" />
										</xsl:when>
										<xsl:otherwise>/images/blank.png</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								
								
                                <a href="{umbraco.library:NiceUrl(@id)}">
                                    <img src="{$url}" width="100" height="100" />
                                </a>
                                <br />
                                <div class="previous_left">
                                    <img src="/images/caret-left.png" />
                                    <xsl:value-of select="title"/> 
                                </div>
                               
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:if>
            </div>
            <a href="{umbraco.library:NiceUrl($girlUrl)}">&#8226;&nbsp; See all our past Cart Girls &nbsp;&#8226;</a>

            <xsl:variable name="nGirl" select="$currentPage/parent::Issue/preceding-sibling::Issue[1]/CartGirlProfile" />
            <div id="bNext" class="past_right">
                <xsl:if test="count($nGirl) &gt; 0">
                    <xsl:for-each select="$nGirl">
                        <xsl:choose>
                            <xsl:when test=" ./regionType = $hRegion/regionType">

								<xsl:variable name="url" >
									<xsl:choose>
										<xsl:when test="./votePageImage != ''">
											<xsl:value-of select="umbraco.library:GetMedia(votePageImage,'false')/umbracoFile" />
										</xsl:when>
										<xsl:otherwise>/images/blank.png</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								
                                <a href="{umbraco.library:NiceUrl(@id)}">
                                    <img src="{$url}" width="100" height="100" />

                                </a>
                                <br />
                                <div class="next_right">
                                    <xsl:value-of select="title"/>
                                     <img src="/images/caret-right.png" />  
                                </div>
                                   
                                    
                                     
                              
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:if>
            </div>    
        </div>
        <div class="clear"></div>
         <div class="tags">
                <xsl:if test="string-length($tags) != 0">
                    Tags:
                    <xsl:for-each select="$tags//value">
                        <xsl:variable name="currentTag" select="current()" />
                        <xsl:if test="position() != last()">
                            <a href="javascript:tagsRedirect('{$currentTag}')">
                                <xsl:value-of select="current()"/>,
                            </a>
                        </xsl:if>
                        <xsl:if test="position() = last()">
                            <a href="javascript:tagsRedirect('{$currentTag}')">
                                <xsl:value-of select="current()"/>
                            </a>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:if>
            </div>
            <div class="related_articles">
                <xsl:if test="string-length($articleType) != 0">
                    <a href="javascript:relatedArticleRedirect('{$articleType}')">Read All Related Articles</a>
                </xsl:if>
            </div>
        
        </xsl:template>  
</xsl:stylesheet>


