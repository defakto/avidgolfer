﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">


    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:variable name="items" select="$currentPage/TeeboxAudio" />
        <div class="audio_links">
            <ul>
            <xsl:if test="count($items) &gt; 0">
                <xsl:for-each select="$items">
                    <li>
                    <xsl:variable name="url" select="umbraco.library:GetMedia(audioFile,'false')/umbracoFile" />
                    <a href="{$url}" target="_blank">
                    <xsl:value-of select="title"/>&nbsp;
                    <!--<xsl:value-of select="date"/>-->
                    </a>    
                    </li>
                    
                </xsl:for-each>
            </xsl:if>
            </ul>
        </div>
    </xsl:template>
</xsl:stylesheet>
