<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            var location;
            var counter = 0;
           function splitCookie(cookie) {
                location = cookie.split("expires"); 
                return location[0];
            }
            
            function countItems() {
                counter++;
                return counter;
            }
            
            function calculateDate(endDate) {
                var today = new Date();
                today.setDate(today.getDate());
                today = today.toGMTString();
                
                return today;
            }
			
			function padDigit(number) {
				return (number < 10 ? '0' : '') + number
			}			
			
			function calculateTimeRemaining(remainingSeconds){
				var numdays = Math.floor(remainingSeconds / 86400);
				var numhours = Math.floor((remainingSeconds % 86400) / 3600);
				var numminutes = Math.floor(((remainingSeconds % 86400) % 3600) / 60);
				
				return padDigit(numdays) + "d:" + padDigit(numhours) + "h:" + padDigit(numminutes) + "m";
			}
            
        ]]>
    </msxml:script>

	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:call-template name="renderWidgetIdealGolfer">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="renderWidgetIdealGolfer">
		<xsl:param name="home"/>
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>
        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="ctRegion" select="$home/Widgets/IdealGolferOffer/descendant::RegionToCity" />
        <xsl:variable name="ctCities">
            <xsl:for-each select="$ctRegion">
                 <xsl:choose>
                <xsl:when test ="./regionType = $cRegion">
                    <xsl:value-of select="./cities" />
                </xsl:when>
            </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="cUrl">
            <xsl:for-each select="$ctRegion">
                <xsl:choose>
                    <xsl:when test="./regionType = $cRegion">
                        <xsl:value-of select="./headerUrl" />
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
                <xsl:variable name="idealOffers" select="umbraco.library:GetXmlDocumentByUrl('http://ws.nimblecommerce.com/rest/v1/deals/feed/getMerchantDealsFeed/xml?partnerKey=XE95atJPqKGDEbhhiG8IHqeR1AAQDoWB')" />
                <xsl:variable name="offers" select="$idealOffers/descendant::offer"/>
                <xsl:for-each select="$offers">
                        <xsl:choose>
                        <xsl:when test="./categoryName = $ctCities and js:countItems() = 1 ">
                                    <div class="idealGolfer">
                                        <xsl:variable name="currentPrice" select="./currentPrice" />
                                        <xsl:variable name="originalPrice" select="./origPrice"/>
                                        <xsl:variable name="endDate" select="./offerEndDateTime" />
                                        <xsl:variable name="savings" select="$originalPrice -$currentPrice"  />
										<xsl:variable name="curDate" select="umbraco.library:Replace(umbraco.library:CurrentDate(), 'T', ' ')" />
                                        <xsl:variable name="dateDiff" select="umbraco.library:DateDiff($endDate, $curDate, 's')" />
									
										<xsl:variable name="imageUrl" select="./imageUrl" />
                                        <a href="{$cUrl}" target="_blank" >
                                           <img src="/images/ideal_golfer_header.png" width="292" height="55" /> 
                                        </a>
										<div class="dealImg">
											<img src="{$imageUrl}" height="150" />
										</div>
										<div class="dealDetails">
											<div class="dealDesc">
												<xsl:value-of select="umbraco.library:StripHtml(./offerTitle)"/>
											</div>
											<div class="dealValue">
												$<xsl:value-of select="./currentPrice"/>
												<div class="dealView">
                                                    <xsl:variable name="bUrl" select="./offerPageUrl"/>
													<a href="{$bUrl}" target="_blank">
														<img src="/images/btnViewDeal.png" />
													</a>
												</div>
												<div class="clear"></div>
											</div>
											<div class="dealTimeLeft">
												<span class="lblTimeLeft">Time Left:</span> <xsl:value-of select="js:calculateTimeRemaining($dateDiff)"/>
											</div>
											<div class="dealStats">
												<ul>
													<li>Value<br />
													<div class="dealStatValue">$<xsl:value-of select="./origPrice"/></div>
												</li>
													<li>
													  Discount<br />
														<div class="dealStatValue"><xsl:value-of select="./percentageDiscount"/>%</div>
													</li>
                                                    <xsl:choose>
                                                      <xsl:when test="number(./buyersCount) &gt; 100">                                               
                                                         <li>
														    Purchased<br />
														    <div class="dealStatValue"><xsl:value-of select="./buyersCount"/></div>
													       </li> 
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <li>
                                                               Savings<br />
                                                                <div class="dealStatValue">
                                                                    $<xsl:value-of select="$savings"/>
																	
                                                                </div>
                                                            </li>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
													
												</ul>
												<div class="clear"></div>
											</div>
										</div>
                                       </div>
                                </xsl:when>
                            <xsl:otherwise>
                            </xsl:otherwise>
                            </xsl:choose>
                </xsl:for-each>
        
    </xsl:template>
</xsl:stylesheet>