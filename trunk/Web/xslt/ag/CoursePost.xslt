<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">



	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:variable name="qsIndex" select="umbraco.library:RequestQueryString('p')" />

		<xsl:choose>
			<xsl:when test="$qsIndex !=''">
				<xsl:call-template name="determinePageIndex"><xsl:with-param name="pageIndex" select="$qsIndex"/></xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="determinePageIndex"><xsl:with-param name="pageIndex" select="1"/></xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:template name="determinePageIndex">
		<xsl:param name="pageIndex"/> 

		<xsl:call-template name="renderFeaturedArticlePost">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
			<xsl:with-param name="pageIndex" select="$pageIndex"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="renderFeaturedArticlePost">
		<xsl:param name="home"/> 
		<xsl:param name="pageIndex"/> 


			<h1><xsl:value-of select="$currentPage/title" /></h1>
			
			<div class="article_content">
				<div class="coursePhoto">
					<xsl:choose>
						<xsl:when test="$currentPage/photo !=''">
							<img src="{umbraco.library:GetMedia($currentPage/photo, 0)/umbracoFile}" width="300" height="300" align="left" />
						</xsl:when>
						<xsl:otherwise>
							<img src="/images/blank.png" width="300" height="300" align="left" />
						</xsl:otherwise>
					</xsl:choose>
				</div>
				<div class="coursePostLeftPane">
					<b>Address:</b> <br />
					<xsl:value-of select="$currentPage/address1" /> <br />
					<xsl:value-of select="$currentPage/address2" /> <br />
					<br />
					<b>Region: </b> <xsl:value-of select="$currentPage/regionDesc" /> <br />
					<b>Phone: </b> <xsl:value-of select="$currentPage/phone" /> <br />
					<b>Price Range: </b> <xsl:value-of select="$currentPage/priceRange" /> <br />
					<b>Region: </b> <xsl:value-of select="$currentPage/regionDesc" /> <br />						
					<br />
					<b>Designer: </b> <xsl:value-of select="$currentPage/designer" /> <br />
					<b>Year Opened: </b> <xsl:value-of select="$currentPage/yearOpened" /> <br />
					<Br />
					<xsl:if test="$currentPage/normalRates !='' ">
						<b>Normal Rates:</b> <br />
						<xsl:value-of select="$currentPage/normalRates" disable-output-escaping="yes" /> <br />
						<br />
					</xsl:if>
					
				</div>
				<div class="clear"></div>
				
				<xsl:value-of select="$currentPage/bodyText" disable-output-escaping="yes" />
				
			</div>
			
			<!--
			<div class="clear"></div>
			<div class="tags">Tags: Golf, Celebrity, Miles Austin</div>
			<div class="related_articles">Read All Related Articles</div>
			<div class="clear"></div>
			-->
			

		<div class="clear"></div>
		
		
	</xsl:template>
		  

</xsl:stylesheet>