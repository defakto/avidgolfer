﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">
   
    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderDetermineIssueYear">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="renderDetermineIssueYear">
        <xsl:param name="home"/>

        <xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>

        <xsl:choose>
            <xsl:when test="count($home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue) = 0">
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year) - 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="renderCartGirlList">
                    <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
                    <xsl:with-param name="year" select="number($year)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template name="renderCartGirlList">
        <xsl:param name="home"/>
        <xsl:param name="year"/>
        <xsl:variable name="hRegion" select="$currentPage/self::CartGirlProfile" />
        <xsl:variable name="items" select="$home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue/CartGirlProfile" />
        <xsl:variable name="girlUrl" select="$home/umbTextpage/CartGirlPage [regionType = $hRegion/regionType]/@id" />
         <div class="right_past_cg">
            <a href="{umbraco.library:NiceUrl($girlUrl)}">&#8226;&nbsp; See all our past Cart Girls &nbsp;&#8226;</a>
            <div id="pPrev" class="up_scroll">
                <a href="#">
                <img src="/images/up_scroll.png" width="40" height="9" align="center" />
                </a>
            </div>
            <div id="prevGirl">
        <xsl:if test="count($items) &gt; 0">
            <xsl:for-each select="$items">
            
                <xsl:choose>
                    <xsl:when test="./regionType = 'All' or ./regionType = $hRegion/regionType and ./@id != $hRegion/@id">
					
                        <xsl:variable name="url" >
							<xsl:choose>
								<xsl:when test="./cartGirlTeaser != ''">
									<xsl:value-of select="umbraco.library:GetMedia(cartGirlTeaser,'false')/umbracoFile" />
								</xsl:when>
								<xsl:otherwise>/images/blank.png</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
                        <xsl:variable name="title" select="title" />
                        <div class="cg_teaser">
                            <a href="{umbraco.library:NiceUrl(./@id)}">
                            <img src="{$url}" width="96" height="96" align="left" />
                            </a>
                            <h2>
                                <xsl:value-of select="month"/>&nbsp;
                                <xsl:value-of select="year"/>
                            </h2>
							
                            <h1>
                                  <xsl:value-of select="$title" />
                            </h1>
                           
                            <a href="{umbraco.library:NiceUrl(./@id)}">
                                <span class="view_profile">View Profile</span>
                            </a>
                        </div>
                    </xsl:when>
                </xsl:choose>         
            </xsl:for-each>
        </xsl:if>
            </div>
            <div id="pNext" class="down_scroll">
                <a href="#">
                <img src="/images/down_scroll.png" width="40" height="9" align="center" />
                </a>
            </div>
             <div class="cgVerticalScrollViewAll">
                 <a href="{umbraco.library:NiceUrl($girlUrl)}">View All</a>
             </div>
            
        </div>
    </xsl:template>
</xsl:stylesheet>