﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">

   
    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>
   
    <xsl:template match="/">
        <xsl:variable name="items" select="$currentPage/CartGirlPhoto" />
						<div class="main_image_scroll">
							<div id="cartMain" class="main_image">
                                <xsl:if test="count($items) &gt; 0">
                                        <xsl:for-each select="$items">
                                        <xsl:variable name="url" select="umbraco.library:GetMedia(photo,'false')/umbracoFile"/>
                                       <img src="{$url}" width="640" height="640" align="center" />  
                                            
                                        </xsl:for-each>
                                </xsl:if>
							</div>
							<div class="scroll">
                                <div id="gPrev" class="left_scroll mini">
                                    <a href="#">
                                    <img src="/images/left_scroll.png" width="9" height="40" align="center" />
                                    </a>
                                </div>
                                <xsl:if test="count($items) &gt; 0">
                                    <ul id="miniCart" class="scroll_options">
                                    <xsl:for-each select="$items">
                                        <xsl:variable name="url" select="umbraco.library:GetMedia(thumbnail,'false')/umbracoFile"/>
                                        <li class="mini">
                                            <a href="#">
                                                <img src="{$url}" width="50" height="50" align="center" />
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                    </ul>    
                                </xsl:if>
								<div id="gNext" class="right_scroll mini">
                                <a href="#">
								<img src="/images/right_scroll.png" width="9" height="40" align="center" />
                                </a>   
								</div>
							</div>
						</div>
    </xsl:template>
</xsl:stylesheet>