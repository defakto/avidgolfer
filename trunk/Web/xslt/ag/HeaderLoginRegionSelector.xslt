<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
        ]]>
    </msxml:script>

	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderRegionList">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template name="renderRegionList">
        <xsl:param name="home" />
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>
        
        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="items" select="$home/SettingsFolder/RegionType/*" />
        <xsl:variable name="ulOpen">
            <xsl:text>&lt;ul&gt;</xsl:text>
        </xsl:variable>
        <xsl:variable name="ulClose">
            <xsl:text>&lt;/ul&gt;</xsl:text>
        </xsl:variable>
        <xsl:variable name="liOpen">
            <xsl:text>&lt;li&gt;</xsl:text>
        </xsl:variable>
        <xsl:variable name="liClose">
            <xsl:text>&lt;/li&gt;</xsl:text>
        </xsl:variable>
		<div class="logoHome">
			<a href="/"><img src="/images/clear.gif" width="160" height="120" /></a>
		</div>
		
        <div class="login_search">
            <div class="search">
                <a id="mOpen" href="#">
                    <img  src="/images/magnifying_lens.png" width="16" height="15" /> 
                 </a>
                <div id="modalSearch">
                       <a class="xClose" href="javascript:closeSearch()">
                             x
                        </a> 
                    <div class="searchContent">
                     Enter Search Terms:
                    <input type="text" id="aSearch" name="search" value="Enter Search Terms" onfocus="clearTextBox(this)" onkeypress="pressEnter(event)" />&nbsp;
                    <a href="javascript:searchRedirect()">
                        <img src="/images/magnifying_lens.png" width="16" height="15" />
                    </a>
                    </div>
                </div>
            </div>
            <div class="pick_city">
                
                <xsl:if test="count($items) &gt; 0">
                    <ul class="sf-region">
                        <xsl:value-of select="$liOpen" disable-output-escaping="yes"/>
                                <a href="javascript:selectRegion('{$cRegion}')">
                                    <xsl:value-of select="$cRegion" />
                                </a>
                                <xsl:value-of select="$ulOpen" disable-output-escaping="yes"/>
                        <xsl:for-each select="$items">
                            <xsl:variable name="region" select="title" />
                            <!--<xsl:if test="position() = 1">
                               
                            </xsl:if>
                            <xsl:if test="position() &gt; 1">-->
                                <xsl:if test="$region != 'All' and $region != $cRegion">
                                     <li>
                                        <a href="javascript:selectRegion('{$region}')">
                                             <xsl:value-of select="title" />
                                        </a>
                                    </li>
                                <!--</xsl:if>-->
                                
                            </xsl:if>
                           
                            <xsl:if test="position() = last()">
                                <xsl:value-of select="$ulClose" disable-output-escaping="yes"/>
                                <xsl:value-of select="$liClose" disable-output-escaping="yes"/>
                            </xsl:if>
                        </xsl:for-each>
                    </ul>
                </xsl:if>
            </div>
            
			<xsl:choose>
				<xsl:when test="umbraco.library:IsLoggedOn()">
					<div class="login">
						<a href="/login.aspx?page=account">My Account</a>
					</div>
					<div class="login">
						<a href="/login.aspx?logout=1">Log Out</a>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="login">
						<a href="/login.aspx">Log In</a>
					</div>
				</xsl:otherwise>	
			</xsl:choose>
                
            
        </div>


    </xsl:template>

</xsl:stylesheet>