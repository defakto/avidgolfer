<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">

    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            var counter = 0;
            
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
             
            function countItems() {
                counter++;
                return counter;
            }
            
            function currentCount() {
                return counter;
            }
        ]]>
    </msxml:script>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:call-template name="renderDetermineIssueYear">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="renderDetermineIssueYear">
		<xsl:param name="home"/>
		<xsl:variable name="year" select="umbraco.library:FormatDateTime(umbraco.library:CurrentDate(),'yyyy')"/>
		
		<xsl:choose>
			<xsl:when test="count($home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue) = 0">
				<xsl:call-template name="renderFeaturedArticlePostList">
					<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
					<xsl:with-param name="year" select="number($year) - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="renderFeaturedArticlePostList">
				<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
					<xsl:with-param name="year" select="number($year)"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
        
	</xsl:template>
    
	  <xsl:template name="renderFeaturedArticlePostList">
		<xsl:param name="home"/> 
		<xsl:param name="year"/>
          <xsl:variable name="default">
              <xsl:text>Dallas Ft. Worth</xsl:text>
          </xsl:variable>
            
          <xsl:variable name="cRegion">
              <xsl:choose>
                  <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                      <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/> 
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:value-of select="$default"/>
                  </xsl:otherwise>
              </xsl:choose>
          </xsl:variable>
          <xsl:variable name="passBanner" select="$home/SettingsFolder/HomepageAdBanner/* [regionType = $cRegion or regionType = 'All']" />
			<xsl:for-each select="$home/AGIssuesArea/YearFolder[@nodeName=$year]/Issue/IssueArticle">
                <xsl:choose>
                    <xsl:when test="(./regionType = 'All' or ./regionType = $cRegion) and js:countItems() &lt; 6">
                        <xsl:variable name="tags" select="umbraco.library:Split(./tags, ',')" />
                        <div class="content_teaser">
							<xsl:choose>
								<xsl:when test="./thumbnail !=''">
									<img src="{umbraco.library:GetMedia(./thumbnail, 0)/umbracoFile}" width="260" height="200" align="left" />
								</xsl:when>
								<xsl:otherwise>
									<img src="/images/blank.png" width="260" height="200" align="left" />
								</xsl:otherwise>
							</xsl:choose>
							
                            
                            <h1>
                                <a href="{umbraco.library:Replace(umbraco.library:NiceUrl(./@id),'ag-home/','')}">
                                    <xsl:value-of select="./title" />
                                </a>
                            </h1>
                            <h2>
								<xsl:choose>
									<xsl:when test="./articleType = 'Presidents Story'">
										President's Story
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="./articleType" />
									</xsl:otherwise>
								</xsl:choose>
                            </h2>

                            <p>
                                <xsl:value-of select="./introduction" />
                                <a href="{umbraco.library:Replace(umbraco.library:NiceUrl(./@id),'ag-home/','')}">
                                    <span class="read_more"> Read More</span>
                                </a>
                            </p>
                            <!--<div class="tags">
                                <xsl:if test="string-length($tags) != 0">
                                    Tags:
                                    <xsl:for-each select="$tags//value">
                                        <xsl:variable name="currentTag" select="current()" />
                                        <xsl:if test="position() != last()">
                                            <a href="javascript:tagsRedirect('{$currentTag}')">
                                                <xsl:value-of select="current()"/>,
                                            </a>
                                        </xsl:if>
                                        <xsl:if test="position() = last()">
                                            <a href="javascript:tagsRedirect('{$currentTag}')">
                                                <xsl:value-of select="current()"/>
                                            </a>
                                        </xsl:if>
                                    </xsl:for-each>
                                </xsl:if>
                            </div>-->
                            <!--<div class="article_share">
                                <a href="">
                                    <img src="/images/small_fb_share.png" width="16" height="16" />
                                </a>
                                <a href="">
                                    <img src="/images/small_twitter_share.png" width="16" height="16" />
                                </a>
                                <a href="">
                                    <img src="/images/small_email_share.png" width="16" height="16" />
                                </a>
                            </div>-->
                        </div>
                        <div class="clear"></div>
						
						<xsl:if test="js:currentCount() = 2">
							<div class="ad_middle_content">
								<div class="passSlice">
                                <xsl:if test="count($passBanner) &gt; 0">
                                    <xsl:variable name="url" select="$passBanner/imageLink"/>
                                    <xsl:variable name="image" select="$passBanner/bannerImage" />
                                    <a href="{$url}">
									<xsl:if test="$image != ''" >
										<img src="{umbraco.library:GetMedia($image, 0)/umbracoFile}"  />
									</xsl:if>
									</a>
                                </xsl:if>
								</div>
							</div>
						</xsl:if>
						
                    </xsl:when>
                </xsl:choose>
			</xsl:for-each>  

	  </xsl:template>
	  
</xsl:stylesheet>