﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  xmlns:js="urn:custom-javascript"
  exclude-result-prefixes="msxml umbraco.library tagsLib js">
    
    <msxml:script language="JavaScript" implements-prefix="js">
        <![CDATA[
            function splitCookie(cookie) {
                var location = cookie.split("expires"); 
                return location[0];
            }
        ]]>
    </msxml:script>

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:param name="currentPage"/>

    <xsl:template match="/">
        <xsl:call-template name="renderWidgetIdealGolfer">
            <xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="renderWidgetIdealGolfer">
        <xsl:param name="home"/>
        <xsl:variable name="default">
            <xsl:text>Dallas Ft. Worth</xsl:text>
        </xsl:variable>
        <xsl:variable name="teeWidget" select="$home/WidgetTeebox"/>
        <xsl:variable name="cRegion">
            <xsl:choose>
                <xsl:when test="umbraco.library:RequestCookies('avidGolferRegion')">
                    <xsl:value-of select="js:splitCookie(umbraco.library:RequestCookies('avidGolferRegion'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$default"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <div class="ad">
            <a href="/teebox">
                <img src="/images/teebox_ad.png" />
            </a>
        </div>

    </xsl:template>



</xsl:stylesheet>
