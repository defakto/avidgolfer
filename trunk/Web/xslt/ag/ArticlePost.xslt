<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library"
  xmlns:tagsLib="urn:tagsLib"
  exclude-result-prefixes="msxml umbraco.library tagsLib">



	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:param name="currentPage"/>

	<xsl:template match="/">
		<xsl:variable name="qsIndex" select="umbraco.library:RequestQueryString('p')" />

		<xsl:choose>
			<xsl:when test="$qsIndex !=''">
				<xsl:call-template name="determinePageIndex"><xsl:with-param name="pageIndex" select="$qsIndex"/></xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="determinePageIndex"><xsl:with-param name="pageIndex" select="1"/></xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:template name="determinePageIndex">
		<xsl:param name="pageIndex"/> 

		<xsl:call-template name="renderFeaturedArticlePost">
			<xsl:with-param name="home" select="$currentPage/ancestor-or-self::umbHomepage"/>  
			<xsl:with-param name="pageIndex" select="$pageIndex"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="renderFeaturedArticlePost">
		<xsl:param name="home"/> 
		<xsl:param name="pageIndex"/> 


		<div class="article">
            <xsl:variable name="articleType" select="$currentPage/articleType" />
            <xsl:variable name="tags" select="umbraco.library:Split($currentPage/tags, ',')" />
			<h1><xsl:value-of select="$currentPage/title" /></h1>
			<h2><xsl:choose>
					<xsl:when test="$currentPage/articleType = 'Presidents Story'">
						President's Story
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$currentPage/articleType" />
					</xsl:otherwise>
				</xsl:choose></h2>
			<div class="clear"></div>
            
			<xsl:variable name="pageCount" select="count($currentPage/umbTextpage)" />
			<xsl:for-each select="$currentPage/umbTextpage">
				<xsl:if test="position() = number($pageIndex)">
                    <div class="article_share">
                        <span class='st_facebook'></span>
                        <span class='st_twitter'></span>
                        <span class='st_email'></span>
                    </div>
                    <div class="clear"></div>
					<div class="article_content">
                        
						<h3><xsl:value-of select="title" /></h3>
						<xsl:value-of select="bodyText" disable-output-escaping="yes" />
					</div>
                   
				</xsl:if>
			</xsl:for-each>
			
			<div class="clear"></div>
			<xsl:if test="$pageCount &gt; 1">
				<div class="article_pagination">
					<div class="previous_left">
						<xsl:choose>
							<xsl:when test="number($pageIndex) &gt; 1">
								<a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={number($pageIndex)-1}">
                                    <img src="/images/caret-left.png" /> Previous Page</a>
							</xsl:when>
							<xsl:otherwise>
								<span class="disabled_link" href="#">
                                    <img src="/images/caret-left-inactive.png" /> Previous Page</span>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<div class="next_right">
						<xsl:choose>
							<xsl:when test="number($pageIndex) &lt; number($pageCount) ">
								<a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={number($pageIndex)+1}">Next Page <img src="/images/caret-right.png" />
                            </a>
							</xsl:when>
							<xsl:otherwise>
								<span class="disabled_link" href="#">Next Page <img src="/images/caret-right-inactive.png" />
                            </span>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<div class="pagination">
						<xsl:for-each select="$currentPage/umbTextpage">
							<!--<xsl:choose>-->
								<!--<xsl:when test="position() = number($pageIndex)">
									<xsl:value-of select="position()" />
								</xsl:when>
								<xsl:otherwise>-->
										<a href="{umbraco.library:NiceUrl($currentPage/@id)}?p={position()}"><xsl:value-of select="position()" />
											<xsl:if test="position() != count($currentPage/umbTextpage)"> |</xsl:if>
										</a>
								<!--</xsl:otherwise>
							</xsl:choose>-->

						</xsl:for-each>
					</div>
					<div class="clear"></div>
				</div>
			</xsl:if>
			
			<div class="clear"></div>
			<div class="tags">
                <xsl:if test="string-length($tags) != 0">
                   Tags:
                <xsl:for-each select="$tags//value">
                    <xsl:variable name="currentTag" select="current()" />
                    <xsl:if test="position() != last()">
                        <a href="javascript:tagsRedirect('{$currentTag}')">
                            <xsl:value-of select="current()"/>,  
                        </a>
                    </xsl:if>
                    <xsl:if test="position() = last()">
                        <a href="javascript:tagsRedirect('{$currentTag}')">
                            <xsl:value-of select="current()"/>
                        </a>
                    </xsl:if>
                </xsl:for-each> 
                </xsl:if>   
            </div>
			<div class="related_articles">
                <xsl:if test="string-length($articleType) != 0">
                    <a href="javascript:relatedArticleRedirect('{$articleType}')">Read All Related Articles</a>
                </xsl:if>
            </div>
			<div class="clear"></div>
			
		</div>
		

		<div class="clear"></div>
		
		
	</xsl:template>
		  

</xsl:stylesheet>