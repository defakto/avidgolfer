/* Javascript file for the home page */
var carouselItems = 2;
$(document).ready(function () {
    $("#prev").click(function () {
        $("#imageScroll").trigger("prev", 1);
    });

    $("#next").click(function () {
        $("#imageScroll").trigger("next", 1);
    });
});

$(function () {

    if ($('.passSlice').html() != '') {
        $('.WidgetCollection').children('div').each(function (i) {
			//alert($(this));
            $(this).addClass('widgetItem' + i);
        });
    }
	if($('.widgetItem0').hasClass('richTextWidget')) {
		$('.widgetItem0').css('margin-bottom', '410px');
	}
	
	if($('.widgetItem0').hasClass('voteCGContainer')) {
		$('.ad_middle_content').css('margin-top', '40px');
	}
});


function setCounter(counter) 
{
    carouselItems = counter;
}
/* carousel functions for home page main image gallery */
$(function() {
	/* attach an unique class name to each thumbnail image */
	$('#thumbs .thumb a').each(function(i) {
		$(this).addClass('itm' + i);
		
		/* add onclick event to thumbnail to make the main carousel scroll to the right slide */
		$(this).click(function() {
			$('#imageScroll').trigger('slideTo', [i, 0, true]);
			return false;
		}); /* end of second nested function */
	}); /* end of first nested function */
	/* highlight the first item on first load */
	$('#thumbs a.itm0').addClass('selected');
	
	/* Carousel for first item on first load */
	$('#imageScroll').carouFredSel({
        width: '100%',
		direction: 'left',
		synchronise: '#thumbs, true, true, 0', 
		circular: true,
		infinite: false,
		items: 1,
		auto: {
            items: 1,
            duration: 1000
        },
		scroll: {
			fx: 'directscroll',
			onBefore: function() {
				/* Everytime the main slideshow changes this ensures that the thumbnail and page are displayed correctly */
				/* Get current position */
				var pos = $(this).triggerHandler('currentPosition');
				
				/* reset and select the current thumbnail item */
				$('#thumbs a').removeClass('selected');
				$('#thumbs a.itm' + pos).addClass('selected');
				
				/* Move the thumbnail to the right page */
				/*var page = Math.floor( pos / 3);
				$('#thumbs').trigger('slideToPage', page); */
			} /* end of onBefore event */
		} /* end of scroll attribute */
	}); /* end of carousel initialize function */
	/* Carousel for the thumbnails */
	$('#thumbs').carouFredSel({
	    direction: 'left',
	    infinite: false,
	    auto: true,
	    align: false,
		synchronise: '#imageScroll, true, true, 0',
		scroll: {
			items: 1
		}
	});
}); /* end of main function */
