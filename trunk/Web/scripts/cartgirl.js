/* check for cookie and if it exists then turn off vote button */
$(document).ready(function () {
   
});

/* Javascript function for the cart girl vote page input button */
function voteGirl(girlId, region) 
{
    $.ajax({
        url: "/scripts/CastVote.ashx",
        data: { nodeId: girlId },
        success: function (result) {
            writeVoteCookie(region);
        }
    });
}

/* Javascript function for the cart girl of the month carousel */
$(function() {
	/* attach an unique class name to each thumbnail image */
	$('#miniCart .mini a').each(function(i) {
		$(this).addClass('itm' + i);
		
		/* add onclick event to thumbnail to make the main carousel scroll to the right slide */
		$(this).click(function() {
			$('#cartMain').trigger('slideTo', [i, 0, true]);
			return false;
		}); /* end of second nested function */
	}); /* end of first nested function */
	
	/* highlight the first item on first load */
	$('#miniCart a.itm0').addClass('selected');
	
	/* Carousel for first item on first load */
	$('#cartMain').carouFredSel({
		direction: 'left', 
		circular: true,
		infinite: false,
		items: 1,
		auto: false,
		scroll: {
			fx: 'directscroll',
			onBefore: function() {
				/* Everytime the main slideshow changes this ensures that the thumbnail and page are displayed correctly */
				/* Get current position */
				var pos = $(this).triggerHandler('currentPosition');
				
				/* reset and select the current thumbnail item */
				$('#miniCart a').removeClass('selected');
				$('#miniCart a.itm' + pos).addClass('selected');
				
			} /* end of onBefore event */
		} /* end of scroll attribute */
	}); /* end of carousel initialize function */
	
	/* Carousel for the thumbnails */
	$('#miniCart').carouFredSel({
		direction: 'left', 
		circular: true,
		infinite: true,
		auto: false,
		align: false,
        items: 8,
		prev: '#gPrev',
		next: '#gNext',
		scroll: {
			items: 1
		}
	});
}); /* end of main function */

/* Javascript for previous girl carousel */
$(function() {
		$('#prevGirl').carouFredSel({
		direction: 'up',
		circular: true,
		infinite: true,
		auto: false,
		align: false,
		prev: '#pPrev',
        next: '#pNext',
        items: 6,
		scroll: {
			items: 1
		}	
	});
}); /* end of carousel initialize function */

function writeVoteCookie(region) {
    var vote = "vote" + region;
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + 1);
    exdate = exdate.toGMTString();
    document.cookie = vote + '=' + region + '; expires =' + exdate;
    readCartGirlCookie();
    alert("Thanks for voting!.");
    
}

