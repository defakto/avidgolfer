/* avid golfer main javascript file */
// global variable for the passbook modal close timer
var setModalTime;
var popupCounter = 0;
$(document).ready(function () {
    $('.sf-menu').superfish();
    $('.sf-region').superfish();
    setTimeout(function () {
        startPassbookModal();
    }, 10000);

    // setModalTime = setTimeout('closePassbookModal()', 15000);
    setRegionDropdown();
    setPriceDropdown();
    setgirlRegionDropdown();
    setYearDropdown();
    readCartGirlCookie();
	setDigitalYearDropdown();
});


function clearTextBox(obj) 
{
    obj.value = "";
}

/* Javascript for the search modal popup */
$(function ($) {
    $('#mOpen').click(function (e) {
        $.modal.close();
        $('#modalSearch').modal();
    });
	
	 $('a.openlogin').bind('click', function(e,data) {
          e.preventDefault();
		  $.modal.close();
		  $('#modalLogin').modal();
	});
});

function closeLoginClick() {
	$.modal.close();
}

function searchRedirect() 
{
    rSearch = $('#aSearch').val();
    if (rSearch != "") {
        rSearch = rSearch.split(' ').join('+');
        rSearch = rSearch.toLowerCase();
        var queryString = "?q=" + rSearch;
        document.location.href = "/search.aspx" + queryString;
    }
    else {
        $.modal.close();
    }
}

function pressEnter(e) 
{
    if (e.keyCode == 13 || e.which == 13) 
    {
        searchRedirect();
    }
}

function closeSearch() 
{
    $.modal.close();
}

function gotoSubscribe() 
{
    document.location.href = "/subscribe.aspx";
}

/* javascript function for checking if cart girl vote is activated */

function showVoteContent(check) 
{
    if (check == 0) 
    {
        $('.vote').hide();
    }
}

/* javascript functions for the region selection drop down list */
function selectRegion(region) {
    eraseCookie();
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + 5000);
    exdate = exdate.toGMTString();
    document.cookie = "avidGolferRegion = " + region + ";expires=" + exdate + ";path=/";

    // location.reload();
	window.location = "/";
}

function readCookie() 
{
    var i, x, y,
    regionCookies = document.cookie.split(";");
    var id = "avidGolferRegion";
    for (i = 0; i < regionCookies.length; i++) {
        x = regionCookies[i].substr(0, regionCookies[i].indexOf("="));
        y = regionCookies[i].substr(regionCookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == id) {
            
        }
    }
}

function eraseCookie() {
    var i, x, y,
    regionCookies = document.cookie.split(";");
    var id = "avidGolferRegion";
    var exdate = new Date();
    exdate.setDate(exdate.getDate() -10);
    exdate = exdate.toGMTString();
    for (i = 0; i < regionCookies.length; i++) {
        x = regionCookies[i].substr(0, regionCookies[i].indexOf("="));
        y = regionCookies[i].substr(regionCookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == id) {
            y = y.split("expires");
            document.cookie = "avidGolferRegion = " + y[0] + ";expires = " + exdate + ";path=/";
        }
    }
}

/* Javascript function to pass query string to related article search page based on article type */
function relatedArticleRedirect(articleType) 
{
    var searchTerms = articleType;
    searchTerms = searchTerms.split(' ').join('+');
    searchTerms = searchTerms.toLowerCase(); 
    var queryString = "?q=" + searchTerms;
    document.location.href = "/related.aspx" + queryString;
}

/* Javascript function to pass query string to related search page based on tags */
function tagsRedirect(tags) 
{
    var searchTerms = tags;
    searchTerms = searchTerms.replace(' ', '');
    searchTerms = searchTerms.replace(",", "");
    searchTerms = searchTerms.split(' ').join('+');
     searchTerms = searchTerms.toLowerCase();
    // may be need but not sure
    // searchTerms = searchTerms.replace(",", "+");
    var queryString = "?q=" + searchTerms;
    document.location.href = "/related.aspx" + queryString;
}

function clearTextBox(obj) {
    obj.value = "";
}

/* Javascript functions for the passbook modal */
function startPassbookModal() {
//    popupCounter = readPopupCookie();

//    if(popupCounter < 3) 
//    {
//        popupCounter++;
//        writePopupCookie(popupCounter);

//        $('#modalPassbook').modal({
//            modal: false
//        });

//        $('#modalPassbook').animate({ 'left': "+500" }, 1500);
//        
//    } 
}

function closePassbookButtonClick() {
//    $.modal.close();
//    popupCounter = 3;
//    writePopupCookie(popupCounter);
}

function closePassbookModal() {
//    $.modal.close();
}

function clearPassbookTimer() 
{
//    clearTimeout(setModalTime);
}

function restartPassbookTimer() 
{
//    setModalTime = setTimeout('closePassbookModal()', 5000);
}

function writePopupCookie(popCounter) 
{
//    erasePopupCookie();
//    document.cookie = "popupCounter=" + popCounter;
}

function erasePopupCookie() 
{
//    var i, x, y,
//    popupCookie = document.cookie.split(";");
//    var id = "popupCounter";
//    var exdate = new Date();
//    exdate.setDate(exdate.getDate() - 10);
//    exdate = exdate.toGMTString();
//    for(i = 0; i < popupCookie.length; i++) {
//        x = popupCookie[i].substr(0, popupCookie[i].indexOf("="));
//        y = popupCookie[i].substr(popupCookie[i].indexOf("=") + 1);
//        x = x.replace(/^\s+|\s+$/g, "");
//        if(x == id) {
//            document.cookie = "popupCounter" + "=" + y + "expires = " + exdate;
//        }
//    }

}

function readPopupCookie() {
//    var i, x, y,
//    counter = 0;
//    var popupCookie = document.cookie.split(";");
//    var id = "popupCounter";
//    for(i = 0; i < popupCookie.length; i++) 
//    {
//        x = popupCookie[i].substr(0, popupCookie[i].indexOf("="));
//        y = popupCookie[i].substr(popupCookie[i].indexOf("=") + 1);
//        x = x.replace(/^\s+|\s+$/g, "");
//        if(x == id) {
//          counter = unescape(y); 
//        }
//  }
//  return counter;
}

jQuery(function ($) {
        $('a.fancySubscribe').fancybox({
            'width': '800px',
            'height': '500px',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe',
            'hideOnContentClick': false,
            'showCloseButton': false
        });
    });

/* Javascript functions for course page dropdown menus */
    function dropdownChange(pageIndex) {

        var regionValue = $('#regionSelect').val();
        var priceValue = $('#priceSelect').val();
        var queryString = "?p=" + pageIndex + "&region=" + regionValue + "&price=" + priceValue;
        document.location.href = queryString;
    }

    function setRegionDropdown() {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        var region = "region";
        for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
            if(decodeURIComponent(pair[0]) == region) {
                $('#regionSelect').val(decodeURIComponent(pair[1]));
            }
        }
    }

    function setPriceDropdown() {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        var price = "price";
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if(decodeURIComponent(pair[0]) == price) {
                $('#priceSelect').val(decodeURIComponent(pair[1])); 
            }
        }
    }

    /* Cart girl voting functions */
 function readCartGirlCookie() {
     var i, x, y;
     var dRegion = "voteDallas Ft. Worth";
     var aRegion = "voteArizona";
     var huRegion = "voteHouston";
     var hiRegion = "voteHillCountry";
     var girlCookie = document.cookie.split(";");

     for (i = 0; i < girlCookie.length; i++) {
        x = girlCookie[i].substr(0, girlCookie[i].indexOf("="));
        y = girlCookie[i].substr(girlCookie[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        y = y.replace("expires", "");
        if (x == dRegion) {
            $('.DallasFtWorth').hide();
        }
        if (x == aRegion) {
            $('.Arizona').hide();
        }
        if (x == huRegion) {
            $('.Houston').hide();
        }
        if (x == hiRegion) {
            $('.HillCountry').hide();
        }
     }
}

/* javascript function that reads blog list query string and adds it to blog url */
function blogQuerystring(title) {
    var queryString = "?blogType=" + title;
       document.location.href = "/blog.aspx" + queryString;
   }

   /* Javascript functions for cart girl vote count page */
   function girlDropdownChange() {
       var regionValue = $('#girlRegion').val();
       var yearValue = $('#yearSelect').val();
       var queryString = "?gregion=" + regionValue + "&year=" + yearValue;
       document.location.href = queryString;
   }

   function setgirlRegionDropdown() {
       var query = window.location.search.substring(1);
       var vars = query.split('&');
       var region = "gregion";
       for (var i = 0; i < vars.length; i++) {
           var pair = vars[i].split('=');
           if(decodeURIComponent(pair[0]) == region) {
               $('#girlRegion').val(decodeURIComponent(pair[1]));
           }
       }
   }

   function setYearDropdown() {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        var year = "year";
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('='); 
            if(decodeURIComponent(pair[0]) == year) {
                $('#yearSelect').val(decodeURIComponent(pair[1]));
            }
        }
   }
   
   function popupLogin() 
   {
		$('.logError').hide();
		var username = $('#mLogin').val();
		var password = $('#mPassword').val();
		
		 $.ajax({
        url: "/scripts/PopupLogin.ashx",
        data: { username: username,
				password: password
		},
        success: function (result) {
            if(result == "success") {
				$.modal.close();
				location.reload();
			}
			else {
				$('.logError').show();
			}
        }
    });
   }
   
   function yearDigitalDropdownChange() {
	   var yearValue = $('#selectYear').val();
       var queryString = "?year=" + yearValue;
       document.location.href = queryString;
   }
   
   function setDigitalYearDropdown() {
		 var query = window.location.search.substring(1);
        var vars = query.split('&');
        var year = "year";
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('='); 
            if(decodeURIComponent(pair[0]) == year) {
                $('#selectYear').val(decodeURIComponent(pair[1]));
            }
        }
   }


      

   