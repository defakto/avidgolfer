﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DailyDealReport.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.DailyDealReport" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="AvidGolfer.Entities"%>
<%@ Import Namespace="AvidGolfer.Components"%>
<%@ Import Namespace="umbraco" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="umbraco.cms.businesslogic.member"%>

<script runat="server" language="C#">
    protected string _debug = String.Empty;

    protected override void OnLoad(EventArgs e)
    {
       
        base.OnLoad(e);
    }
    
</script>
<style>
#dvAnswers
{
    padding-left:20px;
}
</style>

<div id="dvAnswers">
    <div>
        <table class="dateSelect">
            <tr>
                <th>
                    Start Date mm/dd/yyyy
                </th>
                <th>
                    End Date mm/dd/yyyy
                </th>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtStartDate" runat="server" /><br />
                </td>
                <td>
                    <asp:TextBox ID="txtEndDate" runat="server" /><br />  
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RequiredFieldValidator ID="reqStart" runat="server" ForeColor="Red" Display="Dynamic"
                        ErrorMessage="Please provide a start date." SetFocusOnError="false" ControlToValidate="txtStartDate">
                   </asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="reqEnd" runat="server" ForeColor="Red" Display="Dynamic"
                        ErrorMessage="Please provide a end date" SetFocusOnError="false" ControlToValidate="txtEndDate">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                     <asp:Button ID="btnPickDates" runat="server" Text="Retrieve Records" 
                         onclick="btnPickDates_Click" />
                </td>
            </tr>
        </table> 
    </div>
    <div id="activitySummary">
        <asp:Button ID="btnCreateCVS" runat="server" Text="Download CSV File" 
            onclick="btnCreateCVS_Click" /><br />
        <asp:Label ID="lblMessage" runat="server" /><br />
        <asp:GridView ID="gvActivity" runat="server" CellPadding="3" 
            AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" 
            BorderStyle="Solid" BorderWidth="1px" ForeColor="Black" GridLines="Vertical" Width="900px" RowStyle-Width="30px;">
            <AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>
                <asp:BoundField DataField="FirstName" HeaderText="First Name" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Address1" HeaderText="Shipping Address" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Address2" HeaderText="Shipping Address" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="City" HeaderText="City" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="State" HeaderText="State" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="ZipCode" HeaderText="Zip Code" >
                <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="CouponCode" HeaderText="CouponCode" >
                <HeaderStyle Width="100px" />
                </asp:BoundField>
            </Columns> 
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="#2D7B82" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView>
    </div>
</div>
