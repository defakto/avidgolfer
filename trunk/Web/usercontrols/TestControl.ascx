﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="umbraco.BusinessLogic"%>
<%@ Import Namespace="umbraco.cms.businesslogic.web"%>
<%@ Import Namespace="AvidGolfer.Entities"%>
<%@ Import Namespace="AvidGolfer.Components"%>

<script runat="server" language="C#">
    protected string _debug = String.Empty;

    protected override void OnLoad(EventArgs e)
    {
        
		SupplementalContentManager scm = new SupplementalContentManager();

		CartGirlVote cgv = scm.GetCartGirlsByNodeId("12345");
		
		_debug = cgv.TotalVotes.ToString();
		
		// scm.CastCartGirlVote("56789");
		
    }

    bool IsNumber(string value)
    {
	    int number1;
	    return int.TryParse(value, out number1);
    }

</script>

<div >
    <p>
        Test Control 
    </p>
    <p>
        <%=_debug %>
    </p>
	
</div>

