﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="umbraco"%>
<%@ Import Namespace="umbraco.interfaces"%>
<%@ Import Namespace="umbraco.NodeFactory"%>

<script runat="server" language="C#">
    protected string _debug = String.Empty;
    protected umbraco.BusinessLogic.User admin = umbraco.BusinessLogic.User.GetCurrent();

    protected override void OnLoad(EventArgs e)
    {
        if (!IsPostBack)
        {
            // add this line if necessary
            // || !Roles.IsUserInRole("admin")
            if (admin == null)
            {
                Response.Redirect("/");
            }
        }
        
        base.OnLoad(e);
    }

</script>