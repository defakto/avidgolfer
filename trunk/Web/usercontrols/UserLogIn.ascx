﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserLogIn.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserLogIn" %>

<script runat="server" language="C#">
    protected string _debug = String.Empty;
    protected string _url = "/Login.aspx?page=register";
    protected string _emailurl = "/Login.aspx?page=emailpassword";
    protected umbraco.BusinessLogic.User admin = umbraco.BusinessLogic.User.GetCurrent();

    protected override void OnLoad(EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request.QueryString["productid"]))
        {
            _url += "&productid=" + Request.QueryString["productid"].ToString();
            _emailurl += "&productid=" + Request.QueryString["productid"].ToString();
        }
                
        base.OnLoad(e);
    }

</script>

<asp:Panel ID="panelLogIn" runat="server">
<div id="divPage" class="loginPage">

    <div class="padleft">

        <h1>LogIn</h1>

        <h2>
            Please enter your email address and password<br /> or <a href="<%= _url %>" class="paratextblue">register here</a> if you do not have an account.
        </h2>
        <h2>
            If your email is already registered with us and you need your password, <a href="<%= _emailurl %>" class="paratextblue">click here</a>.
        </h2>

        <div>
            <asp:Label ID="lblErr" CssClass="error" runat="server" Visible="false"></asp:Label>
            <asp:ValidationSummary ID="vsLogIn" runat="server" CssClass="failurenotification" ForeColor="" ValidationGroup="vgLogIn"/>
        </div>

        <asp:Panel ID="panelLogInform" runat="server">
            <ul class="nobulletlist">
                <li class="padbottom">
                    <span>
                        Email Address 
                    </span><br />
                    <asp:TextBox ID="tbEmailAddress" runat="server" CssClass="textentry" TabIndex="1" />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" 
                        ControlToValidate="tbEmail" 
                        CssClass="failurenotification" 
                        Display="Dynamic" 
                        ErrorMessage="Please provide a valid email address." 
                        ToolTip="Please provide a valid email address." 
                        ValidationGroup="vgLogIn">*</asp:RequiredFieldValidator>
                </li>
                <li class="padbottom">
					<span>
                    Password
					</span><br />
                    <asp:TextBox ID="tbPassword" runat="server" 
                        CssClass="textentry" 
                        TabIndex="2" 
                        TextMode="Password" />
                </li>
                <!--
                <li class="padbottomcenter">
                    Or
                </li>
                <li class="padbottom">
					<span>
                    Email
					</span>
                    <asp:TextBox ID="tbEMail" runat="server" 
                        CssClass="textentry" 
                        TabIndex="3" />
                </li>
                -->
                <li>
                    <p>
                        <span class="paratextblue">
                        Keep me logged in <asp:CheckBox ID="ckbLogIn" runat="server" TabIndex="4" />
                        </span>
                    </p>
                </li>
            </ul>
        </asp:Panel>

        <asp:Panel ID="panelLogIncommand" runat="server" CssClass="padbottomtop" >
            <table>
                <tr>
                    <td>
                        <asp:Button ID="buttonLogIn" runat="server" CssClass="buttondefault" 
                            TabIndex="5" Text="LogIn" ValidationGroup="vgLogIn" onclick="buttonLogIn_Click" 
                             />
                    </td>
                    <td class="padleft">
                        <asp:Button ID="buttonCancel" runat="server" CssClass="buttondefault" 
                            TabIndex="6" Text="Cancel" onclick="buttonCancel_Click"  />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

</div>
</asp:Panel>
