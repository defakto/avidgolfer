﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="AvidGolfer.Components"%>
<%@ Import Namespace="umbraco.BusinessLogic"%>
<%@ Import Namespace="umbraco.cms.businesslogic.web"%>
<%@ Import Namespace="umbraco.NodeFactory"%>


<script runat="server" language="C#">
	string _debug = "";
    private string _widgetField;
    public string widgetField
    {
        set{ _widgetField = value; }
        get { return _widgetField; }
    }	

	protected UmbracoContentManager cm = new UmbracoContentManager();
	protected Node node = Node.GetCurrent();

    protected override void OnLoad(EventArgs e)
    {
	
		List<string> widgets = cm.GetWidgetNodeIdsFromField(widgetField);

        if (widgets.Count == 0)
        {
            if (node.Parent.GetProperty("rightContentColumnWidgets") != null)
            {
                // _debug = node.Parent.GetProperty("rightContentColumnWidgets").ToString();
                widgets = cm.GetWidgetNodeIdsFromString( node.Parent.GetProperty("rightContentColumnWidgets").ToString() );
            }
            else
            {
                var nodeParent = node.Parent;
                if (nodeParent.Parent.GetProperty("rightContentColumnWidgets") != null)
                {
                    widgets = cm.GetWidgetNodeIdsFromString(nodeParent.Parent.GetProperty("rightContentColumnWidgets").ToString());
                }else{
					var nodeParentParent = node.Parent;
					if (nodeParentParent.Parent.GetProperty("rightContentColumnWidgets") != null)
					{
						widgets = cm.GetWidgetNodeIdsFromString(nodeParentParent.Parent.GetProperty("rightContentColumnWidgets").ToString());
						}else{
						
						var nodeHome = new Node(1063);
						widgets = cm.GetWidgetNodeIdsFromString(nodeHome.GetProperty("rightContentColumnWidgets").ToString());
						
					}
				}
            }
        }

        int cnt = 0;
		foreach(string widget in widgets){
			//_debug = widget;
            cnt++;
			try{
				umbraco.presentation.templateControls.Macro mcr = new umbraco.presentation.templateControls.Macro();
				/*if(node.Id == 1063 && cnt == 2){
					umbraco.presentation.templateControls.Macro mcrSpacer = new umbraco.presentation.templateControls.Macro();
					mcrSpacer.Alias=cm.GetDocumentTypeFromNodeId(1350);
					mcrSpacer.MacroAttributes.Add("datasource",widget);
					this.macroContainer.Controls.Add(mcrSpacer);
				}*/
				mcr.Alias=cm.GetDocumentTypeFromNodeId(Convert.ToInt32(widget));
				mcr.MacroAttributes.Add("datasource",widget);
				this.macroContainer.Controls.Add(mcr);
			}catch{
			}
		}
		
        base.OnLoad(e);
    }
</script>
<%= _debug %>
<asp:Panel id="macroContainer" Class="WidgetCollection" runat="server"></asp:Panel>
