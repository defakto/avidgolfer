﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoteCartGirl.ascx.cs" Inherits="VoteCartGirl.VoteCartGirl" %>
<div class="vote_sidebar">
	    <div class="vote_header">
		    <img src="/images/vote_sidebar_header.png" width="273" height="14" align="center" />
	    </div>
	    <div class="main_vote">
		    <div id="vPrev" class="left_scroll">
			    <img src="/images/left_scroll.png" width="10" height="45" />
		    </div>
		    <div id="voteGirl">
			    <a href="#"><img src="/images/vote_sidebar_cg1_main.png" width="292" height="251" /></a> 
			    <a href="#"><img src="/images/girl2.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl3.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl4.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl5.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl6.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl7.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl8.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl9.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl10.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl11.jpg" width="292" height="251" /></a>
			    <a href="#"><img src="/images/girl12.jpg" width="292" height="251" /></a>
		    </div>
		    <div id="vNext" class="right_scroll">
			    <img src="/images/right_scroll.png" width="10" height="45" />
		    </div>
	    </div>
	    <div id="cartVote" class="vote_options">
		    <ul>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg2_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg3_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg4_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg1_thumb_selected.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg2_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg3_thumb.png" width="44" height="44" /></a></li>
		    </ul>
		    <div class="clear"></div>
		    <ul>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg4_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg1_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg2_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg3_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg4_thumb.png" width="44" height="44" /></a></li>
			    <li class="vGirl"><a href="#"><img src="/images/vote_sidebar_cg1_thumb.png" width="44" height="44" /></a></li>
		    </ul> 		
	    </div>
	    <div class="clear"></div>
	    <div class="blue_line"></div>
	    <div class="vote_now">
		    <a href="#"><img src="/images/vote_now.png" /></a>
	    </div>
    </div>
