﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="umbraco.BusinessLogic"%>
<%@ Import Namespace="umbraco.cms.businesslogic.web"%>
<%@ Import Namespace="umbraco.presentation.nodeFactory" %>
<%@ Import Namespace="AvidGolfer.Entities"%>
<%@ Import Namespace="AvidGolfer.Components"%>

<script runat="server" language="C#">
	protected string _debug = "test";
	
    protected override void OnLoad(EventArgs e)
    {
        string nodeId = umbraco.NodeFactory.Node.getCurrentNodeId().ToString();
        int aNodeId = Convert.ToInt32(nodeId);
        Node aNode = new Node(aNodeId);
        string region = aNode.GetProperty("regionType").Value;
        SupplementalContentManager smb = new SupplementalContentManager();
         smb.IncreaseArticleHitCount(nodeId, region);
    }
</script>

