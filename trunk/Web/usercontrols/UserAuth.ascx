﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAuth.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserAuth" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="AvidGolfer.Entities"%>
<%@ Import Namespace="AvidGolfer.Components"%>
<%@ Import Namespace="umbraco" %>
<%@ Import Namespace="umbraco.cms.businesslogic.member"%>

<script runat="server" language="C#">
    protected string _debug = String.Empty;
    
    protected override void OnLoad(EventArgs e)
    {
        // only allow backend username "admin" access
        // members and other users are denied
        var user = umbraco.BusinessLogic.User.GetCurrent();
        if (user == null || user.LoginName != "admin")
            Response.Redirect("/");
        
        
    }
</script>
