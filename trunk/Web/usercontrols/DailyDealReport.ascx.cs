﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AvidGolfer.Components;
using AvidGolfer.Entities;
using umbraco.cms.businesslogic.member;

namespace AvidGolfer.Web.usercontrols
{
    public partial class DailyDealReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (gvActivity.Rows.Count == 0)
                {
                    btnCreateCVS.Visible = false;
                }
            }
        }

        protected void btnPickDates_Click(object sender, EventArgs e)
        {
            SupplementalContentManager scm = new SupplementalContentManager();
            DateTime start = Convert.ToDateTime(txtStartDate.Text);
            DateTime end = Convert.ToDateTime(txtEndDate.Text);

            List<DailyDealOrder> ddOrder = scm.GetAllDailyDealsByDateRange(start, end);
            

            if (ddOrder.Count != 0)
                btnCreateCVS.Visible = true;

            gvActivity.DataSource = ddOrder;
            gvActivity.DataBind();
        }

        protected string GetUserName(int userId)
        {
            Member m = new Member(userId);
            if (m != null)
                return m.Text;

            return null;

        }

        protected void btnCreateCVS_Click(object sender, EventArgs e)
        {
            RandomHelpers.ExportToCSV("DailyDealReport.csv", this.gvActivity);
            ShowMessage("Daily Deal Report Downloaded");
        }

        protected void ShowMessage(string msg)
        {
            lblMessage.Text = msg;
        }


    }
}