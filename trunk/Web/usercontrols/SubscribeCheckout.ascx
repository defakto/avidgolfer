﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscribeCheckout.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.SubscribeCheckout" %>


<asp:Panel ID="panelCheckOut" runat="server">

	<div id="divPage" class="checkoutPage">

		<h1>
			Subscription Checkout
        </h1>

        <div class="divmessage">
            <asp:UpdatePanel ID="upMessage" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelMessage" runat="server" >
                    <span id="labelmessages" runat="server" class="labelmessage"></span>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="buttonSummary" />
            </Triggers>
            </asp:UpdatePanel>
        </div>
        <!--
       <h2>
            <asp:Panel ID="panelLogIn" runat="server">
                <asp:LinkButton ID="hlLogIn" runat="server" onclick="hlLogIn_Click" 
                    Text="LogIn for express checkout" />
            </asp:Panel>
			OR
            <asp:Panel ID="panelRegister" runat="server">
                <asp:LinkButton ID="hlRegister" runat="server" onclick="hlRegister_Click" 
                    Text="Register if you do not have a user account" />
            </asp:Panel>
        </h2>
        -->

        <asp:UpdatePanel ID="upCreditCard" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div ID="divcreditcard">
                    <asp:Panel ID="panelCreditCardNo" runat="server">
                        <ul class="nobulletlist">
                            <li>
                                CreditCard
                                <asp:Literal ID="literalCreditCard" runat="server"></asp:Literal>
                                &nbsp;
                                <asp:LinkButton ID="lbCreditCard" runat="server" onclick="lbCreditCard_Click">change</asp:LinkButton>
                            </li>
                        </ul>
                    </asp:Panel>
                    <asp:Panel ID="panelCreditCard" runat="server">
                        <script type="text/javascript">
                            function AdjustPhoneEntries() {
                                var value = $("#<%= tbPhone.ClientID %>").val();
                                $("#<%= tbPhone.ClientID %>").val(value.replace(/\D/g, ''));

                                value = $("#<%= tbPhoneS.ClientID %>").val();
                                $("#<%= tbPhoneS.ClientID %>").val(value.replace(/\D/g, ''));
                            }
                            $(document).ready(function () {
                                $("#<%= tbCreditCard.ClientID %>").blur(function () {
                                    var value = $("#<%= tbCreditCard.ClientID %>").val();
                                    $("#<%= tbCreditCard.ClientID %>").val(value.replace(/\D/g, ''));
                                });
                                $("#<%= tbPhone.ClientID %>").blur(function () {
                                    AdjustPhoneEntries();
                                });
                                $("#<%= tbPhoneS.ClientID %>").blur(function () {
                                    AdjustPhoneEntries();
                                });


                            });

                        </script>

                        <ul class="nobulletlist">
                            <li class="profilepadbottom">
                                Credit Card Type
                                <br />
                                <asp:DropDownList ID="ddlCCT" runat="server" TabIndex="2" />
                                <asp:RequiredFieldValidator ID="rfvCCT" runat="server" 
                                    ControlToValidate="ddlCCT" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Select card type, required." 
                                    Text="*" 
                                    ValidationGroup="vgAuthorize" />
                            </li>
                            <li class="profilepadbottom">
                                Credit Card Number
                                <br />
                                <asp:TextBox ID="tbCreditCard" runat="server" CssClass="textentry" 
                                    TabIndex="3" />
                                <asp:RequiredFieldValidator ID="rfvCreditCard" runat="server" 
                                    ControlToValidate="tbCreditCard" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Credit Card is a Required Field" 
                                    Text="*" 
                                    ValidationGroup="vgAuthorize" />
                                <asp:RegularExpressionValidator ID="revCreditCard" runat="server" 
                                    ControlToValidate="tbCreditCard" 
                                    CssClass="failurenotification" 
                                    ErrorMessage="Please Enter a valid Credit Card Number" 
                                    ValidationExpression="^[\s\S]{0,22}$" 
                                    ValidationGroup="vgAuthorize">
						        <span class="failurenotification">* card not valid</span>
						        </asp:RegularExpressionValidator>
                                <br />
                                <div class="subsuptext">
                                    1111222233334444</div>
                            </li>
                            <li class="profilepadbottom">Expiration Month
                                <br />
                                <asp:DropDownList ID="ddlMonth" runat="server" TabIndex="4" />
                                <asp:RequiredFieldValidator ID="rfvExpireMonth" runat="server" 
                                    ControlToValidate="ddlMonth" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Expiration Month is a Required Field" 
                                    Text="*" 
                                    ValidationGroup="vgAuthorize" />
                            </li>
                            <li class="profilepadbottom">Expiration Year
                                <br />
                                <asp:DropDownList ID="ddlYear" runat="server" TabIndex="5" />
                                <asp:RequiredFieldValidator ID="rfvExpireYear" runat="server" 
                                    ControlToValidate="ddlYear" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Expiration Year is a Required Field" 
                                    Text="*" 
                                    ValidationGroup="vgAuthorize" />
                            </li>
                            <li>CCV
                                <br />
                                <asp:TextBox ID="tbCCV" runat="server" CssClass="textentrysmall" TabIndex="6" />
                                <asp:RequiredFieldValidator ID="rfvCCV" runat="server" 
                                    ControlToValidate="tbCCV" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="CCV is a Required Field" 
                                    Text="*" 
                                    ValidationGroup="vgAuthorize" />
                                <asp:RegularExpressionValidator ID="revCCV" runat="server" 
                                    ControlToValidate="tbCCV" 
                                    ErrorMessage="Please Enter a valid CCV Number" 
                                    Text="*" 
                                    ValidationExpression="^[\s\S]{0,4}$" 
                                    ValidationGroup="vgAuthorize">
						        <span class="failurenotification">* CCV number not valid</span>
						        </asp:RegularExpressionValidator>
                                        <div class="subsuptext">
                                            For your security, Avid Golfer requires your credit card verification number. 
                                            For American Express, the verification number is a 4-digit code printed on the 
                                            front of your card. It appears after and to the right of your card number. For 
                                            VISA and MasterCard, it is a 3-digit code printed only the back of your card. It 
                                            appears after and to the right of your card number.
                                        </div>
                            </li>
                            <li>
                                <asp:Button ID="btnCreditCancel" runat="server" 
                                Text="Cancel"  CssClass="buttondefault" onclick="btnCreditCancel_Click" />
                            </li>
                        </ul>
                    </asp:Panel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbCreditCard" />
            </Triggers>
        </asp:UpdatePanel>

        <div>
            <asp:ValidationSummary ID="vsAuthorize" runat="server" 
                CssClass="failurenotification" ForeColor="" ValidationGroup="vgAuthorize" />
        </div>

        <div class="padbottom">
            <div class="checkoutdivleft">
                <!-- Bill --><span class="paratextbold">Billing Address </span>

                <ul class="nobulletlist">
                    <li class="profilepadbottom">First Name<br />
                        <asp:TextBox ID="tbFirstName" runat="server" CssClass="textentry" 
                            TabIndex="7" />
                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" 
                            ControlToValidate="tbFirstName" CssClass="failurenotification" 
                            Display="Dynamic" ErrorMessage="First Name Required" Text="*" 
                            ValidationGroup="vgAuthorize" />
                    </li>
                    <li class="profilepadbottom">Last Name<br />
                        <asp:TextBox ID="tbLastName" runat="server" CssClass="textentry" TabIndex="8" />
                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" 
                            ControlToValidate="tbLastName" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="Last Name Required" 
                            ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="profilepadbottom">Address<br />
                        <asp:TextBox ID="tbAddress" runat="server" CssClass="textentrylarge" 
                            TabIndex="9" />
                        <asp:RequiredFieldValidator ID="rfvAddress" runat="server" 
                            ControlToValidate="tbAddress" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="Address Required" 
                            ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="profilepadbottom">Zip <span class="spanzipheader">zip+4</span>
                        <br />
                        <span>
                        <asp:TextBox ID="tbZip" runat="server" 
                            AutoPostBack="false"
                            CssClass="textentrysmall" 
                            MaxLength="5" 
                            TabIndex="10" />
                        <asp:RequiredFieldValidator ID="rfvZip" runat="server" 
                            ControlToValidate="tbZip" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="Zip Required, 5 numeric" 
                            Text="*" 
                            ValidationGroup="vgAuthorize" />
                        <asp:RegularExpressionValidator ID="revZip" runat="server" 
                            ControlToValidate="tbZip" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="Invalid Zip, require 5 numeric characters." 
                            ValidationExpression="[0-9]{5}" />
                        &nbsp;&nbsp;
                        <asp:TextBox ID="tbZip4" runat="server" CssClass="textentrysmall" MaxLength="4" 
                            TabIndex="11" />
                        <asp:RegularExpressionValidator ID="revZip4" runat="server" 
                            ControlToValidate="tbZip4" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="Invalid Zip, require 4 numeric characters." 
                            ValidationExpression="[0-9]{4}">*</asp:RegularExpressionValidator>
                        </span>
                    </li>
                <asp:UpdatePanel ID="upBill" runat="server" UpdateMode="Conditional" > 
                <ContentTemplate>
                    <li class="profilepadbottom">
                        City
                        <br />
                        <asp:TextBox ID="tbCity" runat="server" 
                            CssClass="textentry"
                            TabIndex="12" />
                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" 
                            ControlToValidate="tbCity" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="City Required" 
                            ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="profilepadbottom">
                        State
                        <br />
                        <asp:DropDownList ID="ddlState" runat="server" TabIndex="13" />
                        <asp:RequiredFieldValidator ID="rfvState" runat="server" 
                            ControlToValidate="ddlState" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="State Required" 
                            ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                    </li>
                </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tbZip" />
                    </Triggers>
                </asp:UpdatePanel>
                    <li class="profilepadbottom">
                        Email
                        <br />
                        <asp:TextBox ID="tbEmail" runat="server" CssClass="textentrylarge" TabIndex="14" />
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" 
                            ControlToValidate="tbEmail" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="Invalid EMail, i.e.:  yourname@someplace.com" 
                            ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$" />
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" 
                            ControlToValidate="tbEmail" 
                            CssClass="failurenotification" 
                            Display="Dynamic" 
                            ErrorMessage="E-mail is required." 
                            ToolTip="E-mail is required." 
                            ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                        <br />
                        <div class="subsuptext">yourname@emailprovider.com</div>
                    </li>
                    <li class="profilepadbottom">Phone
                        <br />
                        <asp:TextBox ID="tbPhone" runat="server" CssClass="textentrymedium" 
                            MaxLength="14" TabIndex="15" />
                       <asp:RequiredFieldValidator ID="reqPhone" runat="server" ControlToValidate="tbPhone"   
                            CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a phone number."
                            ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                       <asp:RegularExpressionValidator ID="regPhone" runat="server" Display="Dynamic" ErrorMessage="Please use numbers only."
                            CssClass="failurenotification" ValidationExpression="^[0-9]{10}$" ControlToValidate="tbPhone"
                            ValidationGroup="vgAuthorize">*</asp:RegularExpressionValidator>
                        <br />
                        <div class="subsuptext">Please use numbers only thank you.</div>
                    </li>
                </ul>
            </div>

            <div class="checkoutdivright"> <!-- Ship -->
                <span class="paratextbold">Shipping Address &nbsp; &nbsp; &nbsp;
                    <asp:LinkButton ID="hlSameBill" runat="server" 
                        CssClass="parasametext"
                        TabIndex="16" onclick="hlSameBill_Click" >
                        Same As Billing? Click Here.
                    </asp:LinkButton>
                </span>
                <asp:UpdatePanel ID="upShip" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <ul class="nobulletlist">
                            <li class="profilepadbottom">First Name
                                <br />
                                <asp:TextBox ID="tbFirstNameS" runat="server" CssClass="textentry" 
                                    TabIndex="17" />
                                <asp:RequiredFieldValidator ID="rfvFirstNameS" runat="server" 
                                    ControlToValidate="tbFirstNameS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="First Name Required" 
                                    Text="*" 
                                    ValidationGroup="vgAuthorize" />
                            </li>
                            <li class="profilepadbottom">Last Name<br />
                                <asp:TextBox ID="tbLastNameS" runat="server" CssClass="textentry" 
                                    TabIndex="18" />
                                <asp:RequiredFieldValidator ID="rfvLastNameS" runat="server" 
                                    ControlToValidate="tbLastNameS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Last Name Required" 
                                    ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                            </li>
                            <li class="profilepadbottom">Address
                                <br />
                                <asp:TextBox ID="tbAddressS" runat="server" CssClass="textentrylarge" 
                                    TabIndex="19" />
                                <asp:RequiredFieldValidator ID="rfvAddressS" runat="server" 
                                    ControlToValidate="tbAddressS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="AddressS Required" 
                                    ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                            </li>
                            <li class="profilepadbottom">Zip <span class="spanzipheader">zip+4</span>
                                <br />
                                <span>
                                <asp:TextBox ID="tbZipS" runat="server" CssClass="textentrysmall" MaxLength="5" 
                                    TabIndex="20" />
                                <asp:RequiredFieldValidator ID="rfvbZipS" runat="server" 
                                    ControlToValidate="tbZipS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Zip Required, 5 numeric" 
                                    ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revZipS" runat="server" 
                                    ControlToValidate="tbZipS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Invalid Zip, require 5 numeric characters." 
                                    ValidationExpression="[0-9]{5}" />
                                &nbsp;&nbsp;
                                <asp:TextBox ID="tbZip4S" runat="server" CssClass="textentrysmall" 
                                    MaxLength="4" TabIndex="21" />
                                <asp:RegularExpressionValidator ID="revZip4S" runat="server" 
                                    ControlToValidate="tbZip4S" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Invalid Zip, require 4 numeric characters." 
                                    ValidationExpression="[0-9]{4}">*</asp:RegularExpressionValidator>
                                </span></li>
                            <li class="profilepadbottom">
                                City 
                                <br />
                                <asp:TextBox ID="tbCityS" runat="server" CssClass="textentry" TabIndex="22" />
                                <asp:RequiredFieldValidator ID="rfvCityS" runat="server" 
                                    ControlToValidate="tbCityS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="City Required" 
                                    ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                            </li> 
                            <li class="profilepadbottom">
                                State
                                <br />
                                <asp:DropDownList ID="ddlStateS" runat="server" TabIndex="23" />
                                <asp:RequiredFieldValidator ID="rfvStateS" runat="server" 
                                    ControlToValidate="ddlStateS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="State Required" 
                                    ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                            </li>
                            <li class="profilepadbottom">
                                Email
                                <br />
                                <asp:TextBox ID="tbEmailS" runat="server" CssClass="textentrylarge" TabIndex="24" />
                                <asp:RegularExpressionValidator ID="revEmailS" runat="server" 
                                    ControlToValidate="tbEmailS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="Invalid EMail, i.e.:  yourname@someplace.com" 
                                    ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$" />
                                <asp:RequiredFieldValidator ID="rfvEmailS" runat="server" 
                                    ControlToValidate="tbEmailS" 
                                    CssClass="failurenotification" 
                                    Display="Dynamic" 
                                    ErrorMessage="E-mail is required." 
                                    ToolTip="E-mail is required." 
                                    ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                                <br />
                                <div class="subsuptext">
                                    yourname@emailprovider.com</div>
                            </li>
                            <li class="profilepadbottom">Phone
                                <br />
                                <asp:TextBox ID="tbPhoneS" runat="server" CssClass="textentrymedium" 
                                    MaxLength="14" TabIndex="25" />
                               <asp:RequiredFieldValidator ID="reqSphone" runat="server" ControlToValidate="tbPhoneS"   
                                    CssClass="failurenotification" Display="Dynamic" ErrorMessage="Please include a phone number."
                                    ValidationGroup="vgAuthorize">*</asp:RequiredFieldValidator>
                              <asp:RegularExpressionValidator ID="regSphone" runat="server" Display="Dynamic" ErrorMessage="Please use numbers only."
                                    CssClass="failurenotification" ValidationExpression="^[0-9]{10}$" ControlToValidate="tbPhoneS"
                                    ValidationGroup="vgAuthorize">*</asp:RegularExpressionValidator>
                        <br />
                        <div class="subsuptext">Please use numbers only thank you.</div>
                            </li>
                        </ul>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hlSameBill" />
                        <asp:AsyncPostBackTrigger ControlID="tbZipS" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div ID="divSubscribecommand" class="checkoutcommand">
            <span>
            <asp:Button ID="buttonSummary" runat="server" 
                CssClass="buttoncheckout" 
                onclick="buttonSummary_Click" 
                TabIndex="26" 
                Text="Continue" 
                ValidationGroup="vgAuthorize" />
            <asp:Button ID="buttonCancel" runat="server" 
                CssClass="buttoncheckoutleft" 
                onclick="buttonCancel_Click" 
                TabIndex="1" 
                Text="Cancel" />
            </span>
        </div>
        <!--
        <div class="checkoutcommand">
            <asp:Button ID="buttonTest" runat="server" 
                CssClass="buttondefault" 
                Text="Test" onclick="buttonTest_Click" />
        </div>
        -->
	</div>
</asp:Panel>
