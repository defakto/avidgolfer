﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAccount.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.UserAccount" %>
<%@ Import Namespace="AvidGolfer.Entities"%>

<script type="text/C#" runat="server">

    protected string GetOrderStatus(int status)
    {
        try
        {
            OrderStatus os = (OrderStatus)status;
            return os.ToString();
        }
        catch (Exception ex)
        {
            return "Unknown";
        }

    }

</script>


<asp:Panel ID="panelLogIn" runat="server">
<div id="divPage" class="accountPage">


    <h1>Account Manager</h1>

    <div class="padleft">
        <h2>Account Options</h2>
        <asp:Panel ID="panelLogInform" runat="server">
            <ul class="nobulletlist">
                <li class="padbottom">
                    <asp:LinkButton ID="lbPassword" runat="server" onclick="lbPassword_Click">Change Password</asp:LinkButton>
                </li>
                <li class="padbottom">
                    <asp:LinkButton ID="lbProfile" runat="server" onclick="lbProfile_Click">Create / Change Profile</asp:LinkButton>
                </li>
                <!--
                <li class="padbottom">
                    <asp:LinkButton ID="lbSubscribe" runat="server" onclick="lbSubscribe_Click">Subscribe to Avid Golfer Magazine</asp:LinkButton>
                </li>
                -->
                <li class="padbottom">
                    <a href="/Order.aspx?page=cancel">Cancel A Subscription</a>
                </li>
                <!--
                <li class="padbottom">
                    <asp:LinkButton ID="lbAdmin" runat="server" onclick="lbAdmin_Click" >Admin Manager (remove on deploy)</asp:LinkButton>
                </li>
                -->
            </ul>
        </asp:Panel>
    </div>

    <h2>Order History</h2>
    <div id="divOrderHistory" class="accountTablediv" runat="server">
    <asp:gridview id="gvOrderHistory" runat="server" cellpadding="2" cellspacing="1"
        gridlines="Both" autogeneratecolumns="False" visible="true">        
        <Columns>
            <asp:BoundField DataField="orderid" HeaderText="Order Number" />
            <asp:BoundField DataField="productname" HeaderText="Product Name" />
            <asp:BoundField DataField="sku" HeaderText="Subscription Code" />

            <asp:TemplateField HeaderText="Price">
            <ItemTemplate>
                $<%# Eval("price").ToString() %>
            </ItemTemplate>
            <HeaderTemplate>Price</HeaderTemplate>
            </asp:TemplateField>

            <%--<asp:BoundField DataField="billemail" HeaderText="Contact Email" />--%>
            <asp:BoundField DataField="billphone" HeaderText="Contact Phone" />
            <asp:BoundField DataField="createdate" HeaderText="Order Date" HtmlEncode="false" 
                    DataFormatString="{0:MMMM d, yyyy}" />
            <asp:BoundField DataField="orderstatus" HeaderText="Order Status" />
        </Columns>
    </asp:gridview>

    </div>


</div>
</asp:Panel>
