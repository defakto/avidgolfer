﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscribeCancel.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.SubscribeCancel" %>
<%@ Import Namespace="umbraco.cms.businesslogic.member"%>
<%@ Import Namespace="umbraco.presentation.nodeFactory"%>

<script runat="server" language="C#">
    protected string _debug = String.Empty;

    protected override void OnLoad(EventArgs e)
    {
        var node = Node.GetCurrent();

        litCancel.Text = (node.GetProperty("cancelSubscriptionInstructions") != null) ? node.GetProperty("cancelSubscriptionInstructions").Value : String.Empty;
               
        base.OnLoad(e);
    }
    
</script>


<asp:Panel ID="panelCancel" runat="server">
<div id="divPage" class="confirmationPage">


    <h1>Subscription Cancelation</h1>

    <asp:Literal ID="litCancel" runat="server" />


    <!--
    <br />
    <div class="padbottom">
        <asp:Button ID="buttonContinue" runat="server" CssClass="buttondefault" 
        Text="Continue" onclick="buttonContinue_Click" />
    </div>
    -->

</div>
</asp:Panel>
