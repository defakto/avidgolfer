﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DailyDealForm.ascx.cs" Inherits="AvidGolfer.Web.usercontrols.DailyDealForm" %>
<%@ Import Namespace="umbraco.cms.businesslogic.member"%>
<%@ Import Namespace="AvidGolfer.Entities"%>


<script runat="server" language="C#">
    protected string _debug = String.Empty;

    protected override void OnLoad(EventArgs e)
    {
        
        
        
    }

</script>

<div class="dailyDealForm" id="DailyDealConfirmation" runat="server" visible="false">


	<div class="article">
		<h1><umbraco:Item field="title" runat="server" /></h1>
		<div class="article_content">
			<umbraco:Item field="thankYouCopy" runat="server" />
		</div>
        <div class="clear"></div>
	</div>


</div>

<div class="dailyDealForm" id="DailyDealFormPage" runat="server">

	<div class="article">
		<h1><umbraco:Item field="title" runat="server" /></h1>
		<div class="article_content">
			<umbraco:Item field="bodyText" runat="server" />
		</div>
        <div class="clear"></div>
	</div>
    
    
    <div class="dailyDealFormFields">
        <div class="dailyDealFormField">
            <asp:Label ID="lblFirstName" Text="First Name" runat="server"></asp:Label>
            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblLastName" Text="Last Name" runat="server"></asp:Label>
            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblAddress1" Text="Shipping Address" runat="server"></asp:Label>
            <asp:TextBox ID="txtAddress1" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblAddress2" Text="Shipping Address" runat="server"></asp:Label>
            <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblCity" Text="City" runat="server"></asp:Label>
            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblState" Text="State" runat="server"></asp:Label>
            <asp:DropDownList ID="selState" runat="server">
                <asp:ListItem Value="AL" Text="Alabama"></asp:ListItem>
                <asp:ListItem Value="AK" Text="Alaska"></asp:ListItem>
                <asp:ListItem Value="AZ" Text="Arizona"></asp:ListItem>
                <asp:ListItem Value="AR" Text="Arkansas"></asp:ListItem>
                <asp:ListItem Value="CA" Text="California"></asp:ListItem>
                <asp:ListItem Value="CO" Text="Colorado"></asp:ListItem>
                <asp:ListItem Value="CT" Text="Connecticut"></asp:ListItem>
                <asp:ListItem Value="DE" Text="Delaware"></asp:ListItem>
                <asp:ListItem Value="DC" Text="District of Columbia"></asp:ListItem>
                <asp:ListItem Value="FL" Text="Florida"></asp:ListItem>
                <asp:ListItem Value="GA" Text="Georgia"></asp:ListItem>
                <asp:ListItem Value="HI" Text="Hawaii"></asp:ListItem>
                <asp:ListItem Value="ID" Text="Idaho"></asp:ListItem>
                <asp:ListItem Value="IL" Text="Illinois"></asp:ListItem>
                <asp:ListItem Value="IN" Text="Indiana"></asp:ListItem>
                <asp:ListItem Value="IA" Text="Iowa"></asp:ListItem>
                <asp:ListItem Value="KS" Text="Kansas"></asp:ListItem>
                <asp:ListItem Value="KY" Text="Kentucky"></asp:ListItem>
                <asp:ListItem Value="LA" Text="Louisiana"></asp:ListItem>
                <asp:ListItem Value="ME" Text="Maine"></asp:ListItem>
                <asp:ListItem Value="MD" Text="Maryland"></asp:ListItem>
                <asp:ListItem Value="MA" Text="Massachusetts"></asp:ListItem>
                <asp:ListItem Value="MI" Text="Michigan"></asp:ListItem>
                <asp:ListItem Value="MN" Text="Minnesota"></asp:ListItem>
                <asp:ListItem Value="MS" Text="Mississippi"></asp:ListItem>
                <asp:ListItem Value="MO" Text="Missouri"></asp:ListItem>
                <asp:ListItem Value="MT" Text="Montana"></asp:ListItem>
                <asp:ListItem Value="NE" Text="Nebraska"></asp:ListItem>
                <asp:ListItem Value="NV" Text="Nevada"></asp:ListItem>
                <asp:ListItem Value="NH" Text="New Hampshire"></asp:ListItem>
                <asp:ListItem Value="NJ" Text="New Jersey"></asp:ListItem>
                <asp:ListItem Value="NM" Text="New Mexico"></asp:ListItem>
                <asp:ListItem Value="NY" Text="New York"></asp:ListItem>
                <asp:ListItem Value="NC" Text="North Carolina"></asp:ListItem>
                <asp:ListItem Value="ND" Text="North Dakota"></asp:ListItem>
                <asp:ListItem Value="OH" Text="Ohio"></asp:ListItem>
                <asp:ListItem Value="OK" Text="Oklahoma"></asp:ListItem>
                <asp:ListItem Value="OR" Text="Oregon"></asp:ListItem>
                <asp:ListItem Value="PA" Text="Pennsylvania"></asp:ListItem>
                <asp:ListItem Value="RI" Text="Rhode Island"></asp:ListItem>
                <asp:ListItem Value="SC" Text="South Carolina"></asp:ListItem>
                <asp:ListItem Value="SD" Text="South Dakota"></asp:ListItem>
                <asp:ListItem Value="TN" Text="Tennessee"></asp:ListItem>
                <asp:ListItem Value="TX" Text="Texas"></asp:ListItem>
                <asp:ListItem Value="UT" Text="Utah"></asp:ListItem>
                <asp:ListItem Value="VT" Text="Vermont"></asp:ListItem>
                <asp:ListItem Value="VA" Text="Virginia"></asp:ListItem>
                <asp:ListItem Value="WA" Text="Washington"></asp:ListItem>
                <asp:ListItem Value="WV" Text="West Virginia"></asp:ListItem>
                <asp:ListItem Value="WI" Text="Wisconsin"></asp:ListItem>
                <asp:ListItem Value="WY" Text="Wyoming"></asp:ListItem>
            </asp:DropDownList>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblZipCode" Text="Zip Code" runat="server"></asp:Label>
            <asp:TextBox ID="txtZipCode" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblEmail" Text="Email" runat="server"></asp:Label>
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblConfirmEmail" Text="Confirm Email" runat="server"></asp:Label>
            <asp:TextBox ID="txtConfirmEmail" runat="server"></asp:TextBox>
        </div>

        <div class="dailyDealFormField">
            <asp:Label ID="lblCouponCode" Text="Coupon Code" runat="server"></asp:Label>
            <asp:TextBox ID="txtCouponCode" runat="server"></asp:TextBox>
        </div>
        <div class="errMsg"><asp:Literal ID="litErrMsg" Text="" runat="server" Visible="false"></asp:Literal></div>
        <div class="dailyDealFormField">
            <asp:Button ID="btnRedeemOrder" Text="Redeem Order" runat="server" 
                onclick="btnRedeemOrder_Click" />
        </div>
    </div>


</div>







