XCOPY ".\AvidGolfer.Web\bin\AvidGolfer*.*" ".\Web\bin" /Y /R
XCOPY ".\AvidGolfer.Web\bin\AuthorizeNET*.*" ".\Web\bin" /Y /R

XCOPY ".\AvidGolfer.Web\bulkCopyToUmbraco\*.*" .\Web" /Y /R
XCOPY ".\AvidGolfer.Web\bulkCopyToUmbraco\css\*.*" .\Web\styles" /Y /R
XCOPY ".\AvidGolfer.Web\bulkCopyToUmbraco\images\*.*" .\Web\images" /Y /R
XCOPY ".\AvidGolfer.Web\bulkCopyToUmbraco\masterpages\*.*" .\Web\masterpages" /Y /R
XCOPY ".\AvidGolfer.Web\bulkCopyToUmbraco\scripts\*.*" .\Web\scripts" /Y /R
XCOPY ".\AvidGolfer.Web\bulkCopyToUmbraco\usercontrols\*.*" .\Web\usercontrols" /Y /R
XCOPY ".\AvidGolfer.Web\bulkCopyToUmbraco\xslt\*.*" .\Web\xslt" /Y /R